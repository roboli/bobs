(defproject bobs "0.1.0-SNAPSHOT"
  :description "(O) Open Business Systems"
  :url "http://example.com/FIXME"
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [ring-server "0.3.1"]
                 [compojure "1.1.6"]
                 [fogus/ring-edn "0.2.0"]
                 [mysql/mysql-connector-java "5.1.6"]
                 [korma "0.3.0-RC6"]
                 ;; To avoid conflict with korma internal dependency on jdbc
                 [org.clojure/java.jdbc "0.2.3"]
                 [lobos "1.0.0-beta1"]
                 [liberator "0.11.1"]
                 [org.clojure/clojurescript "0.0-3058"]
                 [org.clojure/core.async "0.1.301.0-deb34a-alpha"]
                 [om "0.6.3"]
                 [secretary "1.1.0"]
                 [environ "0.5.0"]
                 [com.taoensso/timbre "3.1.6"]
                 [com.taoensso/tower "2.0.2"]
                 [net.unit8/tower-cljs "0.1.0"]
                 [com.cemerick/valip "0.3.2"]
                 ;; austin not compatible with actual cljs version
                 [com.cemerick/piggieback "0.1.5"]
                 [jayq "2.5.1"]
                 [hiccup "1.0.5"]
                 [lib-noir "0.8.4"]
                 [org.clojure/tools.cli "0.2.4"]
                 [org.clojars.roboli/om-autocomplete "0.1.0-SNAPSHOT"]
                 [clj-time "0.8.0"]
                 [camel-snake-kebab "0.2.4"]
                 [com.cemerick/url "0.1.1"]
                 [pdfkit-clj "0.1.5"]]

  :main bobs.main

  :source-paths ["src/clj" "src/cljs"]
  
  :plugins [[lein-ring "0.8.10"]
            [lein-cljsbuild "1.0.3"]
            [lein-environ "0.5.0"]
            [lein-pdo "0.1.1"]
            [com.keminglabs/cljx "0.6.0"]]
  
  :ring {:handler bobs.core/app
         :init bobs.core/init
         :destroy bobs.core/destroy}

  :repl-options {:init-ns bobs.repl
                 :nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}

  :cljx {:builds [{:source-paths ["src/cljx"]
                   :output-path "target/classes"
                   :rules :clj}
                  {:source-paths ["src/cljx"]
                   :output-path "target/classes"
                   :rules :cljs}]}

  :cljsbuild {:crossovers [valip.core valip.predicates]
              :builds {:dev {:source-paths ["src/cljs/bobs" "src/cljs/repl" "target/classes"]
                             :compiler {:output-to "resources/public/js/bobs.js"
                                        :output-dir "resources/public/js/out"
                                        :optimizations :none}}
                       :prod {:source-paths ["src/cljs/bobs" "target/classes"]
                              :compiler {:output-to "resources/public/js/bobs.js"
                                         :externs ["resources/externs/bobs-externs.js"
                                                   "resources/externs/bootstrap-externs.js"
                                                   "resources/externs/jquery-externs.js"
                                                   "resources/externs/jquery-ui-externs.js"
                                                   "resources/externs/react-externs.js"]
                                         :optimizations :advanced
                                         :pretty-print false}}}}
  
  :clean-targets ^{:protect false} ["resources/public/js/out"
                                    "resources/public/js/bobs.js"
                                    "target/"]
    
  :aliases {"up" ["pdo" "cljsbuild" "auto" "dev," "ring" "server-headless"]}
  
  :profiles {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                                  [ring/ring-mock "0.4.0"]]
                   :env {:dev true
                         :lobos-src-dir "src/clj/"
                         :app-lang :es}}
             :prod {:env {:lobos-src-dir "src/clj/"
                          :app-lang :es}}
             :uberjar {:aot :all}})
