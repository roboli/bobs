# BOBS

(B) Open Business Systems

## Prerequisites

You will need [Leiningen][1] 1.7.0 or above installed.

[1]: https://github.com/technomancy/leiningen

## Running

To start a web server for the application, run:

    lein ring server

## Running with Docker (dev)

First, copy the `docker/.env.example` to `.env` file and fill in the ENV variables. Then run the docker script:

```
./docker-dev-run start
```

Or start REPL (defaults to 5050 port):

```
./docker-dev-run repl
```

## Running with Docker (prod)

Export all the necessary variables to your environment and run:

```
./docker-run start
```

Now connect to the REPL and run some commands.

## Useful Commands

Here are some frequently used commands for the REPL:

```clojure
(do (require '[bobs.models.query :as q])
    (require '[bobs.models.users :as u])
    (require '[bobs.models.companies :as c])
    (require '[bobs.models.currencies :as cu])
    (require '[bobs.models.document-configurations :as dc]))

;; create user
(q/insert u/users {:values
                   {:username "username"
                    :name "name"
                    :password "pass"}})

;; create company
(q/insert c/companies {:values
                       {:name ""
                        :alias ""}})

;; add user to company
(c/add-user c/companies {:values
                         {:company-id 1
                          :user-id 1
                          :is-default 1}})

;; add currency to company
(q/insert cu/currencies {:values
                         {:company-id 1
                          :code "GTQ"
                          :is-default 1}})

;; create a document configuration
;; 1 - Purchase
;; 2 - Purchase Return
;; 3 - Shipment
;; 4 - Sales Return
;; 5 - Entry
;; 6 - Withdraw
(q/insert dc/document-configurations {:values
                                      {:company-id 1
                                       :description "Compra"
                                       :document-type 1
                                       :initial 1001
                                       :is-default 1}})

;; set default company
(u/set-default-company u/users user-id company-id)

http://54.149.123.238/users/default-company/:user-id/:company-id


;; change password
(do (require '[korma.core :as km])
    (require '[noir.util.crypt :as crypt])
    (require '[bobs.models.entities :refer [user]]))

(km/update user (km/set-fields {:password (crypt/encrypt "xxx"}}) (km/where {:id x}))

;; start server
(start-server)
```

## License

Copyright © 2025 bobs.sytems
