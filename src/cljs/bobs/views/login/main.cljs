(ns bobs.views.login.main
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [chan put! timeout pub sub]]
            [bobs.utils :refer [error?]]
            [bobs.data.config :refer [app-name
                                      app-name-ext
                                      app-brand
                                      app-email
                                      chrome
                                      firefox]]
            [bobs.data.request :refer [request]]
            [bobs.widgets.utils :refer [alert-danger]]
            [clojure.string :as str]
            [bobs.i18n :refer [t]]
            [bobs.views.utils :refer [handle-response wait]]
            [bobs.data.routes :refer [routes]]))

(def send-request (request "/session"))

(defn listen-display-loading [owner p]
  (let [c (chan)]
    (sub p :login c)
    (go (while true
          (if (:show (<! c))
            (om/set-state! owner :show-loading true)
            (om/set-state! owner :show-loading false))))))

(defn create-session [owner handler]
  (fn [_]
    (go (let [resp (<! (send-request {:method :post
                                      :body {:username (.-value (om/get-node owner "username"))
                                             :password (.-value (om/get-node owner "password"))}}))]
          (<! (handler resp))))))

(defn login [cursor response]
  (om/update! cursor :logged-in true)
  (om/update! cursor [:user :username] (get-in response [:user :username]))
  (om/update! cursor [:user :name] (get-in response [:user :name]))
  (om/update! cursor [:company :alias] (get-in response [:company :alias]))
  (om/update! cursor [:company :name] (get-in response [:company :name]))
  (let [route (:before-session-expired-route @cursor)]
    (om/update! cursor :before-session-expired-route nil)
    (set! (-> js/window .-location .-hash) (str "#" (or route (get-in routes [:home :route]))))))

(defn listen-login [cursor owner]
  (let [c (om/get-state owner :login-chan)
        loading {:loading-chan (om/get-state owner :loading-chan)
                 :topic-key :login}
        handler (->> (wait (assoc loading :action :stop))
                     (handle-response cursor)
                     (create-session owner)
                     (wait (assoc loading :action :start)))]
    (go (while true
          (<! c)
          (let [resp (<! (handler))]
            (if (error? resp)
              (if-let [errors (if (coll? (:error resp)) (:error resp))]
                (do (om/set-state! owner :hints (reduce #(assoc %1 (key %2) (first (val %2))) {} errors))
                    (om/set-state! owner :validation-state (reduce #(assoc %1 %2 :on-error) {} (keys errors)))
                    (.select (om/get-node owner "username")))
                (do (om/set-state! owner :hints {})
                    (om/set-state! owner :validation-state {})))
              (login (:session cursor) resp)))))))

(defn main-view [cursor owner]
  (reify
    om/IInitState
    (init-state [_]
      (let [loading-chan (chan)]
        {:validation-state {}
         :hints {}
         :show-loading false
         :loading-chan loading-chan
         :loading-pub-chan (pub loading-chan :loading)
         :login-chan (chan)}))

    om/IWillMount
    (will-mount [_]
      (listen-display-loading owner (om/get-state owner :loading-pub-chan))
      (listen-login {:err-msgs (:err-msgs cursor)
                     :session (:session cursor)} owner))

    om/IDidMount
    (did-mount [_]
      (.select (om/get-node owner "username")))

    om/IRenderState
    (render-state [_ state]
      (dom/div #js {:className "container-fluid"
                    :style #js {:height "100%"
                                :min-height "450px"}}
               (dom/div #js {:className "row"
                             :style #js {:height "100%"}}
                        (dom/div #js {:className "col-md-offset-1 col-md-10 col-xs-offset-1 col-xs-10"
                                      :style #js {:height "100%"}}
                                 (dom/div #js {:className "row v-align"
                                               :style #js {:height "90%"}}
                                          (dom/div #js {:className "panel panel-default"
                                                        :style #js {:height "90%"}}
                                                   (dom/div #js {:className "panel-body"
                                                                 :style #js {:height "100%"}}
                                                            (dom/div #js {:className "col-md-8 v-align hidden-xs"
                                                                          :style #js {:height "30%"}}
                                                                     (dom/p #js {:className "text-center"}
                                                                            (dom/span #js {:style #js {:font-size "8em"
                                                                                                       :color "gray"
                                                                                                       :font-weight "bold"}} app-name)
                                                                            (dom/span #js {:style #js {:font-size "8em"
                                                                                                       :color "gray"
                                                                                                       :font-weight "bold"}} app-name-ext)))
                                                            (dom/div #js {:className "col-xs-12 visible-xs v-align-xs"}
                                                                     (dom/p #js {:className "text-center"}
                                                                            (dom/span #js {:style #js {:font-size "2.5em"
                                                                                                       :color "gray"
                                                                                                       :font-weight "bold"}} app-name)
                                                                            (dom/span #js {:style #js {:font-size "2.5em"
                                                                                                       :color "gray"
                                                                                                       :font-weight "bold"}} app-name-ext)))
                                                            (dom/div #js {:className "col-md-3 col-xs-12 v-align"
                                                                          :style #js {:height "50%"}}
                                                                     (dom/div #js {:className "row"}
                                                                              (dom/div #js {:className "col-md-12"}
                                                                                       (when-not (empty? (:err-msgs cursor))
                                                                                         (apply dom/div #js {:style #js {:position "absolute"
                                                                                                                         :top "-7em"}}
                                                                                                (for [msg (:err-msgs cursor)] (om/build alert-danger msg))))
                                                                                       (dom/h3 #js {:className "hidden-xs"} (t :default/begin-session))
                                                                                       (dom/h5 #js {:className "text-center visible-xs"} (t :default/begin-session))
                                                                                       (when-not (empty? (:hints state)) (dom/p #js {:className "text-danger"} (:all (:hints state))))
                                                                                       (dom/form #js {:className "form"
                                                                                                      :role "form"
                                                                                                      :onSubmit (fn [e]
                                                                                                                  (.preventDefault e)
                                                                                                                  (put! (:login-chan state) true))}
                                                                                                 (dom/div #js {:className (str/join " " ["form-group" (if (:all (:validation-state state)) "has-error")])}
                                                                                                          (dom/input #js {:ref "username"
                                                                                                                          :type "text"
                                                                                                                          :className "form-control"
                                                                                                                          :placeholder (t :default/username)
                                                                                                                          :defaultValue ""}))
                                                                                                 (dom/div #js {:className (str/join " " ["form-group" (if (:all (:validation-state state)) "has-error")])}
                                                                                                          (dom/input #js {:ref "password"
                                                                                                                          :type "password"
                                                                                                                          :className "form-control"
                                                                                                                          :placeholder (t :default/password)
                                                                                                                          :defaultValue ""}))
                                                                                                 (dom/button #js {:className "btn btn-default hidden-xs"} "Login")
                                                                                                 (dom/button #js {:className "btn btn-default btn-block visible-xs"} "Login")
                                                                                                 (if (:show-loading state)
                                                                                                   (dom/img #js {:style #js {:margin-left "1em"}
                                                                                                                 :src "images/ajax-loader.gif"})))))
                                                                     (dom/div #js {:className "row hidden-xs"
                                                                                   :style #js {:margin-top "15px"}}
                                                                              (dom/div #js {:className "col-md-12"}
                                                                                       (dom/p nil
                                                                                              (dom/small nil
                                                                                                         (t :default/minimal-requirements)
                                                                                                         (dom/a #js {:href (:url chrome)
                                                                                                                     :target "_blank"} (:version chrome))
                                                                                                         ", "
                                                                                                         (dom/a #js {:href (:url firefox)
                                                                                                                     :target "_blank"} (:version firefox))))))))))
                                 (dom/div #js {:className "row"}
                                          (dom/div #js {:className "col-md-12"}
                                                   (dom/p #js {:className "text-center"}
                                                          (dom/small nil app-brand)
                                                          " | "
                                                          (dom/a #js {:href (str "mailto:" app-email)} app-email))))))))))

(defn render-root [cursor target]
  (om/root main-view cursor {:target target}))
