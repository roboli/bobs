(ns bobs.views.documents.sales-returns
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [bobs.views.documents.documents :as docs]
            [bobs.utils :as utils :refer [error? browser-supports-date-input?]]
            [bobs.i18n :refer [t date-fmt date-user-fmt date-jquery-fmt]]
            [bobs.data.request :refer [request]]
            [bobs.views.menus-view :refer [menus-view]]
            [bobs.views.documents.form-view :refer [handler form-view]]
            [bobs.validators.sales-returns :as pur-val]
            [bobs.validators.sales-return-details :as pur-det-val]))

(def resource "sales-returns")

(def send-request (request (str "/" resource)))

(def send-request-details (request (str "/" resource "/drafts")))

(def fmt-page
  (partial handler (fn [response]
                     (go (assoc response
                           :records (reduce
                                     #(conj %1 (assoc %2
                                                 :date (docs/format-date (:date %2)))) ; convert date value to string
                                     []
                                     (:records response))
                           :warehouses (:warehouses response))))))

(defn format-page [& [handler]]
  (docs/get-warehouses (fmt-page handler)))

(def format-values
  (partial handler (fn [response]
                     (go (let [selected (:selected response)
                               date (docs/format-date (:date selected))
                               datetime (docs/format-datetime (:date selected))
                               currency-symbol (:currency-symbol selected)]
                           {:selected (assoc selected
                                        :date datetime
                                        :records (map
                                                  #(assoc %
                                                     :cost (docs/format-currency currency-symbol (:cost %))
                                                     :quantity (docs/format-quantity (:quantity %) (:uom-abbreviation %))
                                                     :total (docs/format-currency currency-symbol (:total %))
                                                     :image (docs/format-image %))
                                                  (:records selected))
                                        :total (docs/format-currency currency-symbol (:total selected)))
                            :records (assoc (:records response) :date date)})))))

(defn get-record [& [handler]]
  (docs/format-document (format-values handler)))

(def get-empty-record
  (partial handler (fn [& [data]]
                     (go (assoc docs/empty-record-entry
                           :customer-id ""
                           :warehouse-id "")))))

(def get-customers
  (partial handler (fn [response]
                     (go (let [resp (<! ((request "/customers") {:method :get}))]
                           (if (error? resp)
                             resp
                             (assoc response :customers resp)))))))

(def get-warehouses
  (partial handler (fn [response]
                     (go (let [resp (<! ((request "/warehouses") {:method :get
                                                                  :query {:order "name"}}))]
                           (if (error? resp)
                             resp
                             (assoc response :warehouses resp)))))))

(defn get-new-record [& [handler]]
  (-> handler
      (get-warehouses)
      (get-customers)
      ((docs/get-draft send-request-details))
      (get-empty-record)))

(def get-print
  (partial handler (fn [id]
                     (go (<! ((request "/sales-returns/print" :edn-parse? false) {:method :get
                                                                                  :path [id]}))))))

(def get-print-summary
  (partial handler (fn [params]
                     (go (<! ((request "/sales-returns/print/summary" :edn-parse? false) {:method :get
                                                                                          :query params}))))))

(def labels (assoc docs/labels
              :customer-name (t :default/customer)
              :customers (t :default/customer)
              :warehouse-name (t :default/warehouse)
              :warehouses (t :default/warehouse)))

(def types (assoc docs/types
             :customers {:type :select
                         :value-key :id
                         :text-key :name
                         :selected-key :customer-id
                         :error-key-aliases [:customer-id]}
             :warehouses {:type :select
                          :value-key :id
                          :text-key :name
                          :selected-key :warehouse-id
                          :error-key-aliases [:warehouse-id]}))

(defn render-root [cursor target]
  (om/root menus-view cursor {:target target
                              :opts {:main-active-route :sales
                                     :second-active-route :sales-returns
                                     :heading (t :default/sales)
                                     :form-view form-view
                                     :form-opts (assoc (docs/form-opts :entry)
                                                  :heading (t :default/returns)
                                                  :send-request send-request
                                                  :send-request-details send-request-details
                                                  :download-url resource
                                                  :labels labels
                                                  :types types
                                                  :focus :customers
                                                  :form-display [:number :date :customer-name :customers :warehouse-name :warehouses :comment]
                                                  :table-display (into docs/table-display [:customer-name :warehouse-name])
                                                  :optionals [:reference :comment]
                                                  :validate (docs/validate pur-val/rules)
                                                  :validate-rules pur-val/rules
                                                  :validate-detail (docs/validate-detail pur-det-val/rules)
                                                  :validate-detail-rules pur-det-val/rules
                                                  :persist-keys [:customer-id :warehouse-id :purchase-date :reference :comment]
                                                  :get-page format-page
                                                  :get-record get-record
                                                  :get-new-record get-new-record
                                                  :get-print get-print
                                                  :get-print-summary get-print-summary)}}))
