(ns bobs.views.documents.stock-view
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [put! chan <!]]
            [bobs.views.documents.stock-form-view :refer [form]]
            [bobs.utils :refer [css-trans-group error?]]
            [bobs.widgets.tables :refer [table-view table]]
            [bobs.widgets.modals :refer [confirm-modal image-modal]]
            [bobs.widgets.pager :refer [pager]]
            [bobs.widgets.input-controls :refer [text-listen]]
            [bobs.i18n :refer [t]]
            [bobs.views.utils :refer [handle-response wait]]
            [goog.string :as gstring]
            [goog.string.format]))

(defn handler [f & [handler]]
  (fn [data]
    (go (let [resp (if (error? data)
                     data
                     (<! (f data)))]
          (if-not handler
            resp
            (<! (handler resp)))))))

(defn default-handler [& [handler]]
  (fn [data]
    (go (if-not handler
          data
          (<! (handler data))))))

(defn get-page [request]
  (partial handler (fn [data]
                     (go (<! (request {:method :get
                                       :path ["page"]
                                       :query data}))))))

(defn get-record [request pagination]
  (partial handler (fn [sku]
                     (go (<! (request {:method :get
                                       :path [sku "stock"]
                                       :query {:include-header 1}}))))))

(defn update-records-pagination [resp owner]
  (om/set-state! owner :records (:records resp))
  (om/set-state! owner [:pagination :page] (:page resp))
  (om/set-state! owner [:pagination :page-size] (:page-size resp))
  (om/set-state! owner [:pagination :total-records] (:total-records resp))
  (om/set-state! owner [:pagination :total-pages] (:total-pages resp))
  (om/set-state! owner [:pagination :order] (:order resp))
  (om/set-state! owner [:pagination :sort] (:sort resp))
  (om/set-state! owner [:pagination :search] (:search resp)))

(defn listen-search [owner]
  (let [c (om/get-state owner :search-chan)
        get-page-chan (om/get-state owner :get-page-chan)]
    (go (while true
          (let [value (<! c)]
            (if-not (= value "")
              (put! get-page-chan {:page 1 :search value})
              (put! get-page-chan {:page 1 :search nil})))))))

(defn listen-get-page [owner handler]
  (let [c (om/get-state owner :get-page-chan)]
    (go (while true
          (let [data (<! c)
                pagination {:page-size (om/get-state owner [:pagination :page-size])
                            :order (om/get-state owner [:pagination :order])
                            :sort (om/get-state owner [:pagination :sort])
                            :search (om/get-state owner [:pagination :search])}
                data (if (coll? data)
                       (merge pagination data)
                       (assoc pagination :page data))]
            (let [resp (<! (handler data))]
              (if-not (error? resp)
                (update-records-pagination resp owner))))))))

(defn listen-get [owner handler]
  (let [c (om/get-state owner :get-chan)]
    (go (while true
          (let [sku (<! c)
                resp (<! (handler sku))]
            (if-not (error? resp)
              (let [index (first (keep-indexed (fn [i v] (if (= sku (:sku v)) i)) (om/get-state owner :records)))]
                (om/set-state! owner :selected (or (:selected resp) resp))
                (om/set-state! owner [:records index] (or (:records resp) resp))
                (om/set-state! owner :status :viewing))))))))

(defn listen-back-form [owner]
  (let [c (om/get-state owner :back-chan)]
    (go (while true
          (<! c)
          (om/set-state! owner :selected nil)
          (om/set-state! owner :status :idle)
          (om/set-state! owner :err-msgs [])))))

(defn stock-view [cursor owner {:keys [send-request] :as opts}]
  (reify
    om/IInitState
    (init-state [_]
      {:pagination {:page nil
                    :page-size nil
                    :order nil
                    :sort nil
                    :search nil
                    :total-pages nil
                    :total-records nil}
       :records []
       :selected nil
       :status :idle
       :search-chan (chan)
       :get-page-chan (chan)
       :get-chan (chan)
       :back-chan (chan)})
    
    om/IWillMount
    (will-mount [_]
      (let [loading (:loading opts)
            page-handler (or (:get-page opts) default-handler)
            record-handler (or (:get-record opts) default-handler)
            start-waiting (partial wait (assoc loading :action :start))
            stop-waiting (partial wait (assoc loading :action :stop))
            handle-response (partial handle-response cursor)
            get-page (get-page send-request)
            get-record (get-record send-request (:pagination-details opts))]
        (listen-search owner)
        (listen-get-page owner (->> (stop-waiting)
                                    (handle-response)
                                    (page-handler)
                                    (get-page)
                                    (start-waiting)))
        (listen-get owner (->> (stop-waiting)
                               (handle-response)
                               (record-handler)
                               (get-record)
                               (start-waiting)))
        (listen-back-form owner))
      (let [pagination (:pagination opts)]
        (put! (om/get-state owner :get-page-chan) {:page (:page pagination)
                                                   :page-size (:page-size pagination)
                                                   :order (:order pagination)
                                                   :sort (:sort pagination)})))

    om/IDidUpdate
    (did-update [_ _ prev]
      (if-not (= (om/get-state owner :status) (:status prev))
        (om/update! (:err-msgs cursor) [])))

    om/IRenderState
    (render-state  [_ state]
      (dom/div #js {:className "panel panel-default"}
               (dom/div #js {:className "panel-heading hidden-xs"}
                        (dom/h3 #js {:className "panel-title"} (:heading opts)))
               (dom/h4 #js {:className "text-center visible-xs"} (:heading opts))
               (dom/div #js {:className "panel-body"}
                        (css-trans-group #js {:transitionName "fade"
                                              :transitionLeave false}
                                         (if (= (:status state) :idle)
                                           (dom/div #js {:key "search"
                                                         :className "row"}
                                                    (dom/div #js {:className "col-md-5 col-xs-12"}
                                                             (om/build text-listen nil {:init-state {:value (get-in state [:pagination :search])
                                                                                                     :old-value (get-in state [:pagination :search])}
                                                                                        :opts {:changed-chan (:search-chan state)
                                                                                               :placeholder (t :default/search)}}))
                                                    (dom/div #js {:className "col-md-offset-5 col-md-2 col-xs-12"}
                                                             (dom/div #js {:className "btn-group pull-right hidden-xs"}
                                                                      (dom/button #js {:className "btn btn-default glyphicon glyphicon-refresh"
                                                                                       :title (t :default/refresh)
                                                                                       :onClick #(put! (om/get-state owner :get-page-chan) (get-in state [:pagination :page]))}))
                                                             (dom/button #js {:className "btn btn-default btn-block glyphicon glyphicon-refresh visible-xs"
                                                                              :title (t :default/refresh)
                                                                              :onClick #(put! (om/get-state owner :get-page-chan) (get-in state [:pagination :page]))})))
                                           (om/build form nil {:init-state {:record (into {} (:selected state))}
                                                               :opts {:buttons-labels {:back (t :default/back)
                                                                                       :print (t :default/print)}
                                                                      :back-chan (:back-chan state)}}))))
               (css-trans-group #js {:transitionName "fade"
                                     :transitionLeave false}
                                (if (and (= (:status state) :idle) (not (nil? (get-in state [:pagination :total-records]))))
                                  (om/build table-view nil {:state {:pagination (:pagination state)
                                                                    :records (:records state)}
                                                            :opts {:labels (:labels opts)
                                                                   :display (:table-display opts)
                                                                   :types (:types opts)
                                                                   :id-key (:id-key opts)
                                                                   :id-key-image :id
                                                                   :show-image (:show-image opts)
                                                                   :image-key :image
                                                                   :image-src (get-in opts [:image-src :thumbnail])
                                                                   :image-not-available-msg (:image-not-available-msg opts)
                                                                   :header-key (:table-header-key opts)
                                                                   :pager pager
                                                                   :pages-summary (t :default/pages-summary)
                                                                   :no-records (t :default/no-records)
                                                                   :page-sizes [5 10 20 50]
                                                                   :pager-size (get-in opts [:pagination :pager-size])
                                                                   :per-page-label (t :default/per-page-label)
                                                                   :selected-chan (:get-chan state)
                                                                   :change-page-chan (:get-page-chan state)}})
                                  (if (= (:status state) :viewing)
                                    (om/build table nil {:state {:records (get-in state [:selected :records])}
                                                         :opts {:id-key :id
                                                                :display (:details-display opts)
                                                                :types (:types opts)
                                                                :headers (:details-labels opts)
                                                                :no-records (t :default/no-records)}})
                                    (dom/div #js {:key "empty"}))))
               (if-not (= (:status state) :idle)
                 (dom/div #js {:className "row"}
                          (dom/div #js {:className "col-md-offset-8 col-md-3 hidden-xs"
                                        :style #js {:margin-bottom "1em"}}
                                   (dom/h4 nil (str (t :default/total) ": " (get-in state [:selected :total]))))
                          (dom/div #js {:className "col-xs-12 text-center visible-xs"
                                        :style #js {:margin-bottom "1em"}}
                                   (dom/h5 nil (str (t :default/total) ": " (get-in state [:selected :total]))))))))))
