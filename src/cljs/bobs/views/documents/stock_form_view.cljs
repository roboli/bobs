(ns bobs.views.documents.stock-form-view
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [put! chan pub]]
            [bobs.utils :refer [browser-supports-date-input?]]
            [bobs.i18n :refer [t date-fmt date-user-fmt date-jquery-fmt]]
            [bobs.widgets.input-controls :refer [date-listen select-inline]]))

(defn form [cursor owner opts]
  (reify
    om/IRenderState
    (render-state [_ state]
      (dom/div nil
               (dom/div #js {:key "back"
                             :className "row"}
                        (dom/div #js {:className "col-md-12 col-xs-12"
                                      :style #js {:border-bottom "1px solid #eee"
                                                  :padding-bottom "1em"
                                                  :margin-bottom "2em"}}
                                 (dom/div #js {:className "col-md-offset-11 col-md-1 col-xs-12"}
                                          (dom/div #js {:className "btn-group pull-right hidden-xs"}
                                                   (dom/button #js {:className "btn btn-default glyphicon glyphicon-share-alt"
                                                                    :type "button"
                                                                    :title (or (get-in opts [:buttons-labels :back]) "Go back")
                                                                    :onClick #(put! (:back-chan opts) true)}))
                                          (dom/button #js {:className "btn btn-default btn-block glyphicon glyphicon-share-alt visible-xs"
                                                           :type "button"
                                                           :title (or (get-in opts [:buttons-labels :back]) "Go back")
                                                           :onClick #(put! (:back-chan opts) true)}))))
               (dom/form #js {:className "form-horizontal"
                              :role "form"}
                         (dom/div #js {:className "row"}
                                  (dom/div #js {:className "col-md-offset-1 col-md-11"}
                                           (dom/fieldset #js {:disabled true}
                                                         (dom/div #js {:className  "form-group"}
                                                                  (dom/label #js {:className "col-md-2 control-label"} (t :default/description))
                                                                  (dom/div #js {:className "col-md-6"}
                                                                           (dom/input #js {:className "form-control"
                                                                                           :type "text"
                                                                                           :defaultValue (get-in state [:record :description])})))
                                                         (dom/div #js {:className  "form-group"}
                                                                  (dom/label #js {:className "col-md-2 control-label"} (t :default/sku))
                                                                  (dom/div #js {:className "col-md-6"}
                                                                           (dom/input #js {:className "form-control"
                                                                                           :type "text"
                                                                                           :defaultValue (get-in state [:record :sku])})))
                                                         (dom/div #js {:className  "form-group"}
                                                                  (dom/label #js {:className "col-md-2 control-label"} (t :default/uom))
                                                                  (dom/div #js {:className "col-md-6"}
                                                                           (dom/input #js {:className "form-control"
                                                                                           :type "text"
                                                                                           :defaultValue (get-in state [:record :uom-name])})))
                                                         (dom/div #js {:className "form-group"}
                                                                  (dom/div #js {:className "col-md-offset-2 col-md-6"}
                                                                           (dom/div #js {:className "checkbox"}
                                                                                    (dom/label nil
                                                                                               (dom/input #js {:type "checkbox"
                                                                                                               :checked (get-in state [:record :active])}
                                                                                                          (t :default/active))))))))))))))
