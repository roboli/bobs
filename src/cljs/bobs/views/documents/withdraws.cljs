(ns bobs.views.documents.withdraws
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [bobs.views.documents.documents :as docs]
            [bobs.utils :as utils :refer [error?]]
            [bobs.i18n :refer [t]]
            [bobs.data.request :refer [request]]
            [bobs.views.menus-view :refer [menus-view]]
            [bobs.validators.withdraws :as wit-val]
            [bobs.validators.withdraw-details :as wit-det-val]
            [bobs.views.documents.form-view :refer [form-view handler]]))

(def resource "withdraws")

(def send-request (request (str "/" resource)))

(def send-request-details (request (str "/" resource "/drafts")))

(def fmt-page
  (partial handler (fn [response]
                     (go (assoc response
                           :records (reduce
                                     #(conj %1 (assoc %2
                                                 :date (docs/format-date (:date %2)))) ; convert date value to string
                                     []
                                     (:records response))
                           :warehouses (:warehouses response))))))

(defn format-page [& [handler]]
  (docs/get-warehouses (fmt-page handler)))

(def format-values
  (partial handler (fn [response]
                     (go (let [selected (:selected response)
                               date (docs/format-date (:date selected))
                               datetime (docs/format-datetime (:date selected))
                               currency-symbol (:currency-symbol selected)]
                           {:selected (assoc selected
                                        :date datetime
                                        :records (map
                                                  #(assoc %
                                                     :price (docs/format-currency currency-symbol (:price %))
                                                     :quantity (docs/format-quantity (:quantity %) (:uom-abbreviation %))
                                                     :total (docs/format-currency currency-symbol (:total %))
                                                     :image (docs/format-image %))
                                                  (:records selected))
                                        :total (docs/format-currency currency-symbol (:total selected)))
                            :records (assoc (:records response) :date date)})))))

(defn get-record [& [handler]]
  (docs/format-document (format-values handler)))

(def get-empty-record
  (partial handler (fn [& [data]]
                     (go (assoc-in
                          docs/empty-record-withdraw
                          [:selected :warehouse-id] "")))))

(def get-warehouses
  (partial handler (fn [response]
                     (go (let [resp (<! ((request "/warehouses") {:method :get
                                                                  :query {:order "name"}}))]
                           (if (error? resp)
                             resp
                             (assoc-in response [:selected :warehouses] resp)))))))

(defn get-new-record [& [handler]]
  (-> handler
      (get-warehouses)
      ((docs/get-draft send-request-details))
      (get-empty-record)))

(def get-print
  (partial handler (fn [id]
                     (go (<! ((request "/withdraws/print" :edn-parse? false) {:method :get
                                                                              :path [id]}))))))

(def get-print-summary
  (partial handler (fn [params]
                     (go (<! ((request "/withdraws/print/summary" :edn-parse? false) {:method :get
                                                                                      :query params}))))))

(defn render-root [cursor target]
  (om/root menus-view cursor {:target target
                              :opts {:main-active-route :inventory
                                     :second-active-route :withdraws
                                     :heading (t :default/inventory)
                                     :form-view form-view
                                     :form-opts (assoc (docs/form-opts :withdraw :amount-key-resp :cost)
                                                  :heading (t :default/withdraws)
                                                  :send-request send-request
                                                  :send-request-details send-request-details
                                                  :download-url resource
                                                  :labels docs/labels
                                                  :types docs/types
                                                  :focus :comment
                                                  :form-display [:number :date :comment]
                                                  :table-display docs/table-display
                                                  :optionals [:comment]
                                                  :validate (docs/validate wit-val/rules)
                                                  :validate-rules wit-val/rules
                                                  :validate-detail (docs/validate-detail wit-det-val/rules)
                                                  :validate-detail-rules wit-det-val/rules
                                                  :persist-keys [:comment]
                                                  :get-page format-page
                                                  :get-record get-record
                                                  :get-new-record get-new-record
                                                  :get-print get-print
                                                  :get-print-summary get-print-summary)}}))
