(ns bobs.views.documents.inventory
  (:require [om.core :as om :include-macros true]
            [bobs.i18n :refer [t]]
            [bobs.views.menus-view :refer [menus-view]]))

(defn render-root [cursor target]
  (om/root menus-view cursor {:target target
                              :opts {:main-active-route :inventory
                                     :heading (t :default/inventory)}}))
