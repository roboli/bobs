(ns bobs.views.documents.purchase-receipts
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [bobs.views.documents.documents :as docs]
            [bobs.utils :as utils :refer [error? browser-supports-date-input?]]
            [bobs.i18n :refer [t date-fmt date-user-fmt date-jquery-fmt]]
            [bobs.data.request :refer [request]]
            [bobs.views.menus-view :refer [menus-view]]
            [bobs.views.documents.form-view :refer [handler form-view]]
            [bobs.validators.purchase-receipts :as pur-val]
            [bobs.validators.purchase-receipt-details :as pur-det-val]))

(def resource "purchase-receipts")

(def send-request (request (str "/" resource)))

(def send-request-details (request (str "/" resource "/drafts")))

(def fmt-page
  (partial handler (fn [response]
                     (go (assoc response
                           :records (reduce
                                     #(conj %1 (assoc %2
                                                 :date (docs/format-date (:date %2))
                                                 :purchase-date (docs/format-date (:purchase-date %2)))) ; convert date value to string
                                     []
                                     (:records response))
                           :warehouses (:warehouses response))))))

(defn format-page [& [handler]]
  (docs/get-warehouses (fmt-page handler)))

(def format-values
  (partial handler (fn [response]
                     (go (let [selected (:selected response)
                               date (docs/format-date (:date selected))
                               datetime (docs/format-datetime (:date selected))
                               purchase-date (docs/format-date (:purchase-date selected))
                               currency-symbol (:currency-symbol selected)]
                           {:selected (assoc selected
                                        :date datetime
                                        :records (map
                                                  #(assoc %
                                                     :cost (docs/format-currency currency-symbol (:cost %))
                                                     :quantity (docs/format-quantity (:quantity %) (:uom-abbreviation %))
                                                     :total (docs/format-currency currency-symbol (:total %))
                                                     :image (docs/format-image %))
                                                  (:records selected))
                                        :total (docs/format-currency currency-symbol (:total selected)))
                            :records (assoc (:records response) :date date :purchase-date purchase-date)})))))

(defn get-record [& [handler]]
  (docs/format-document (format-values handler)))

(def get-empty-record
  (partial handler (fn [& [data]]
                     (go (assoc docs/empty-record-entry
                           :purchase-date ""
                           :supplier-id ""
                           :warehouse-id ""
                           :reference "")))))

(def get-suppliers
  (partial handler (fn [response]
                     (go (let [resp (<! ((request "/suppliers") {:method :get}))]
                           (if (error? resp)
                             resp
                             (assoc response :suppliers resp)))))))

(def get-warehouses
  (partial handler (fn [response]
                     (go (let [resp (<! ((request "/warehouses") {:method :get
                                                                  :query {:order "name"}}))]
                           (if (error? resp)
                             resp
                             (assoc response :warehouses resp)))))))

(defn get-new-record [& [handler]]
  (-> handler
      (get-warehouses)
      (get-suppliers)
      ((docs/get-draft send-request-details))
      (get-empty-record)))

(def get-print
  (partial handler (fn [id]
                     (go (<! ((request "/purchase-receipts/print" :edn-parse? false) {:method :get
                                                                                      :path [id]}))))))

(def get-print-summary
  (partial handler (fn [params]
                     (go (<! ((request "/purchase-receipts/print/summary" :edn-parse? false) {:method :get
                                                                                              :query params}))))))

(def labels (assoc docs/labels
              :purchase-date (t :default/purchase-date)
              :reference (t :default/reference)
              :supplier-name (t :default/supplier)
              :suppliers (t :default/supplier)
              :warehouse-name (t :default/warehouse)
              :warehouses (t :default/warehouse)))

(def types (assoc docs/types
             :suppliers {:type :select
                         :value-key :id
                         :text-key :name
                         :selected-key :supplier-id
                         :error-key-aliases [:supplier-id]}
             :warehouses {:type :select
                          :value-key :id
                          :text-key :name
                          :selected-key :warehouse-id
                          :error-key-aliases [:warehouse-id]}
             :purchase-date {:type :date
                             :jquery-datepicker? (not browser-supports-date-input?)
                             :date-format date-fmt
                             :date-format-jquery date-jquery-fmt
                             :date-format-user date-user-fmt}))

(defn render-root [cursor target]
  (om/root menus-view cursor {:target target
                              :opts {:main-active-route :purchases
                                     :second-active-route :purchase-receipts
                                     :heading (t :default/purchases)
                                     :form-view form-view
                                     :form-opts (assoc (docs/form-opts :entry)
                                                  :heading (t :default/purchase-receipts)
                                                  :send-request send-request
                                                  :send-request-details send-request-details
                                                  :download-url resource
                                                  :labels labels
                                                  :types types
                                                  :focus :suppliers
                                                  :form-display [:number :date :supplier-name :suppliers :warehouse-name :warehouses :purchase-date :reference :comment]
                                                  :table-display (into docs/table-display [:supplier-name :warehouse-name :purchase-date :reference])
                                                  :optionals [:reference :comment]
                                                  :validate (docs/validate pur-val/rules)
                                                  :validate-rules pur-val/rules
                                                  :validate-detail (docs/validate-detail pur-det-val/rules)
                                                  :validate-detail-rules pur-det-val/rules
                                                  :persist-keys [:supplier-id :warehouse-id :purchase-date :reference :comment]
                                                  :get-page format-page
                                                  :get-record get-record
                                                  :get-new-record get-new-record
                                                  :get-print get-print
                                                  :get-print-summary get-print-summary)}}))
