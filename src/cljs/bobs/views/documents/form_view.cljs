(ns bobs.views.documents.form-view
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [take! put! chan pub sub]]
            [bobs.utils :refer [css-trans-group error? browser-supports-date-input?]]
            [bobs.widgets.tables :refer [table-view table]]
            [bobs.widgets.pager :refer [pager]]
            [bobs.widgets.modals :refer [confirm-modal table-modal]]
            [bobs.widgets.input-controls :refer [text-listen date-listen select-inline]]
            [bobs.i18n :refer [t date-fmt date-user-fmt date-jquery-fmt]]
            [bobs.views.utils :refer [handle-response wait]]
            [bobs.widgets.form-details :refer [form form-inline]]))

(defn handler [f & [handler]]
  (fn [data]
    (go (let [resp (if (error? data)
                     data
                     (<! (f data)))]
          (if-not handler
            resp
            (<! (handler resp)))))))

(defn default-handler [& [handler]]
  (fn [data]
    (go (if-not handler
          data
          (<! (handler data))))))

(defn validate-handler [validate & [handler]]
  (fn [data]
    (go (let [errors (validate (:validate data))
              resp (if-not (empty? errors)
                     {:error errors}
                     (dissoc data :validate))]
          (if (error? resp)
            resp
            (if-not handler
              resp
              (<! (handler resp))))))))

(defn get-page [request]
  (partial handler (fn [data]
                     (go (<! (request {:method :get
                                       :path ["page"]
                                       :query data}))))))

(defn get-record [request pagination]
  (partial handler (fn [id]
                     (go (<! (request {:method :get
                                       :path [id "details" "page"]
                                       :query {:page (:page pagination)
                                               :page-size (:page-size pagination)
                                               :sort (:sort pagination)
                                               :include-header 1}}))))))

(defn get-record-details-page [request]
  (partial handler (fn [data]
                     (go (<! (request {:method :get
                                       :path [(:id data) "details" "page"]
                                       :query (:query data)}))))))

(defn post [request persist-keys pagination]
  (partial handler (fn [data]
                     (go (<! (request {:method :post
                                       :path [(:id data)]
                                       :query {:respond-with-details-pagination 1
                                               :page (:page pagination)
                                               :page-size (:page-size pagination)
                                               :sort (:sort pagination)}
                                       :body (select-keys (:body data) persist-keys)}))))))

(defn put [request]
  (partial handler (fn [id]
                     (go (<! (request {:method :put
                                       :path [id]
                                       :query {:void 1}}))))))

(defn get-details-page [request]
  (partial handler (fn [data]
                     (go (<! (request {:method :get
                                       :path [(:id data) "page"]
                                       :query (:query data)}))))))

(defn post-detail [request persist-keys pagination]
  (partial handler (fn [data]
                     (go (<! (request {:method :post
                                       :path [(:id data)]
                                       :query {:respond-with-details-pagination 1
                                               :page-size (:page-size pagination)}
                                       :body (select-keys (:body data) persist-keys)}))))))

(defn delete-detail [request]
  (partial handler (fn [data]
                     (go (<! (request {:method :delete
                                       :path data}))))))

(defn update-records-pagination [resp owner]
  (om/set-state! owner :records (:records resp))
  (om/set-state! owner :warehouses (:warehouses resp))
  (om/set-state! owner [:pagination :page] (:page resp))
  (om/set-state! owner [:pagination :page-size] (:page-size resp))
  (om/set-state! owner [:pagination :total-records] (:total-records resp))
  (om/set-state! owner [:pagination :total-pages] (:total-pages resp))
  (om/set-state! owner [:pagination :order] (:order resp))
  (om/set-state! owner [:pagination :sort] (:sort resp))
  (om/set-state! owner [:pagination :number] (:number resp))
  (om/set-state! owner [:pagination :warehouse-id] (:warehouse-id resp))
  (om/set-state! owner [:pagination :from-date] (:from-date resp))
  (om/set-state! owner [:pagination :to-date] (:to-date resp)))

(defn listen-search [owner]
  (let [c (om/get-state owner :search-chan)
        get-page-chan (om/get-state owner :get-page-chan)]
    (go (while true
          (let [value (<! c)]
            (if-not (= value "")
              (put! get-page-chan {:page 1 :number value})
              (put! get-page-chan {:page 1 :number nil})))))))

(defn listen-warehouse [owner]
  (let [c (om/get-state owner :warehouse-chan)
        get-page-chan (om/get-state owner :get-page-chan)]
    (go (while true
          (let [value (<! c)]
            (if-not (= value "")
              (put! get-page-chan {:page 1 :warehouse-id value})
              (put! get-page-chan {:page 1 :warehouse-id nil})))))))

(defn listen-from-date [owner]
  (let [c (om/get-state owner :from-date-chan)
        get-page-chan (om/get-state owner :get-page-chan)]
    (go (while true
          (let [value (<! c)]
            (if-not (= value "")
              (put! get-page-chan {:page 1 :from-date value})
              (put! get-page-chan {:page 1 :from-date nil})))))))

(defn listen-to-date [owner]
  (let [c (om/get-state owner :to-date-chan)
        get-page-chan (om/get-state owner :get-page-chan)]
    (go (while true
          (let [value (<! c)]
            (if-not (= value "")
              (put! get-page-chan {:page 1 :to-date value})
              (put! get-page-chan {:page 1 :to-date nil})))))))

(defn listen-get-page [owner handler]
  (let [c (om/get-state owner :get-page-chan)]
    (go (while true
          (let [data (<! c)
                pagination {:page-size (om/get-state owner [:pagination :page-size])
                            :order (om/get-state owner [:pagination :order])
                            :sort (om/get-state owner [:pagination :sort])
                            :from-date (om/get-state owner [:pagination :from-date])
                            :to-date (om/get-state owner [:pagination :to-date])
                            :warehouse-id (om/get-state owner [:pagination :warehouse-id])
                            :number (om/get-state owner [:pagination :number])}
                data (if (coll? data)
                       (merge pagination data)
                       (assoc pagination :page data))]
            (let [resp (<! (handler data))]
              (if-not (error? resp)
                (update-records-pagination resp owner))))))))

(defn listen-get [owner handler]
  (let [c (om/get-state owner :get-chan)]
    (go (while true
          (let [id (<! c)
                resp (<! (handler id))]
            (if-not (error? resp)
              (let [index (first (keep-indexed (fn [i v] (if (= id (:id v)) i)) (om/get-state owner :records)))]
                (om/set-state! owner :selected (or (:selected resp) resp))
                (om/set-state! owner [:records index] (or (:records resp) resp))
                (om/set-state! owner :status :viewing))))))))

(defn listen-get-record-details-page [owner handler]
  (let [c (om/get-state owner :get-record-details-page-chan)]
    (go (while true
          (let [data (<! c)
                pagination {:page-size (om/get-state owner [:selected :pagination :page-size])
                            :sort (om/get-state owner [:selected :pagination :sort])}
                data (if (coll? data)
                       (merge pagination data)
                       (assoc pagination :page data))]
            (let [resp (<! (handler {:id (om/get-state owner [:selected :id])
                                     :query data}))]
              (when-not (error? resp)
                (om/set-state! owner [:selected :pagination] {:page (:page resp)
                                                              :page-size (:page-size resp)
                                                              :total-pages (:total-pages resp)
                                                              :total-records (:total-records resp)
                                                              :sort (:sort resp)})
                (om/set-state! owner [:selected :records] (:records resp)))))))))

(defn listen-insert [owner pagination post-handler page-handler record-handler]
  (let [c (om/get-state owner :insert-chan)]
    (go (while true
          (let [record (<! c)
                resp (<! (post-handler {:id (:draft-id record)
                                        :body record
                                        :validate (assoc record :records (om/get-state owner [:selected :records]))}))]
            (if-let [errors (:error resp)]
              (>! c {:sucess false
                     :errors errors})
              (do (let [resp (<! (page-handler pagination))]
                    (if-not (error? resp)
                      (update-records-pagination resp owner)))
                  (let [resp (<! (record-handler resp))]
                    (om/set-state! owner :selected (or (:selected resp) resp))
                    (om/set-state! owner :status :viewing))
                  (>! c {:sucess true}))))))))

(defn listen-void [owner handler]
  (let [c (om/get-state owner :void-chan)]
    (go (while true
          (let [id (<! c)
                resp (<! (handler id))]
            (if-not (error? resp)
              (let [index (first (keep-indexed (fn [i v] (if (= id (:id v)) i)) (om/get-state owner :records)))]
                (om/set-state! owner [:selected :void] true)
                (om/update-state! owner [:records index] (fn [record] (assoc record :void true ))))))))))

(defn listen-back-form [owner]
  (let [c (om/get-state owner :back-chan)]
    (go (while true
          (<! c)
          (om/set-state! owner :selected nil)
          (om/set-state! owner :status :idle)
          (om/set-state! owner :err-msgs [])))))

(defn listen-get-details-page [owner handler]
  (let [c (om/get-state owner :get-details-page-chan)]
    (go (while true
          (let [data (<! c)
                pagination {:page-size (om/get-state owner [:selected :pagination :page-size])}
                data (if (coll? data)
                       (merge pagination data)
                       (assoc pagination :page data))]
            (let [resp (<! (handler {:id (om/get-state owner [:selected :draft-id])
                                     :query data}))]
              (when-not (error? resp)
                (om/set-state! owner [:selected :pagination] {:page (:page resp)
                                                              :page-size (:page-size resp)
                                                              :total-pages (:total-pages resp)
                                                              :total-records (:total-records resp)})
                (om/set-state! owner [:selected :quantity] (:quantity resp))
                (om/set-state! owner [:selected :total] (:total resp))
                (om/set-state! owner [:selected :records] (:records resp)))))))))

(defn listen-insert-detail [owner handler]
  (let [c (om/get-state owner :insert-detail-chan)]
    (go (while true
          (let [record (<! c)
                id (om/get-state owner [:selected :draft-id])
                resp (<! (handler {:id id
                                   :body record
                                   :validate record}))]
            (if-let [errors (:error resp)]
              (>! c {:sucess false
                     :errors errors})
              (do (om/set-state! owner [:selected :pagination] {:page (:page resp)
                                                                :page-size (:page-size resp)
                                                                :total-pages (:total-pages resp)
                                                                :total-records (:total-records resp)})
                  (om/set-state! owner [:selected :quantity] (:quantity resp))
                  (om/set-state! owner [:selected :total] (:total resp))
                  (om/set-state! owner [:selected :records] (:records resp))
                  (>! c {:sucess true}))))))))

(defn listen-delete-detail [owner handler]
  (let [c (om/get-state owner :delete-detail-chan)]
    (go (while true
          (let [detail-id (<! c)
                id (om/get-state owner [:selected :draft-id])
                resp (<! (handler [id detail-id]))]
            (if-not (error? resp)
              (put! (om/get-state owner :get-details-page-chan) {:page (om/get-state owner [:selected :pagination :page])
                                                                 :last-page-on-miss 1})))))))

(defn listen-on-insert [owner]
  (let [c (chan)
        c-form (om/get-state owner :on-insert-chan)
        c-modal (om/get-state owner :modal-chan)]
    (sub (om/get-state owner :modal-pub-chan) :confirm c)
    (go (while true
          (<! c-form)
          (>! c-modal {:modal :modal
                       :show true})
          (>! c-form (:result (<! c)))))))

(defn listen-on-void [owner]
  (let [c (chan)
        c-form (om/get-state owner :on-void-chan)
        c-modal (om/get-state owner :modal-void-chan)]
    (sub (om/get-state owner :modal-void-pub-chan) :confirm c)
    (go (while true
          (<! c-form)
          (>! c-modal {:modal :modal
                       :show true})
          (>! c-form (:result (<! c)))))))

(defn listen-on-print [owner modal-chan handler]
  (let [c-form (om/get-state owner :on-print-chan)]
    (go (while true
          (let [id (<! c-form)
                resp (<! (handler id))]
            (if-not (error? resp)
              (>! modal-chan {:modal :modal
                              :content resp
                              :show true})))))))

(defn listen-on-print-summary [owner modal-chan handler]
  (let [c-form (om/get-state owner :on-print-summary-chan)]
    (go (while true
          (let [params (<! c-form)
                resp (<! (handler{:order "id"
                                  :sort 0
                                  :number (om/get-state owner [:pagination :number])
                                  :warehouse-id (om/get-state owner [:pagination :warehouse-id])
                                  :from-date (om/get-state owner [:pagination :from-date])
                                  :to-date (om/get-state owner [:pagination :to-date])}))]
            (if-not (error? resp)
              (>! modal-chan {:modal :modal
                              :content resp
                              :show true})))))))

(defn listen-on-stock [owner show-chan warehouse-chan handler]
  (let [c-modal (om/get-state owner :modal-stock-chan)
        close {:modal :modal
               :show false}]
    (go (while true
          (let [sku (<! show-chan)
                resp (<! (handler sku))]
            (when-not (error? resp)
              (>! c-modal {:modal :modal
                           :show true
                           :table {:display [:warehouse-name :total :quantity]
                                   :headers {:warehouse-name (t :default/warehouse)
                                             :total ""
                                             :quantity (t :default/quantity)}
                                   :id-key :warehouse-id
                                   :command-columns {:select {:cmd-chan c-modal
                                                              :title "Select"
                                                              :icon "glyphicon glyphicon-ok"}}
                                   :records resp}})
              (let [v (<! c-modal)]
                (if-not (map? v)
                  (do (put! warehouse-chan v)
                      (put! c-modal close))
                  (put! c-modal close)))))))))

(defn form-view [cursor owner {:keys [send-request send-request-details] :as opts}]
  (reify
    om/IInitState
    (init-state [_]
      (let [modal-chan (chan)
            modal-void-chan (chan)
            modal-stock-chan (chan)]
        {:pagination {:page nil
                      :page-size nil
                      :order nil
                      :sort nil
                      :search nil
                      :total-pages nil
                      :total-records nil}
         :records []
         :warehouses []
         :selected nil
         :status :idle
         ;; Break rules, use atom to prevent
         ;; rerendering the whole form
         :showing-more (atom {:showing false})
         :search-chan (chan)
         :warehouse-chan (chan)
         :from-date-chan (chan)
         :to-date-chan (chan)
         :get-page-chan (chan)
         :get-chan (chan)
         :get-record-details-page-chan (chan)
         :insert-chan (chan)
         :back-chan (chan)
         :on-blur-chan (chan)
         :on-change-chan (chan)
         :get-details-page-chan (chan)
         :insert-detail-chan (chan)
         :void-chan (chan)
         :delete-detail-chan (chan)
         :on-insert-chan (chan)
         :on-void-chan (chan)
         :on-print-chan (chan)
         :on-print-summary-chan (chan)
         :on-stock-chan (chan)
         :modal-chan modal-chan
         :modal-pub-chan (pub modal-chan :modal)
         :modal-void-chan modal-void-chan
         :modal-void-pub-chan (pub modal-void-chan :modal)
         :modal-stock-chan modal-stock-chan
         :modal-stock-pub-chan (pub modal-stock-chan :modal)}))
    
    om/IWillMount
    (will-mount [_]
      (let [loading (:loading opts)
            pagination {:page (get-in opts [:pagination :page])
                        :page-size (get-in opts [:pagination :page-size])
                        :order (get-in opts [:pagination :order])
                        :sort (get-in opts [:pagination :sort])}
            pagination-details (:pagination-details opts)
            page-handler (or (:get-page opts) default-handler)
            record-handler (or (:get-record opts) default-handler)
            page-details-handler (or (:get-details-page opts) default-handler)
            print-handler (or (:get-print opts) default-handler)
            print-summary-handler (or (:get-print-summary opts) default-handler)
            validate (if (:validate opts) (partial validate-handler (:validate opts)) default-handler)
            validate-detail (if (:validate-detail opts) (partial validate-handler (:validate-detail opts)) default-handler)
            start-waiting (partial wait (assoc loading :action :start))
            stop-waiting (partial wait (assoc loading :action :stop))
            handle-response (partial handle-response cursor)
            get-page (get-page send-request)
            get-record (get-record send-request pagination-details)
            get-record-details-page (get-record-details-page send-request)
            post (post send-request (:persist-keys opts) pagination-details)
            get-details-page (get-details-page send-request-details)
            post-detail (post-detail send-request-details (:form-inline-persist-keys opts) pagination-details)
            put (put send-request)
            delete-detail (delete-detail send-request-details)
            stock-handler (or (:get-stock opts) default-handler)]
        (listen-search owner)
        (listen-warehouse owner)
        (listen-from-date owner)
        (listen-to-date owner)
        (listen-get-page owner (->> (stop-waiting)
                                    (handle-response)
                                    (page-handler)
                                    (get-page)
                                    (start-waiting)))
        (listen-get owner (->> (stop-waiting)
                               (handle-response)
                               (record-handler)
                               (get-record)
                               (start-waiting)))
        (listen-get-record-details-page owner (->> (stop-waiting)
                                                   (handle-response)
                                                   (page-details-handler)
                                                   (get-record-details-page)
                                                   (start-waiting)))
        (listen-insert owner pagination (->> (stop-waiting)
                                             (handle-response)
                                             (post)
                                             (start-waiting)
                                             (validate))
                       (->> (stop-waiting)
                            (handle-response)
                            (page-handler)
                            (get-page)
                            (start-waiting))
                       (record-handler))
        (listen-void owner (->> (stop-waiting)
                                (handle-response)
                                (put)
                                (start-waiting)))
        (listen-back-form owner)
        (listen-get-details-page owner (->> (stop-waiting)
                                            (handle-response)
                                            (page-details-handler)
                                            (get-details-page)
                                            (start-waiting)))
        (listen-insert-detail owner (->> (stop-waiting)
                                         (handle-response)
                                         (page-details-handler)
                                         (post-detail)
                                         (start-waiting)
                                         (validate-detail)))
        (listen-delete-detail owner (->> (stop-waiting)
                                         (handle-response)
                                         (delete-detail)
                                         (start-waiting)))
        (listen-on-insert owner)
        (listen-on-void owner)
        (listen-on-print owner (:modal-print-chan opts) (->> (stop-waiting)
                                                             (handle-response)
                                                             (print-handler)
                                                             (start-waiting)))
        (listen-on-print-summary owner (:modal-print-chan opts) (->> (stop-waiting)
                                                                     (handle-response)
                                                                     (print-summary-handler)
                                                                     (start-waiting)))
        (if (:stock-show-chan opts)
          (listen-on-stock owner
                           (:stock-show-chan opts)
                           (:warehouse-selected-chan opts)
                           (->> (stop-waiting)
                                (handle-response)
                                (stock-handler)
                                (start-waiting))))
        (put! (om/get-state owner :get-page-chan) pagination)))

    om/IDidUpdate
    (did-update [_ _ prev]
      (if-not (= (om/get-state owner :status) (:status prev))
        (om/update! (:err-msgs cursor) [])))

    om/IRenderState
    (render-state  [_ state]
      (dom/div #js {:className "panel panel-default"}
               (dom/div #js {:className "panel-heading hidden-xs"}
                        (dom/h3 #js {:className "panel-title"} (:heading opts)))
               (dom/h4 #js {:className "text-center visible-xs"} (:heading opts))
               (dom/div #js {:className "panel-body"}
                        (om/build confirm-modal nil {:opts {:id "confirm-modal"
                                                            :title (t :default/save)
                                                            :text (t :default/readonly-document-when-saved)
                                                            :buttons-labels {:cancel (t :default/cancel)}
                                                            :modal-chan (:modal-chan state)
                                                            :pub-chan (:modal-pub-chan state)}})
                        (om/build confirm-modal nil {:opts {:id "confirm-void-modal"
                                                            :title (t :default/void)
                                                            :text (t :default/void-document)
                                                            :buttons-labels {:cancel (t :default/cancel)}
                                                            :modal-chan (:modal-void-chan state)
                                                            :pub-chan (:modal-void-pub-chan state)}})
                        (om/build table-modal nil {:opts {:id "stock-modal"
                                                          :title "Stock"
                                                          :buttons-labels {:cancel (t :default/cancel)}
                                                          :modal-chan (:modal-stock-chan state)
                                                          :pub-chan (:modal-stock-pub-chan state)}})
                        (apply css-trans-group #js {:transitionName "fade"
                                                    :transitionLeave false}
                               (if (= (:status state) :idle)
                                 (list
                                  (dom/div #js {:key "add-search"
                                                :className "row"}
                                           (let [searching (not= (:search-text state) "")]
                                             (dom/div #js {:className "col-sm-8 col-xs-12"}
                                                      (dom/div #js {:className "row"}
                                                               (dom/div #js {:className "col-sm-6 col-xs-12"}
                                                                        (om/build text-listen nil {:init-state {:value (get-in state [:pagination :id])
                                                                                                                :old-value (get-in state [:pagination :id])}
                                                                                                   :opts {:changed-chan (:search-chan state)
                                                                                                          :placeholder (t :default/search)}}))
                                                               (dom/div #js {:className "col-sm-6 hidden-xs"}
                                                                        (om/build select-inline nil {:state {:value (get state :warehouses)}
                                                                                                     :opts {:widgets-pub-chan (pub (chan) :none)
                                                                                                            :value-key :id
                                                                                                            :text-key :name
                                                                                                            :empty-text (str "-- " (t :default/warehouse) " --")
                                                                                                            :on-key-down #()
                                                                                                            :on-change #(put! (:warehouse-chan state) %)}})))
                                                      (dom/div #js {:className "row collapse hidden-xs"
                                                                    :id "more-search"}
                                                               (dom/div #js {:className "col-sm-6"}
                                                                        (om/build date-listen nil {:init-state {:value (get-in state [:pagination :from-date])
                                                                                                                :old-value (get-in state [:pagination :from-date])}
                                                                                                   :opts {:id "from-date"
                                                                                                          :label (t :default/from)
                                                                                                          :jquery-datepicker? (not browser-supports-date-input?)
                                                                                                          :date-format date-fmt
                                                                                                          :date-format-jquery date-jquery-fmt
                                                                                                          :date-format-user date-user-fmt
                                                                                                          :changed-chan (:from-date-chan state)}}))
                                                               (dom/div #js {:className "col-sm-6"}
                                                                        (om/build date-listen nil {:init-state {:value (get-in state [:pagination :to-date])
                                                                                                                :old-value (get-in state [:pagination :to-date])}
                                                                                                   :opts {:id "to-date"
                                                                                                          :label (t :default/to)
                                                                                                          :jquery-datepicker? (not browser-supports-date-input?)
                                                                                                          :date-format date-fmt
                                                                                                          :date-format-jquery date-jquery-fmt
                                                                                                          :date-format-user date-user-fmt
                                                                                                          :changed-chan (:to-date-chan state)}})))))
                                           (dom/div #js {:className "col-sm-1 hidden-xs"}
                                                    (om/build
                                                     (fn [cursor owner opts]
                                                       (reify
                                                         om/IRenderState
                                                         (render-state [_ state]
                                                           (dom/a #js {:className "btn btn-sm btn-default"
                                                                       :role "button"
                                                                       :data-toggle "collapse"
                                                                       :href "#more-search"
                                                                       :onClick (fn [_]
                                                                                  (om/update-state! owner :showing-more #(not %))
                                                                                  ;; Break rules, swap the atom
                                                                                  (swap! cursor update-in [:showing] #(not %)))}
                                                                  (if (:showing-more state) (t :default/less) (t :default/more))))))
                                                     (:showing-more state)
                                                     {:init-state {:showing-more (:showing (deref (:showing-more state)))}}))
                                           (dom/div #js {:className "col-sm-3 hidden-xs"}
                                                    (dom/div #js {:className "btn-group pull-right"}
                                                             (dom/button #js {:className "btn btn-default glyphicon glyphicon-refresh"
                                                                              :title (t :default/refresh)
                                                                              :onClick #(put! (om/get-state owner :get-page-chan) (get-in state [:pagination :page]))})
                                                             (dom/button #js {:className "btn btn-default glyphicon glyphicon-print"
                                                                              :title (t :default/print)
                                                                              :onClick #(put! (get state :on-print-summary-chan) true)})
                                                             (dom/a #js {:className "btn btn-default glyphicon glyphicon-download-alt"
                                                                         :title (t :default/download)
                                                                         :href (let [from-date (get-in state [:pagination :from-date])
                                                                                     to-date (get-in state [:pagination :to-date])
                                                                                     warehouse-id (get-in state [:pagination :warehouse-id])]
                                                                                 (str (:download-url opts)
                                                                                      "/pdf/summary?order="
                                                                                      (get-in state [:pagination :order])
                                                                                      "&sort="
                                                                                      (get-in state [:pagination :sort])
                                                                                      (if from-date (str "&from-date=" from-date) "")
                                                                                      (if to-date (str "&to-date=" to-date) "")
                                                                                      (if warehouse-id (str "&warehouse-id=" warehouse-id) "")))})
                                                             (dom/button #js {:className "btn btn-default glyphicon glyphicon-plus"
                                                                              :title (t :default/add)
                                                                              :onClick (let [loading (:loading opts)
                                                                                             handler (->> (wait (assoc loading :action :stop))
                                                                                                          (handle-response cursor)
                                                                                                          ((:get-new-record opts))
                                                                                                          (wait (assoc loading :action :start)))]
                                                                                         (fn [e]
                                                                                           (.preventDefault e)
                                                                                           (go (let [resp (<! (handler))]
                                                                                                 (when-not (error? resp)
                                                                                                   (om/set-state! owner :selected resp)
                                                                                                   (om/set-state! owner :status :adding))))))}))))
                                  (dom/div #js {:className "row visible-xs"}
                                           (dom/div #js {:className "col-xs-12"}
                                                    (dom/button #js {:className "btn btn-default btn-block glyphicon glyphicon-refresh"
                                                                     :title (t :default/refresh)
                                                                     :onClick #(put! (om/get-state owner :get-page-chan) (get-in state [:pagination :page]))}))))
                                 (list
                                  (om/build form {:status (:status state)
                                                  :selected (:selected state)}
                                            {:init-state {:record (into {} (:selected state))}
                                             :opts {:id-key (:id-key opts)
                                                    :labels (:labels opts)
                                                    :types (:types opts)
                                                    :display (:form-display opts)
                                                    :focus (:focus opts)
                                                    :put-on-change? true
                                                    :optionals (:optionals opts)
                                                    :optional-label (t :default/optional)
                                                    :validate-rules (:validate-rules opts)
                                                    :status-labels {:viewing (t :default/viewing)
                                                                    :adding (t :default/adding)
                                                                    :editing (t :default/editing)}
                                                    :buttons-labels {:save (t :default/save)
                                                                     :discard (t :default/discard)
                                                                     :void (t :default/void)
                                                                     :back (t :default/back)
                                                                     :print (t :default/print)
                                                                     :download (t :default/download)}
                                                    :download-url (str (:download-url opts) "/pdf/" (get-in state [:selected :id]))
                                                    :insert-chan (:insert-chan state)
                                                    :void-chan (:void-chan state)
                                                    :on-blur-chan (:on-blur-chan state)
                                                    :on-change-chan (:on-change-chan state)
                                                    :back-chan (:back-chan state)
                                                    :on-insert-chan (:on-insert-chan state)
                                                    :on-void-chan (:on-void-chan state)
                                                    :on-print-chan (:on-print-chan state)
                                                    :form-inline {:new-record (into {} (get-in state [:selected :selected]))
                                                                  :new-keys (:form-inline-new-keys opts)
                                                                  :display (:form-inline-display opts)
                                                                  :size-add-btn (:form-inline-add-btn opts)
                                                                  :types (assoc
                                                                             (:form-inline-types opts)
                                                                           :sku (let [loading {:loading-chan (get-in opts [:loading :loading-chan])
                                                                                               :topic-key :fetch
                                                                                               :requests (atom 0)
                                                                                               :waiting (atom false)
                                                                                               :wait-ms 100}]
                                                                                  {:type :search
                                                                                   :size (get-in (:form-inline-types opts) [:sku :size])
                                                                                   :cols (get-in (:form-inline-types opts) [:sku :cols])
                                                                                   :loading-pub-chan (get-in opts [:loading :loading-pub-chan])
                                                                                   :suggestions-fn (let [handler (->> (wait (assoc loading :action :stop))
                                                                                                                      (handle-response cursor)
                                                                                                                      ((:get-items opts))
                                                                                                                      (wait (assoc loading :action :start)))]
                                                                                                     (fn [value suggestions-chan _]
                                                                                                       (if-not (> (count value) 1)
                                                                                                         (put! suggestions-chan [])
                                                                                                         (go (put! suggestions-chan (<! (handler value)))))))
                                                                                   :text-fn (fn [item _] (str (:description item)))
                                                                                   :result-fn (fn [owner]
                                                                                                (fn [result]
                                                                                                  (om/set-state! owner [:record :sku] (:sku (val result)))))
                                                                                   :fetch-chan (let [handler (->> (wait (assoc loading :action :stop))
                                                                                                                  (handle-response cursor)
                                                                                                                  ((:get-item opts))
                                                                                                                  (wait (assoc loading :action :start)))
                                                                                                     amount-key (:amount-key opts)]
                                                                                                 (fn [owner]
                                                                                                   (let [c (chan)]
                                                                                                     (go (while true
                                                                                                           (let [value (<! c)
                                                                                                                 stock-enable-chan (or (:stock-enable-chan opts) (chan))]
                                                                                                             (if-not (empty? value)
                                                                                                               (do (let [resp (<! (handler value))]
                                                                                                                     (if (coll? resp)
                                                                                                                       (do (om/set-state! owner [:record :description] (:description resp))
                                                                                                                           (om/set-state! owner [:record amount-key] (str ((:amount-key-resp opts) resp)))
                                                                                                                           (om/set-state! owner [:record :quantity] "1")
                                                                                                                           (put! stock-enable-chan true))
                                                                                                                       (do (om/set-state! owner [:record :description] (t :default/not-available-abbr))
                                                                                                                           (om/set-state! owner [:record amount-key] "")
                                                                                                                           (om/set-state! owner [:record :quantity] "")
                                                                                                                           (put! stock-enable-chan false))))
                                                                                                                   (>! c true))
                                                                                                               (put! stock-enable-chan false)))))
                                                                                                     c)))}))
                                                                  :focus (:form-inline-focus opts)
                                                                  :focus-global-error :details-count
                                                                  :hint-keys (:form-inline-hint-keys opts)
                                                                  :labels (:details-labels opts)
                                                                  :buttons-labels {:add (t :default/add)}
                                                                  :add-chan (:insert-detail-chan state)}}})))))
               (css-trans-group #js {:transitionName "fade"
                                     :transitionLeave false}
                                (if (and (= (:status state) :idle) (not (nil? (get-in state [:pagination :total-records]))))
                                  (om/build table-view nil {:state {:pagination (:pagination state)
                                                                    :records (:records state)}
                                                            :opts {:labels (:labels opts)
                                                                   :display (:table-display opts)
                                                                   :types (:types opts)
                                                                   :id-key (:id-key opts)
                                                                   :show-image (:show-image opts)
                                                                   :image-available-key (:image-available-key opts)
                                                                   :image-src (get-in opts [:image-src :thumbnail])
                                                                   :image-not-available (:image-not-available opts)
                                                                   :header-key (:table-header-key opts)
                                                                   :header-legend (:table-header-legend opts)
                                                                   :header-legend-keys (:table-header-legend-keys opts)
                                                                   :pager pager
                                                                   :pages-summary (t :default/pages-summary)
                                                                   :no-records (t :default/no-records)
                                                                   :page-sizes [5 10 20 50]
                                                                   :pager-size (get-in opts [:pagination :pager-size])
                                                                   :per-page-label (t :default/per-page-label)
                                                                   :selected-chan (:get-chan state)
                                                                   :change-page-chan (:get-page-chan state)}})
                                  (if (or (= (:status state) :adding) (= (:status state) :viewing))
                                    (om/build table nil {:state {:pagination (get-in state [:selected :pagination])
                                                                 :records (get-in state [:selected :records])
                                                                 :command-columns (if (= (:status state) :adding)
                                                                                    {:delete {:cmd-chan (:delete-detail-chan state)
                                                                                              :title (t :default/delete)
                                                                                              :icon "glyphicon glyphicon-remove"}})}
                                                         :opts {:id-key :id
                                                                :display (:details-display opts)
                                                                :types (:types opts)
                                                                :headers (:details-labels opts)
                                                                :no-records (t :default/no-records)
                                                                :pager pager
                                                                :pages-summary (t :default/pages-summary)
                                                                :page-sizes [5 10 20 50]
                                                                :pager-size 5
                                                                :per-page-label (t :default/per-page-label)
                                                                :change-page-chan (if (= (:status state) :adding)
                                                                                    (:get-details-page-chan state)
                                                                                    (:get-record-details-page-chan state))}})
                                    (dom/div #js {:key "empty"}))))
               (if-not (= (:status state) :idle)
                 (dom/div nil
                          (dom/div #js {:className "row hidden-xs"}
                                   (dom/div #js {:className "col-md-offset-2 col-md-3"
                                                 :style #js {:margin-bottom "1em"}}
                                            (dom/h4 nil (str (t :default/quantity) ": " (get-in state [:selected :quantity]))))
                                   (dom/div #js {:className "col-md-offset-3 col-md-3"
                                                 :style #js {:margin-bottom "1em"}}
                                            (dom/h4 nil (str (t :default/total) ": " (get-in state [:selected :total])))))
                          (dom/div #js {:className "row visible-xs"}
                                   (dom/div #js {:className "col-xs-12 text-center"}
                                            (dom/h5 nil (str (t :default/quantity) ": " (get-in state [:selected :quantity]))))
                                   (dom/div #js {:className "col-xs-12 text-center"
                                                 :style #js {:margin-bottom "1em"}}
                                            (dom/h5 nil (str (t :default/total) ": " (get-in state [:selected :total])))))))))))
