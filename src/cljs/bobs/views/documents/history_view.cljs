(ns bobs.views.documents.history-view
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [put! chan <!]]
            [bobs.views.documents.history-form-view :refer [form]]
            [bobs.utils :refer [css-trans-group error?]]
            [bobs.widgets.tables :refer [table-view table]]
            [bobs.widgets.modals :refer [confirm-modal image-modal]]
            [bobs.widgets.pager :refer [pager]]
            [bobs.widgets.input-controls :refer [text-listen]]
            [bobs.i18n :refer [t]]
            [bobs.views.utils :refer [handle-response wait]]
            [goog.string :as gstring]
            [goog.string.format]))

(defn handler [f & [handler]]
  (fn [data]
    (go (let [resp (if (error? data)
                     data
                     (<! (f data)))]
          (if-not handler
            resp
            (<! (handler resp)))))))

(defn default-handler [& [handler]]
  (fn [data]
    (go (if-not handler
          data
          (<! (handler data))))))

(defn get-page [request]
  (partial handler (fn [data]
                     (go (<! (request {:method :get
                                       :path ["page"]
                                       :query data}))))))

(defn get-record [request pagination]
  (partial handler (fn [id]
                     (go (<! (request {:method :get
                                       :path [id "history"]
                                       :query {:page (:page pagination)
                                               :page-size (:page-size pagination)
                                               :include-header 1}}))))))

(defn get-record-details-page [request]
  (partial handler (fn [data]
                     (go (<! (request {:method :get
                                       :path [(:id data) "history"]
                                       :query (:query data)}))))))

(defn update-records-pagination [resp owner]
  (om/set-state! owner :records (:records resp))
  (om/set-state! owner [:pagination :page] (:page resp))
  (om/set-state! owner [:pagination :page-size] (:page-size resp))
  (om/set-state! owner [:pagination :total-records] (:total-records resp))
  (om/set-state! owner [:pagination :total-pages] (:total-pages resp))
  (om/set-state! owner [:pagination :order] (:order resp))
  (om/set-state! owner [:pagination :sort] (:sort resp))
  (om/set-state! owner [:pagination :search] (:search resp)))

(defn listen-search [owner]
  (let [c (om/get-state owner :search-chan)
        get-page-chan (om/get-state owner :get-page-chan)]
    (go (while true
          (let [value (<! c)]
            (if-not (= value "")
              (put! get-page-chan {:page 1 :search value})
              (put! get-page-chan {:page 1 :search nil})))))))

(defn listen-get-page [owner handler]
  (let [c (om/get-state owner :get-page-chan)]
    (go (while true
          (let [data (<! c)
                pagination {:page-size (om/get-state owner [:pagination :page-size])
                            :order (om/get-state owner [:pagination :order])
                            :sort (om/get-state owner [:pagination :sort])
                            :search (om/get-state owner [:pagination :search])}
                data (if (coll? data)
                       (merge pagination data)
                       (assoc pagination :page data))]
            (let [resp (<! (handler data))]
              (if-not (error? resp)
                (update-records-pagination resp owner))))))))

(defn listen-from-date [owner]
  (let [c (om/get-state owner :details-from-date-chan)
        get-page-chan (om/get-state owner :get-record-details-page-chan)]
    (go (while true
          (let [value (<! c)]
            (if-not (= value "")
              (put! get-page-chan {:page 1 :from-date value})
              (put! get-page-chan {:page 1 :from-date nil})))))))

(defn listen-to-date [owner]
  (let [c (om/get-state owner :details-to-date-chan)
        get-page-chan (om/get-state owner :get-record-details-page-chan)]
    (go (while true
          (let [value (<! c)]
            (if-not (= value "")
              (put! get-page-chan {:page 1 :to-date value})
              (put! get-page-chan {:page 1 :to-date nil})))))))

(defn listen-warehouse [owner]
  (let [c (om/get-state owner :details-warehouse-chan)
        get-page-chan (om/get-state owner :get-record-details-page-chan)]
    (go (while true
          (let [value (<! c)]
            (if-not (= value "")
              (put! get-page-chan {:page 1 :warehouse-id value})
              (put! get-page-chan {:page 1 :warehouse-id nil})))))))

(defn listen-get [owner handler]
  (let [c (om/get-state owner :get-chan)]
    (go (while true
          (let [id (<! c)
                resp (<! (handler id))]
            (if-not (error? resp)
              (let [index (first (keep-indexed (fn [i v] (if (= id (:id v)) i)) (om/get-state owner :records)))]
                (om/set-state! owner :selected (or (:selected resp) resp))
                (om/set-state! owner [:records index] (or (:records resp) resp))
                (om/set-state! owner :status :viewing))))))))

(defn listen-get-record-details-page [owner handler]
  (let [c (om/get-state owner :get-record-details-page-chan)]
    (go (while true
          (let [data (<! c)
                pagination {:page-size (om/get-state owner [:selected :pagination :page-size])
                            :from-date (om/get-state owner [:selected :pagination :from-date])
                            :to-date (om/get-state owner [:selected :pagination :to-date])
                            :warehouse-id (om/get-state owner [:selected :pagination :warehouse-id])}
                data (if (coll? data)
                       (merge pagination data)
                       (assoc pagination :page data))]
            (let [resp (<! (handler {:id (om/get-state owner [:selected :id])
                                     :query data}))]
              (when-not (error? resp)
                (om/set-state! owner [:selected :pagination] {:page (:page resp)
                                                              :page-size (:page-size resp)
                                                              :total-pages (:total-pages resp)
                                                              :total-records (:total-records resp)
                                                              :from-date (:from-date resp)
                                                              :to-date (:to-date resp)
                                                              :warehouse-id (:warehouse-id resp)})
                (om/set-state! owner [:selected :records] (:records resp)))))))))

(defn listen-back-form [owner]
  (let [c (om/get-state owner :back-chan)]
    (go (while true
          (<! c)
          (om/set-state! owner :selected nil)
          (om/set-state! owner :status :idle)
          (om/set-state! owner :err-msgs [])))))

(defn listen-on-print [owner modal-chan handler]
  (let [c-form (om/get-state owner :on-print-chan)]
    (go (while true
          (let [id (<! c-form)
                resp (<! (handler {:id id
                                   :from-date (om/get-state owner [:selected :pagination :from-date])
                                   :to-date (om/get-state owner [:selected :pagination :to-date])}))]
            (if-not (error? resp)
              (>! modal-chan {:modal :modal
                              :content resp
                              :show true})))))))

(defn history-view [cursor owner {:keys [send-request] :as opts}]
  (reify
    om/IInitState
    (init-state [_]
      {:pagination {:page nil
                    :page-size nil
                    :order nil
                    :sort nil
                    :search nil
                    :total-pages nil
                    :total-records nil}
       :records []
       :selected nil
       :status :idle
       :search-chan (chan)
       :get-page-chan (chan)
       :get-chan (chan)
       :get-record-details-page-chan (chan)
       :details-from-date-chan (chan)
       :details-to-date-chan (chan)
       :details-warehouse-chan (chan)
       :back-chan (chan)
       :on-print-chan (chan)})
    
    om/IWillMount
    (will-mount [_]
      (let [loading (:loading opts)
            page-handler (or (:get-page opts) default-handler)
            record-handler (or (:get-record opts) default-handler)
            page-details-handler (or (:get-details-page opts) default-handler)
            print-handler (or (:get-print opts) default-handler)
            start-waiting (partial wait (assoc loading :action :start))
            stop-waiting (partial wait (assoc loading :action :stop))
            handle-response (partial handle-response cursor)
            get-page (get-page send-request)
            get-record (get-record send-request (:pagination-details opts))
            get-record-details-page (get-record-details-page send-request)]
        (listen-search owner)
        (listen-get-page owner (->> (stop-waiting)
                                    (handle-response)
                                    (page-handler)
                                    (get-page)
                                    (start-waiting)))
        (listen-get owner (->> (stop-waiting)
                               (handle-response)
                               (record-handler)
                               (get-record)
                               (start-waiting)))
        (listen-get-record-details-page owner (->> (stop-waiting)
                                                   (handle-response)
                                                   (page-details-handler)
                                                   (get-record-details-page)
                                                   (start-waiting)))
        (listen-back-form owner)
        (listen-from-date owner)
        (listen-to-date owner)
        (listen-warehouse owner)
        (listen-on-print owner (:modal-print-chan opts) (->> (stop-waiting)
                                                             (handle-response)
                                                             (print-handler)
                                                             (start-waiting))))
      (let [pagination (:pagination opts)]
        (put! (om/get-state owner :get-page-chan) {:page (:page pagination)
                                                   :page-size (:page-size pagination)
                                                   :order (:order pagination)
                                                   :sort (:sort pagination)})))

    om/IDidUpdate
    (did-update [_ _ prev]
      (if-not (= (om/get-state owner :status) (:status prev))
        (om/update! (:err-msgs cursor) [])))

    om/IRenderState
    (render-state  [_ state]
      (dom/div #js {:className "panel panel-default"}
               (dom/div #js {:className "panel-heading hidden-xs"}
                        (dom/h3 #js {:className "panel-title"} (:heading opts)))
               (dom/h4 #js {:className "text-center visible-xs"} (:heading opts))
               (dom/div #js {:className "panel-body"}
                        (css-trans-group #js {:transitionName "fade"
                                              :transitionLeave false}
                                         (if (= (:status state) :idle)
                                           (dom/div #js {:key "search"
                                                         :className "row"}
                                                    (dom/div #js {:className "col-md-5 col-xs-12"}
                                                             (om/build text-listen nil {:init-state {:value (get-in state [:pagination :search])
                                                                                                     :old-value (get-in state [:pagination :search])}
                                                                                        :opts {:changed-chan (:search-chan state)
                                                                                               :placeholder (t :default/search)}}))
                                                    (dom/div #js {:className "col-md-offset-5 col-md-2 col-xs-12"}
                                                             (dom/div #js {:className "btn-group pull-right hidden-xs"}
                                                                      (dom/button #js {:className "btn btn-default glyphicon glyphicon-refresh"
                                                                                       :title (t :default/refresh)
                                                                                       :onClick #(put! (om/get-state owner :get-page-chan) (get-in state [:pagination :page]))}))
                                                             (dom/button #js {:className "btn btn-default btn-block glyphicon glyphicon-refresh visible-xs"
                                                                              :title (t :default/refresh)
                                                                              :onClick #(put! (om/get-state owner :get-page-chan) (get-in state [:pagination :page]))})))
                                           (om/build form nil {:init-state {:record (into {} (:selected state))}
                                                               :opts {:buttons-labels {:back (t :default/back)
                                                                                       :print (t :default/print)}
                                                                      :back-chan (:back-chan state)
                                                                      :from-date-chan (:details-from-date-chan state)
                                                                      :to-date-chan (:details-to-date-chan state)
                                                                      :warehouse-chan (:details-warehouse-chan state)
                                                                      :on-print-chan (:on-print-chan state)}}))))
               (css-trans-group #js {:transitionName "fade"
                                     :transitionLeave false}
                                (if (and (= (:status state) :idle) (not (nil? (get-in state [:pagination :total-records]))))
                                  (om/build table-view nil {:state {:pagination (:pagination state)
                                                                    :records (:records state)}
                                                            :opts {:labels (:labels opts)
                                                                   :display (:table-display opts)
                                                                   :types (:types opts)
                                                                   :id-key (:id-key opts)
                                                                   :show-image (:show-image opts)
                                                                   :image-key :image
                                                                   :image-src (get-in opts [:image-src :thumbnail])
                                                                   :image-not-available-msg (:image-not-available-msg opts)
                                                                   :header-key (:table-header-key opts)
                                                                   :pager pager
                                                                   :pages-summary (t :default/pages-summary)
                                                                   :no-records (t :default/no-records)
                                                                   :page-sizes [5 10 20 50]
                                                                   :pager-size (get-in opts [:pagination :pager-size])
                                                                   :per-page-label (t :default/per-page-label)
                                                                   :selected-chan (:get-chan state)
                                                                   :change-page-chan (:get-page-chan state)}})
                                  (if (= (:status state) :viewing)
                                    (om/build table nil {:state {:pagination (get-in state [:selected :pagination])
                                                                 :records (get-in state [:selected :records])
                                                                 :command-columns (if (= (:status state) :adding)
                                                                                    {:delete {:cmd-chan (:delete-detail-chan state)
                                                                                              :title (t :default/delete)
                                                                                              :icon "glyphicon glyphicon-remove"}})}
                                                         :opts {:id-key :id
                                                                :display (:details-display opts)
                                                                :display-alt (:details-display-alt opts)
                                                                :display-row-alt (:details-row-alt opts)
                                                                :types (:types opts)
                                                                :headers (:details-labels opts)
                                                                :headers-alt (:details-labels-alt opts)
                                                                :no-records (t :default/no-records)
                                                                :pager pager
                                                                :pages-summary (t :default/pages-summary)
                                                                :page-sizes [5 10 20 50]
                                                                :pager-size 5
                                                                :per-page-label (t :default/per-page-label)
                                                                :change-page-chan (:get-record-details-page-chan state)}})
                                    (dom/div #js {:key "empty"}))))))))
