(ns bobs.views.documents.documents
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [chan put!]]
            [bobs.utils :as utils :refer [error? decimal integer text]]
            [bobs.i18n :refer [t date-fmt datetime-fmt]]
            [bobs.data.request :refer [request]]
            [bobs.validators.validator :as validator]
            [bobs.views.documents.form-view :refer [handler]]
            [clojure.string :as str]))

(def ^:private amount-keys
  {:entry :cost
   :withdraw :price})

(def pagination {:page 1
                 :page-size 10
                 :order "id"
                 :sort 1
                 :pager-size 5})

(def pagination-details {:page 1
                         :page-size 5
                         :sort 0})

(def labels {:number (t :default/number-abbr)
             :date (t :default/date)
             :comment (t :default/comment)
             :void (t :default/voided)})

(def types {:void {:type :checkbox}
            :comment {:type :textarea
                      :max-length text
                      :rows 3}
            :image {:type :image}})

(def get-items
  (partial handler (fn [value]
                     (go (<! ((request "/items") {:method :get
                                                  :query {:search value
                                                          :exclude-sku 1}}))))))

(def get-item
  (partial handler (fn [value]
                     (go (<! ((request "/items/sku") {:method :get
                                                      :path [value]}))))))

(def get-warehouses
  (partial handler (fn [response]
                     (go (let [resp (<! ((request "/warehouses") {:method :get
                                                                  :query {:order "name"}}))]
                           (if (error? resp)
                             resp
                             (assoc response :warehouses resp)))))))

(def format-document
  (partial handler (fn [response]
                     (go {:selected (assoc (:header response)
                                      :pagination {:page (:page response)
                                                   :page-size (:page-size response)
                                                   :total-pages (:total-pages response)
                                                   :total-records (:total-records response)
                                                   :sort (:sort response)}
                                      :records (:records response))
                          :records (:header response)}))))

(defn build-image-url [record]
  (str "/items/" (:item-id record) "/image?thumbnail=1&name=" (:image record)))

(def format-date (partial utils/format-datetime date-fmt))

(def format-datetime (partial utils/format-datetime datetime-fmt))

(defn format-currency [currency-symbol amount]
  (str currency-symbol " " (utils/format-currency amount)))

(defn format-quantity [quantity uom-abbreviation]
  (str (utils/format-quantity quantity) " " uom-abbreviation))

(defn format-image [record]
  (if (:image record) {:src (build-image-url record)} {:text (t :default/not-available-abbr)}))

(defn empty-record [amount-key]
  {:comment ""
   :selected {:sku ""
              :description ""
              amount-key ""
              :quantity ""}})

(def empty-record-entry (empty-record :cost))

(def empty-record-withdraw (assoc-in (empty-record :price)
                                     [:selected :stock] ""))

(defn get-draft [send-request-details]
  (partial handler (fn [response]
                     (go (let [resp (<! (send-request-details {:method :post
                                                               :query {:respond-with-details-pagination 1
                                                                       :page (:page pagination-details)
                                                                       :page-size (:page-size pagination-details)
                                                                       :sort (:sort pagination-details)}}))]
                           (if (error? resp)
                             resp
                             (assoc response
                               :draft-id (:id resp)
                               :quantity (:quantity resp)
                               :total (format-currency (:currency-symbol resp) (:total resp))
                               :pagination {:page (:page resp)
                                            :page-size (:page-size resp)
                                            :total-pages (:total-pages resp)
                                            :total-records (:total-records resp)}
                               :records (or (:records resp) []))))))))

(defn format-details-page [amount-key]
  (partial handler (fn [response]
                     (go (let [currency-symbol (:currency-symbol response)]
                           (assoc response
                             :records (map
                                       #(assoc %
                                          amount-key (format-currency currency-symbol (amount-key %))
                                          :quantity (format-quantity (:quantity %) (:uom-abbreviation %))
                                          :total (format-currency currency-symbol (:total %))
                                          :image (format-image %))
                                       (:records response))
                             :quantity (:quantity response)
                             :total (format-currency currency-symbol (:total response))))))))

(def get-stock
  (partial handler (fn [sku]
                     (go (let [resp (<! ((request "/items") {:method :get
                                                             :path [sku "stock"]}))]
                           (if (error? resp)
                             resp
                             (conj (vec (map #(assoc % :total nil) (:records resp)))
                                   {:warehouse-name ""
                                    :total (t :default/total)
                                    :quantity (:total resp)
                                    :select :none})))))))

(defn validate [rules]
  (fn [record]
    ((validator/validate rules) (assoc record :details-count (str (count (:records record)))))))

(defn validate-detail [rules]
  (validator/validate rules))

(defn details-labels [amount-key]
  {:item-description (t :default/description)
   :description (t :default/description)
   :warehouse-name (t :default/warehouse)
   amount-key (t (keyword (str "default/" (name amount-key))))
   :sku (t :default/sku)
   :quantity (t :default/quantity)
   :total (t :default/subtotal)
   :image (t :default/image)})

(defn form-inline-types [amount-key]
  {:sku {:size :small
         :cols 3}
   :description {:type :text
                 :size :small
                 :readonly true
                 :cols 4}
   amount-key {:type :text
               :size :small
               :max-length decimal
               :cols 2}
   :quantity {:type :text
              :size :small
              :max-length integer
              :cols 2}})

(def form-inline-types-entry (form-inline-types :cost))

(defn stock-control [_ owner {:keys [enable-chan] :as opts}]
  (reify
    om/IInitState
    (init-state [_]
      {:enabled false})

    om/IWillMount
    (will-mount [_]
      (go (while true
            (om/set-state! owner :enabled (<! enable-chan))))
      ((:listen-warehouse-selection opts)))
    
    om/IRenderState
    (render-state [_ state]
      (dom/div #js {:className (str/join " " ["form-group" (str "col-md-" (:cols opts))])
                    :style #js {:padding "0"}}
               (dom/button #js {:className "btn btn-default btn-sm"
                                :disabled (not (:enabled state))
                                :title "Stock"
                                :style #js {:width "80%"}
                                :onClick #((:on-click opts))}
                           (dom/span #js {:className "glyphicon glyphicon-stats"}))))))

(defn form-inline-types-withdraw [stock-enable-chan stock-show-chan warehouse-selected-chan]
  (let [types (-> (form-inline-types :price)
                  (assoc-in [:sku :cols] 2)
                  (assoc-in [:description :cols] 2)
                  (assoc-in [:price :cols] 2)
                  (assoc-in [:quantity :cols] 2))]
    (assoc types
      :warehouses {:type :select
                   :size :small
                   :cols 2
                   :value-key :id
                   :text-key :name
                   :selected-key :warehouse-id
                   :error-key-aliases [:warehouse-id]
                   :empty-text (str "-- " (t :default/warehouse) " --")
                   :warehouse-selected-chan warehouse-selected-chan}
      :stock {:type :custom
              :control stock-control
              :readonly true
              :more-opts (fn [owner]
                           {:cols 1
                            :enable-chan stock-enable-chan
                            :on-click (fn [_]
                                        (put! stock-show-chan (om/get-state owner [:record :sku])))
                            :listen-warehouse-selection (fn [_]
                                                          (go (while true
                                                                (om/set-state! owner [:record :warehouse-id] (str (<! warehouse-selected-chan))))))})})))

(defn persist-keys [amount-key]
  [:sku :quantity amount-key])

(def table-display [:void])

(defn form-opts [doc-type & {:keys [amount-key-resp]}]
  (let [stock-enable-chan (if (= doc-type :withdraw) (chan))
        stock-show-chan (if (= doc-type :withdraw) (chan))
        warehouse-selected-chan (if (= doc-type :withdraw) (chan))
        amount-key-resp (or amount-key-resp (doc-type amount-keys))]
    {:id-key :id
     :table-header-key :id
     :table-header-legend (t :default/document-header-legend)
     :table-header-legend-keys [:number :date]
     :amount-key (doc-type amount-keys)
     :amount-key-resp amount-key-resp
     :stock-enable-chan stock-enable-chan
     :stock-show-chan stock-show-chan
     :warehouse-selected-chan warehouse-selected-chan
     :form-inline-add-btn :small
     :form-inline-types (if (= doc-type :entry) form-inline-types-entry (form-inline-types-withdraw stock-enable-chan
                                                                                                    stock-show-chan
                                                                                                    warehouse-selected-chan))
     :form-inline-focus :sku
     :form-inline-display (if (= doc-type :entry)
                            [:sku :description :quantity :cost]
                            [:sku :description :warehouses :stock :quantity :price])
     :form-inline-hint-keys (if (= doc-type :entry)
                              [:details-count :sku :quantity :cost]
                              [:details-count :sku :warehouse-id :quantity :price])
     :form-inline-persist-keys (if (= doc-type :entry)
                                 (persist-keys :cost)
                                 (conj (persist-keys :price) :warehouse-id))
     :form-inline-new-keys (if (= doc-type :entry)
                             [:sku :description :quantity :cost]
                             [:sku :description :quantity :price])
     :details-labels (details-labels (doc-type amount-keys))
     :details-display (if (= doc-type :entry)
                        [:item-description :quantity :cost :total :image]
                        [:item-description :warehouse-name :quantity :price :total :image])
     :pagination pagination
     :pagination-details pagination-details
     :get-details-page (format-details-page (doc-type amount-keys))
     :get-items get-items
     :get-item get-item
     :get-stock (if (= doc-type :withdraw) get-stock)}))
