(ns bobs.views.documents.stock
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [put!]]
            [bobs.i18n :refer [t date-fmt]]
            [bobs.views.menus-view :refer [menus-view]]
            [bobs.data.request :refer [request]]
            [bobs.utils :as utils :refer [error? string decimal]]
            [bobs.views.documents.stock-view :refer [stock-view handler]]
            [bobs.widgets.input-controls :refer [listen-focus]]
            [clojure.string :as str]))

(def send-request (request "/items"))

(defn format-brand-name [brand-name]
  (if (empty? brand-name) (t :default/not-available-abbr) brand-name))

(defn format-currency [currency-symbol amount]
  (str currency-symbol " " (utils/format-currency amount)))

(defn format-quantity [quantity uom-abbreviation]
  (str (utils/format-quantity quantity) " " uom-abbreviation))

(def format-page
  (partial handler (fn [response]
                     (go (assoc response
                           :records (reduce
                                     #(conj %1 (assoc %2
                                                 :price (format-currency (:currency-symbol %2) (:price %2))
                                                 :brand-name (format-brand-name (:brand-name %2))
                                                 :quantity (format-quantity (:quantity %2) (:uom-abbreviation %2))))
                                     []
                                     (:records response)))))))

(def format-item
  (partial handler (fn [response]
                     (go (let [selected (:header response)]
                           {:selected (assoc selected :records (:records response) :total (:total response))
                            :records (assoc selected
                                       :price (format-currency (:currency-symbol selected) (:price selected))
                                       :brand-name (format-brand-name (get-in response [:selected :brand-name]))
                                       :quantity (format-quantity (:quantity selected) (:uom-abbreviation selected)))})))))

(def labels {:description (t :default/description)
             :sku (t :default/sku)
             :brand-name (t :default/brand)
             :price (t :default/price)
             :quantity (t :default/quantity)})

(def details-labels {:warehouse-name (t :default/warehouse)
                     :quantity (t :default/quantity)})

(def pagination {:page 1
                 :page-size 10
                 :order "description"
                 :sort 0
                 :pager-size 5})

(def image-src {:image "/items/%d/image"
                :thumbnail "/items/%d/image?thumbnail=1"})

(defn render-root [cursor target]
  (om/root menus-view cursor {:target target
                              :opts {:main-active-route :inventory
                                     :second-active-route :stock
                                     :heading (t :default/inventory)
                                     :form-view stock-view
                                     :form-opts {:heading (t :default/stock)
                                                 :send-request send-request
                                                 :labels labels
                                                 :form-display [:description :sku]
                                                 :table-display [:sku :price :brand-name :quantity]
                                                 :details-labels details-labels
                                                 :details-display [:warehouse-name :quantity]
                                                 :id-key :sku
                                                 :table-header-key :description
                                                 :show-image true
                                                 :image-src image-src
                                                 :image-not-available-msg (t :default/image-not-available)
                                                 :pagination pagination
                                                 :get-page format-page
                                                 :get-record format-item}}}))
