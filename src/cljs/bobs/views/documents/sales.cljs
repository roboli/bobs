(ns bobs.views.documents.sales
  (:require [om.core :as om :include-macros true]
            [bobs.i18n :refer [t]]
            [bobs.views.menus-view :refer [menus-view]]))

(defn render-root [cursor target]
  (om/root menus-view cursor {:target target
                              :opts {:main-active-route :sales
                                     :heading (t :default/sales)}}))
