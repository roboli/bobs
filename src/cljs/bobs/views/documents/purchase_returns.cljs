(ns bobs.views.documents.purchase-returns
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [bobs.views.documents.documents :as docs]
            [bobs.utils :as utils :refer [error?]]
            [bobs.i18n :refer [t]]
            [bobs.data.request :refer [request]]
            [bobs.views.menus-view :refer [menus-view]]
            [bobs.validators.purchase-returns :as shi-val]
            [bobs.validators.purchase-return-details :as shi-det-val]
            [bobs.views.documents.form-view :refer [form-view handler]]))

(def resource "purchase-returns")

(def send-request (request (str "/" resource)))

(def send-request-details (request (str "/" resource "/drafts")))

(def fmt-page
  (partial handler (fn [response]
                     (go (assoc response
                           :records (reduce
                                     #(conj %1 (assoc %2
                                                 :date (docs/format-date (:date %2)))) ; convert date value to string
                                     []
                                     (:records response))
                           :warehouses (:warehouses response))))))

(defn format-page [& [handler]]
  (docs/get-warehouses (fmt-page handler)))

(def format-values
  (partial handler (fn [response]
                     (go (let [selected (:selected response)
                               date (docs/format-date (:date selected))
                               datetime (docs/format-datetime (:date selected))
                               currency-symbol (:currency-symbol selected)]
                           {:selected (assoc selected
                                        :date datetime
                                        :records (map
                                                  #(assoc %
                                                     :price (docs/format-currency currency-symbol (:price %))
                                                     :quantity (docs/format-quantity (:quantity %) (:uom-abbreviation %))
                                                     :total (docs/format-currency currency-symbol (:total %))
                                                     :image (docs/format-image %))
                                                  (:records selected))
                                        :total (docs/format-currency currency-symbol (:total selected)))
                            :records (assoc (:records response) :date date)})))))

(defn get-record [& [handler]]
  (docs/format-document (format-values handler)))

(def get-empty-record
  (partial handler (fn [& [data]]
                     (go (assoc-in
                          (assoc docs/empty-record-withdraw :supplier-id "")
                          [:selected :warehouse-id] "")))))

(def get-suppliers
  (partial handler (fn [response]
                     (go (let [resp (<! ((request "/suppliers") {:method :get}))]
                           (if (error? resp)
                             resp
                             (assoc response :suppliers resp)))))))

(def get-warehouses
  (partial handler (fn [response]
                     (go (let [resp (<! ((request "/warehouses") {:method :get
                                                                  :query {:order "name"}}))]
                           (if (error? resp)
                             resp
                             (assoc-in response [:selected :warehouses] resp)))))))

(defn get-new-record [& [handler]]
  (-> handler
      (get-warehouses)
      (get-suppliers)
      ((docs/get-draft send-request-details))
      (get-empty-record)))

(def get-print
  (partial handler (fn [id]
                     (go (<! ((request "/purchase-returns/print" :edn-parse? false) {:method :get
                                                                                     :path [id]}))))))

(def get-print-summary
  (partial handler (fn [params]
                     (go (<! ((request "/purchase-returns/print/summary" :edn-parse? false) {:method :get
                                                                                             :query params}))))))

(def labels (assoc docs/labels
              :supplier-name (t :default/supplier)
              :suppliers (t :default/supplier)))

(def types (assoc docs/types
             :suppliers {:type :select
                         :value-key :id
                         :text-key :name
                         :selected-key :supplier-id
                         :error-key-aliases [:supplier-id]}))

(defn render-root [cursor target]
  (om/root menus-view cursor {:target target
                              :opts {:main-active-route :purchases
                                     :second-active-route :purchase-returns
                                     :heading (t :default/purchases)
                                     :form-view form-view
                                     :form-opts (assoc (docs/form-opts :withdraw :amount-key-resp :cost)
                                                  :heading (t :default/returns)
                                                  :send-request send-request
                                                  :send-request-details send-request-details
                                                  :download-url resource
                                                  :labels labels
                                                  :types types
                                                  :focus :suppliers
                                                  :form-display [:number :date :supplier-name :suppliers :comment]
                                                  :table-display (into docs/table-display [:supplier-name])
                                                  :optionals [:reference :comment]
                                                  :validate (docs/validate shi-val/rules)
                                                  :validate-rules shi-val/rules
                                                  :validate-detail (docs/validate-detail shi-det-val/rules)
                                                  :validate-detail-rules shi-det-val/rules
                                                  :persist-keys [:supplier-id :comment]
                                                  :get-page format-page
                                                  :get-record get-record
                                                  :get-new-record get-new-record
                                                  :get-print get-print
                                                  :get-print-summary get-print-summary)}}))
