(ns bobs.views.documents.history
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [put!]]
            [bobs.i18n :refer [t date-fmt]]
            [bobs.views.menus-view :refer [menus-view]]
            [bobs.data.request :refer [request]]
            [bobs.utils :as utils :refer [error? string decimal]]
            [bobs.views.documents.history-view :refer [history-view handler]]
            [bobs.widgets.input-controls :refer [listen-focus]]
            [clojure.string :as str]))

(def send-request (request "/items"))

(defn format-brand-name [brand-name]
  (if (empty? brand-name) (t :default/not-available-abbr) brand-name))

(defn format-currency [currency-symbol amount]
  (str currency-symbol " " (utils/format-currency amount)))

(defn format-quantity [quantity uom-abbreviation]
  (str (utils/format-quantity quantity) " " uom-abbreviation))

(def format-page
  (partial handler (fn [response]
                     (go (assoc response
                           :records (reduce
                                     #(conj %1 (assoc %2
                                                 :price (format-currency (:currency-symbol %2) (:price %2))
                                                 :brand-name (format-brand-name (:brand-name %2))
                                                 :quantity (format-quantity (:quantity %2) (:uom-abbreviation %2))))
                                     []
                                     (:records response)))))))

(defn format-details [m]
  (assoc
      (if (:document-type m)
        (assoc m
          :document-type (t (keyword (str "default/" (name (:document-type m)))))
          :date (utils/format-datetime date-fmt (:date m))
          :void (if (:void m) (t :default/voided) "")
          :void-alt (if (:void m) (t :default/yes) (t :default/no))
          :withdraw (if (:withdraw m) (utils/format-quantity (:withdraw m)))
          :entry (if (:entry m) (utils/format-quantity (:entry m))))
        (assoc m
          :number nil
          :document-type nil
          :date nil
          :void nil
          :withdraw nil
          :entry nil))
    :warehouse-name (:warehouse-name m)
    :balance (utils/format-quantity (:balance m))))

(def format-item
  (partial handler (fn [response]
                     (go (let [selected (:header response)]
                           {:selected (assoc selected
                                        :records (map format-details (:records response))
                                        :pagination {:page (:page response)
                                                     :page-size (:page-size response)
                                                     :total-pages (:total-pages response)
                                                     :total-records (:total-records response)
                                                     :to-date (:to-date response)
                                                     :from-date (:from-date response)
                                                     :warehouse-id (:warehouse-id response)})
                            :records (assoc selected
                                       :price (format-currency (:currency-symbol selected) (:price selected))
                                       :brand-name (format-brand-name (get-in response [:selected :brand-name]))
                                       :quantity (format-quantity (:quantity selected) (:uom-abbreviation selected)))})))))

(def get-warehouses
  (partial handler (fn [response]
                     (go (let [resp (<! ((request "/warehouses") {:method :get
                                                                  :query {:order "name"}}))]
                           (if (error? resp)
                             resp
                             (assoc-in response [:selected :warehouses] resp)))))))

(defn get-record [& [handler]]
  (format-item (get-warehouses handler)))

(def format-details-page
  (partial handler (fn [response]
                     (go (assoc response :records (map format-details (:records response)))))))

(def get-print
  (partial handler (fn [data]
                     (go (<! ((request "/items" :edn-parse? false) {:method :get
                                                                    :path [(:id data) "history" "print"]
                                                                    :query {:from-date (:from-date data)
                                                                            :to-date (:to-date data)}}))))))

(def labels {:description (t :default/description)
             :sku (t :default/sku)
             :brand-name (t :default/brand)
             :price (t :default/price)
             :quantity (t :default/quantity)})

(def details-labels {:number (t :default/number-abbr)
                     :document-type (t :default/document)
                     :date (t :default/date)
                     :void ""
                     :warehouse-name (t :default/warehouse)
                     :withdraw (t :default/withdraw)
                     :entry (t :default/entry)
                     :balance (t :default/balance)})

(def pagination {:page 1
                 :page-size 10
                 :order "description"
                 :sort 0
                 :pager-size 5})

(def pagination-details {:page 1
                         :page-size 10
                         :sort 0})

(def image-src {:image "/items/%d/image"
                :thumbnail "/items/%d/image?thumbnail=1"})

(defn render-root [cursor target]
  (om/root menus-view cursor {:target target
                              :opts {:main-active-route :inventory
                                     :second-active-route :history
                                     :heading (t :default/inventory)
                                     :form-view history-view
                                     :form-opts {:heading (t :default/history)
                                                 :send-request send-request
                                                 :labels labels
                                                 :form-display [:description :sku]
                                                 :table-display [:sku :price :brand-name :quantity]
                                                 :details-labels details-labels
                                                 :details-labels-alt (assoc details-labels :void (t :default/voided))
                                                 :details-display [:number :document-type :date :void :warehouse-name :withdraw :entry :balance]
                                                 :details-display-alt [:number :document-type :date :void-alt :warehouse-name :withdraw :entry :balance]
                                                 :details-row-alt {:index 0 :labels [(:balance details-labels)] :display [:balance]}
                                                 :id-key :id
                                                 :table-header-key :description
                                                 :show-image true
                                                 :image-src image-src
                                                 :image-not-available-msg (t :default/image-not-available)
                                                 :pagination pagination
                                                 :pagination-details pagination-details
                                                 :get-page format-page
                                                 :get-record get-record
                                                 :get-details-page format-details-page
                                                 :get-print get-print}}}))
