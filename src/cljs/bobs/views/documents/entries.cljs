(ns bobs.views.documents.entries
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [bobs.views.documents.documents :as docs]
            [bobs.utils :as utils :refer [error?]]
            [bobs.i18n :refer [t]]
            [bobs.data.request :refer [request]]
            [bobs.views.menus-view :refer [menus-view]]
            [bobs.validators.entries :as ent-val]
            [bobs.validators.entry-details :as ent-det-val]
            [bobs.views.documents.form-view :refer [form-view handler]]))

(def resource "entries")

(def send-request (request (str "/" resource)))

(def send-request-details (request (str "/" resource "/drafts")))

(def fmt-page
  (partial handler (fn [response]
                     (go (assoc response
                           :records (reduce
                                     #(conj %1 (assoc %2
                                                 :date (docs/format-date (:date %2)))) ; convert date value to string
                                     []
                                     (:records response))
                           :warehouses (:warehouses response))))))

(defn format-page [& [handler]]
  (docs/get-warehouses (fmt-page handler)))

(def format-values
  (partial handler (fn [response]
                     (go (let [selected (:selected response)
                               date (docs/format-date (:date selected))
                               datetime (docs/format-datetime (:date selected))
                               currency-symbol (:currency-symbol selected)]
                           {:selected (assoc selected
                                        :date datetime
                                        :records (map
                                                  #(assoc %
                                                     :cost (docs/format-currency currency-symbol (:cost %))
                                                     :quantity (docs/format-quantity (:quantity %) (:uom-abbreviation %))
                                                     :total (docs/format-currency currency-symbol (:total %))
                                                     :image (docs/format-image %))
                                                  (:records selected))
                                        :total (docs/format-currency currency-symbol (:total selected)))
                            :records (assoc (:records response) :date date)})))))

(defn get-record [& [handler]]
  (docs/format-document (format-values handler)))

(def get-empty-record
  (partial handler (fn [& [data]]
                     (go (assoc docs/empty-record-entry
                           :warehouse-id "")))))

(def get-warehouses
  (partial handler (fn [response]
                     (go (let [resp (<! ((request "/warehouses") {:method :get
                                                                  :query {:order "name"}}))]
                           (if (error? resp)
                             resp
                             (assoc response :warehouses resp)))))))

(defn get-new-record [& [handler]]
  (-> handler
      (get-warehouses)
      ((docs/get-draft send-request-details))
      (get-empty-record)))

(def get-print
  (partial handler (fn [id]
                     (go (<! ((request "/entries/print" :edn-parse? false) {:method :get
                                                                            :path [id]}))))))

(def get-print-summary
  (partial handler (fn [params]
                     (go (<! ((request "/entries/print/summary" :edn-parse? false) {:method :get
                                                                                    :query params}))))))

(def labels (assoc docs/labels
              :warehouse-name (t :default/warehouse)
              :warehouses (t :default/warehouse)))

(def types (assoc docs/types
             :warehouses {:type :select
                          :value-key :id
                          :text-key :name
                          :selected-key :warehouse-id
                          :error-key-aliases [:warehouse-id]}))

(defn render-root [cursor target]
  (om/root menus-view cursor {:target target
                              :opts {:main-active-route :inventory
                                     :second-active-route :entries
                                     :heading (t :default/inventory)
                                     :form-view form-view
                                     :form-opts (assoc (docs/form-opts :entry)
                                                  :heading (t :default/entries)
                                                  :send-request send-request
                                                  :send-request-details send-request-details
                                                  :download-url resource
                                                  :labels labels
                                                  :types types
                                                  :focus :warehouses
                                                  :form-display [:number :date :warehouse-name :warehouses :comment]
                                                  :table-display (into docs/table-display [:warehouse-name])
                                                  :optionals [:comment]
                                                  :validate (docs/validate ent-val/rules)
                                                  :validate-rules ent-val/rules
                                                  :validate-detail (docs/validate-detail ent-det-val/rules)
                                                  :validate-detail-rules ent-det-val/rules
                                                  :persist-keys [:warehouse-id :comment]
                                                  :get-page format-page
                                                  :get-record get-record
                                                  :get-new-record get-new-record
                                                  :get-print get-print
                                                  :get-print-summary get-print-summary)}}))
