(ns bobs.views.menus-view
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [clojure.string :as str]
            [cljs.core.async :refer [chan pub]]
            [bobs.data.config :refer [app-name]]
            [bobs.widgets.menus :refer [main-menu sub-menu]]
            [bobs.data.routes :refer [routes]]
            [bobs.i18n :refer [t]]
            [bobs.views.utils :refer [listen-logout]]
            [bobs.widgets.utils :refer [alert-danger]]
            [bobs.widgets.modals :refer [loading-modal print-modal]]))

(defn menus-view [cursor owner {:keys [form-view main-active-route second-active-route] :as opts}]
  (reify
    om/IInitState
    (init-state [_]
      (let [loading-chan (chan)
            modal-print-chan (chan)]
        {:printable? true
         :loading-chan loading-chan
         :loading-pub-chan (pub loading-chan :loading)
         :modal-print-chan modal-print-chan
         :modal-print-pub-chan (pub modal-print-chan :modal)}))

    om/IWillMount
    (will-mount [_]
      (om/update! cursor [:navigation :main-active-route] main-active-route)
      (om/update! cursor [:navigation :second-active-route] second-active-route))
    
    om/IRenderState
    (render-state [_ state]
      (let [loading {:loading-chan (:loading-chan state)
                     :loading-pub-chan (:loading-pub-chan state)
                     :topic-key :modal}]
        (dom/div nil
                 (om/build print-modal nil {:opts {:id "print-modal"
                                                   :title (t :default/print)
                                                   :buttons-labels {:close (t :default/close)}
                                                   :modal-chan (:modal-print-chan state)
                                                   :pub-chan (:modal-print-pub-chan state)
                                                   :finish-print #(om/set-state! owner :printable? %)}})
                 (dom/div #js {:className (if-not (:printable? state) "hidden-print")}
                          (om/build main-menu (get-in cursor [:navigation :main-active-route]) {:opts {:title app-name
                                                                                                       :routes routes
                                                                                                       :session (:session cursor)
                                                                                                       :logout-chan (listen-logout {:err-msgs (:err-msgs cursor)
                                                                                                                                    :session (:session cursor)} loading)}})
                          (dom/div #js {:className "container-fluid"}
                                   (dom/div #js {:className "row"}
                                            (om/build loading-modal nil {:opts {:id "loading-modal"
                                                                                :text (t :default/wait)
                                                                                :modal-chan (:loading-chan state)
                                                                                :pub-chan (:loading-pub-chan state)}})
                                            (dom/div #js {:className "col-sm-9 col-xs-12"}
                                                     (when-not (empty? (:err-msgs cursor))
                                                       (apply dom/div nil
                                                              (for [msg (:err-msgs cursor)] (om/build alert-danger msg))))
                                                     (if (and form-view (get-in cursor [:session :logged-in])) ;; verify session is active due to js/window route to login is slow
                                                       (om/build form-view {:err-msgs (:err-msgs cursor)
                                                                            :session (:session cursor)} {:opts (assoc (:form-opts opts)
                                                                                                                 :loading loading
                                                                                                                 :modal-print-chan (:modal-print-chan state))})))
                                            (dom/div #js {:className (str/join " " [(if form-view "hidden-xs")
                                                                                    "col-sm-3 col-xs-12"])}
                                                     (if-let [children (get-in routes [main-active-route :children])]
                                                       (om/build sub-menu (get-in cursor [:navigation :second-active-route]) {:opts {:routes children
                                                                                                                                     :heading (:heading opts)}})))))))))))
