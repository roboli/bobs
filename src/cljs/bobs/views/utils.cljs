(ns bobs.views.utils
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [put! timeout chan]]
            [bobs.utils :refer [error?]]
            [bobs.data.request :refer [request]]
            [bobs.data.routes :refer [routes]]
            [bobs.i18n :refer [t]]))

(def login-route (str "#" (get-in routes [:login :route])))

(def wait-ms 1000)

(def waiting (atom false))

(def requests (atom 0))

(defn wait [{:keys [loading-chan action topic-key requests waiting wait-ms]
             :or {requests requests
                  waiting waiting
                  wait-ms wait-ms}} & [handler]]
  (fn [& [data]]
    (go (condp = action
          :start (do (swap! requests inc)
                     (when-not @waiting
                       (reset! waiting true)
                       (go (<! (timeout wait-ms))
                           (if @waiting
                             (>! loading-chan {:loading topic-key
                                               :show true})))))
          :stop (do (swap! requests dec)
                    (when (= @requests 0)
                      (reset! waiting false)
                      (>! loading-chan {:loading topic-key
                                        :show false}))))
        (if-not handler
          data
          (<! (handler data))))))

(defn logout
  ([cursor] (logout cursor false))
  ([cursor is-error]
     (om/update! (:session cursor) :logged-in false)
     (om/update! (:session cursor) :username nil)
     (when is-error
       (om/update! (:err-msgs cursor) [(t :default/session-expired)])
       (om/update! (:session cursor) :before-session-expired-route (->> js/window
                                                                        .-location
                                                                        .-hash
                                                                        (remove #((set "#") %))
                                                                        (apply str))))
     (set! (-> js/window .-location .-hash) login-route)))

(defn handle-response [cursor & [handler]]
  (fn [response]
    (go (let [resp (if (error? response)
                     (condp = (get-in response [:error :status])
                       400 {:error (get-in response [:error :body])}
                       401 (do (logout cursor true)
                               {:error nil})
                       (do (om/update! (:err-msgs cursor) [(get-in response [:error :body])])
                           {:error true}))
                     (do (om/update! (:err-msgs cursor) [])
                         response))]
          (if-not handler
            resp
            (<! (handler resp)))))))

(defn delete-session [handler]
  (fn [_]
    (go (let [resp (<! ((request "/session") {:method :delete}))]
          (<! (handler resp))))))

(defn listen-logout [cursor loading]
  (let [c (chan)
        handler (->> (wait (assoc loading :action :stop))
                     (handle-response cursor)
                     (delete-session)
                     (wait (assoc loading :action :start)))]
    (go (while true
          (<! c)
          (let [resp (<! (handler))]
            (if-not (error? resp)
              (logout cursor)))))
    c))
