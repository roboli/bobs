(ns bobs.views.maintenance.form-view
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [take! put! chan <! pub sub]]
            [bobs.utils :refer [css-trans-group error?]]
            [bobs.widgets.tables :refer [table-view]]
            [bobs.widgets.form :refer [form]]
            [bobs.widgets.modals :refer [confirm-modal image-modal]]
            [bobs.widgets.pager :refer [pager]]
            [bobs.widgets.input-controls :refer [text-listen]]
            [bobs.i18n :refer [t]]
            [bobs.views.utils :refer [handle-response wait]]
            [goog.string :as gstring]
            [goog.string.format]))

(defn handler [f & [handler]]
  (fn [data]
    (go (let [resp (if (error? data)
                     data
                     (<! (f data)))]
          (if-not handler
            resp
            (<! (handler resp)))))))

(defn default-handler [& [handler]]
  (fn [data]
    (go (if-not handler
          data
          (<! (handler data))))))

(defn validate-handler [validate & [handler]]
  (fn [data]
    (go (let [errors (validate (:validate data))
              resp (if-not (empty? errors)
                     {:error errors}
                     (dissoc data :validate))]
          (if (error? resp)
            resp
            (if-not handler
              resp
              (<! (handler resp))))))))

(defn get-page [request]
  (partial handler (fn [data]
                     (go (<! (request {:method :get
                                       :path ["page"]
                                       :query data}))))))

(defn get-page-id [request owner]
  (partial handler (fn [id]
                     (go (<! (request {:method :get
                                       :path ["page" id]
                                       :query {:page-size (om/get-state owner [:pagination :page-size])
                                               :order (om/get-state owner [:pagination :order])
                                               :sort (om/get-state owner [:pagination :sort])}}))))))

(defn get-record [request]
  (partial handler (fn [id]
                     (go (<! (request {:method :get
                                       :path [id]}))))))

(defn post [request persist-keys]
  (partial handler (fn [data]
                     (go (<! (request {:method :post
                                       :body (select-keys (:body data) persist-keys)}))))))

(defn put [request persist-keys]
  (partial handler (fn [data]
                     (go (<! (request {:method :put
                                       :path [(:id data)]
                                       :body (select-keys (:body data) persist-keys)}))))))

(defn delete [request]
  (partial handler (fn [id]
                     (go (<! (request {:method :delete
                                       :path [id]}))))))

(defn update-records-pagination [resp owner]
  (om/set-state! owner :records (:records resp))
  (om/set-state! owner [:pagination :page] (:page resp))
  (om/set-state! owner [:pagination :page-size] (:page-size resp))
  (om/set-state! owner [:pagination :total-records] (:total-records resp))
  (om/set-state! owner [:pagination :total-pages] (:total-pages resp))
  (om/set-state! owner [:pagination :order] (:order resp))
  (om/set-state! owner [:pagination :sort] (:sort resp))
  (om/set-state! owner [:pagination :search] (:search resp)))

(defn listen-search [owner]
  (let [c (om/get-state owner :search-chan)
        get-page-chan (om/get-state owner :get-page-chan)]
    (go (while true
          (let [value (<! c)]
            (if-not (= value "")
              (put! get-page-chan {:page 1 :search value})
              (put! get-page-chan {:page 1 :search nil})))))))

(defn listen-get-page [owner handler]
  (let [c (om/get-state owner :get-page-chan)]
    (go (while true
          (let [data (<! c)
                pagination {:page-size (om/get-state owner [:pagination :page-size])
                            :order (om/get-state owner [:pagination :order])
                            :sort (om/get-state owner [:pagination :sort])
                            :search (om/get-state owner [:pagination :search])}
                data (if (coll? data)
                       (merge pagination data)
                       (assoc pagination :page data))]
            (let [resp (<! (handler data))]
              (if-not (error? resp)
                (update-records-pagination resp owner))))))))

(defn listen-get [owner handler]
  (let [c (om/get-state owner :get-chan)]
    (go (while true
          (let [id (<! c)
                resp (<! (handler id))]
            (if-not (error? resp)
              (let [index (first (keep-indexed (fn [i v] (if (= id (:id v)) i)) (om/get-state owner :records)))]
                (om/set-state! owner :selected (or (:selected resp) resp))
                (om/set-state! owner [:records index] (or (:records resp) resp))
                (om/set-state! owner :status :viewing))))))))

(defn listen-insert [owner post-handler page-handler record-handler]
  (let [c (om/get-state owner :insert-chan)]
    (go (while true
          (let [record (<! c)
                resp (<! (post-handler {:body record :validate record}))]
            (if-let [errors (:error resp)]
              (>! c {:sucess false
                     :errors errors})
              (do (let [resp (<! (page-handler (:id resp)))]
                    (if-not (error? resp)
                      (update-records-pagination resp owner)))
                  (let [resp (<! (record-handler resp))]
                    (om/set-state! owner :selected (or (:selected resp) resp))
                    (om/set-state! owner :status :viewing))
                  (>! c {:sucess true}))))))))

(defn listen-update [owner handler]
  (let [c (om/get-state owner :update-chan)]
    (go (while true
          (let [record (<! c)
                resp (<! (handler {:id (:id record)
                                   :body record
                                   :validate record}))]
            (if-let [errors (:error resp)]
              (>! c {:sucess false
                     :errors errors})
              (do (put! (om/get-state owner :get-chan) (:id record))
                  (>! c {:sucess true}))))))))

(defn listen-delete [owner handler]
  (let [c (om/get-state owner :delete-chan)]
    (go (while true
          (let [id (<! c)
                resp (<! (handler id))]
            (if-not (error? resp)
              (put! (om/get-state owner :get-page-chan) {:page (om/get-state owner [:pagination :page])
                                                         :last-page-on-miss 1})))))))

(defn listen-edit [owner]
  (let [c (om/get-state owner :edit-chan)]
    (go (while true
          (<! c)
          (om/set-state! owner :status :editing)))))

(defn listen-back-form [owner]
  (let [c (om/get-state owner :back-chan)]
    (go (while true
          (<! c)
          (om/set-state! owner :selected nil)
          (om/set-state! owner :status :idle)
          (om/set-state! owner :err-msgs [])))))

(defn listen-on-delete [owner]
  (let [c (chan)
        c-form (om/get-state owner :on-delete-chan)
        c-modal (om/get-state owner :modal-chan)]
    (sub (om/get-state owner :modal-pub-chan) :confirm c)
    (go (while true
          (<! c-form)
          (>! c-modal {:modal :modal
                       :show true})
          (>! c-form (:result (<! c)))))))

(defn listen-on-click-image [owner]
  (let [c-form (om/get-state owner :on-click-image-chan)
        c-modal (om/get-state owner :image-chan)]
    (go (while true
          (<! c-form)
          (>! c-modal {:modal :modal
                       :show true})))))

(defn form-view [cursor owner {:keys [send-request] :as opts}]
  (reify
    om/IInitState
    (init-state [_]
      (let [modal-chan (chan)
            image-chan (chan)]
        {:pagination {:page nil
                      :page-size nil
                      :order nil
                      :sort nil
                      :search nil
                      :total-pages nil
                      :total-records nil}
         :records []
         :selected nil
         :status :idle
         :search-chan (chan)
         :get-page-chan (chan)
         :get-chan (chan)
         :insert-chan (chan)
         :update-chan (chan)
         :delete-chan (chan)
         :edit-chan (chan)
         :back-chan (chan)
         :on-delete-chan (chan)
         :on-click-image-chan (chan)
         :modal-chan modal-chan
         :modal-pub-chan (pub modal-chan :modal)
         :image-chan image-chan
         :image-pub-chan (pub image-chan :modal)}))
    
    om/IWillMount
    (will-mount [_]
      (let [loading (:loading opts)
            page-handler (or (:get-page opts) default-handler)
            record-handler (or (:get-record opts) default-handler)
            validate (if (:validate opts) (partial validate-handler (:validate opts)) default-handler)
            persist-keys (:persist-keys opts)
            start-waiting (partial wait (assoc loading :action :start))
            stop-waiting (partial wait (assoc loading :action :stop))
            handle-response (partial handle-response cursor)
            get-page (get-page send-request)
            get-page-id (get-page-id send-request owner)
            get-record (get-record send-request)
            post (post send-request persist-keys)
            put (put send-request persist-keys)
            delete (delete send-request)]
        (listen-search owner)
        (listen-get-page owner (->> (stop-waiting)
                                    (handle-response)
                                    (page-handler)
                                    (get-page)
                                    (start-waiting)))
        (listen-get owner (->> (stop-waiting)
                               (handle-response)
                               (record-handler)
                               (get-record)
                               (start-waiting)))
        (listen-insert owner (->> (stop-waiting)
                                  (handle-response)
                                  (post)
                                  (start-waiting)
                                  (validate))
                       (->> (stop-waiting)
                            (handle-response)
                            (page-handler)
                            (get-page-id)
                            (start-waiting))
                       (record-handler))
        (listen-update owner (->> (stop-waiting)
                                  (handle-response)
                                  (put)
                                  (start-waiting)
                                  (validate)))
        (listen-delete owner (->> (stop-waiting)
                                  (handle-response)
                                  (delete)
                                  (start-waiting)))
        (listen-edit owner)
        (listen-back-form owner)
        (listen-on-delete owner)
        (listen-on-click-image owner))
      (let [pagination (:pagination opts)]
        (put! (om/get-state owner :get-page-chan) {:page (:page pagination)
                                                   :page-size (:page-size pagination)
                                                   :order (:order pagination)
                                                   :sort (:sort pagination)})))

    om/IDidUpdate
    (did-update [_ _ prev]
      (if-not (= (om/get-state owner :status) (:status prev))
        (om/update! (:err-msgs cursor) [])))

    om/IRenderState
    (render-state  [_ state]
      (dom/div #js {:className "panel panel-default"}
               (dom/div #js {:className "panel-heading hidden-xs"}
                        (dom/h3 #js {:className "panel-title"} (:heading opts)))
               (dom/h4 #js {:className "text-center visible-xs"} (:heading opts))
               (dom/div #js {:className "panel-body"}
                        (om/build confirm-modal nil {:opts {:id "confirm-modal"
                                                            :title (t :default/delete)
                                                            :text (t :default/delete-confirm)
                                                            :buttons-labels {:cancel (t :default/cancel)}
                                                            :modal-chan (:modal-chan state)
                                                            :pub-chan (:modal-pub-chan state)}})
                        (if-let [record (:selected state)]
                          (if (and (:show-image opts) (:image record))
                            (om/build image-modal nil {:state {:title ((:table-header-key opts) record)
                                                               :src (str (gstring/format (get-in opts [:image-src :image]) ((:id-key opts) record)) "?name=" (:image record))}
                                                       :opts {:id "image-modal"
                                                              :buttons-labels {:close (t :default/close)}
                                                              :modal-chan (:image-chan state)
                                                              :pub-chan (:image-pub-chan state)}})))
                        (apply css-trans-group #js {:transitionName "fade"
                                                    :transitionLeave false}
                               (if (= (:status state) :idle)
                                 (list
                                  (dom/div #js {:key "add-search"
                                                :className "row"}
                                           (dom/div #js {:className "col-md-5 col-xs-12"}
                                                    (om/build text-listen nil {:init-state {:value (get-in state [:pagination :search])
                                                                                            :old-value (get-in state [:pagination :search])}
                                                                               :opts {:changed-chan (:search-chan state)
                                                                                      :placeholder (t :default/search)}}))
                                           (dom/div #js {:className "col-md-offset-5 col-md-2 hidden-xs"}
                                                    (dom/div #js {:className "btn-group pull-right"}
                                                             (dom/button #js {:className "btn btn-default glyphicon glyphicon-refresh"
                                                                              :title (t :default/refresh)
                                                                              :onClick #(put! (om/get-state owner :get-page-chan) (get-in state [:pagination :page]))})
                                                             (dom/button #js {:className "btn btn-default glyphicon glyphicon-plus"
                                                                              :title (t :default/add)
                                                                              :onClick (let [loading (:loading opts)
                                                                                             handler (->> (wait (assoc loading :action :stop))
                                                                                                          (handle-response cursor)
                                                                                                          ((:get-new-record opts))
                                                                                                          (wait (assoc loading :action :start)))]
                                                                                         (fn [e]
                                                                                           (.preventDefault e)
                                                                                           (go (let [resp (<! (handler))]
                                                                                                 (when-not (error? resp)
                                                                                                   (om/set-state! owner :selected resp)
                                                                                                   (om/set-state! owner :status :adding))))))}))))
                                  (dom/div #js {:className "row visible-xs"}
                                           (dom/div #js {:className "col-xs-12"}
                                                    (dom/button #js {:className "btn btn-default btn-block glyphicon glyphicon-refresh"
                                                                     :title (t :default/refresh)
                                                                     :onClick #(put! (om/get-state owner :get-page-chan) (get-in state [:pagination :page]))}))))
                                 (list
                                  (om/build form {:status (:status state)
                                                  :selected (:selected state)}
                                            {:init-state {:record (into {} (:selected state))}
                                             :state {:image-available (get-in state [:selected :image])
                                                     :image-src (if (:show-image opts)
                                                                  (str
                                                                   (gstring/format (get-in opts [:image-src :thumbnail]) (get-in state [:selected (:id-key opts)]))
                                                                   "?name=" (get-in state [:selected :image])))}
                                             :opts {:id-key (:id-key opts)
                                                    :labels (:labels opts)
                                                    :display (:form-display opts)
                                                    :types (:types opts)
                                                    :focus (:focus opts)
                                                    :show-image (:show-image opts)
                                                    :image-not-available-msg (:image-not-available-msg opts)
                                                    :no-image-label (t :default/no-image)
                                                    :optionals (:optionals opts)
                                                    :optional-label (t :default/optional)
                                                    :put-on-change? true
                                                    :validate-rules (:validate-rules opts)
                                                    :status-labels {:viewing (t :default/viewing)
                                                                    :adding (t :default/adding)
                                                                    :editing (t :default/editing)}
                                                    :buttons-labels {:save (t :default/save)
                                                                     :discard (t :default/discard)
                                                                     :edit (t :default/edit)
                                                                     :delete (t :default/delete)
                                                                     :back (t :default/back)}
                                                    :edit-chan (:edit-chan state)
                                                    :insert-chan (:insert-chan state)
                                                    :update-chan (:update-chan state)
                                                    :delete-chan (:delete-chan state)
                                                    :back-chan (:back-chan state)
                                                    :refresh-view-chan (:get-chan state)
                                                    :on-blur-chan (:on-blur-chan state)
                                                    :on-change-chan (:on-change-chan state)
                                                    :on-delete-chan (:on-delete-chan state)
                                                    :on-click-image-chan (:on-click-image-chan state)}})))))
               (css-trans-group #js {:transitionName "fade"
                                     :transitionLeave false}
                                (if (and (= (:status state) :idle) (not (nil? (get-in state [:pagination :total-records]))))
                                  (om/build table-view nil {:state {:pagination (:pagination state)
                                                                    :records (:records state)}
                                                            :opts {:labels (:labels opts)
                                                                   :display (:table-display opts)
                                                                   :types (:types opts)
                                                                   :id-key (:id-key opts)
                                                                   :show-image (:show-image opts)
                                                                   :image-key :image
                                                                   :image-src (get-in opts [:image-src :thumbnail])
                                                                   :image-not-available-msg (:image-not-available-msg opts)
                                                                   :header-key (:table-header-key opts)
                                                                   :pager pager
                                                                   :pages-summary (t :default/pages-summary)
                                                                   :no-records (t :default/no-records)
                                                                   :page-sizes [5 10 20 50]
                                                                   :pager-size (get-in opts [:pagination :pager-size])
                                                                   :per-page-label (t :default/per-page-label)
                                                                   :selected-chan (:get-chan state)
                                                                   :change-page-chan (:get-page-chan state)}})
                                  (dom/div #js {:key "empty"})))))))
