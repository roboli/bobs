(ns bobs.views.maintenance.brands
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [bobs.utils :refer [small-string]]
            [bobs.i18n :refer [t]]
            [bobs.data.request :refer [request]]
            [bobs.validators.validator :as validator]
            [bobs.validators.brands :refer [rules]]
            [bobs.views.menus-view :refer [menus-view]]
            [bobs.views.maintenance.form-view :refer [form-view handler]]))

(def send-request (request "/brands"))

(def validate (validator/validate rules))

(def get-new-record
  (partial handler (fn [& [data]]
                     (go {:name ""}))))

(def labels {:id (t :default/id)
             :name (t :default/name)})

(def types {:name {:type :text
                   :max-length small-string}})

(def pagination {:page 1
                 :page-size 10
                 :order "name"
                 :sort 0
                 :pager-size 5})

(defn render-root [cursor target]
  (om/root menus-view cursor {:target target
                              :opts {:main-active-route :maintenance
                                     :second-active-route :brands
                                     :heading (t :default/maintenance)
                                     :form-view form-view
                                     :form-opts {:heading (t :default/brands)
                                                 :send-request send-request
                                                 :get-new-record get-new-record
                                                 :labels labels
                                                 :form-display [:name]
                                                 :table-display []
                                                 :table-header-key :name
                                                 :id-key :id
                                                 :focus :name
                                                 :types types
                                                 :validate validate
                                                 :validate-rules rules
                                                 :optionals [:description]
                                                 :optional-label (t :default/optional)
                                                 :pagination pagination
                                                 :persist-keys [:name]}}}))
