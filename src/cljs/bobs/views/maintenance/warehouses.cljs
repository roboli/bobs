(ns bobs.views.maintenance.warehouses
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [bobs.utils :refer [small-string string]]
            [bobs.i18n :refer [t]]
            [bobs.views.menus-view :refer [menus-view]]
            [bobs.data.request :refer [request]]
            [bobs.validators.validator :as validator]
            [bobs.validators.warehouses :refer [rules]]
            [bobs.views.maintenance.form-view :refer [form-view handler]]))

(def send-request (request "/warehouses"))

(def validate (validator/validate rules))

(defn format-description [description]
  (if (empty? description) (t :default/not-available-abbr) description))

(def format-page
  (partial handler (fn [response]
                     (go (assoc response
                           :records (reduce
                                     #(conj %1 (assoc %2
                                                 :description (format-description (:description %2))))
                                     []
                                     (:records response)))))))

(def format-warehouse
  (partial handler (fn [response]
                     (go {:selected response
                          :records (assoc response
                                     :description (format-description (:description response)))}))))

(def get-new-record
  (partial handler (fn [& [data]]
                     (go {:name ""
                          :description nil}))))

(def labels {:id (t :default/id)
             :name (t :default/name)
             :description (t :default/description)})

(def types {:name {:type :text
                   :max-length small-string}
            :description {:type :text
                          :max-length string}})

(def pagination {:page 1
                 :page-size 10
                 :order "name"
                 :sort 0
                 :pager-size 5})

(defn render-root [cursor target]
  (om/root menus-view cursor {:target target
                              :opts {:main-active-route :maintenance
                                     :second-active-route :warehouses
                                     :heading (t :default/maintenance)
                                     :form-view form-view
                                     :form-opts {:heading (t :default/warehouses)
                                                 :send-request send-request
                                                 :labels labels
                                                 :form-display [:name :description]
                                                 :table-display [:description]
                                                 :types types
                                                 :id-key :id
                                                 :table-header-key :name
                                                 :focus :name
                                                 :validate validate
                                                 :validate-rules rules
                                                 :optionals [:description]
                                                 :pagination pagination
                                                 :persist-keys [:name :description]
                                                 :get-page format-page
                                                 :get-record format-warehouse
                                                 :get-new-record get-new-record}}}))
