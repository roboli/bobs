(ns bobs.views.maintenance.uom
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [bobs.utils :refer [small-string string]]
            [bobs.i18n :refer [t]]
            [bobs.views.menus-view :refer [menus-view]]
            [bobs.data.request :refer [request]]
            [bobs.validators.validator :as validator]
            [bobs.validators.uom :refer [rules]]
            [bobs.views.maintenance.form-view :refer [form-view handler]]))

(def send-request (request "/uom"))

(def validate (validator/validate rules))

(def get-new-record
  (partial handler (fn [& [data]]
                     (go {:name ""
                          :abbreviation ""}))))

(def labels {:id (t :default/id)
             :name (t :default/name)
             :abbreviation (t :default/abbreviation)})

(def types {:name {:type :text
                   :max-length small-string}
            :abbreviation {:type :text
                           :max-length 4}})

(def pagination {:page 1
                 :page-size 10
                 :order "name"
                 :sort 0
                 :pager-size 5})

(defn render-root [cursor target]
  (om/root menus-view cursor {:target target
                              :opts {:main-active-route :maintenance
                                     :second-active-route :uom
                                     :heading (t :default/maintenance)
                                     :form-view form-view
                                     :form-opts {:heading (t :default/units-of-measure)
                                                 :send-request send-request
                                                 :labels labels
                                                 :form-display [:name :abbreviation]
                                                 :table-display [:abbreviation]
                                                 :types types
                                                 :id-key :id
                                                 :table-header-key :name
                                                 :focus :name
                                                 :validate validate
                                                 :validate-rules rules
                                                 :pagination pagination
                                                 :persist-keys [:name :abbreviation]
                                                 :get-new-record get-new-record}}}))
