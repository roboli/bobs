(ns bobs.views.maintenance.items
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [put!]]
            [bobs.i18n :refer [t]]
            [bobs.views.menus-view :refer [menus-view]]
            [bobs.data.request :refer [request]]
            [bobs.validators.validator :as validator]
            [bobs.validators.items :refer [rules]]
            [bobs.utils :as utils :refer [error? string decimal return-nil]]
            [bobs.views.maintenance.form-view :refer [form-view handler]]
            [bobs.widgets.input-controls :refer [listen-focus]]
            [bobs.widgets.helpers :refer [question-mark]]
            [clojure.string :as str]))

(def send-request (request "/items" :formdata? true))

(defn send-request-currency [] ((request "/currencies/default") {:method :get}))

(defn send-request-brands [] ((request "/brands") {:method :get
                                                   :query {:order "name"}}))

(defn send-request-item-types [] ((request "/item-types") {:method :get
                                                           :query {:order "name"}}))

(defn send-request-uom [] ((request "/uom") {:method :get
                                             :query {:order "name"}}))

(defn format-brand-name [brand-name]
  (if (empty? brand-name) (t :default/not-available-abbr) brand-name))

(defn format-currency [currency-symbol amount]
  (str currency-symbol " " (utils/format-currency amount)))

(def format-page
  (partial handler (fn [response]
                     (go (assoc response
                           :records (reduce
                                     #(conj %1 (assoc %2
                                                 :price (format-currency (:currency-symbol %2) (:price %2))
                                                 :brand-name (format-brand-name (:brand-name %2))))
                                     []
                                     (:records response)))))))

(def get-brands
  (partial handler (fn [response]
                     (go (let [resp (<! (send-request-brands))]
                           (if (error? resp)
                             resp
                             {:selected (assoc response :brands resp)
                              :records response}))))))

(def get-item-types
  (partial handler (fn [response]
                     (go (let [resp (<! (send-request-item-types))]
                           (if (error? resp)
                             resp
                             {:selected (assoc (:selected response) :item-types resp)
                              :records (:records response)}))))))

(def get-uom
  (partial handler (fn [response]
                     (go (let [resp (<! (send-request-uom))]
                           (if (error? resp)
                             resp
                             {:selected (assoc (:selected response) :uom resp)
                              :records (:records response)}))))))

(def get-currency
  (partial handler (fn [response]
                     (go (let [resp (<! (send-request-currency))]
                           (if (error? resp)
                             resp
                             {:selected (assoc (:selected response) :currencies [resp])
                              :records (:records response)}))))))

(def format-item
  (partial handler (fn [response]
                     (go (let [selected (:selected response)]
                           {:selected (assoc (:selected response) :price (utils/format-currency (:price selected)) :auto-sku false :image-filename nil)
                            :records (assoc (:records response)
                                       :price (format-currency (:currency-symbol selected) (:price selected))
                                       :brand-name (format-brand-name (get-in response [:selected :brand-name])))})))))

(defn get-record [& [handler]]
  (-> handler
      (format-item)
      (get-currency)
      (get-uom)
      (get-item-types)
      (get-brands)))

(def get-empty-record
  (partial handler (fn [& [data]]
                     (go {:description ""
                          :sku ""
                          :auto-sku false
                          :brand-id nil
                          :item-type-id nil
                          :uom-id nil
                          :price ""
                          :inventory-type 1
                          :active true
                          :lifo false
                          :image nil
                          :image-filename nil
                          :can-change-inventory-type true}))))

(def get-new-record-brands
  (partial handler (fn [& [data]]
                     (go (let [resp (<! (send-request-brands))]
                           (if (error? resp)
                             resp
                             (assoc data :brands resp)))))))

(def get-new-record-item-types
  (partial handler (fn [& [data]]
                     (go (let [resp (<! (send-request-item-types))]
                           (if (error? resp)
                             resp
                             (assoc data :item-types resp)))))))

(def get-new-record-uom
  (partial handler (fn [& [data]]
                     (go (let [resp (<! (send-request-uom))]
                           (if (error? resp)
                             resp
                             (assoc data :uom resp)))))))

(def get-new-currency
  (partial handler (fn [& [data]]
                     (go (let [resp (<! (send-request-currency))]
                           (if (error? resp)
                             resp
                             (assoc data
                               :currency-id (:id resp)
                               :currencies [resp])))))))

(defn get-new-record [& [handler]]
  (-> handler
      (get-new-currency)
      (get-new-record-uom)
      (get-new-record-item-types)
      (get-new-record-brands)
      (get-empty-record)))

(defn validate [record]
  (let [rules (if (:auto-sku record) (dissoc rules :sku) rules)]
    ((validator/validate rules) record)))

(def labels {:description (t :default/description)
             :sku (t :default/sku)
             :brand-name (t :default/brand)
             :brands (t :default/brand)
             :item-types (t :default/type)
             :uom (t :default/uom)
             :currencies (t :default/currency)
             :price (t :default/price)
             :image (t :default/image)
             :generate (t :default/generate)
             :inventory (t :default/inventory)
             :perpetual (t :default/perpetual)
             :periodic (t :default/periodic)
             :method (t :default/method)
             :fifo (t :default/fifo)
             :lifo (t :default/lifo)
             :active (t :default/active)})

(defn sku-control [_ owner {:keys [on-change] :as opts}]
  (reify
    om/IInitState
    (init-state [_]
      {:auto-sku false})

    om/IWillMount
    (will-mount [_]
      (listen-focus owner (:widgets-pub-chan opts) "sku"))

    om/IDidUpdate
    (did-update [_ _ _]
      (when (= (om/get-state owner :status) :viewing)
        (om/set-state! owner :auto-sku false)))

    om/IRenderState
    (render-state [_ {:keys [status hint auto-sku] :as state}]
      (dom/div nil
               (dom/div #js {:className (str/join " " [(if-not auto-sku
                                                         (condp = (:validation-state state)
                                                           :on-error "has-error"
                                                           :on-success "has-success"
                                                           nil))
                                                       "form-group"])}
                        (dom/label #js {:htmlFor "sku"
                                        :className "col-md-2 control-label"} (:sku labels))
                        (dom/div #js {:className (if (= status :viewing) "col-md-6" "col-md-4")}
                                 (dom/input #js {:id "sku"
                                                 :ref "sku"
                                                 :className "form-control"
                                                 :type "text"
                                                 :value (if (and (not= status :viewing) auto-sku) "" (:value state))
                                                 :disabled auto-sku
                                                 :maxLength string
                                                 :onKeyDown #((:on-key-down opts) (.-keyCode %))
                                                 :onChange #(on-change (.. % -target -value))
                                                 :onBlur #((:on-blur opts) (.. % -target -value))}))
                        (if-not (= status :viewing)
                          (dom/div #js {:className "col-md-2"}
                                   (dom/div #js {:className "checkbox"}
                                            (dom/label nil
                                                       (dom/input #js {:type "checkbox"
                                                                       :onChange (fn [e]
                                                                                   (let [value (.. e -target -checked)]
                                                                                     (on-change {:auto-sku value})
                                                                                     (om/set-state! owner :auto-sku value)))}
                                                                  (:generate labels))))))
                        (if-not auto-sku
                          (dom/span #js {:className (str/join " " [(if (not hint) "hidden") "help-block"])}
                                    (dom/small nil hint))))))))

(def help-texts {:help-perpetual (t :default/help-perpetual)
                 :help-periodic (t :default/help-periodic)
                 :help-fifo (t :default/help-fifo)
                 :help-lifo (t :default/help-lifo)
                 :help-inventory-disabled (t :default/help-inventory-disabled)
                 :note (t :default/note)})

(defn inventory-type-control [_ owner {:keys [on-change] :as opts}]
  (reify
    om/IRenderState
    (render-state [_ state]
      (dom/div #js {:className "form-group"}
               (dom/label #js {:className "col-md-2 control-label"} (:inventory labels))
               (dom/div #js {:className "col-md-3"}
                        (dom/label #js {:className "radio-inline"}
                                   (dom/input #js {:type "radio"
                                                   :name "inventory-type"
                                                   :disabled (not (:enabled state))
                                                   :checked (= (:value state) 1)
                                                   :onChange #(on-change 1)})
                                   (:perpetual labels))
                        (dom/label #js {:className "radio-inline"}
                                   (dom/input #js {:type "radio"
                                                   :name "inventory-type"
                                                   :disabled (not (:enabled state))
                                                   :checked (= (:value state) 2)
                                                   :onChange #(on-change 2)})
                                   (:periodic labels)))
               (dom/div #js {:className "col-md-3"}
                        (dom/span #js {:style #js {:display "block"
                                                   :margin-top "7px"}}
                                  (om/build question-mark nil {:opts {:id "inventory-type-helper"
                                                                      :content (str
                                                                                "<b>" (:perpetual labels) ":</b> "
                                                                                (:help-perpetual help-texts) "<br />"
                                                                                "<b>" (:periodic labels) ":</b> "
                                                                                (:help-periodic help-texts) "<br />"
                                                                                "<br /><i>" (:note help-texts) "</i>"
                                                                                (:help-inventory-disabled help-texts))}})))))))

(defn inventory-method-control [_ owner {:keys [on-change] :as opts}]
  (reify
    om/IRenderState
    (render-state [_ state]
      (dom/div #js {:className "form-group"}
               (dom/label #js {:className "col-md-2 control-label"} (:method labels))
               (dom/div #js {:className "col-md-3"}
                        (dom/label #js {:className "radio-inline"}
                                   (dom/input #js {:type "radio"
                                                   :name "inventory-method"
                                                   :disabled (not (:enabled state))
                                                   :checked (= (:value state) false)
                                                   :onChange #(on-change false)})
                                   (:fifo labels))
                        (dom/label #js {:className "radio-inline"}
                                   (dom/input #js {:type "radio"
                                                   :name "inventory-method"
                                                   :disabled (not (:enabled state))
                                                   :checked (= (:value state) true)
                                                   :onChange #(on-change true)})
                                   (:lifo labels)))
               (dom/div #js {:className "col-md-3"}
                        (dom/span #js {:style #js {:display "block"
                                                   :margin-top "7px"}}
                                  (om/build question-mark nil {:opts {:id "method-type-helper"
                                                                      :content (str
                                                                                "<b>" (:fifo labels) ":</b> "
                                                                                (:help-fifo help-texts) "<br />"
                                                                                "<b>" (:lifo labels) ":</b> "
                                                                                (:help-lifo help-texts) "<br />"
                                                                                "<br /><i>" (:note help-texts) "</i>"
                                                                                (:help-inventory-disabled help-texts))}})))))))

(def types {:description {:type :text
                          :max-length string}
            :sku {:type :custom
                  :control sku-control
                  :on-change (fn [owner & [on-change-chan]]
                               (fn [value]
                                 (if (map? value)
                                   (om/set-state! owner [:record :auto-sku] (:auto-sku value))
                                   (om/set-state! owner [:record :sku] value))
                                 (if (and on-change-chan (not (map? value)))
                                   (put! on-change-chan {:sku value}))))
                  :more-state return-nil}
            :brands {:type :select
                     :value-key :id
                     :text-key :name
                     :selected-key :brand-id
                     :error-key-aliases [:brand-id]}
            :item-types {:type :select
                         :value-key :id
                         :text-key :name
                         :selected-key :item-type-id
                         :error-key-aliases [:item-type-id]}
            :uom {:type :select
                  :value-key :id
                  :text-key :name
                  :selected-key :uom-id
                  :error-key-aliases [:uom-id]}
            :active {:type :checkbox}
            :currencies {:type :select
                         :value-key :id
                         :text-key :name
                         :selected-key :currency-id
                         :error-key-aliases [:currency-id]}
            :price {:type :text
                    :max-length decimal}
            :inventory-type {:type :custom
                             :control inventory-type-control
                             :on-change (fn [owner & [on-change-chan]]
                                          (fn [value]
                                            (om/set-state! owner [:record :inventory-type] value)))
                             :more-state (fn [owner]
                                           {:enabled (om/get-state owner [:record :can-change-inventory-type])})}
            :lifo {:type :custom
                   :control inventory-method-control
                   :on-change (fn [owner & [on-change-chan]]
                                (fn [value]
                                  (om/set-state! owner [:record :lifo] value)))
                   :more-state (fn [owner]
                                 {:enabled (and (= (om/get-state owner [:record :inventory-type]) 1)
                                                (om/get-state owner [:record :can-change-inventory-type]))})}
            :image {:type :image
                    :filename-key :image-filename
                    :error-key-aliases [:image-filename :image-file :image-file-size]}})

(def pagination {:page 1
                 :page-size 10
                 :order "description"
                 :sort 0
                 :pager-size 5})

(def image-src {:image "/items/%d/image"
                :thumbnail "/items/%d/image?thumbnail=1"})

(defn render-root [cursor target]
  (om/root menus-view cursor {:target target
                              :opts {:main-active-route :maintenance
                                     :second-active-route :items
                                     :heading (t :default/maintenance)
                                     :form-view form-view
                                     :form-opts {:heading (t :default/items)
                                                 :send-request send-request
                                                 :labels labels
                                                 :form-display [:description :sku :currencies :price :brands :item-types :uom :active :inventory-type :lifo :image]
                                                 :table-display [:sku :price :brand-name]
                                                 :id-key :id
                                                 :table-header-key :description
                                                 :types types
                                                 :focus :description
                                                 :show-image true
                                                 :image-src image-src
                                                 :image-not-available-msg (t :default/image-not-available)
                                                 :optionals [:brand-id :item-type-id :uom-id :image]
                                                 :validate validate
                                                 :validate-rules rules
                                                 :pagination pagination
                                                 :persist-keys [:description :sku :auto-sku :brand-id :item-type-id :uom-id :currency-id :price :active :inventory-type :lifo :image]
                                                 :get-page format-page
                                                 :get-record get-record
                                                 :get-new-record get-new-record}}}))
