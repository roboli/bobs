(ns bobs.views.maintenance.partners
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [bobs.utils :refer [small-string string big-string]]
            [bobs.i18n :refer [t]]
            [bobs.views.menus-view :refer [menus-view]]
            [bobs.data.request :refer [request]]
            [bobs.validators.validator :as validator]
            [bobs.validators.partners :refer [rules]]
            [bobs.views.maintenance.form-view :refer [form-view handler]]))

(def send-request (request "/partners"))

(def validate (validator/validate rules))

(def get-new-record
  (partial handler (fn [& [data]]
                     (go {:id ""
                          :name ""
                          :address ""
                          :telephone ""
                          :uid ""
                          :email ""
                          :is-customer false
                          :is-supplier false}))))

(def labels {:id (t :default/id)
             :name (t :default/name)
             :address (t :default/address)
             :telephone (t :default/telephone)
             :uid (t :default/uid)
             :email (t :default/email)
             :is-customer (t :default/customer)
             :is-supplier (t :default/supplier)})

(def types {:name {:type :text
                   :max-length string}
            :address {:type :text
                      :max-length big-string}
            :telephone {:type :text
                        :max-length string}
            :uid {:type :text
                  :max-length small-string}
            :email {:type :text
                    :max-length string}
            :is-customer {:type :checkbox
                          :edit-on-editing? #(:can-downgrade-from-customer %)}
            :is-supplier {:type :checkbox
                          :edit-on-editing? #(:can-downgrade-from-supplier %)}})

(def pagination {:page 1
                 :page-size 10
                 :order "name"
                 :sort 0
                 :pager-size 5})

(defn render-root [cursor target]
  (om/root menus-view cursor {:target target
                              :opts {:main-active-route :maintenance
                                     :second-active-route :partners
                                     :heading (t :default/maintenance)
                                     :form-view form-view
                                     :form-opts {:heading (t :default/partners)
                                                 :send-request send-request
                                                 :get-new-record get-new-record
                                                 :labels labels
                                                 :form-display [:name :address :telephone :uid :email :is-customer :is-supplier]
                                                 :table-display [:address :is-customer :is-supplier]
                                                 :types types
                                                 :optionals [:uid :email]
                                                 :id-key :id
                                                 :table-header-key :name
                                                 :focus :name
                                                 :validate validate
                                                 :validate-rules rules
                                                 :pagination pagination
                                                 :persist-keys [:name :address :telephone :email :uid :is-supplier :is-customer]}}}))
