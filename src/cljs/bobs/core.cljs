(ns bobs.core
  (:require [om.core :as om :include-macros true]
            [bobs.routes :as routes]
            [bobs.data.app :refer [app-state]]
            [bobs.data.routes :refer [routes]]
            [secretary.core :refer [dispatch!]]
            [goog.events :as events])
  (:import goog.History
           goog.History.EventType))

(def hist (doto (History.) (.setEnabled true)))

(defn login []
  (when (. js/Session -loggedIn)
    (swap! app-state update-in [:session :logged-in] (fn [_] true))
    (swap! app-state update-in [:session :user :username] (fn [_] (.. js/Session -user -username)))
    (swap! app-state update-in [:session :user :name] (fn [_] (.. js/Session -user -name)))
    (swap! app-state update-in [:session :company :alias] (fn [_] (.. js/Session -company -alias)))
    (swap! app-state update-in [:session :company :name] (fn [_] (.. js/Session -company -name)))
    true))

(defn render-app [f cursor]
  (f cursor (.getElementById js/document "app")))

(events/listen hist (.-NAVIGATE EventType)
               #(let [route (if (get-in @app-state [:session :logged-in]) (.-token %) (get-in routes [:login :route]))
                      {:keys [f cursor]} (dispatch! route)]
                  (render-app f cursor)))

(let [route (-> js/window .-location .-hash)
      route (apply str (remove #((set "#") %) route))
      route (if (empty? route) (get-in routes [:home :route]) route)
      route (if (login) route (get-in routes [:login :route]))
      {:keys [f cursor]} (dispatch! route)]
  (render-app f cursor))
