(ns bobs.routes
  (:require [secretary.core :as secretary :refer-macros [defroute]]
            [bobs.data.routes :refer [routes]]
            [bobs.data.app :refer [app-state]]
            [bobs.views.home.main :as home]
            [bobs.views.login.main :as login]
            [bobs.views.maintenance.main :as maintenance]
            [bobs.views.maintenance.brands :as brands]
            [bobs.views.maintenance.uom :as uom]
            [bobs.views.maintenance.item-types :as item-types]
            [bobs.views.maintenance.items :as items]
            [bobs.views.maintenance.warehouses :as warehouses]
            [bobs.views.maintenance.partners :as partners]
            [bobs.views.documents.purchases :as purchases]
            [bobs.views.documents.purchase-receipts :as purchase-receipts]
            [bobs.views.documents.purchase-returns :as purchase-returns]
            [bobs.views.documents.sales :as sales]
            [bobs.views.documents.shipments :as shipments]
            [bobs.views.documents.sales-returns :as sales-returns]
            [bobs.views.documents.inventory :as inventory]
            [bobs.views.documents.history :as history]
            [bobs.views.documents.stock :as stock]
            [bobs.views.documents.entries :as entries]
            [bobs.views.documents.withdraws :as withdraws]))

(defroute (get-in routes [:home :route])
  [] {:f home/render-root :cursor app-state})

(defroute (get-in routes [:login :route])
  [] {:f login/render-root :cursor app-state})

(defroute (get-in routes [:purchases :route])
  [] {:f purchases/render-root :cursor app-state})

(defroute (get-in routes [:purchases :children :purchase-receipts :route])
  [] {:f purchase-receipts/render-root :cursor app-state})

(defroute (get-in routes [:purchases :children :purchase-returns :route])
  [] {:f purchase-returns/render-root :cursor app-state})

(defroute (get-in routes [:sales :route])
  [] {:f sales/render-root :cursor app-state})

(defroute (get-in routes [:sales :children :shipments :route])
  [] {:f shipments/render-root :cursor app-state})

(defroute (get-in routes [:sales :children :sales-returns :route])
  [] {:f sales-returns/render-root :cursor app-state})

(defroute (get-in routes [:inventory :route])
  [] {:f inventory/render-root :cursor app-state})

(defroute (get-in routes [:inventory :children :entries :route])
  [] {:f entries/render-root :cursor app-state})

(defroute (get-in routes [:inventory :children :withdraws :route])
  [] {:f withdraws/render-root :cursor app-state})

(defroute (get-in routes [:inventory :children :history :route])
  [] {:f history/render-root :cursor app-state})

(defroute (get-in routes [:inventory :children :stock :route])
  [] {:f stock/render-root :cursor app-state})

(defroute (get-in routes [:maintenance :route])
  [] {:f maintenance/render-root :cursor app-state})

(defroute (get-in routes [:maintenance :children :brands :route])
  [] {:f brands/render-root :cursor app-state})

(defroute (get-in routes [:maintenance :children :uom :route])
  [] {:f uom/render-root :cursor app-state})

(defroute (get-in routes [:maintenance :children :item-types :route])
  [] {:f item-types/render-root :cursor app-state})

(defroute (get-in routes [:maintenance :children :items :route])
  [] {:f items/render-root :cursor app-state})

(defroute (get-in routes [:maintenance :children :partners :route])
  [] {:f partners/render-root :cursor app-state})

(defroute (get-in routes [:maintenance :children :warehouses :route])
  [] {:f warehouses/render-root :cursor app-state})

(defroute "*"
  [] (.log js/console "No match for route!"))
