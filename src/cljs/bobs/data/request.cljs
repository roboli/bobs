(ns bobs.data.request
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [bobs.utils :refer [send-request]]
            [goog.uri.utils :as gutils]
            [clojure.string :as str]
            [cemerick.url :refer [url-encode]]))

(defn build-url [route {:keys [path query]}]
  (str route
       (if path (str "/" (str/join "/" (map url-encode path))))
       (if query (str "?" (gutils/buildQueryDataFromMap (clj->js (reduce-kv #(assoc %1 %2 (url-encode %3))
                                                                            {}
                                                                            query)))))))

(defn make-request [route formdata? edn-parse? data]
  (condp = (:method data)
    :get (send-request :get (build-url route {:path (:path data)
                                              :query (:query data)}) :formdata? formdata? :edn-parse? edn-parse?)
    
    :post (send-request :post (build-url route {:path (:path data)
                                                :query (:query data)}) :data (:body data) :formdata? formdata? :edn-parse? edn-parse?)
    
    :put (send-request :put (build-url route {:path (:path data)
                                              :query (:query data)}) :data (:body data) :formdata? formdata? :edn-parse? edn-parse?)
    
    :delete (send-request :delete (build-url route {:path (:path data)}) :formdata? formdata? :edn-parse? edn-parse?)))

(defn request [route & {:keys [formdata? edn-parse?] :or {formdata? false
                                                          edn-parse? true}}]
  (partial make-request route formdata? edn-parse?))
