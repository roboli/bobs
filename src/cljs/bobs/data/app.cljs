(ns bobs.data.app)

(def app-state (atom {:err-msgs []
                      :session {:logged-in false
                                :user {:username nil
                                       :name nil}
                                :company {:alias nil
                                          :name nil}
                                :before-session-expired-route nil}
                      :navigation {:main-active-route :home
                                   :second-active-route nil}}))
