(ns bobs.data.routes
  (:require [bobs.i18n :refer [t]]))

(def routes  {:home {:route "/"
                     :label (t :default/home)
                     :display true}
              :login {:route "/login"
                      :display false}
              :purchases {:route "/purchases"
                          :label (t :default/purchases)
                          :display true
                          :children {:purchase-receipts {:route "/purchase-receipts"
                                                         :label (t :default/purchase-receipts)
                                                         :display true}
                                     :purchase-returns {:route "/purchase-returns"
                                                        :label (t :default/returns)
                                                        :display true}}}
              :sales {:route "/sales"
                      :label (t :default/sales)
                      :display true
                      :children {:shipments {:route "/shipments"
                                             :label (t :default/shipments)
                                             :display true}
                                 :sales-returns {:route "/sales-returns"
                                                 :label (t :default/returns)
                                                 :display true}}}
              :inventory {:route "/inventory"
                          :label (t :default/inventory)
                          :display true
                          :children {:entries {:route "/entries"
                                               :label (t :default/entries)
                                               :display true}
                                     :withdraws {:route "/withdraws"
                                                 :label (t :default/withdraws)
                                                 :display true}
                                     :stock {:route "/stock"
                                             :label (t :default/stock)
                                             :display true}
                                     :history {:route "/history"
                                               :label (t :default/history)
                                               :display true}}}
              :maintenance {:route "/maintenance"
                            :label (t :default/maintenance)
                            :display true
                            :children {:brands {:route "/brands"
                                                :label (t :default/brands)
                                                :display true}
                                       :item-types {:route "/item-types"
                                                    :label (t :default/item-types)
                                                    :display true}
                                       :uom {:route "/uom"
                                             :label (t :default/units-of-measure)
                                             :display true}
                                       :items {:route "/items"
                                               :label (t :default/items)
                                               :display true}
                                       :partners {:route "/partners"
                                                  :label (t :default/partners)
                                                  :display true}
                                       :warehouses {:route "/warehouses"
                                                    :label (t :default/warehouses)
                                                    :display true}}}})
