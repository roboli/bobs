(ns bobs.data.config
  (:require [bobs.cross-config :as cc]))

(def app-name cc/app-name)
(def app-name-ext cc/app-name-ext)
(def app-brand cc/app-brand)
(def app-lang cc/app-lang)
(def app-email cc/app-email)
(def chrome {:version "Google Chrome 34.x"
             :url "http://www.google.com/chrome/"})
(def firefox {:version "Firefox 30.x"
              :url "https://www.mozilla.org/en-US/firefox/new/"})
(def image-file-size cc/image-file-size)
