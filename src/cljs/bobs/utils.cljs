(ns bobs.utils
  (:require [cljs.reader :as reader]
            [goog.events :as events]
            [cljs.core.async :refer [put! chan]]
            [bobs.i18n :refer [t]])
  (:import [goog.net.EventType]
           [goog.events EventType]
           [goog.net XhrIo]
           goog.i18n.NumberFormat
           goog.i18n.DateTimeFormat))

(def small-string 50)
(def string 100)
(def big-string 500)
(def text 9999)
(def decimal 15)
(def integer 10)

(def css-trans-group (-> js/React
                         (aget "addons")
                         (aget "CSSTransitionGroup")))

(def timeout-ms 30000)

(def err-msgs {0 (t :default/cannot-find-server)
               6 (t :default/server-not-responding)
               400 (t :default/bad-request)
               -1 (t :default/error-ocurred)})

(def ^:private meths
  {:get "GET"
   :put "PUT"
   :post "POST"
   :delete "DELETE"})

(def return-nil (fn [& _] nil))

(def browser-supports-date-input? (.-inputDate js/BrowserFeatures))

(defn hide [hide]
  (if hide "hidden" ""))

(defn error? [res]
  (contains? res :error))

(defn format-currency [n]
  (let [nf (NumberFormat. NumberFormat.Format.DECIMAL)]
    (.setMinimumFractionDigits nf 2)
    (.format nf (js/Number. n))))

(defn format-quantity [n]
  (.format (NumberFormat. NumberFormat.Format.DECIMAL) (js/Number. n)))

(defn format-datetime [format value]
  (.format (DateTimeFormat. format) value))

(defn format-formdata [data]
  (let [form (js/FormData.)]
    (doseq [value data]
      (.append form (name (key value)) (val value)))
    form))

(defn asort
  "Same as select-keys but keeps order."
  [m order]
  (let [order-map (apply hash-map (interleave order (range)))]
    (conj
     (sorted-map-by #(compare (order-map %1) (order-map %2))) ; empty map with the desired ordering
     (select-keys m order))))

(defn edn-xhr [{:keys [method url data edn-parse? formdata? on-success on-error]}]
  (let [xhr (XhrIo.)]
    (events/listen xhr goog.net.EventType.SUCCESS
                   (fn [e]
                     (let [resp (.getResponseText xhr)]
                       (on-success (if (not (empty? resp))
                                     (if-not edn-parse? resp (reader/read-string resp))
                                     true)))))
    (events/listen xhr goog.net.EventType.ERROR
                   (fn [e]
                     (let [status (.getStatus xhr)
                           text (.getResponseText xhr)
                           body (if (not (empty? text)) (reader/read-string text))
                           error (condp = status
                                   0 {:error {:status status :body (err-msgs status)}}
                                   400 {:error {:status status :body body}}
                                   403 {:error {:status status :body text}}
                                   422 {:error (if (:validations body)
                                                 {:status 400 :body (:validations body)}
                                                 {:status status :body text})}
                                   {:error {:status status :body (err-msgs -1)}})]
                       (on-error error)
                       (.log js/console (str "Error " status ": " text)))))
    (events/listen xhr goog.net.EventType.TIMEOUT
                   (fn [e]
                     (on-error {:error {:status 6 :body (err-msgs 6)}})
                     (.log js/console (str "Error: 6: Request timeout."))))
    (. xhr (setTimeoutInterval timeout-ms))
    (let [data (when data
                 (if formdata? (format-formdata data) (pr-str data)))]
      (. xhr
         (send url (meths method) data
               (if-not formdata? #js {"Content-Type" "application/edn"}))))))

(defn send-request [method url & {:keys [data formdata? edn-parse?]}]
  (let [c (chan)]
    (edn-xhr {:method method
              :url url
              :data data
              :edn-parse? edn-parse?
              :formdata? formdata?
              :on-success #(put! c %)
              :on-error #(put! c %)})
    c))
