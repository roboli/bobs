(ns bobs.widgets.utils
  (:require [om.dom :as dom :include-macros true]))

(defn alert-danger [text owner]
  (dom/div #js {:className "alert alert-danger"} text))
