(ns bobs.widgets.tables
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [clojure.string :as str]
            [cljs.core.async :refer [put!]]
            [goog.string :as gstring]
            [goog.string.format]))

(defn render-type [type value]
  (condp = type
    :checkbox (dom/input #js {:type "checkbox"
                              :checked (boolean value)
                              :disabled true})
    :image (if (:src value)
             (dom/img #js {:src (:src value)
                           :className "img-thumbnail"
                           :height "100"
                           :width "100"})
             (dom/span #js {:className "img-thumbnail"
                            :style #js {:width "100px"
                                        :text-align "center"
                                        :line-height "50px"}} (:text value)))
    value))

(defn table-row [cursor owner {:keys [id-key types display] :as opts}]
  (reify
    om/IRenderState
    (render-state [_ {:keys [command-columns]}]
      (apply dom/tr nil
             (concat
              (into [] (for [v (select-keys cursor display)]
                         (dom/td #js {:style #js {:vertical-align "middle"}}
                                 (render-type (:type ((key v) types)) (val v)))))
              (for [cmd command-columns]
                (if-not (= ((key cmd) cursor) :none)
                  (let [col (val cmd)]
                    (dom/td #js {:style #js {:vertical-align "middle"}}
                            (dom/a #js {:href "#"
                                        :title (:title col)
                                        :onClick (fn [e]
                                                   (.preventDefault e)
                                                   (put! (:cmd-chan col) (get cursor id-key)))}
                                   (dom/span #js {:className (:icon col)}))))
                  (dom/td nil))))))))

(defn table-header [values owner {:keys [capitalize] :as opts}]
  (reify
    om/IRenderState
    (render-state [_ {:keys [command-columns]}]
      (apply dom/tr nil
             (concat
              (into [] (for [v values]
                         (dom/th nil (if capitalize (str/capitalize v) v))))
              (if command-columns
                (repeat (count command-columns)
                        (dom/th nil))))))))

(defn table-mobile-details [cursor owner {:keys [headers records types display capitalize row-alt] :as opts}]
  (reify
    om/IRender
    (render [_]
      (dom/tr nil
              (apply dom/td #js {:style #js {:width "10%"}} ;; This is a quirck!
                     (let [details (for [r cursor]
                                     (apply dom/table #js {:className "table"}
                                            (map (fn [h v]
                                                   (dom/tr nil
                                                           (dom/td nil (if capitalize (str/capitalize h) h))
                                                           (dom/td nil
                                                                   (render-type (:type ((key v) types)) (val v)))))
                                                 headers
                                                 (select-keys r display))))]
                       (if row-alt
                         (assoc (vec details) (:index row-alt)
                                (apply dom/table #js {:className "table"}
                                       (map (fn [h k]
                                              (dom/tr nil
                                                      (dom/td nil (if capitalize (str/capitalize h) h))
                                                      (dom/td nil (render-type (:type (get types k)) (get (nth cursor (:index row-alt)) k)))))
                                            (:labels row-alt)
                                            (:display row-alt))))
                         details)))))))

(defn table [_ owner {:keys [headers] :as opts}]
  (reify
    om/IRenderState
    (render-state [_ state]
      (let [records (:records state)
            pagination (:pagination state)
            page (:page pagination)
            page-size (:page-size pagination)
            total-records (:total-records pagination)
            first-record (- (* page page-size) (- page-size 1))
            last-record (if (= page (:total-pages pagination)) total-records (* page page-size))]
        (if (= total-records 0)
          (dom/table #js {:key "table"
                          :className "table"}
                     (dom/tbody nil
                                (dom/tr nil
                                        (dom/td nil (or (:no-records opts) "No records found")))))
          (let [row (reduce #(assoc %1 (key %2) (name (key %2))) {} (first records))
                headers (merge row headers)
                headers (select-keys headers (or (:display opts) (keys row)))]
            (dom/table #js {:key "table"
                            :className "table table-striped"}
                       (dom/thead #js {:className "hidden-xs"}
                                  (om/build table-header (vals headers) {:state {:command-columns (:command-columns state)}
                                                                         :opts {:capitalize (:capitalize opts)}}))
                       (apply dom/tbody #js {:className "hidden-xs"}
                              (om/build-all table-row records {:state {:command-columns (:command-columns state)}
                                                               :opts {:id-key (:id-key opts)
                                                                      :types (:types opts)
                                                                      :display (or (:display opts) (keys row))}}))
                       (dom/tbody #js {:className "visible-xs"}
                                  (om/build table-mobile-details records {:opts {:headers (vals (or (:headers-alt opts) headers))
                                                                                 :capitalize (:capitalize opts)
                                                                                 :types (:types opts)
                                                                                 :display (or (:display-alt opts) (:display opts) (keys row))
                                                                                 :row-alt (:display-row-alt opts)}}))
                       (if (:pager opts)
                         (dom/tfoot nil
                                    (dom/tr nil
                                            (dom/td #js {:colSpan (+ (count headers) 1)}
                                                    (dom/div #js {:className "pull-right hidden-xs"}
                                                             (dom/small nil
                                                                        (if (:pages-summary opts)
                                                                          (gstring/format (:pages-summary opts) first-record last-record total-records)
                                                                          (str first-record " - " last-record " of " total-records))))
                                                    (dom/div #js {:className "text-center visible-xs"}
                                                             (dom/small nil
                                                                        (if (:pages-summary opts)
                                                                          (gstring/format (:pages-summary opts) first-record last-record total-records)
                                                                          (str first-record " - " last-record " of " total-records))))
                                                    (if (> (:total-records pagination) (first (:page-sizes opts)))
                                                      (om/build (:pager opts) nil {:state {:page page
                                                                                           :page-size page-size
                                                                                           :total-pages (:total-pages pagination)}
                                                                                   :opts {:page-sizes (:page-sizes opts)
                                                                                          :pager-size (:pager-size opts)
                                                                                          :per-page-label (:per-page-label opts)
                                                                                          :change-page-chan (:change-page-chan opts)}})))))))))))))

(defn table-row-view [cursor owner {:keys [id-key header-key header-legend header-legend-keys labels image-key] :as opts}]
  (reify
    om/IRender
    (render [_]
      (let [row (reduce #(assoc %1 (key %2) (name (key %2))) {} cursor)
            labels (merge row labels)
            labels (select-keys labels (or (:display opts) (keys row)))
            on-click (fn [e]
                       (.preventDefault e)
                       (put! (:selected-chan opts) (id-key cursor)))]
        (dom/tr nil
                (dom/td nil
                        (dom/div #js {:className "row"}
                                 (apply dom/div #js {:className "col-md-6 col-xs-12"}
                                        (apply concat
                                               [(dom/h4 nil
                                                        (dom/a #js {:href "#"
                                                                    :onClick on-click}
                                                               (if (and header-legend header-legend-keys)
                                                                 (apply gstring/format (:header-legend opts) (map cursor header-legend-keys))
                                                                 (header-key cursor))))]
                                               (let [fields (select-keys cursor (keys labels))
                                                     last (last fields)]
                                                 (for [field fields]
                                                   (list
                                                    (dom/div #js {:className "row hidden-xs"
                                                                  :style #js {:margin-left "0.5em"
                                                                              :border-bottom (if-not (= field last) "1px solid #eee")}}
                                                             (dom/div #js {:className "col-md-4"}
                                                                      (dom/small nil
                                                                                 (dom/label nil (let [value ((key field) labels)]
                                                                                                  (if (:capitalize opts) (str/capitalize value) value)))))
                                                             (dom/div #js {:className "col-md-8"}
                                                                      (dom/small nil
                                                                                 (dom/span nil (render-type (:type ((key field) (:types opts))) (val field))))))
                                                    (dom/div #js {:className "row visible-xs"}
                                                             (dom/div #js {:className "col-xs-4 text-right"}
                                                                      (dom/small nil
                                                                                 (dom/label nil (let [value ((key field) labels)]
                                                                                                   (if (:capitalize opts) (str/capitalize value) value)))))
                                                             (dom/div #js {:className "col-xs-8"}
                                                                      (dom/small nil
                                                                                 (dom/span nil (render-type (:type ((key field) (:types opts))) (val field)))))))))))
                                 (dom/div #js {:className "col-md-4 col-xs-12"}
                                          (when (:show-image opts)
                                            (if (image-key cursor)
                                              (dom/a #js {:href "#"
                                                          :onClick on-click
                                                          :className "thumbnail"
                                                          :style #js {:marginTop "1em"}}
                                                     (dom/img #js {:src (str (gstring/format (:image-src opts) ((or (:id-key-image opts) id-key) cursor)) "&name=" (image-key cursor))}))
                                              (dom/a #js {:href "#"
                                                          :onClick on-click
                                                          :className "thumbnail"
                                                          :style #js {:marginTop "1em"
                                                                      :text-align "center"
                                                                      :line-height "140px"}} (or (:image-not-available-msg opts) "Image not available"))))))))))))

(defn table-view [_ owner opts]
  (reify
    om/IRenderState
    (render-state [_ state]
      (let [records (:records state)
            pagination (:pagination state)
            page (:page pagination)
            page-size (:page-size pagination)
            total-records (:total-records pagination)
            first-record (- (* page page-size) (- page-size 1))
            last-record (if (= page (:total-pages pagination)) total-records (* page page-size))]
        (if (= total-records 0)
          (dom/table #js {:key "table"
                          :className "table"}
                     (dom/tbody nil
                                (dom/tr nil
                                        (dom/td nil (or (:no-records opts) "No records found")))))
          (dom/table #js {:key "table"
                          :className "table"}
                     (apply dom/tbody nil
                            (om/build-all table-row-view records {:opts opts}))
                     (if (:pager opts)
                       (dom/tfoot nil
                                  (dom/tr nil
                                          (dom/td nil
                                                  (dom/div #js {:className "pull-right hidden-xs"}
                                                           (dom/small nil
                                                                      (if (:pages-summary opts)
                                                                        (gstring/format (:pages-summary opts) first-record last-record total-records)
                                                                        (str first-record " - " last-record " of " total-records))))
                                                  (dom/div #js {:className "text-center visible-xs"}
                                                           (dom/small nil
                                                                      (if (:pages-summary opts)
                                                                        (gstring/format (:pages-summary opts) first-record last-record total-records)
                                                                        (str first-record " - " last-record " of " total-records))))
                                                  (if (> (:total-records pagination) (first (:page-sizes opts)))
                                                    (om/build (:pager opts) nil {:state {:page page
                                                                                         :page-size page-size
                                                                                         :total-pages (:total-pages pagination)}
                                                                                 :opts {:page-sizes (:page-sizes opts)
                                                                                        :pager-size (:pager-size opts)
                                                                                        :per-page-label (:per-page-label opts)
                                                                                        :change-page-chan (:change-page-chan opts)}}))))))))))))
