(ns bobs.widgets.modals
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [put! chan pub sub]]
            [jayq.core :refer [$]]
            [bobs.widgets.tables :refer [table]]))

(defn listen-display [id p]
  (let [c (chan)]
    (sub p :modal c)
    (go (while true
          (if (:show (<! c))
            (do (-> ($ (str "#" id))
                    (.modal #js {:backdrop "static"})
                    (.on "shown.bs.modal" #(.focus ($ "#ok_modal")))))
            (-> ($ (str "#" id))
                (.modal "hide")))))))

(defn listen-print-display [owner id p finish-print]
  (let [c (chan)]
    (sub p :modal c)
    (go (while true
          (let [data (<! c)]
            (if (:show data)
              (do (finish-print false)
                  (om/set-state! owner :html (:content data))
                  (-> ($ (str "#" id))
                      (.modal #js {:backdrop "static"})))
              (do (finish-print true)
                  (-> ($ (str "#" id))
                      (.modal "hide")))))))))

(defn listen-table-display [owner id p]
  (let [c (chan)]
    (sub p :modal c)
    (go (while true
          (let [data (<! c)]
            (if (:show data)
              (do (om/set-state! owner :table (:table data))
                  (-> ($ (str "#" id))
                      (.modal #js {:backdrop "static"})))
              (-> ($ (str "#" id))
                  (.modal "hide"))))))))

(defn confirm-modal [cursor owner {:keys [id buttons-labels modal-chan pub-chan] :as opts}]
  (reify
    om/IWillMount
    (will-mount [_]
      (listen-display id pub-chan))
    
    om/IRender
    (render [_]
      (dom/div #js {:id id
                    :className "modal fade"
                    :role "modal"}
               (dom/div #js {:className "modal-dialog"}
                        (dom/div #js {:className "modal-content"}
                                 (dom/div #js {:className "modal-header"}
                                          (dom/h4 #js {:className "modal-title"} (:title opts)))
                                 (dom/div #js {:className "modal-body"}
                                          (dom/p nil (:text opts)))
                                 (dom/div #js {:className "modal-footer"}
                                          (dom/button #js {:className "btn btn-primary"
                                                           :id "ok_modal"
                                                           :onClick (fn [_]
                                                                      (go (>! modal-chan {:modal :confirm
                                                                                          :result true})
                                                                          (>! modal-chan {:modal :modal
                                                                                          :show false})))} (or (:ok buttons-labels) "Ok"))
                                          (dom/button #js {:className "btn btn-default"
                                                           :onClick (fn [_]
                                                                      (go (>! modal-chan {:modal :confirm
                                                                                          :result false})
                                                                          (>! modal-chan {:modal :modal
                                                                                          :show false})))} (or (:cancel buttons-labels) "Cancel")))))))))

(defn loading-modal [cursor owner {:keys [id modal-chan pub-chan] :as opts}]
  (reify
    om/IWillMount
    (will-mount [_]
      (listen-display id pub-chan))
    
    om/IRender
    (render [_]
      (dom/div #js {:id id
                    :className "modal fade"
                    :role "modal"}
               (dom/div #js {:className "modal-dialog modal-sm"}
                        (dom/div #js {:className "modal-content"}
                                 (dom/div #js {:className "modal-body"}
                                          (dom/p #js {:className "text-center"}
                                                 (dom/span nil (str (:text opts) "   "))
                                                 (dom/img #js {:src "images/ajax-loader.gif"})))))))))

(defn image-modal [cursor owner {:keys [id buttons-labels modal-chan pub-chan] :as opts}]
  (reify
    om/IWillMount
    (will-mount [_]
      (listen-display id pub-chan))
    
    om/IRenderState
    (render-state [_ state]
      (dom/div #js {:id id
                    :className "modal fade"
                    :role "modal"}
               (dom/div #js {:className "modal-dialog modal-lg modal-sm"}
                        (dom/div #js {:className "modal-content"}
                                 (dom/div #js {:className "modal-header"}
                                          (dom/h4 #js {:className "modal-title"} (:title state)))
                                 (dom/div #js {:className "modal-body"}
                                          (dom/img #js {:className "img-responsive"
                                                        :src (:src state)}))
                                 (dom/div #js {:className "modal-footer"}
                                          (dom/button #js {:className "btn btn-primary"
                                                           :onClick (fn [_]
                                                                      (go (>! modal-chan {:modal :modal
                                                                                          :show false})))} (or (:close buttons-labels) "Close")))))))))

(defn print-modal [cursor owner {:keys [id buttons-labels modal-chan pub-chan] :as opts}]
  (reify
    om/IInitState
    (init-state [_]
      {:html nil})

    om/IWillMount
    (will-mount [_]
      (listen-print-display owner id pub-chan (:finish-print opts)))

    om/IRenderState
    (render-state [_ state]
      (dom/div #js {:id id
                    :className "modal fade"
                    :role "modal"}
               (dom/div #js {:className "modal-dialog modal-lg"}
                        (dom/div #js {:className "modal-content"}
                                 (dom/div #js {:className "modal-header hidden-print"}
                                          (dom/div #js {:className "row"}
                                                   (dom/div #js {:className "col-md-2"}
                                                            (dom/h4 #js {:className "modal-title"} (or (:title opts) "Print")))
                                                   (dom/div #js {:className "col-md-offset-9 col-md-1"
                                                                 :title (or (:title opts) "Print")}
                                                            (dom/button #js {:className "btn btn-default glyphicon glyphicon-print"
                                                                             :onClick (fn [_] (.print js/window))}))))
                                 (dom/div #js {:className "modal-body"}
                                          (dom/div #js {:className "visible-print-block"
                                                        :dangerouslySetInnerHTML #js {:__html (:html state)}}))
                                 (dom/div #js {:className "modal-footer hidden-print"}
                                          (dom/button #js {:className "btn btn-primary"
                                                           :onClick (fn [_]
                                                                      (go (>! modal-chan {:modal :modal
                                                                                          :show false})))} (or (:close buttons-labels) "Close")))))))))

(defn table-modal [cursor owner {:keys [id buttons-labels modal-chan pub-chan] :as opts}]
  (reify
    om/IInitState
    (init-state [_]
      {:records nil})

    om/IWillMount
    (will-mount [_]
      (listen-table-display owner id pub-chan))
    
    om/IRenderState
    (render-state [_ state]
      (dom/div #js {:id id
                    :className "modal fade"
                    :role "modal"}
               (dom/div #js {:className "modal-dialog"}
                        (dom/div #js {:className "modal-content"}
                                 (dom/div #js {:className "modal-header"}
                                          (dom/h4 #js {:className "modal-title"} (:title opts)))
                                 (dom/div #js {:className "modal-body"}
                                          (om/build table nil {:state {:records (get-in state [:table :records])
                                                                       :command-columns (get-in state [:table :command-columns])}
                                                               :opts {:display (get-in state [:table :display])
                                                                      :headers (get-in state [:table :headers])
                                                                      :id-key (get-in state [:table :id-key])}}))
                                 (dom/div #js {:className "modal-footer"}
                                          (dom/button #js {:className "btn btn-primary"
                                                           :onClick (fn [_]
                                                                      (go (>! modal-chan {:modal :modal
                                                                                          :show false})))} (or (:close buttons-labels) "Close")))))))))
