(ns bobs.widgets.form
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [put! chan pub]]
            [clojure.set :as cljset]
            [clojure.string :as str]
            [bobs.utils :refer [hide asort]]
            [bobs.widgets.input-controls :refer [text checkbox select image]]
            [goog.string :as gstring]
            [goog.string.format]
            [valip.core :as valip]))

(defn reset-validations [owner]
  (fn [& [errors]]
    (if-not (nil? errors)
      (do (om/set-state! owner :hints (reduce #(assoc %1 (key %2) (first (val %2))) {} errors))
          (om/set-state! owner :validation-state (reduce #(assoc %1 %2 :on-error) {} (keys errors))))
      (do (om/set-state! owner :hints {})
          (om/set-state! owner :validation-state {})))))

(defn listen-on-edit [owner rules aliases]
  (let [c1 (om/get-state owner :on-blur-chan)
        c2 (om/get-state owner :on-change-chan)]
    (go (while true
          (let [[v ch] (alts! [c1 c2])
                k (apply key v)
                errors (apply (partial valip/validate v) (k rules))
                k (or (k aliases) k)]
            (if-not (empty? errors)
              (do (om/update-state! owner :hints (fn [xs] (assoc xs k (ffirst (vals errors)))))
                  (om/update-state! owner :validation-state (fn [xs] (assoc xs k :on-error))))
              (do (om/update-state! owner :hints (fn [xs] (dissoc xs k)))
                  (om/update-state! owner :validation-state (fn [xs]
                                                              (dissoc xs k)
                                                              (assoc xs k :on-success))))))))))

(defn controls [_ owner {:keys [id-key buttons-labels reset-validations] :as opts}]
  (reify
    om/IRenderState
    (render-state [_ {:keys [status record]}]
      (let [viewing (= status :viewing)
            adding (= status :adding)
            editing (= status :editing)
            btn "button"
            smt "submit"
            handle-response #(if-not (:sucess %)
                               (when-let [errors (if (coll? (:errors %)) (:errors %))]
                                 (let [errors (cljset/rename-keys errors (:error-key-aliases opts))]
                                   (reset-validations errors)
                                   (put! (:widgets-chan opts) {:input-controls :focus :key (key (first (select-keys errors (:focus-error-order opts))))})))
                               (reset-validations))]
        (condp = status
          :viewing (dom/div #js {:className "btn-group pull-right"}
                            (dom/button #js {:className "btn btn-default glyphicon glyphicon-pencil"
                                             :type btn
                                             :title (or (:edit buttons-labels) "Edit")
                                             :onClick #(put! (:edit-chan opts) true)})
                            (dom/button #js {:className "btn btn-default glyphicon glyphicon-trash"
                                             :type btn
                                             :title (or (:delete buttons-labels) "Delete")
                                             :onClick (fn [_]
                                                        (let [c (:on-delete-chan opts)]
                                                          (go (>! c true)
                                                              (when (<! c)
                                                                (>! (:delete-chan opts) (id-key record))
                                                                (>! (:back-chan opts) true)))))})
                            (dom/button #js {:className "btn btn-default glyphicon glyphicon-share-alt"
                                             :type btn
                                             :title (or (:back buttons-labels) "Go back")
                                             :onClick #(put! (:back-chan opts) true)}))
          :adding (dom/div #js {:className "btn-group pull-right"}
                           (dom/button #js {:className "btn btn-default glyphicon glyphicon-floppy-disk"
                                            :type (if adding smt btn)
                                            :title (or (:save buttons-labels) "Save")
                                            :onClick (fn [e]
                                                       (.preventDefault e)
                                                       (let [c (:insert-chan opts)]
                                                         (go (>! c record)
                                                             (handle-response (<! c)))))})
                           (dom/button #js {:className "btn btn-default glyphicon glyphicon-remove"
                                            :type btn
                                            :title (or (:discard buttons-labels) "Discard")
                                            :onClick #((:discard-add opts))}))
          :editing (dom/div #js {:className "btn-group pull-right"}
                            (dom/button #js {:className "btn btn-default glyphicon glyphicon-floppy-disk"
                                             :type (if editing smt btn)
                                             :title (or (:save buttons-labels) "Save")
                                             :onClick (fn [e]
                                                        (.preventDefault e)
                                                        (let [c (:update-chan opts)]
                                                          (go (>! c record)
                                                              (handle-response (<! c)))))})
                            (dom/button #js {:className "btn btn-default glyphicon glyphicon-remove"
                                             :type btn
                                             :title (or (:discard buttons-labels) "Discard")
                                             :onClick #((:discard-edit opts))}))
          :none (dom/div nil))))))

(defn form [{:keys [status]} owner {:keys [types] :as opts}]
  (reify
    om/IInitState
    (init-state [_]
      (let [widgets-chan (chan)]
        {:validation-state {}
         :hints {}
         :error-key-aliases (reduce (fn [m t]
                                      (merge m
                                             (when (:error-key-aliases (val t))
                                               (reduce #(assoc %1 %2 (key t))
                                                       {}
                                                       (:error-key-aliases (val t))))))
                                    {}
                                    types)
         :widgets-chan widgets-chan
         :widgets-pub-chan (pub widgets-chan :input-controls)
         :on-blur-chan (chan)
         :on-change-chan (chan)}))

    om/IWillMount
    (will-mount [_]
      (listen-on-edit owner (:validate-rules opts) (om/get-state owner :error-key-aliases)))

    om/IWillReceiveProps
    (will-receive-props [_ props]
      (if-not (= status (:status props))
        (om/set-state! owner :record (into {} (:selected props)))))

    om/IRenderState
    (render-state [_ {:keys [record validation-state hints widgets-pub-chan] :as state}]
      (let [reset-validations (reset-validations owner)
            discard-add (fn [] (put! (:back-chan opts) true))
            discard-edit (fn []
                           (reset-validations)
                           (put! (:refresh-view-chan opts) ((:id-key opts) record)))]
        (dom/form #js {:key "form"
                       :className "form-horizontal"
                       :role "form"}
                  (dom/div #js {:className "row"}
                           (dom/div #js {:className "col-md-12 col-xs-12"
                                         :style #js {:border-bottom "1px solid #eee"
                                                     :padding-bottom "1em"
                                                     :margin-bottom "2em"}}
                                    (dom/div #js {:className "row"}
                                             (dom/div #js {:className "col-md-9 hidden-xs"}
                                                      (dom/h5 #js {:className "text-muted"}
                                                              (dom/em nil (condp = status
                                                                            :viewing (or (:viewing (:status-labels opts)) "Viewing...")
                                                                            :adding (or (:adding (:status-labels opts) "Adding..."))
                                                                            :editing (or (:editing (:status-labels opts) "Editing.."))))))
                                             (dom/div #js {:className "col-md-3 hidden-xs"}
                                                      (om/build controls nil {:state {:status status
                                                                                      :record record}
                                                                              :opts (merge opts {:widgets-chan (:widgets-chan state)
                                                                                                 :discard-add discard-add
                                                                                                 :discard-edit discard-edit
                                                                                                 :reset-validations reset-validations
                                                                                                 :focus-error-order (:display opts)
                                                                                                 :error-key-aliases (:error-key-aliases state)})}))
                                             (dom/div #js {:className "col-xs-12 visible-xs"}
                                                      (dom/button #js {:className "btn btn-default btn-block glyphicon glyphicon-share-alt"
                                                                       :type "button"
                                                                       :title (or (get-in opts [:buttons-labels :back]) "Back")
                                                                       :onClick #(put! (:back-chan opts) true)})))))
                  (dom/div #js {:className "row"}
                           (dom/div #js {:className "col-md-offset-1 col-md-11"}
                                    (apply dom/fieldset #js {:disabled (= status :viewing)}
                                           (for [field (asort record (or (:display opts) (keys record)))]
                                             (let [k (key field)
                                                   v (val field)
                                                   id (name k)
                                                   label (or (k (:labels opts)) id)
                                                   label (if (:capitalize opts) (str/capitalize label) label)
                                                   disabled (or (:readonly (k types))
                                                                (condp = status
                                                                  :adding (if (:edit-on-adding? (k types))
                                                                            (not ((:edit-on-adding? (k types)) record))
                                                                            false)
                                                                  :editing (if (:edit-on-editing? (k types))
                                                                             (not ((:edit-on-editing? (k types)) record))
                                                                             false)
                                                                  false))
                                                   on-change (fn [value]
                                                               (om/set-state! owner [:record k] value)
                                                               (if (:put-on-change? opts)
                                                                 (put! (:on-change-chan state) (hash-map k value))))
                                                   on-blur (fn [value]
                                                             (put! (:on-blur-chan state) (hash-map k value)))
                                                   on-key-down (fn [value]
                                                                 (when (= value 27)
                                                                   (if (= status :adding) (discard-add) (discard-edit))))]
                                               (condp = (:type (k types))
                                                 :checkbox (om/build checkbox nil {:state {:status status
                                                                                           :value v
                                                                                           :disabled disabled}
                                                                                   :opts {:id id
                                                                                          :label label
                                                                                          :is-focus (= k (:focus opts))
                                                                                          :on-change on-change
                                                                                          :on-blur on-blur}})
                                                 :select (let [selected-key (:selected-key (k types))]
                                                           (om/build select nil {:state {:status status
                                                                                         :validation-state (k validation-state)
                                                                                         :hint (k hints)
                                                                                         :value v
                                                                                         :disabled disabled
                                                                                         :selected-value (selected-key record)
                                                                                         :optional (if (and (not= status :viewing) (nil? (k hints))) (some #{selected-key} (:optionals opts)))}
                                                                                 :opts {:id id
                                                                                        :label label
                                                                                        :text-key (:text-key (k types))
                                                                                        :value-key (:value-key (k types))
                                                                                        :optional-label (:optional-label opts)
                                                                                        :is-focus (= k (:focus opts))
                                                                                        :on-key-down on-key-down
                                                                                        :on-change (fn [value]
                                                                                                     (om/set-state! owner [:record selected-key] value)
                                                                                                     (if (:put-on-change? opts)
                                                                                                       (put! (:on-change-chan state) (hash-map selected-key value))))
                                                                                        :on-blur (fn [value]
                                                                                                   (put! (:on-blur-chan state) (hash-map selected-key value)))
                                                                                        :widgets-pub-chan widgets-pub-chan}}))
                                                 :image (om/build image nil {:state {:status status
                                                                                     :has-image (:image-available state)
                                                                                     :validation-state (k validation-state)
                                                                                     :hint (k hints)
                                                                                     :optional (if (and (not= status :viewing) (nil? (k hints))) (some #{k} (:optionals opts)))}
                                                                             :opts {:id id
                                                                                    :label label
                                                                                    :no-image-label (:no-image-label opts)
                                                                                    :optional-label (:optional-label opts)
                                                                                    :on-change (fn [file]
                                                                                                 (om/set-state! owner [:record k] file)
                                                                                                 (om/set-state! owner [:record (:filename-key (k types))] (if file (.-name file))))
                                                                                    :widgets-pub-chan widgets-pub-chan}})
                                                 :custom (om/build (:control (k types)) nil {:state (assoc ((:more-state (k types)) owner)
                                                                                                      :status status
                                                                                                      :validation-state (k validation-state)
                                                                                                      :hint (k hints)
                                                                                                      :value v)
                                                                                             :opts {:on-key-down on-key-down
                                                                                                    :on-change ((:on-change (k types)) owner (if (:put-on-change? opts) (:on-change-chan state)))
                                                                                                    :on-blur on-blur
                                                                                                    :widgets-pub-chan widgets-pub-chan}})
                                                 (om/build text nil {:state {:status status
                                                                             :validation-state (k validation-state)
                                                                             :hint (k hints)
                                                                             :value v
                                                                             :disabled disabled
                                                                             :optional (if (and (not= status :viewing) (nil? (k hints))) (some #{k} (:optionals opts)))}
                                                                     :opts {:id id
                                                                            :label label
                                                                            :max-length (:max-length (k types))
                                                                            :optional-label (:optional-label opts)
                                                                            :is-focus (= k (:focus opts))
                                                                            :on-key-down on-key-down
                                                                            :on-change on-change
                                                                            :on-blur on-blur
                                                                            :widgets-pub-chan widgets-pub-chan}})))))
                                    (if (and (:show-image opts) (= status :viewing))
                                      (dom/div #js {:className "col-md-offset-2 col-md-5"}
                                               (if (:image-available state)
                                                 (dom/a #js {:href "#"
                                                             :className "thumbnail"
                                                             :style #js {:marginTop "1em"}
                                                             :onClick (fn [e]
                                                                        (.preventDefault e)
                                                                        (put! (:on-click-image-chan opts) true))}
                                                        (dom/img #js {:src (:image-src state)}))
                                                 (dom/span #js {:className "thumbnail"
                                                                :style #js {:text-align "center"
                                                                            :line-height "140px"}} (or (:image-not-available-msg opts) "Image not available"))))))))))))
