(ns bobs.widgets.form-details
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [put! chan pub sub]]
            [clojure.set :as cljset]
            [clojure.string :as str]
            [bobs.utils :refer [hide]]
            [bobs.widgets.input-controls :refer [text textarea date checkbox select text-inline select-inline autocomplete]]
            [goog.string :as gstring]
            [goog.string.format]
            [valip.core :as valip]))

(defn reset-validations [owner]
  (fn [& [errors]]
    (if-not (nil? errors)
      (do (om/set-state! owner :hints (reduce #(assoc %1 (key %2) (first (val %2))) {} errors))
          (om/set-state! owner :validation-state (reduce #(assoc %1 %2 :on-error) {} (keys errors))))
      (do (om/set-state! owner :hints {})
          (om/set-state! owner :validation-state {})))))

(defn listen-on-edit [owner rules aliases]
  (let [c1 (om/get-state owner :on-blur-chan)
        c2 (om/get-state owner :on-change-chan)]
    (go (while true
          (let [[v ch] (alts! [c1 c2])
                k (apply key v)
                errors (apply (partial valip/validate v) (k rules))
                k (or (k aliases) k)]
            (if-not (empty? errors)
              (do (om/update-state! owner :hints (fn [xs] (assoc xs k (ffirst (vals errors)))))
                  (om/update-state! owner :validation-state (fn [xs] (assoc xs k :on-error))))
              (do (om/update-state! owner :hints (fn [xs] (dissoc xs k)))
                  (om/update-state! owner :validation-state (fn [xs]
                                                              (dissoc xs k)
                                                              (assoc xs k :on-success))))))))))

(defn inline-reset-validations [owner ks]
  (fn [& [error]]
    (if-not (nil? error)
      (do (om/update-state! owner :hints (fn [xs] (assoc (apply dissoc xs ks) (key error) (first (val error)))))
          (om/update-state! owner :validation-state (fn [xs] (assoc (apply dissoc xs ks) (key error) :on-error))))
      (do (om/update-state! owner :hints (fn [xs] (apply dissoc xs ks)))
          (om/update-state! owner :validation-state (fn [xs] (apply dissoc xs ks)))))))

(defn listen-focus [owner pub widgets-chan error-key id-key]
  (let [c (chan)]
    (sub pub :focus c)
    (go (while true
          (if (= error-key (:key (<! c)))
            (put! widgets-chan {:input-controls :focus :key id-key}))))))

(defn listen-submit [owner opts reset-validations]
  (let [c (om/get-state owner :submit-chan)]
    (go (while true
          (<! c)
          (let [record (om/get-state owner :record)
                add-chan (:add-chan opts)]
            (>! add-chan record)
            (let [resp (<! add-chan)]
              (if (:sucess resp)
                (do (om/set-state! owner :record (into record (select-keys (:new-record opts) (:new-keys opts))))
                    (put! (:widgets-chan opts) {:input-controls :focus :key (:focus opts)})
                    (reset-validations))
                (if-let [errors (if (coll? (:errors resp)) (:errors resp))]
                  (reset-validations (first (select-keys errors (:hint-keys opts))))
                  (reset-validations)))))))))

(defn form-inline [_ owner {:keys [buttons-labels validations-owner types] :as opts}]
  (let [reset-validations (inline-reset-validations validations-owner (:hint-keys opts))]
    (reify
      om/IInitState
      (init-state [_]
        {:submit-chan (chan)})

      om/IWillMount
      (will-mount [_]
        (listen-focus owner (:widgets-pub-chan opts) (:widgets-chan opts) (:focus-global-error opts) (:focus opts))
        (listen-submit owner opts reset-validations))
      
      om/IDidUpdate
      (did-update [_ _ prev]
        (let [validation-state (om/get-state owner :validation-state)]
          (if-not (= validation-state (:validation-state prev))
            (if-let [error (first (select-keys validation-state (:hint-keys opts)))]
              (let [error (key error)
                    error (get (:error-key-aliases opts) error error)]
                (if-not (= error (:focus-global-error opts))
                  (put! (:widgets-chan opts) {:input-controls :focus :key error})))))))

      om/IRenderState
      (render-state [_ {:keys [record validation-state] :as state}]
        (let [error-key (first (select-keys validation-state (:hint-keys opts)))
              form-hint (if error-key ((key error-key) (:hints state)))]
          (dom/div {:className "row"}
                   (dom/p #js {:className "text-danger text-center"}
                          (dom/small nil form-hint))
                   (apply dom/div #js {:className "form-inline center-block"
                                       :style #js {:width "90%"
                                                   :margin-top (if form-hint "0" "2em")
                                                   :margin-bottom "2em"
                                                   :padding-top "1em"
                                                   :padding-bottom "1em"
                                                   :border-top "1px solid #eee"
                                                   :border-bottom "1px solid #eee"}}
                          (conj
                           (into [] (for [field (select-keys record (or (:display opts) (keys record)))]
                                      (let [k (key field)
                                            id (name k)
                                            label (or (k (:labels opts)) id)
                                            label (if (:capitalize opts) (str/capitalize label) label)
                                            disabled (:readonly (k types))
                                            on-error? (and error-key (= (key error-key) k))
                                            on-change #(om/set-state! owner [:record k] %)
                                            on-blur #(if on-error? (reset-validations))
                                            on-key-down #(if (= % 13) (put! (:submit-chan state) true))]
                                        (condp = (:type (k types))
                                          :search (om/build autocomplete nil {:state {:value (get-in state [:record k])
                                                                                      :on-error? on-error?}
                                                                              :opts {:id id
                                                                                     :label label
                                                                                     :size (:size (k types))
                                                                                     :cols (:cols (k types))
                                                                                     :on-change on-change
                                                                                     :on-blur on-blur
                                                                                     :fetch (let [c ((:fetch-chan (k types)) owner)]
                                                                                              (fn [v]
                                                                                                (go (>! c  v)
                                                                                                    (<! c))))
                                                                                     :fetch-add (let [c ((:fetch-chan (k types)) owner)]
                                                                                                  (fn [v]
                                                                                                    (go (>! c v)
                                                                                                        (<! c)
                                                                                                        (>! (:submit-chan state) true))))
                                                                                     :suggestions-fn (:suggestions-fn (k types))
                                                                                     :text-fn (:text-fn (k types))
                                                                                     :result-fn ((:result-fn (k types)) owner)
                                                                                     :widgets-pub-chan (:widgets-pub-chan opts)
                                                                                     :loading-pub-chan (:loading-pub-chan (k types))}})
                                          :select (let [selected-key (:selected-key (k types))
                                                        on-error? (and error-key (= (key error-key) selected-key))]
                                                    (om/build select-inline nil {:state {:value (get-in state [:record k])
                                                                                         :on-error? on-error?
                                                                                         :disabled disabled
                                                                                         :selected-value (selected-key record)}
                                                                                 :opts {:id id
                                                                                        :label label
                                                                                        :size (:size (k types))
                                                                                        :cols (:cols (k types))
                                                                                        :text-key (:text-key (k types))
                                                                                        :value-key (:value-key (k types))
                                                                                        :empty-text (:empty-text (k types))
                                                                                        :on-key-down #()
                                                                                        :on-change (fn [value]
                                                                                                     (om/set-state! owner [:record selected-key] value))
                                                                                        :on-blur #(if on-error? (reset-validations))
                                                                                        :widgets-pub-chan (:widgets-pub-chan opts)}}))
                                          :custom (om/build (:control (k types)) nil {:state {:value (get-in state [:record k])
                                                                                              :on-error? on-error?
                                                                                              :disabled disabled}
                                                                                      :opts ((:more-opts (k types)) owner)})
                                          (om/build text-inline nil {:state {:value (get-in state [:record k])
                                                                             :on-error? on-error?
                                                                             :disabled disabled}
                                                                     :opts {:id id
                                                                            :label label
                                                                            :size (:size (k types))
                                                                            :max-length (:max-length (k types))
                                                                            :cols (:cols (k types))
                                                                            :autocomplete "off"
                                                                            :on-change on-change
                                                                            :on-blur on-blur
                                                                            :on-key-down on-key-down
                                                                            :widgets-pub-chan (:widgets-pub-chan opts)}})))))
                           (dom/div #js {:className "form-group col-md-1"}
                                    (dom/button #js {:type "submit"
                                                     :className (str "btn btn-default" (condp = (:size-add-btn opts)
                                                                                         :small " btn-sm"
                                                                                         :large " btn-lg"
                                                                                         nil))
                                                     :title (or (:add buttons-labels) "Add")
                                                     :onClick #(put! (:submit-chan state) true)}
                                                (dom/span #js {:className "glyphicon glyphicon-plus"})))
                           (dom/div #js {:className "clearfix"})))))))))

(defn controls [_ owner {:keys [id-key buttons-labels reset-validations] :as opts}]
  (reify
    om/IRenderState
    (render-state [_ {:keys [status record void]}]
      (let [viewing (= status :viewing)
            adding (= status :adding)
            editing (= status :editing)
            btn "button"
            smt "submit"
            handle-response #(if-not (:sucess %)
                               (when-let [errors (if (coll? (:errors %)) (:errors %))]
                                 (let [errors (cljset/rename-keys errors (:error-key-aliases opts))]
                                   (reset-validations errors)
                                   (put! (:widgets-chan opts) {:input-controls :focus :key (key (first (select-keys errors (:focus-error-order opts))))})))
                               (reset-validations))]
        (condp = status
          :viewing (dom/div #js {:className "btn-group pull-right"}
                            (dom/button #js {:className "btn btn-default glyphicon glyphicon-ban-circle"
                                             :type btn
                                             :title (or (:void buttons-labels) "Void")
                                             :style #js {:color (if void "red")}
                                             :disabled void
                                             :onClick (fn [e]
                                                        (let [c (:on-void-chan opts)]
                                                          (go (>! c true)
                                                              (when (<! c)
                                                                (>! (:void-chan opts) (id-key record))))))})
                            (dom/button #js {:className "btn btn-default glyphicon glyphicon-print"
                                             :type btn
                                             :title (or (:print buttons-labels) "Print")
                                             :onClick #(put! (:on-print-chan opts) (id-key record))})
                            (dom/a #js {:className "btn btn-default glyphicon glyphicon-download-alt"
                                        :title (or (:download buttons-labels) "Download")
                                        :href (:download-url opts)})
                            (dom/button #js {:className "btn btn-default glyphicon glyphicon-share-alt"
                                             :type btn
                                             :title (or (:back buttons-labels) "Back")
                                             :onClick #(put! (:back-chan opts) true)}))
          :adding (dom/div #js {:className "btn-group pull-right"}
                           (dom/button #js {:className "btn btn-default glyphicon glyphicon-floppy-disk"
                                            :type (if adding smt btn)
                                            :title (or (:save buttons-labels) "Save")
                                            :onClick (fn [e]
                                                       (.preventDefault e)
                                                       (let [c (:on-insert-chan opts)]
                                                         (go (>! c true)
                                                             (when (<! c)
                                                               (let [c (:insert-chan opts)]
                                                                 (>! c record)
                                                                 (handle-response (<! c)))))))})
                           (dom/button #js {:className "btn btn-default glyphicon glyphicon-remove"
                                            :type btn
                                            :title (or (:discard buttons-labels) "Discard")
                                            :onClick #((:discard-add opts))}))
          :none (dom/div nil))))))

(defn form [{:keys [status] :as cursor} owner {:keys [types] :as opts}]
  (reify
    om/IInitState
    (init-state [_]
      (let [widgets-chan (chan)]
        {:validation-state {}
         :hints {}
         :error-key-aliases (reduce (fn [m t]
                                      (merge m
                                             (when (:error-key-aliases (val t))
                                               (reduce #(assoc %1 %2 (key t))
                                                       {}
                                                       (:error-key-aliases (val t))))))
                                    {}
                                    types)
         :widgets-chan widgets-chan
         :widgets-pub-chan (pub widgets-chan :input-controls)
         :on-blur-chan (chan)
         :on-change-chan (chan)}))

    om/IWillMount
    (will-mount [_]
      (listen-on-edit owner (:validate-rules opts) (om/get-state owner :error-key-aliases)))

    om/IWillReceiveProps
    (will-receive-props [_ props]
      (if-not (= status (:status props))
        (om/set-state! owner :record (into {} (:selected props)))))

    om/IRenderState
    (render-state [_ {:keys [record validation-state hints widgets-pub-chan] :as state}]
      (dom/div #js {:key "form"}
               (let [discard-add (fn [] (put! (:back-chan opts) true))]
                 (dom/form #js {:className "form-horizontal"
                                :role "form"}
                           (dom/div #js {:className "row"}
                                    (dom/div #js {:className "col-md-12 col-xs-12"
                                                  :style #js {:border-bottom "1px solid #eee"
                                                              :padding-bottom "1em"
                                                              :margin-bottom "2em"}}
                                             (dom/div #js {:className "row"}
                                                      (dom/div #js {:className "col-md-9 hidden-xs"}
                                                               (dom/h5 #js {:className "text-muted"}
                                                                       (dom/em nil (condp = status
                                                                                     :viewing (or (:viewing (:status-labels opts)) "Viewing...")
                                                                                     :adding (or (:adding (:status-labels opts) "Adding..."))
                                                                                     :editing (or (:editing (:status-labels opts) "Editing.."))))))
                                                      (dom/div #js {:className "col-md-3 hidden-xs"}
                                                               (om/build controls nil {:state {:status status
                                                                                               :record record
                                                                                               :void (get-in cursor [:selected :void])}
                                                                                       :opts (merge opts {:widgets-chan (:widgets-chan state)
                                                                                                          :discard-add discard-add
                                                                                                          :reset-validations (reset-validations owner)
                                                                                                          :focus-error-order (conj
                                                                                                                              (:display opts)
                                                                                                                              (get-in opts [:form-inline :focus-global-error]))
                                                                                                          :error-key-aliases (:error-key-aliases state)})}))
                                                      (dom/div #js {:className "col-xs-12 visible-xs"}
                                                               (dom/button #js {:className "btn btn-default btn-block glyphicon glyphicon-share-alt"
                                                                                :type "button"
                                                                                :title (or (get-in opts [:buttons-labels :back]) "Back")
                                                                                :onClick #(put! (:back-chan opts) true)})))))
                           (dom/div #js {:className "row"}
                                    (dom/div #js {:className "col-md-offset-1 col-md-11"}
                                             (apply dom/fieldset #js {:disabled (= status :viewing)}
                                                    (for [field (select-keys record (or (:display opts) (keys record)))]
                                                      (let [k (key field)
                                                            v (val field)
                                                            id (name k)
                                                            label (or (k (:labels opts)) id)
                                                            label (if (:capitalize opts) (str/capitalize label) label)
                                                            disabled (or (:readonly (k types))
                                                                         (condp = status
                                                                           :adding (if (:edit-on-adding? (k types))
                                                                                     (not ((:edit-on-adding? (k types)) record))
                                                                                     false)
                                                                           :editing (if (:edit-on-editing? (k types))
                                                                                      (not ((:edit-on-editing? (k types)) record))
                                                                                      false)
                                                                           false))
                                                            on-change (fn [value]
                                                                        (om/set-state! owner [:record k] value)
                                                                        (if (:put-on-change? opts)
                                                                          (put! (:on-change-chan state) (hash-map k value))))
                                                            on-blur (fn [value]
                                                                      (put! (:on-blur-chan state) (hash-map k value)))
                                                            on-key-down (fn [value]
                                                                          (when (= value 27)
                                                                            (if (= status :adding) (discard-add))))]
                                                        (condp = (:type (k types))
                                                          :date (om/build date nil {:state {:status status
                                                                                            :validation-state (k validation-state)
                                                                                            :hint (k hints)
                                                                                            :value v
                                                                                            :disabled disabled
                                                                                            :optional (if (and (not= status :viewing) (nil? (k hints))) (some #{k} (:optionals opts)))}
                                                                                    :opts {:id id
                                                                                           :label label
                                                                                           :size (:size (k types))
                                                                                           :jquery-datepicker? (:jquery-datepicker? (k types))
                                                                                           :date-format (:date-format (k types))
                                                                                           :date-format-jquery (:date-format-jquery (k types))
                                                                                           :date-format-user (:date-format-user (k types))
                                                                                           :max-length (:max-length (k types))
                                                                                           :optional-label (:optional-label opts)
                                                                                           :is-focus (= k (:focus opts))
                                                                                           :on-key-down on-key-down
                                                                                           :on-change on-change
                                                                                           :on-blur on-blur
                                                                                           :widgets-pub-chan widgets-pub-chan}})
                                                          :checkbox (om/build checkbox nil {:state {:status status
                                                                                                    :value v
                                                                                                    :disabled disabled}
                                                                                            :opts {:id id
                                                                                                   :label label
                                                                                                   :is-focus (= k (:focus opts))
                                                                                                   :on-change on-change
                                                                                                   :on-blur on-blur}})
                                                          :select (let [selected-key (:selected-key (k types))]
                                                                    (om/build select nil {:state {:status status
                                                                                                  :validation-state (k validation-state)
                                                                                                  :hint (k hints)
                                                                                                  :value v
                                                                                                  :disabled disabled
                                                                                                  :selected-value (selected-key record)
                                                                                                  :optional (if (and (not= status :viewing) (nil? (k hints)))
                                                                                                              (some #{selected-key} (:optionals opts)))}
                                                                                          :opts {:id id
                                                                                                 :label label
                                                                                                 :size (:size (k types))
                                                                                                 :text-key (:text-key (k types))
                                                                                                 :value-key (:value-key (k types))
                                                                                                 :optional-label (:optional-label opts)
                                                                                                 :is-focus (= k (:focus opts))
                                                                                                 :on-key-down on-key-down
                                                                                                 :on-change (fn [value]
                                                                                                              (om/set-state! owner [:record selected-key] value)
                                                                                                              (if (:put-on-change? opts)
                                                                                                                (put! (:on-change-chan state) (hash-map selected-key value))))
                                                                                                 :on-blur (fn [value]
                                                                                                            (put! (:on-blur-chan state) (hash-map selected-key value)))
                                                                                                 :widgets-pub-chan widgets-pub-chan}}))
                                                          :textarea (om/build textarea nil {:state {:status status
                                                                                                    :validation-state (k validation-state)
                                                                                                    :hint (k hints)
                                                                                                    :value v
                                                                                                    :disabled disabled
                                                                                                    :optional (if (and (not= status :viewing) (nil? (k hints))) (some #{k} (:optionals opts)))}
                                                                                            :opts {:id id
                                                                                                   :label label
                                                                                                   :size (:size (k types))
                                                                                                   :max-length (:max-length (k types))
                                                                                                   :rows (:rows (k types))
                                                                                                   :optional-label (:optional-label opts)
                                                                                                   :is-focus (= k (:focus opts))
                                                                                                   :on-key-down on-key-down
                                                                                                   :on-change on-change
                                                                                                   :on-blur on-blur
                                                                                                   :widgets-pub-chan widgets-pub-chan}})
                                                          (om/build text nil {:state {:status status
                                                                                      :validation-state (k validation-state)
                                                                                      :hint (k hints)
                                                                                      :value v
                                                                                      :disabled disabled
                                                                                      :optional (if (and (not= status :viewing) (nil? (k hints))) (some #{k} (:optionals opts)))}
                                                                              :opts {:id id
                                                                                     :label label
                                                                                     :size (:size (k types))
                                                                                     :max-length (:max-length (k types))
                                                                                     :optional-label (:optional-label opts)
                                                                                     :is-focus (= k (:focus opts))
                                                                                     :on-key-down on-key-down
                                                                                     :on-change on-change
                                                                                     :on-blur on-blur
                                                                                     :widgets-pub-chan widgets-pub-chan}})))))))))
               (if-not (= status :viewing)
                 (let [opts (:form-inline opts)]
                   (om/build form-inline nil {:init-state {:record (:new-record opts)}
                                              :state {:validation-state validation-state
                                                      :hints hints}
                                              :opts {:new-record (:new-record opts)
                                                     :new-keys (:new-keys opts)
                                                     :size-add-btn (:size-add-btn opts)
                                                     :display (:display opts)
                                                     :types (:types opts)
                                                     :focus (:focus opts)
                                                     :focus-global-error (:focus-global-error opts)
                                                     :hint-keys (:hint-keys opts)
                                                     :error-key-aliases (reduce (fn [m t]
                                                                                  (merge m
                                                                                         (when (:error-key-aliases (val t))
                                                                                           (reduce #(assoc %1 %2 (key t))
                                                                                                   {}
                                                                                                   (:error-key-aliases (val t))))))
                                                                                {}
                                                                                (:types opts))
                                                     :labels (:labels opts)
                                                     :buttons-labels (:buttons-labels opts)
                                                     :validations-owner owner
                                                     :add-chan (:add-chan opts)
                                                     :widgets-chan (:widgets-chan state)
                                                     :widgets-pub-chan widgets-pub-chan}})))))))
