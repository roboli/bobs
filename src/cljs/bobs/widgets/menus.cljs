(ns bobs.widgets.menus
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [put!]]
            [clojure.string :as str]))

(def css-active "active")

(defn main-menu [active owner {:keys [title routes session logout-chan] :as opts}]
  (reify
    om/IRender
    (render [_]
      (dom/nav #js {:className "navbar navbar-default"
                    :role "navigation"}
               (dom/div #js {:className "container-fluid"}
                        (dom/div #js {:className "navbar-header"}
                                 (dom/button #js {:className "navbar-toggle collapsed"
                                                  :type "button"
                                                  :data-toggle "collapse"
                                                  :data-target "#main-menu-collapse"}
                                             (dom/span #js {:className "icon-bar"})
                                             (dom/span #js {:className "icon-bar"})
                                             (dom/span #js {:className "icon-bar"}))
                                 (dom/a #js {:className "navbar-brand"
                                             :href "#"} title))
                        (dom/div #js {:id "main-menu-collapse"
                                      :className "collapse navbar-collapse"}
                                 (apply dom/ul #js {:className "nav navbar-nav"}
                                        (concat
                                         (for [route routes]
                                           (let [v (val route)]
                                             (if (:display v)
                                               (dom/li #js {:className (if (= (key route) active) css-active)}
                                                       (dom/a #js {:href (str "#" (:route v))} (:label v))))))
                                         [(dom/li #js {:className "visible-xs"}
                                                  (dom/a #js {:onClick (fn [_] (put! logout-chan true))}
                                                         (dom/span nil "Logout")
                                                         (dom/span #js {:className "pull-right"}
                                                                   (str (get-in session [:user :username])
                                                                        "@" (get-in session [:company :alias])))))]))
                                 (dom/button #js {:type "button"
                                                  :className "btn btn-default navbar-btn navbar-right hidden-xs"
                                                  :onClick (fn [_] (put! logout-chan true))} "Logout")
                                 (dom/p #js {:className "navbar-text navbar-right hidden-xs"
                                             :style #js {:marginRight "10px;"}}
                                        (dom/span #js {:title (get-in session [:user :name])} (get-in session [:user :username]))
                                        "@"
                                        (dom/span #js {:title (get-in session [:company :name])} (get-in session [:company :alias])))))))))

(defn sub-menu [active owner {:keys [routes heading] :as opts}]
  (reify
    om/IRender
    (render [_]
      (dom/div #js {:className "panel panel-default"}
               (dom/div #js {:className "panel-heading hidden-xs"} heading)
               (dom/h4 #js {:className "text-center visible-xs"} heading)
               (dom/div #js {:className "panel-body hidden-xs"}
                        (apply dom/div #js {:className "list-group"}
                               (for [route routes]
                                 (let [v (val route)]
                                   (dom/a #js {:className (str/join " " [(if (= (key route) active) css-active) "list-group-item"])
                                               :href (str "#" (:route v))} (:label v))))))
               (apply dom/div #js {:className "list-group visible-xs"}
                      (for [route routes]
                        (let [v (val route)]
                          (dom/a #js {:className (str/join " " [(if (= (key route) active) css-active) "list-group-item"])
                                      :href (str "#" (:route v))} (:label v)))))))))
