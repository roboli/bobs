(ns bobs.widgets.input-controls
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [clojure.string :as str]
            [cljs.core.async :refer [chan sub put! timeout]]
            [arosequist.om-autocomplete :as ac]
            [arosequist.om-autocomplete.bootstrap :as ac-bootstrap]
            [jayq.core :refer [$]])
  (:import goog.i18n.DateTimeParse
           goog.i18n.DateTimeFormat))

(def ^:private sizes
  {:small "input-sm"
   :large "input-lg"})

(def ^:private btn-sizes
  {:small "btn-sm"
   :large "btn-lg"})

(def ^:private group-sizes
  {:small "input-group-sm"
   :large "input-group-lg"})

(defn listen-focus [owner pub id]
  (let [c (chan)]
    (sub pub :focus c)
    (go (while true
          (if (= id (name (:key (<! c))))
            (if-let [e (om/get-node owner id)] ; if-let because e sometimes is js null??
              (if (.-select e) (.select e) (.focus e))))))))

(defn listen-changes [owner changed-chan & [listen-every-ms]]
  (fn []
    (go (while (om/get-state owner :listening)
          (when-let [new-value (om/get-state owner :value)]
            (when-not (= new-value (om/get-state owner :old-value))
              (om/set-state! owner :old-value new-value)
              (put! changed-chan new-value)))
          (<! (timeout (or listen-every-ms 800)))))))

(defn text [_ owner {:keys [id max-length is-focus] :as opts}]
  (reify
    om/IWillMount
    (will-mount [_]
      (listen-focus owner (:widgets-pub-chan opts) id))
    
    om/IDidMount
    (did-mount [_]
      (if (and is-focus (= (om/get-state owner :status) :adding))
        (.select (om/get-node owner id))))

    om/IDidUpdate
    (did-update [_ _ previous]
      (let [status (om/get-state owner :status)]
        (if (and is-focus (not= (:status previous) status)
                 (= status :editing))
          (.select (om/get-node owner id)))))

    om/IRenderState
    (render-state [_ {:keys [validation-state hint] :as state}]
      (dom/div #js {:className (str/join " " [(condp = validation-state
                                                :on-error "has-error"
                                                :on-success "has-success"
                                                nil) "form-group"])}
               (dom/label #js {:htmlFor id
                               :className "col-md-2 control-label"} (:label opts))
               (dom/div #js {:className "col-md-6"}
                        (dom/input #js {:id id
                                        :ref id
                                        :className (str/join " " ["form-control" (get sizes (:size opts))])
                                        :type "text"
                                        :value (:value state)
                                        :disabled (:disabled state)
                                        :maxLength max-length
                                        :onKeyDown #((:on-key-down opts) (.-keyCode %))
                                        :onChange #((:on-change opts) (.. % -target -value))
                                        :onBlur #((:on-blur opts) (.. % -target -value))}))
               (dom/span #js {:className (str/join " " [(if (not hint) "hidden") "help-block"])}
                         (dom/small nil hint))
               (dom/span #js {:className (str/join " " [(if (not (:optional state)) "hidden") "help-block"])}
                         (dom/small nil (str "(" (or (:optional-label opts) "optional") ")")))))))

(defn textarea [_ owner {:keys [id max-length rows is-focus] :as opts}]
  (reify
    om/IWillMount
    (will-mount [_]
      (listen-focus owner (:widgets-pub-chan opts) id))
    
    om/IDidMount
    (did-mount [_]
      (if (and is-focus (= (om/get-state owner :status) :adding))
        (.select (om/get-node owner id))))

    om/IDidUpdate
    (did-update [_ _ previous]
      (let [status (om/get-state owner :status)]
        (if (and is-focus (not= (:status previous) status)
                 (= status :editing))
          (.select (om/get-node owner id)))))

    om/IRenderState
    (render-state [_ {:keys [validation-state hint] :as state}]
      (dom/div #js {:className (str/join " " [(condp = validation-state
                                                :on-error "has-error"
                                                :on-success "has-success"
                                                nil) "form-group"])}
               (dom/label #js {:htmlFor id
                               :className "col-md-2 control-label"} (:label opts))
               (dom/div #js {:className "col-md-6"}
                        (dom/textarea #js {:id id
                                           :ref id
                                           :className (str/join " " ["form-control" (get sizes (:size opts))])
                                           :value (:value state)
                                           :disabled (:disabled state)
                                           :maxLength max-length
                                           :rows rows
                                           :onKeyDown #((:on-key-down opts) (.-keyCode %))
                                           :onChange #((:on-change opts) (.. % -target -value))
                                           :onBlur #((:on-blur opts) (.. % -target -value))}))
               (dom/span #js {:className (str/join " " [(if (not hint) "hidden") "help-block"])}
                         (dom/small nil hint))
               (dom/span #js {:className (str/join " " [(if (not (:optional state)) "hidden") "help-block"])}
                         (dom/small nil (str "(" (or (:optional-label opts) "optional") ")")))))))

(defn text-inline [_ owner {:keys [id max-length] :as opts}]
  (reify
    om/IWillMount
    (will-mount [_]
      (listen-focus owner (:widgets-pub-chan opts) id))
    
    om/IRenderState
    (render-state [_ state]
      (dom/div #js {:className (str/join " " [(if (:on-error? state) "has-error") "form-group" (str "col-md-" (:cols opts))])
                    :style #js {:padding "0"}}
               (dom/input #js {:ref id
                               :id id
                               :type "text"
                               :value (:value state)
                               :disabled (:disabled state)
                               :maxLength max-length
                               :className (str/join " " ["form-control" (get sizes (:size opts))])
                               :placeholder (:label opts)
                               :autoComplete (or (:autocomplete opts) "on")
                               :style #js {:width "99%"}
                               :onChange #((:on-change opts) (.. % -target -value))
                               :onBlur (:on-blur opts)
                               :onKeyDown #((:on-key-down opts) (.-keyCode %))})))))

(defn text-listen [_ owner opts]
  (reify
    om/IInitState
    (init-state [_]
      {:listening false})
    
    om/IRenderState
    (render-state [_ state]
      (let [listen (listen-changes owner (:changed-chan opts) (:listen-every-ms opts))
            listening (not (empty? (:value state)))]
        (dom/div #js {:className (if listening (str/join " " ["input-group" (get group-sizes (:size opts))]))}
                 (dom/input #js {:type "text"
                                 :value (:value state)
                                 :className (str/join " " ["form-control" (get sizes (:size opts))])
                                 :placeholder (:placeholder opts)
                                 :onChange #(om/set-state! owner :value (.. % -target -value))
                                 :onFocus (fn [_]
                                            (om/set-state! owner :listening true)
                                            (listen))
                                 :onBlur #(om/set-state! owner :listening false)})
                 (if listening
                   (dom/span #js {:className "input-group-btn"}
                             (dom/button #js {:type "button"
                                              :className "btn btn-default" 
                                              :onClick (fn [_]
                                                         (om/set-state! owner :value "")
                                                         (om/set-state! owner :old-value "")
                                                         (put! (:changed-chan opts) ""))}
                                         (dom/span #js {:className "glyphicon glyphicon-remove"})))))))))

(defn date-changed-too [new-value & [format-date]]
  (let [db-format-date "yyyy-MM-dd"
        format (or format-date db-format-date)
        parser (DateTimeParse. format)
        date (js/Date.)]
    (if (empty? new-value)
      new-value
      (if (and (= (count new-value) (count format)) (> (.strictParse parser new-value date) 0))
        (if format-date
          (.format (DateTimeFormat. db-format-date) date)
          new-value)
        new-value))))

(defn date [_ owner {:keys [id max-length is-focus jquery-datepicker?] :as opts}]
  (reify
    om/IInitState
    (init-state [_]
      {:jquery-datepicker? false})

    om/IWillMount
    (will-mount [_]
      (listen-focus owner (:widgets-pub-chan opts) id))
    
    om/IDidMount
    (did-mount [_]
      (when jquery-datepicker?
        (-> ($ (str "#" id))
            (.datepicker #js {:dateFormat (:date-format-jquery opts)
                              :onSelect #((:on-change opts) (date-changed-too % (:date-format opts)))})))
      (if (and is-focus (= (om/get-state owner :status) :adding))
        (.select (om/get-node owner id))))

    om/IDidUpdate
    (did-update [_ _ previous]
      (let [status (om/get-state owner :status)]
        (if (and is-focus (not= (:status previous) status)
                 (= status :editing))
          (.select (om/get-node owner id)))))

    om/IRenderState
    (render-state [_ {:keys [validation-state hint] :as state}]
      (dom/div #js {:className (str/join " " [(condp = validation-state
                                                :on-error "has-error"
                                                :on-success "has-success"
                                                nil) "form-group"])}
               (dom/label #js {:htmlFor id
                               :className "col-md-2 control-label"} (:label opts))
               (dom/div #js {:className "col-md-6"}
                        (dom/input #js {:id id
                                        :ref id
                                        :className (str/join " " ["form-control" (get sizes (:size opts))])
                                        :type "date"
                                        :defaultValue (if-not (= (:status state) :adding)
                                                        (.format (DateTimeFormat. (if jquery-datepicker? (:date-format opts) "yyyy-MM-dd")) (:value state))
                                                        "")
                                        :disabled (:disabled state)
                                        :placeholder (if jquery-datepicker? (:date-format-user opts))
                                        :maxLength max-length
                                        :onKeyDown #((:on-key-down opts) (.-keyCode %))
                                        :onChange #((:on-change opts) (date-changed-too (.. % -target -value)
                                                                                        (if jquery-datepicker? (:date-format opts))))
                                        :onBlur #((:on-blur opts) (.. % -target -value))}))
               (dom/span #js {:className (str/join " " [(if (not hint) "hidden") "help-block"])}
                         (dom/small nil hint))
               (dom/span #js {:className (str/join " " [(if (not (:optional state)) "hidden") "help-block"])}
                         (dom/small nil (str "(" (or (:optional-label opts) "optional") ")")))))))

(defn date-changed [owner changed-chan & [format-date]]
  (let [new-value (om/get-state owner :value)]
    (if-not (= new-value (om/get-state owner :old-value))
      (let [db-format-date "yyyy-MM-dd"
            format (or format-date db-format-date)
            parser (DateTimeParse. format)
            date (js/Date.)]
        (if-let [new-value (if (empty? new-value)
                             new-value
                             (if (and (= (count new-value) (count format)) (> (.strictParse parser new-value date) 0))
                               (if format-date
                                 (.format (DateTimeFormat. db-format-date) date)
                                 new-value)))]
          (put! changed-chan new-value))
        (om/set-state! owner :old-value new-value)))))

(defn listen-date-changes [owner changed-chan & [listen-every-ms format-date]]
  (fn []
    (go (while (om/get-state owner :listening)
          (date-changed owner changed-chan format-date)
          (<! (timeout (or listen-every-ms 800)))))))

(defn date-listen [_ owner {:keys [jquery-datepicker?] :as opts}]
  (reify
    om/IInitState
    (init-state [_]
      {:listening false
       :jquery-datepicker? false})

    om/IDidMount
    (did-mount [_]
      (when jquery-datepicker?
        (-> ($ (str "#" (:id opts)))
            (.datepicker #js {:dateFormat (:date-format-jquery opts)
                              :onSelect (fn [value]
                                          (om/set-state! owner :value value)
                                          (date-changed owner (:changed-chan opts) (:date-format opts)))}))))

    om/IRenderState
    (render-state [_ state]
      (let [listen (listen-date-changes owner (:changed-chan opts) (:listen-every-ms opts)
                                        (if jquery-datepicker? (:date-format opts)))]
        (dom/div #js {:className "input-group"}
                 (dom/div #js {:className "input-group-addon"} (:label opts))
                 (dom/input #js {:type "date"
                                 :id (:id opts)
                                 :value (:value state)
                                 :className (str/join " " ["form-control" (get sizes (:size opts))])
                                 :placeholder (if jquery-datepicker? (:date-format-user opts))
                                 :onChange #(om/set-state! owner :value (.. % -target -value))
                                 :onFocus (fn [_]
                                            (om/set-state! owner :listening true)
                                            (listen))
                                 :onBlur #(om/set-state! owner :listening false)})
                 (if (and jquery-datepicker? (not (empty? (:value state))))
                   (dom/span #js {:className "input-group-btn"}
                             (dom/button #js {:type "button"
                                              :className (str/join " " ["btn btn-default" (get btn-sizes (:size opts))])
                                              :onClick (fn [_]
                                                         (om/set-state! owner :value "")
                                                         (om/set-state! owner :old-value "")
                                                         (put! (:changed-chan opts) ""))}
                                         (dom/span #js {:className "glyphicon glyphicon-remove"})))))))))

(defn checkbox [_ owner {:keys [id is-focus] :as opts}]
  (reify
    om/IDidMount
    (did-mount [_]
      (if (and is-focus (= (om/get-state owner :status) :adding))
        (.focus (om/get-node owner id))))

    om/IDidUpdate
    (did-update [_ _ previous]
      (let [status (om/get-state owner :status)]
        (if (and is-focus (not= (:status previous) status)
                 (= status :editing))
          (.focus (om/get-node owner id)))))

    om/IRenderState
    (render-state [_ state]
      (dom/div #js {:className "form-group"}
               (dom/div #js {:className "col-md-offset-2 col-md-6"}
                        (dom/div #js {:className "checkbox"}
                                 (dom/label nil
                                            (dom/input #js {:id id
                                                            :ref id
                                                            :type "checkbox"
                                                            :checked (boolean (:value state))
                                                            :disabled (:disabled state)
                                                            :onChange #((:on-change opts) (.. % -target -checked))
                                                            :onBlur #((:on-blur opts) (.. % -target -checked))} (:label opts)))))))))

(defn select [_ owner {:keys [id is-focus] :as opts}]
  (reify
    om/IWillMount
    (will-mount [_]
      (listen-focus owner (:widgets-pub-chan opts) id))
    
    om/IDidMount
    (did-mount [_]
      (if (and is-focus (= (om/get-state owner :status) :adding))
        (.focus (om/get-node owner id))))

    om/IDidUpdate
    (did-update [_ _ previous]
      (let [status (om/get-state owner :status)]
        (when (not= (:status previous) status)
          (set! (.-value (om/get-node owner id)) (om/get-state owner :selected-value)) ; Because it does not work properly in Firefox
          (if (and is-focus (= status :editing))
            (.focus (om/get-node owner id))))))

    om/IRenderState
    (render-state [_ {:keys [validation-state hint] :as state}]
      (dom/div #js {:className (str/join " "  [(condp = validation-state
                                                 :on-error "has-error"
                                                 :on-success "has-success"
                                                 nil)
                                               "form-group"])}
               (dom/label #js {:htmlFor id
                               :className "col-md-2 control-label"} (:label opts))
               (dom/div #js {:className "col-md-6"}
                        (apply dom/select #js {:id id
                                               :ref id
                                               :className (str/join " " ["form-control" (get sizes (:size opts))])
                                               :defaultValue (:selected-value state)
                                               :disabled (:disabled state)
                                               :onKeyDown #((:on-key-down opts) (.-keyCode %))
                                               :onChange #((:on-change opts) (.. % -target -value))
                                               :onBlur #((:on-blur opts) (.. % -target -value))}
                               (concat [(dom/option #js {:value ""} (or (:empty-text opts) ""))]
                                       (for [option (:value state)]
                                         (dom/option #js {:value ((:value-key opts) option)} ((:text-key opts) option))))))
               (dom/span #js {:className (str/join " " [(if (not hint) "hidden") "help-block"])}
                         (dom/small nil hint))
               (dom/span #js {:className (str/join " " [(if (not (:optional state)) "hidden") "help-block"])}
                         (dom/small nil (str "(" (or (:optional-label opts) "optional") ")")))))))

(defn select-inline [_ owner {:keys [id max-length] :as opts}]
  (reify
    om/IWillMount
    (will-mount [_]
      (listen-focus owner (:widgets-pub-chan opts) id))
    
    om/IRenderState
    (render-state [_ state]
      (dom/div #js {:className (str/join " " [(if (:on-error? state) "has-error") "form-group" (str "col-md-" (:cols opts))])
                    :style #js {:padding "0"}}
               (apply dom/select #js {:id id
                                      :ref id
                                      :className (str/join " " ["form-control" (get sizes (:size opts))])
                                      :placeholder (:label opts)
                                      :value (:selected-value state) ; :value instead :defaultValue because value can
                                                                     ; be setted programmatically
                                      :disabled (:disabled state)
                                      :style #js {:width "99%"}
                                      :onKeyDown #((:on-key-down opts) (.-keyCode %))
                                      :onChange #((:on-change opts) (.. % -target -value))
                                      :onBlur (:on-blur opts)}
                      (concat [(dom/option #js {:value ""} (or (:empty-text opts) ""))]
                              (for [option (:value state)]
                                (dom/option #js {:value ((:value-key opts) option)} ((:text-key opts) option)))))))))

(defn image [_ owner {:keys [id on-change] :as opts}]
  (reify
    om/IInitState
    (init-state [_]
      {:no-image-checked false})

    om/IWillMount
    (will-mount [_]
      (listen-focus owner (:widgets-pub-chan opts) id))

    om/IWillReceiveProps
    (will-receive-props [_ props]
      (if (= props :viewing)
        (om/set-state! owner :no-image-checked false)))

    om/IDidUpdate
    (did-update [_ _ prev]
      (let [status (om/get-state owner :status)]
        (if (and (= status :viewing) (not (:no-image-checked prev)))
          (set! (.-value (om/get-node owner id)) ""))))
    
    om/IRenderState
    (render-state [_ {:keys [status no-image-checked hint] :as state}]
      (dom/div nil
               (if-not no-image-checked
                 (dom/div #js {:className (str/join " "  [(condp = (:validation-state state)
                                                            :on-error "has-error"
                                                            :on-success "has-success"
                                                            nil)
                                                          "form-group"
                                                          "hidden-xs"])}
                          (dom/label #js {:htmlFor id
                                          :className "col-md-2 control-label"} (:label opts))
                          (dom/div #js {:className "col-md-6"}
                                   (dom/input #js {:id id
                                                   :ref id
                                                   :type "file"
                                                   :onChange #(on-change (-> %
                                                                             (.. -target -files)
                                                                             (.item 0)))}))
                          (dom/span #js {:className (str/join " " [(if (not hint) "hidden") "help-block"])}
                                    (dom/small nil hint))
                          (dom/span #js {:className (str/join " " [(if (not (:optional state)) "hidden") "help-block"])}
                                    (dom/small nil (str "(" (or (:optional-label opts) "optional") ")")))))
               (if (and (= status :editing) (:has-image state))
                 (dom/div #js {:className "form-group hidden-xs"}
                          (dom/div #js {:className "col-md-offset-2 col-md-6"}
                                   (dom/div #js {:className "checkbox"}
                                            (dom/label nil
                                                       (dom/input #js {:type "checkbox"
                                                                       :onChange #(on-change (if (.. % -target -checked)
                                                                                               (do (om/set-state! owner :no-image-checked true)
                                                                                                   0)
                                                                                               (do (om/set-state! owner :no-image-checked false)
                                                                                                   nil)))}
                                                                  (or (:no-image-label opts) "No image")))))))))))


(defn listen-display-loading [owner p]
  (let [c (chan)]
    (sub p :fetch c)
    (go (while true
          (if (:show (<! c))
            (om/set-state! owner :show-loading true)
            (om/set-state! owner :show-loading false))))))

(defn input-view [_ owner {:keys [id class-name placeholder on-change on-blur fetch fetch-add
                                  wait-before-blur-ms listen-every-ms widgets-pub-chan loading-pub-chan]}]
  (reify
    om/IInitState
    (init-state [_]
      {:listening false
       :old-value ""
       :show-loading false})

    om/IWillMount
    (will-mount [_]
      (listen-focus owner widgets-pub-chan id)
      (listen-display-loading owner loading-pub-chan))
    
    om/IRenderState
    (render-state [_ {:keys [focus-ch value-ch highlight-ch select-ch value highlighted-index displayed? show-loading]}]
      (let [listen (listen-changes owner value-ch listen-every-ms)]
        (dom/div nil
                 (dom/input #js {:id id
                                 :ref id
                                 :type "text"
                                 :autoComplete "off"
                                 :spellCheck "false"
                                 :className (str/join " " ["form-control" class-name])
                                 :style #js {:width "95%"}
                                 :placeholder placeholder
                                 :value value
                                 :onFocus (fn [_]
                                            (put! focus-ch true)
                                            (om/set-state! owner :listening true)
                                            (listen))
                                 :onBlur (fn [e]
                                           (on-blur)
                                           (fetch (.. e -target -value))
                                           (go (om/set-state! owner :listening false)
                                               (<! (timeout (or wait-before-blur-ms 100)))
                                               ;; If we don't wait, then the dropdown will disappear before
                                               ;; its onClick renders and a selection won't be made.
                                               ;; This is a hack, of course, but I don't know how to fix it
                                               (put! focus-ch false)))
                                 :onKeyDown (fn [e]
                                              (case (.-keyCode e)
                                                40 (put! highlight-ch (inc highlighted-index)) ;; up
                                                38 (put! highlight-ch (dec highlighted-index)) ;; down
                                                13 (if displayed?
                                                     (put! select-ch highlighted-index) 
                                                     (fetch-add (.. e -target -value)))
                                                nil))
                                 :onChange #(on-change (.. % -target -value))})
                 (if show-loading
                   (dom/img #js {:style #js {:position "absolute"
                                             :margin-top "10px"
                                             :margin-left "3px"}
                                 :src "images/ajax-loader.gif"})))))))

(defn autocomplete [_ owner opts]
  (reify
    om/IInitState
     (init-state [_]
       {:result-ch (chan)})

     om/IWillMount
     (will-mount [_]
       (let [result-ch (om/get-state owner :result-ch)]
         (go (while true
               ((:result-fn opts) (<! result-ch))))))

     om/IRenderState
     (render-state [_ {:keys [value result-ch] :as state}]
       (om/build ac/autocomplete nil {:state {:value value}
                                      :opts {:container-view ac-bootstrap/container-view
                                             :container-view-opts {:class-name (str/join " " [(if (:on-error? state) "has-error") "form-group" (str "col-md-" (:cols opts))])}
                                             :input-view input-view
                                             :input-view-opts {:id (:id opts)
                                                               :class-name (get sizes (:size opts))
                                                               :placeholder (:label opts)
                                                               :wait-before-blur-ms 500
                                                               :on-change (:on-change opts)
                                                               :on-blur (:on-blur opts)
                                                               :fetch (:fetch opts)
                                                               :fetch-add (:fetch-add opts)
                                                               :widgets-pub-chan (:widgets-pub-chan opts)
                                                               :loading-pub-chan (:loading-pub-chan opts)}
                                             :results-view ac-bootstrap/results-view
                                             :results-view-opts {:class-name "absolute"
                                                                 :render-item ac-bootstrap/render-item
                                                                 :render-item-opts {:text-fn (:text-fn opts)
                                                                                    :focus-id (:id opts)}}
                                             :result-ch result-ch
                                             :suggestions-fn (:suggestions-fn opts)}}))))
