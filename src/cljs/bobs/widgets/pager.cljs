(ns bobs.widgets.pager
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [clojure.string :as str]
            [cljs.core.async :refer [put!]]))

(defn pager [_ owner {:keys [change-page-chan] :as opts}]
  (reify
    om/IRenderState
    (render-state [_ {:keys [page page-size total-pages]}]
      (let [bof (= page 1)
            eof (= page total-pages)
            pager-size (:pager-size opts)
            first-page (if (<= page pager-size) 1
                           (if (= (mod page pager-size) 0) (- page (- pager-size 1)) (+ (* (Math/floor (/ page pager-size)) pager-size) 1)))
            last-page (let [last-page (* (Math/ceil (/ page pager-size)) pager-size)]
                        (if (< last-page total-pages) last-page total-pages))]
        (apply dom/nav nil
               (list
                (apply dom/ul #js {:className "pagination pagination-sm hidden-xs"}
                       (concat
                        [(dom/li #js {:className (if bof "disabled")}
                                 (dom/a #js {:href "#"
                                             :onClick (fn [e]
                                                        (.preventDefault e)
                                                        (if-not bof
                                                          (put! change-page-chan 1)))} "\u00ab"))]
                        [(dom/li #js {:className (if bof "disabled")}
                                 (dom/a #js {:href "#"
                                             :onClick (fn [e]
                                                        (.preventDefault e)
                                                        (if-not bof
                                                          (put! change-page-chan (dec page))))} "\u2039"))]
                        (if (> page pager-size)
                          [(dom/li nil
                                   (dom/a #js {:href "#"
                                               :onClick (fn [e]
                                                          (.preventDefault e)
                                                          (put! change-page-chan (dec first-page)))} "..."))])
                        (for [p (range first-page (+ last-page 1))]
                          (dom/li #js {:className (if (= page p) "active")}
                                  (dom/a #js {:href "#"
                                              :onClick (fn [e]
                                                         (.preventDefault e)
                                                         (if-not (= page p)
                                                           (put! change-page-chan p)))} p)))
                        (if (< last-page total-pages)
                          [(dom/li nil
                                   (dom/a #js {:href "#"
                                               :onClick (fn [e]
                                                          (.preventDefault e)
                                                          (put! change-page-chan (inc last-page)))} "..."))])
                        [(dom/li #js {:className (if eof "disabled")}
                                 (dom/a #js {:href "#"
                                             :onClick (fn [e]
                                                        (.preventDefault e)
                                                        (if-not eof
                                                          (put! change-page-chan (inc page))))} "\u203a"))]
                        [(dom/li #js {:className (if eof "disabled")}
                                 (dom/a #js {:href "#"
                                             :onClick (fn [e]
                                                        (.preventDefault e)
                                                        (if-not eof
                                                          (put! change-page-chan total-pages)))} "\u00bb"))]
                        [(dom/li #js {:className "dropdown"
                                      :style #js {:position "absolute"}}
                                 (dom/a #js {:href "#"
                                             :data-toggle "dropdown"
                                             :onClick (fn [e] (.preventDefault e))} (str page-size " " (or (:per-page-label opts) "per page") " ") (dom/span #js {:className "caret"}))
                                 (apply dom/ul #js {:className "dropdown-menu"
                                                    :role "menu"}
                                        (for [size (:page-sizes opts)]
                                          (dom/li #js {:role "presentation"
                                                       :className (if (= size page-size) "active")}
                                                  (dom/a #js {:href "#"
                                                              :role "menuitem"
                                                              :onClick (fn [e]
                                                                         (.preventDefault e)
                                                                         (put! change-page-chan {:page page
                                                                                                 :page-size size
                                                                                                 :last-page-on-miss 1}))} (str size " " (or (:per-page-label opts) "per page")))))))]))
                (apply dom/ul #js {:className "pager visible-xs"}
                       (concat
                        [(dom/li #js {:className (str "previous " (if bof "disabled"))}
                                 (dom/a #js {:href "#"
                                             :onClick (fn [e]
                                                        (.preventDefault e)
                                                        (if-not bof
                                                          (put! change-page-chan 1)))} "\u2039"))]
                        [(dom/li #js {:className (str "next " (if eof "disabled"))}
                                 (dom/a #js {:href "#"
                                             :onClick (fn [e]
                                                        (.preventDefault e)
                                                        (if-not eof
                                                          (put! change-page-chan (inc page))))} "\u203a"))]))))))))
