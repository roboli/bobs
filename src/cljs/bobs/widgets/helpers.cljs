(ns bobs.widgets.helpers
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [jayq.core :refer [$]]))

(defn question-mark [cursor owner {:keys [id content]}]
  (reify
    om/IDidMount
    (did-mount [_]
      (-> ($ (str "#" id))
          (.popover)))

    om/IRender
    (render [_]
      (dom/span #js {:id id
                     :className "glyphicon glyphicon-question-sign"
                     :data-toggle "popover"
                     :data-trigger "hover"
                     :data-html true
                     :data-content content}))))
