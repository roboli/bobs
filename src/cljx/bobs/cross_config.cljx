(ns bobs.cross-config)

(def app-lang :es)
(def app-name "bobs")
(def app-name-ext "_")
(def app-brand "bobsntech @ 2025")
(def app-email "info@bobsntech.com")
(def image-file-size 200000)
