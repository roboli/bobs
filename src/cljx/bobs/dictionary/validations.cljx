(ns bobs.dictionary.validations)

(def msgs {:en {:validations {:abbreviation-empty "Abbreviation is required"
                              :address-empty "Address is required"
                              :already-void "Document is already voided"
                              :brand-empty "Brand is required"
                              :cannot-delete-record-in-use "Cannot delete record that is in use"
                              :cannot-create-document-without-sequential-number "Cannot create document without its respective configuration"
                              :customer-empty "Customer is required"
                              :cost-empty "Cost is required"
                              :cost-invalid "Cost is invalid"
                              :currency-empty "Currency is required"
                              :description-empty "Description is required"
                              :details-empty "At least one detail is required"
                              :email-empty "Email is required"
                              :email-invalid "Email is invalid"
                              :image-file-invalid "File is not a valid png|jpeg|jpg image"
                              :image-file-size-invalid "File must be smaller than %d Kb"
                              :image-filename-invalid "Only png|jpeg|jpg files allowed"
                              :invalid-username-password "Invalid username or password"
                              :inventory-type-empty "Inventory type required"
                              :name-empty "Name is required"
                              :partner-must-be "Partner must be customer, supplier or both"
                              :partner-cannot-downgrade-from-supplier "Partner is already being used as supplier"
                              :partner-cannot-downgrade-from-customer "Partner is already being used as customer"
                              :price-empty "Price is required"
                              :price-invalid "Price is invalid"
                              :purchase-date-empty "Purchase date is required"
                              :purchase-date-invalid "Purchase date is invalid"
                              :quantity-empty "Quantity is required"
                              :quantity-invalid "Quantity is invalid"
                              :quantity-no-longer-available "Quantity %d for item %s no longer available"
                              :quantity-not-available "Quantity not available"
                              :quantity-over-zero "Quantity must be greater than zero"
                              :sku-empty "SKU is required"
                              :sku-available "SKU is already in use"
                              :sku-non-existent "No item found with that SKU"
                              :supplier-empty "Supplier is required"
                              :telephone-empty "Telephone is required"
                              :uid-available "UID is already in use"
                              :void-leaves-negative-balance "Cannot void document as it leaves a negative balance in one or more of its items"
                              :warehouse-empty "Warehouse is required"}}
           :es {:validations {:abbreviation-empty "La abreviatura es requerida"
                              :address-empty "La direcci\u00f3n es requerida"
                              :already-void "Documento ya esta anulado"
                              :brand-empty "La marca es requerida"
                              :cannot-delete-record-in-use "No se puede eliminar registro en uso"
                              :cannot-create-document-without-sequential-number "No se puede crear documento sin su respectiva configuraci\u00f3n"
                              :customer-empty "El cliente es requerido"
                              :cost-empty "El costo es requerido"
                              :cost-invalid "El costo es inv\u00e1lido"
                              :currency-empty "La moneda es requerida"
                              :description-empty "La descripci\u00f3n es requerida"
                              :details-empty "Al menos un detalle es requerido"
                              :email-empty "El email es requerido"
                              :email-invalid "El email es inv\u00e1lido"
                              :image-file-invalid "Archivo no es una imagen png|jpeg|jpg v\u00e1lida"
                              :image-file-size-invalid "El archivo debe ser menor a %d Kb"
                              :image-filename-invalid "Solo archivos png|jpeg|jpg permitidos"
                              :invalid-username-password "Usuario o contrase\u00f1a inv\u00e1lidos"
                              :inventory-type-empty "El tipo de inventario es requerido"
                              :name-empty "El nombre es requerido"
                              :partner-must-be "Socio debe ser cliente, proveedor o ambos"
                              :partner-cannot-downgrade-from-supplier "Socio ya esta siendo utilizado como proveedor"
                              :partner-cannot-downgrade-from-customer "Socio ya esta siendo utilizado como cliente"
                              :price-empty "El precio es requerido"
                              :price-invalid "El precio es inv\u00e1lido"
                              :purchase-date-empty "La fecha de compra es requerida"
                              :purchase-date-invalid "La fecha de compra es inv\u00e1lida"
                              :quantity-empty "La cantidad es requerida"
                              :quantity-invalid "La cantidad es inv\u00e1lida"
                              :quantity-no-longer-available "Cantidad %.2f para art\u00edculo %s ya no esta disponible"
                              :quantity-not-available "Cantidad no disponible"
                              :quantity-over-zero "La cantidad debe ser mayor a cero"
                              :sku-empty "El SKU es requerido"
                              :sku-available "EL SKU ya esta en uso"
                              :sku-non-existent "No se encontro art\u00edculo con ese SKU"
                              :supplier-empty "El proveedor es requerido"
                              :telephone-empty "El tel\u00e9fono es requerido"
                              :uid-available "EL NIT ya esta en uso"
                              :void-leaves-negative-balance "Documento no se puede anular ya que deja un saldo negativo en uno o m\u00e1s de sus art\u00edculos"
                              :warehouse-empty "La bodega es requerida"}}})
