(ns bobs.formats)

(defn date-fmt [app-lang]
  (condp = app-lang
    :en "MM/dd/yyyy"
    :es "dd/MM/yyyy"))

(defn datetime-fmt [app-lang]
  (condp = app-lang
    :en "MM/dd/yyyy HH:mm:ss"
    :es "dd/MM/yyyy HH:mm:ss"))
