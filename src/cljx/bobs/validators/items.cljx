(ns bobs.validators.items
  (:require [bobs.validators.rules :refer [description-empty
                                           sku-empty
                                           currency-empty
                                           price-empty
                                           price-invalid
                                           inventory-type-empty
                                           opt-image-filename-invalid
                                           opt-image-file-size-invalid
                                           #+clj opt-image-file-invalid]]))

(def rules {:description [description-empty]
            :sku [sku-empty]
            :currency-id [currency-empty]
            :price [price-empty price-invalid]
            :inventory-type [inventory-type-empty]
            :image-filename [opt-image-filename-invalid]
            #+cljs :image #+cljs [opt-image-file-size-invalid]
            #+clj :image-file #+clj [opt-image-file-invalid]
            #+clj :image-file-size #+clj [opt-image-file-size-invalid]})
