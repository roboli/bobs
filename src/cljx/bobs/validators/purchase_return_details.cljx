(ns bobs.validators.purchase-return-details
  (:require [bobs.validators.rules :refer [sku-empty
                                           warehouse-empty
                                           quantity-empty
                                           quantity-invalid
                                           quantity-over-zero
                                           price-empty
                                           price-invalid]]))

(def rules {:sku [sku-empty]
            :warehouse-id [warehouse-empty]
            :quantity [quantity-empty quantity-invalid quantity-over-zero]
            :price [price-empty price-invalid]})
