(ns bobs.validators.brands
  (:require [bobs.validators.rules :refer [name-empty]]))

(def rules {:name [name-empty]})
