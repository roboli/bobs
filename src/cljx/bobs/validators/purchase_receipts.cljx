(ns bobs.validators.purchase-receipts
  (:require [bobs.validators.rules :refer [supplier-empty
                                           warehouse-empty
                                           purchase-date-empty
                                           #+clj purchase-date-invalid
                                           #+cljs details-empty]]))

(def rules {:supplier-id [supplier-empty]
            :warehouse-id [warehouse-empty]
            :purchase-date [purchase-date-empty #+clj purchase-date-invalid]
            #+cljs :details-count #+cljs [details-empty]})
