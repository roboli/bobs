(ns bobs.validators.shipments
  (:require [bobs.validators.rules :refer [customer-empty
                                           #+cljs details-empty]]))

(def rules {:partner-id [customer-empty]
            #+cljs :details-count #+cljs [details-empty]})
