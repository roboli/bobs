(ns bobs.validators.rules
  (:require [valip.predicates :refer [present? email-address? over]]
            [bobs.i18n :refer [t]]
            #+clj [bobs.models.items :refer [sku-available?]]
            #+clj [bobs.config :refer [image-file-size]]
            #+cljs [bobs.data.config :refer [image-file-size]]
            #+clj [clj-time.core :as t]
            #+clj [clj-time.format :as tf]
            #+cljs [goog.string :as gstring]
            #+cljs [goog.string.format])
  #+clj
  (:import javax.imageio.ImageIO))

#+clj
(defn image-file?
  "Returns true if file is an image."
  [file]
  (boolean (if (.exists file) (ImageIO/read file))))

#+clj
(defn image-file-size?
  "Returns true if file size does not exceed the expected."
  [size]
  (boolean (<= size image-file-size)))

#+cljs
(defn image-file-size?
  "Returns true if file size does not exceed the expected."
  [file]
  (boolean (<= (.-size file) image-file-size)))

#+clj
(defn date?
  "Returns true if date is valid. Must be in 'year-month-day'
  format and before year 9999."
  [s]
  (let [fmr (tf/formatters :year-month-day)]
    (try
      (t/before? (tf/parse fmr s) (t/date-time 9999))
      (catch IllegalArgumentException ex
        false))))

(defn image-filename?
  "Returns true if the string represents an image filename."
  [s]
  (boolean (re-matches #".*\.(jpeg|jpg|png)$" s)))

(defn number-string?
  "Returns true if the string represents a number."
  [s]
  (boolean (and (not (empty? s)) (re-matches #"\s*[+-]?(\d+(,?\d{3})*)?(\.\d+(M|M|N)?)?\s*" s))))

(defn over-number
  "Returns a predicate function that returns true if x is greater than n.
  Assumes that x is a valid number."
  [n]
  (fn [x]
    (boolean (let [x (apply str (remove #((set ",") %) x))]
               (if-let [x (if (number-string? x)
                            #+clj (Double/parseDouble x)
                            #+cljs (js/parseFloat x))]
                 (> x n))))))

(def abbreviation-empty [:abbreviation present? (t :validations/abbreviation-empty)])
(def address-empty [:address present? (t :validations/address-empty)])
(def brand-empty [:brand-id present? (t :validations/brand-empty)])
(def cost-empty [:cost present? (t :validations/cost-empty)])
(def cost-invalid [:cost number-string? (t :validations/cost-invalid)])
(def currency-empty [:currency-id present? (t :validations/currency-empty)])
(def customer-empty [:customer-id present? (t :validations/customer-empty)])
(def description-empty [:description present? (t :validations/description-empty)])
#+cljs (def details-empty [:details-count (over 0) (t :validations/details-empty)])
(def email-empty [:email present? (t :validations/email-empty)])
(def email-invalid [:email email-address? (t :validations/email-invalid)])
(def opt-email-invalid [:email #(or (empty? %) (email-address? %)) (t :validations/email-invalid)])
(def name-empty [:name present? (t :validations/name-empty)])
#+clj (def opt-image-file-invalid [:image-file #(or (nil? %) (image-file? %)) (t :validations/image-file-invalid)])
(def opt-image-filename-invalid [:image-filename #(or (empty? %) (image-filename? %)) (t :validations/image-filename-invalid)])
#+clj (def opt-image-file-size-invalid [:image-file-size #(or (nil? %) (image-file-size? %)) (format (t :validations/image-file-size-invalid) (/ image-file-size 1000))])
#+cljs (def opt-image-file-size-invalid [:image #(or (not= (type %) js/File) (image-file-size? %)) (gstring/format (t :validations/image-file-size-invalid) (/ image-file-size 1000))])
(def inventory-type-empty [:inventory-type present? (t :validations/inventory-type-empty)])
(def quantity-empty [:quantity present? (t :validations/quantity-empty)])
(def quantity-invalid [:quantity number-string? (t :validations/quantity-invalid)])
(def quantity-over-zero [:quantity (over-number 0) (t :validations/quantity-over-zero)])
(def supplier-empty [:supplier-id present? (t :validations/supplier-empty)])
(def price-empty [:price present? (t :validations/price-empty)])
(def price-invalid [:price number-string? (t :validations/price-invalid)])
(def purchase-date-empty [:purchase-date present? (t :validations/purchase-date-empty)])
#+clj (def purchase-date-invalid [:purchase-date date? (t :validations/purchase-date-invalid)])
(def sku-empty [:sku present? (t :validations/sku-empty)])
(def telephone-empty [:telephone present? (t :validations/telephone-empty)])
(def warehouse-empty [:warehouse-id present? (t :validations/warehouse-empty)])
