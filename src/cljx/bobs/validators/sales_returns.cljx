(ns bobs.validators.sales-returns
  (:require [bobs.validators.rules :refer [customer-empty
                                           warehouse-empty
                                           #+cljs details-empty]]))

(def rules {:customer-id [customer-empty]
            :warehouse-id [warehouse-empty]
            #+cljs :details-count #+cljs [details-empty]})
