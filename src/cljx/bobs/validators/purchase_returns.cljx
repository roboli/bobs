(ns bobs.validators.purchase-returns
  (:require [bobs.validators.rules :refer [supplier-empty
                                           #+cljs details-empty]]))

(def rules {:partner-id [supplier-empty]
            #+cljs :details-count #+cljs [details-empty]})
