(ns bobs.validators.partners
  (:require [bobs.validators.rules :as rules]))

(def rules {:name [rules/name-empty]
            :address [rules/address-empty]
            :telephone [rules/telephone-empty]
            :email [rules/opt-email-invalid]})
