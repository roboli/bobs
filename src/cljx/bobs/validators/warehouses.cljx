(ns bobs.validators.warehouses
  (:require [bobs.validators.rules :refer [name-empty]]))

(def rules {:name [name-empty]})
