(ns bobs.validators.uom
  (:require [bobs.validators.rules :refer [name-empty abbreviation-empty]]))

(def rules {:name [name-empty]
            :abbreviation [abbreviation-empty]})
