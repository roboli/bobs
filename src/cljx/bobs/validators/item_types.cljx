(ns bobs.validators.item-types
  (:require [bobs.validators.rules :refer [name-empty]]))

(def rules {:name [name-empty]})
