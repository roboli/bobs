(ns bobs.validators.validator
  (:require [valip.core :as valip]))

(defn validate [rules]
  (fn [subject]
    (apply (partial valip/validate subject) (apply concat (vals rules)))))
