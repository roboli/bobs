(ns bobs.validators.entries
  (:require [bobs.validators.rules :refer [warehouse-empty
                                           #+cljs details-empty]]))

(def rules {:warehouse-id [warehouse-empty]
            #+cljs :details-count #+cljs [details-empty]})
