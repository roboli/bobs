(ns bobs.validators.entry-details
  (:require [bobs.validators.rules :refer [sku-empty
                                           quantity-empty
                                           quantity-invalid
                                           quantity-over-zero
                                           cost-empty
                                           cost-invalid]]))

(def rules {:sku [sku-empty]
            :quantity [quantity-empty quantity-invalid quantity-over-zero]
            :cost [cost-empty cost-invalid]})
