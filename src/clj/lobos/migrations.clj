(ns lobos.migrations
  (:refer-clojure :exclude [alter drop bigint boolean char double float time])
  (:use (lobos [migration :only [defmigration]] core schema config)))

(def small-string 50)
(def string 100)
(def big-string 500)

(defn auto-inc-key [table]
  (integer table :id :primary-key :auto-inc))

(defn active [table]
  (boolean table :active :not-null (default 1)))

(defmigration create-user-table
  (up [] (create
          (table :user
                 (auto-inc-key)
                 (active)
                 (varchar :username small-string :unique :not-null)
                 (varchar :password string :not-null)
                 (varchar :name string :not-null))))
  (down [] (drop (table :user))))

(defmigration create-company-table
  (up [] (create
          (table :company
                 (auto-inc-key)
                 (active)
                 (varchar :alias small-string :unique :not-null)
                 (varchar :name string :not-null))))
  (down [] (drop (table :company))))

(defmigration create-company-user-table
  (up [] (create
          (table :company_user
                 (auto-inc-key)
                 (active)
                 (integer :company_id [:refer :company :id] :not-null)
                 (integer :user_id [:refer :user :id] :not-null)
                 (boolean :is_default :not-null (default 0)))))
  (down [] (drop (table :company_user))))

(defmigration create-currency-table
  (up [] (create
          (table :currency
                 (auto-inc-key)
                 (active)
                 (integer :company_id [:refer :company :id] :not-null)
                 (varchar :code 4 :not-null)
                 (boolean :is_default :not-null (default 0))
                 (unique [:company_id :code]))))
  (down [] (drop (table :currency))))

(defmigration create-brand-table
  (up [] (create
          (table :brand
                 (auto-inc-key)
                 (active)
                 (integer :company_id [:refer :company :id] :not-null)
                 (varchar :name small-string :not-null))))
  (down [] (drop (table :brand))))

(defmigration create-uom-table
  (up [] (create
          (table :uom
                 (auto-inc-key)
                 (active)
                 (integer :company_id [:refer :company :id] :not-null)
                 (varchar :name small-string :not-null)
                 (varchar :abbreviation 4 :not-null))))
  (down [] (drop (table :uom))))

(defmigration create-item-type-table
  (up [] (create
          (table :item_type
                 (auto-inc-key)
                 (active)
                 (integer :company_id [:refer :company :id] :not-null)
                 (varchar :name small-string :not-null))))
  (down [] (drop (table :item_type))))

(defmigration create-item-table
  (up [] (create
          (table :item
                 (auto-inc-key)
                 (active)
                 (integer :company_id [:refer :company :id] :not-null)
                 (varchar :sku string :not-null)
                 (varchar :description string :not-null)
                 (integer :currency_id [:refer :currency :id] :not-null)
                 (integer :brand_id [:refer :brand :id])
                 (integer :item_type_id [:refer :item_type :id])
                 (integer :uom_id [:refer :uom :id])
                 (decimal :price 15 2 :not-null (default 0))
                 (varchar :image string)
                 (varchar :thumbnail string)
                 (boolean :is_service :not-null (default 0))
                 (unique [:company_id :sku]))))
  (down [] (drop (table :item))))

(defmigration create-warehouse-table
  (up [] (create
          (table :warehouse
                 (auto-inc-key)
                 (active)
                 (integer :company_id [:refer :company :id] :not-null)
                 (varchar :name small-string :not-null)
                 (varchar :description string))))
  (down [] (drop (table :warehouse))))

(defmigration create-partner-table
  (up [] (create
          (table :partner
                 (auto-inc-key)
                 (active)
                 (integer :company_id [:refer :company :id] :not-null)
                 (varchar :name string :not-null)
                 (varchar :address big-string :not-null)
                 (varchar :telephone string :not-null)
                 (varchar :email string))))
  (down [] (drop (table :partner))))

(defmigration create-supplier-table
  (up [] (create
          (table :supplier
                 (auto-inc-key)
                 (integer :partner_id [:refer :partner :id :on-delete :cascade] :not-null))))
  (down [] (drop (table :supplier))))

(defmigration create-customer-table
  (up [] (create
          (table :customer
                 (auto-inc-key)
                 (integer :partner_id [:refer :partner :id :on-delete :cascade] :not-null))))
  (down [] (drop (table :customer))))

(defmigration create-inventory-table
  (up [] (create
          (table :inventory
                 (auto-inc-key)
                 (integer :item_id [:refer :item :id :on-delete :cascade] :not-null)
                 (smallint :inventory_type :not-null)
                 (decimal :quantity 10 2 :not-null (default 0))
                 (decimal :allocated 10 2 :not-null (default 0))
                 (decimal :balance_forward 10 2 :not-null (default 0))
                 (boolean :lifo :not-null (default 0)))))
  (down [] (drop (table :inventory))))

(defmigration create-lot-table
  (up [] (create
          (table :lot
                 (auto-inc-key)
                 (active)
                 (integer :item_id [:refer :item :id] :not-null)
                 (integer :uom_id [:refer :uom :id])
                 (integer :warehouse_id [:refer :warehouse :id] :not-null)
                 (decimal :cost 15 2 :not-null (default 0))
                 (decimal :quantity 10 2 :not-null (default 0))
                 (decimal :allocated 10 2 :not-null (default 0)))))
  (down [] (drop (table :lot))))

(defmigration create-document-configuration-table
  (up [] (create
          (table :document_configuration
                 (auto-inc-key)
                 (active)
                 (integer :company_id [:refer :company :id] :not-null)
                 (smallint :document_type :not-null)
                 (varchar :description string :not-null)
                 (varchar :serial 10)
                 (integer :initial :not-null)
                 (integer :final)
                 (integer :current :not-null (default 0))
                 (boolean :is_default :not-null (default 0))
                 (unique :doc_config_unique [:company_id :document_type :serial :initial]))))
  (down [] (drop (table :document_configuration))))

(defmigration create-document-table
  (up [] (create
          (table :document
                 (auto-inc-key)
                 (integer :document_configuration_id [:refer :document_configuration :id] :not-null)
                 (integer :number :not-null)
                 (integer :user_id [:refer :user :id] :not-null)
                 (integer :currency_id [:refer :currency :id] :not-null)
                 (timestamp :created_on :not-null (default (now)))
                 (text :comment)
                 (decimal :total 15 2 :not-null (default 0))
                 (boolean :void :not-null (default 0)))))
  (down [] (drop (table :document))))

(defmigration create-purchase-receipt-table
  (up [] (create
          (table :purchase_receipt
                 (auto-inc-key)
                 (integer :document_id [:refer :document :id] :not-null)
                 (date :purchase_date :not-null)
                 (integer :supplier_id [:refer :supplier :id] :not-null)
                 (integer :warehouse_id [:refer :warehouse :id])
                 (varchar :reference small-string))))
  (down [] (drop (table :purchase_receipt))))

(defmigration create-purchase-return-table
  (up [] (create
          (table :purchase_return
                 (auto-inc-key)
                 (integer :document_id [:refer :document :id] :not-null)
                 (integer :supplier_id [:refer :supplier :id] :not-null))))
  (down [] (drop (table :purchase_return))))

(defmigration create-shipment-table
  (up [] (create
          (table :shipment
                 (auto-inc-key)
                 (integer :document_id [:refer :document :id] :not-null)
                 (integer :customer_id [:refer :customer :id] :not-null))))
  (down [] (drop (table :shipment))))

(defmigration create-sales-return-table
  (up [] (create
          (table :sales_return
                 (auto-inc-key)
                 (integer :document_id [:refer :document :id] :not-null)
                 (integer :customer_id [:refer :customer :id] :not-null)
                 (integer :warehouse_id [:refer :warehouse :id]))))
  (down [] (drop (table :sales_return))))

(defmigration create-entry-table
  (up [] (create
          (table :entry
                 (auto-inc-key)
                 (integer :document_id [:refer :document :id] :not-null)
                 (integer :warehouse_id [:refer :warehouse :id]))))
  (down [] (drop (table :entry))))

(defmigration create-withdraw-table
  (up [] (create
          (table :withdraw
                 (auto-inc-key)
                 (integer :document_id [:refer :document :id] :not-null))))
  (down [] (drop (table :withdraw))))

(defmigration create-document-item-table
  (up [] (create
          (table :document_item
                 (auto-inc-key)
                 (integer :document_id [:refer :document :id] :not-null)
                 (smallint :row_number :not-null)
                 (integer :item_id [:refer :item :id] :not-null)
                 (integer :uom_id [:refer :uom :id])
                 (decimal :amount 15 2 :not-null (default 0))
                 (decimal :quantity 10 2 :not-null (default 0)))))
  (down [] (drop (table :document_item))))

(defmigration create-document-item-inventory-table
  (up [] (create
          (table :document_item_inventory
                 (auto-inc-key)
                 (integer :document_item_id [:refer :document_item :id] :not-null)
                 (integer :warehouse_id [:refer :warehouse :id] :not-null))))
  (down [] (drop (table :document_item_inventory))))

(defmigration create-document-item-lot-table
  (up [] (create
          (table :document_item_lot
                 (auto-inc-key)
                 (integer :document_item_id [:refer :document_item :id] :not-null)
                 (integer :lot_id [:refer :lot :id] :not-null)
                 (unique [:document_item_id :lot_id]))))
  (down [] (drop (table :document_item_lot))))

(defmigration add-uid-column-partner-table
  (up [] (alter :add
                (table :partner
                       (varchar :uid small-string))))
  (down [] (alter :drop
                  (table :partner
                         (column :uid :drop-default)))))

(defmigration add-quantity-column-document-table
  (up [] (alter :add
                (table :document
                       (decimal :quantity 15 2 :not-null (default 0)))))
  (down [] (alter :drop
                  (table :document
                         (column :quantity :drop-default)))))

(defmigration add-log-columns-item-table
  (up [] (alter :add
                (table :item
                       (integer :user_id [:refer :user :id])
                       (timestamp :created_on :not-null (default (now))))))
  (down [] (do (alter :drop
                      (table :item
                             (foreign-key [:user_id])))
               (alter :drop
                      (table :item
                             (column :user_id)
                             (column :created_on :drop-default))))))

(defmigration create-item-log-table
  (up [] (create
          (table :item_log
                 (auto-inc-key)
                 (integer :item_id [:refer :item :id :on-delete :cascade])
                 (integer :deleted_item_id)
                 (varchar :sku string :not-null)
                 (varchar :description string :not-null)
                 (decimal :price 15 2 :not-null (default 0))
                 (boolean :active)
                 (integer :user_id [:refer :user :id] :not-null)
                 (timestamp :created_on :not-null (default (now))))))
  (down [] (drop (table :item_log))))

(defmigration create-brand-log-table
  (up [] (create
          (table :brand_log
                 (auto-inc-key)
                 (integer :brand_id [:refer :brand :id :on-delete :cascade])
                 (integer :deleted_brand_id)
                 (varchar :name small-string :not-null)
                 (boolean :active)
                 (integer :user_id [:refer :user :id] :not-null)
                 (timestamp :created_on :not-null (default (now))))))
  (down [] (drop (table :brand_log))))

(defmigration add-log-columns-brand-table
  (up [] (alter :add
                (table :brand
                       (integer :user_id [:refer :user :id])
                       (timestamp :created_on :not-null (default (now))))))
  (down [] (do (alter :drop
                      (table :brand
                             (foreign-key [:user_id])))
               (alter :drop
                      (table :brand
                             (column :user_id)
                             (column :created_on :drop-default))))))
