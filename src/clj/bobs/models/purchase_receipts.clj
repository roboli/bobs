(ns bobs.models.purchase-receipts
  (:require [korma.core :as km :refer :all :exclude [insert update delete]]
            [korma.db :refer [transaction rollback]]
            [bobs.models.entities :refer :all]
            [bobs.models.query :refer :all]
            [bobs.models.documents :refer :all]
            [bobs.models.document-configurations :refer [document-configurations get-default pop-next-number!]]
            [bobs.models.drafts :as drafts]
            [bobs.models.purchase-receipts-drafts :refer [purchase-receipts-drafts]]
            [bobs.models.items :refer [items use-lots? update-quantity]]
            [bobs.config :refer [currencies document-types]]
            [bobs.i18n :refer [time-zone-offset]]))

(deftype PurchaseReceipt [entity amount-key])

(extend PurchaseReceipt
  Queryable
  (assoc basic-query-document
    :get-all (fn [_ data]
               (select purchase_receipt
                       (with document
                             (with document_configuration))
                       (with supplier
                             (with partner))
                       (with warehouse)
                       (fields :id [:document.number :number] [:document.created-on :date] [:document.void :void] :supplier-id [:partner.name :supplier-name] :warehouse-id
                               [:warehouse.name :warehouse-name] :purchase-date :reference [:document.total :total])
                       (where (and {:document-configuration.company-id (:company-id data)}
                                   (if (:number data) {:document.number (:number data)} true)
                                   (if (:warehouse-id data) {:warehouse.id (:warehouse-id data)} true)
                                   (if (:from-date data) {(raw (str "DATE(CONVERT_TZ(document.created_on, @@session.time_zone,'" time-zone-offset "'))")) [>= (:from-date data)]} true)
                                   (if (:to-date data) {(raw (str "DATE(CONVERT_TZ(document.created_on, @@session.time_zone,'" time-zone-offset "'))")) [<= (:to-date data)]} true)))
                       (order (:order data) (if (= (:sort data) 1) :DESC :ASC))
                       (limit (:limit data))
                       (offset (:offset data))))

    :get-row (fn [model data]
               (if-let [record (first (select purchase_receipt
                                              (with document
                                                    (with currency))
                                              (with supplier
                                                    (with partner))
                                              (with warehouse)
                                              (fields :id [:document.number :number] [:document.void :void] [:document.created-on :date] :supplier-id
                                                      [:partner.name :supplier-name] :warehouse-id [:warehouse.name :warehouse-name] :purchase-date :reference
                                                      :document.comment [:currency.code :currency-code] [:document.total :total] [:document.quantity :quantity])
                                              (where {:id (:id data)})))]
                 ((:build-row basic-query-document) model (assoc data :record record))))))

(extend PurchaseReceipt
  IDocument
  (assoc basic-document
    :insert-from-draft (fn [_ draft-id data]
                         (transaction
                          (try
                            (let [draft (drafts/get-row purchase-receipts-drafts draft-id)
                                  doc-conf-id (:id (get-default document-configurations (:company-id data) (get-in document-types [:purchase-receipt :id])))
                                  document-id (:generated-key (km/insert document
                                                                         (values (assoc
                                                                                     (select-keys data [:user-id :created-on :comment])
                                                                                   :document-configuration-id doc-conf-id
                                                                                   :number (pop-next-number! document-configurations doc-conf-id)
                                                                                   :currency-id (:currency-id draft)
                                                                                   :quantity (:quantity draft)
                                                                                   :total (:total draft)))))
                                  purchase-receipt-id (:generated-key (km/insert purchase_receipt
                                                                                 (values (assoc
                                                                                             (select-keys data [:supplier-id :warehouse-id :purchase-date :reference])
                                                                                           :document-id document-id))))
                                  warehouse-id (:warehouse-id data)]
                              (dorun (map-indexed (fn [idx row]
                                                    (let [entry-id (:generated-key (km/insert document_item
                                                                                              (values {:document-id document-id
                                                                                                       :row-number (inc idx)
                                                                                                       :item-id (:item-id row)
                                                                                                       :uom-id (:uom-id row)
                                                                                                       :amount (:cost row)
                                                                                                       :quantity (:quantity row)})))]
                                                      (if (use-lots? items (:item-id row))
                                                        (let [lot-id (:generated-key (km/insert lot
                                                                                                (values (assoc
                                                                                                            (select-keys row [:item-id :uom-id :cost :quantity])
                                                                                                          :warehouse-id warehouse-id))))]
                                                          (km/insert document_item_lot
                                                                     (values {:document-item-id entry-id
                                                                              :lot-id lot-id})))
                                                        (km/insert document_item_inventory
                                                                   (values {:document-item-id entry-id
                                                                            :warehouse-id warehouse-id})))
                                                      (update-quantity items (:item-id row) inc (:quantity row))))
                                                  (:details draft)))
                              (drafts/delete purchase-receipts-drafts draft-id)
                              purchase-receipt-id)
                            (catch Exception ex
                              (rollback)
                              (throw ex)))))

    :void (fn [model id]
            ((:void-document basic-document) model id dec))))

(def purchase-receipts (PurchaseReceipt. purchase_receipt :cost))
