(ns bobs.models.query
  (:require [korma.core :as km :refer :all :exclude [insert update delete]]
            [korma.db :refer [transaction rollback]])
  (:import [com.mysql.jdbc.exceptions.jdbc4 MySQLIntegrityConstraintViolationException]))

(defprotocol Queryable
  (get-all [model data])
  (get-row [model data])
  (get-count [model data])
  (get-record-row-number [model data])
  (insert [model data])
  (update [model data])
  (can-delete? [model data])
  (delete [model data]))

(def basic-query
  {:get-all (fn [model data]
              (select (.entity model)
                      (where {:company-id (:company-id data)
                              (:search-field data) [like (str "%" (:search-value data) "%")]})
                      (order (:order data) (if (= (:sort data) 1) :DESC :ASC))
                      (limit (:limit data))
                      (offset (:offset data))))
   :get-row (fn [model data]
              (first (select (.entity model)
                             (where {:id (:id data)})
                             (limit 1))))

   :get-count (fn [model data]
                (:count (first (select (.entity model)
                                       (aggregate (count :*) :count)
                                       (where {:company-id (:company-id data)
                                               (:search-field data) [like (str "%" (:search-value data) "%")]})))))
   :get-record-row-number (fn [model data]
                            (transaction
                             (exec-raw ["SET @row := 0"])
                             (select [(subselect (.entity model)
                                                 (fields [(raw "@row := @row + 1") :row] :id)
                                                 (where {:company-id (:company-id data)})
                                                 (order (:order data) (if (= (:sort data) 1) :DESC :ASC))) :temp]
                                     (where {:id (:id data)}))))
   :insert (fn [model data]
             (:generated-key (km/insert (.entity model)
                                        (values (:values data)))))
   :update (fn [model data]
             (km/update (.entity model)
                        (set-fields (:values data))
                        (where {:id (:id data)})))
   :can-delete? (fn [model data]
                  (transaction
                   (try
                     (km/delete (.entity model) (where {:id (:id data)}))
                     (rollback)
                     true
                     (catch MySQLIntegrityConstraintViolationException ex
                       false))))
   :delete (fn [model data]
             (km/delete (.entity model)
                        (where {:id (:id data)})))})

(deftype Query [entity])

(extend Query
  Queryable
  basic-query)
