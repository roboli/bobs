(ns bobs.models.item-types
  (:require [bobs.models.entities :refer [item-type]])
  (:import bobs.models.query.Query))

(def item-types (Query. item-type))
