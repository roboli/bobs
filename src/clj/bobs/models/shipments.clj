(ns bobs.models.shipments
  (:require [korma.core :as km :refer :all :exclude [insert update delete]]
            [korma.db :refer [transaction rollback]]
            [bobs.models.entities :refer :all]
            [bobs.models.query :refer :all]
            [bobs.models.documents :refer :all]
            [bobs.models.document-configurations :refer [document-configurations get-default pop-next-number!]]
            [bobs.models.drafts :as drafts]
            [bobs.models.shipments-drafts :refer [shipments-drafts]]
            [bobs.models.items :refer [items use-lots? update-quantity]]
            [bobs.models.lots :as lots]
            [bobs.config :refer [currencies document-types]]
            [bobs.i18n :refer [time-zone-offset]]))

(deftype Shipment [entity amount-key])

(extend Shipment
  Queryable
  (assoc basic-query-document
    :get-all (fn [_ data]
               (select shipment
                       (with document
                             (with document_configuration))
                       (with customer
                             (with partner))
                       (join document_item (= :document-item.document-id :document.id))
                       (join document_item_lot (= :document-item-lot.document-item-id :document-item.id))
                       (join lot (= :lot.id :document-item-lot.lot-id))
                       (modifier "DISTINCT")
                       (fields :id [:document.number :number] [:document.created-on :date] [:document.void :void] :customer-id [:partner.name :customer-name]
                               [:document.total :total])
                       (where (and {:document-configuration.company-id (:company-id data)}
                                   (if (:number data) {:document.number (:number data)} true)
                                   (if (:warehouse-id data) {:lot.warehouse-id (:warehouse-id data)} true)
                                   (if (:from-date data) {(raw (str "DATE(CONVERT_TZ(document.created_on, @@session.time_zone,'" time-zone-offset "'))")) [>= (:from-date data)]} true)
                                   (if (:to-date data) {(raw (str "DATE(CONVERT_TZ(document.created_on, @@session.time_zone,'" time-zone-offset "'))")) [<= (:to-date data)]} true)))
                       (order (:order data) (if (= (:sort data) 1) :DESC :ASC))
                       (limit (:limit data))
                       (offset (:offset data))))

    :get-row (fn [model data]
               (if-let [record (first (select shipment
                                              (with document
                                                    (with currency))
                                              (with customer
                                                    (with partner))
                                              (fields :id [:document.number :number] [:document.void :void] [:document.created-on :date] :customer-id [:partner.name :customer-name]
                                                      :document.comment [:currency.code :currency-code] [:document.total :total] [:document.quantity :quantity])
                                              (where {:id (:id data)})))]
                 ((:build-row basic-query-document) model (assoc data :record record))))

    :get-count (:get-withdraw-count basic-query-document)))

(extend Shipment
  IDocument
  (assoc basic-document
    :insert-from-draft (fn [_ draft-id data]
                         (transaction
                          (try
                            (let [draft (drafts/get-row shipments-drafts draft-id)
                                  doc-conf-id (:id (get-default document-configurations (:company-id data) (get-in document-types [:shipment :id])))
                                  document-id (:generated-key (km/insert document
                                                                         (values (assoc
                                                                                     (select-keys data [:user-id :created-on :comment])
                                                                                   :document-configuration-id doc-conf-id
                                                                                   :number (pop-next-number! document-configurations doc-conf-id)
                                                                                   :currency-id (:currency-id draft)
                                                                                   :quantity (:quantity draft)
                                                                                   :total (:total draft)))))
                                  shipment-id (:generated-key (km/insert shipment
                                                                         (values {:customer-id (:customer-id data)
                                                                                  :document-id document-id})))]
                              (dorun (map-indexed (fn [idx row]
                                                    (let [withdraw-id (:generated-key (km/insert document_item
                                                                                                 (values {:document-id document-id
                                                                                                          :row-number (inc idx)
                                                                                                          :item-id (:item-id row)
                                                                                                          :uom-id (:uom-id row)
                                                                                                          :amount (:price row)
                                                                                                          :quantity (:quantity row)})))]
                                                      (if (use-lots? items (:item-id row))
                                                        (let [lots (lots/available (:item-id row) (:warehouse-id row) (:quantity row))]
                                                          (dorun (map (fn [lot]
                                                                        (km/insert document_item_lot
                                                                                   (values {:document-item-id withdraw-id
                                                                                            :lot-id (:id lot)}))
                                                                        (lots/update-quantity (:id lot) dec (:quantity lot)))
                                                                      lots)))
                                                        (km/insert document_item_inventory
                                                                   (values {:document-item-id withdraw-id
                                                                            :warehouse-id (:warehouse-id row)})))
                                                      (update-quantity items (:item-id row) dec (:quantity row))))
                                                  (:details draft)))
                              (drafts/delete shipments-drafts draft-id)
                              shipment-id)
                            (catch Exception ex
                              (rollback)
                              (throw ex)))))

    :void (fn [model id]
            ((:void-document basic-document) model id inc))))

(def shipments (Shipment. shipment :price))
