(ns bobs.models.users
  (:require [korma.core :as km :refer :all :exclude [insert update delete]]
            [noir.util.crypt :as crypt]
            [bobs.models.entities :refer [user company company_user]]
            [bobs.models.query :refer :all]))

(defprotocol IUser
  (get-id [model username])
  (set-default-company [model user-id company-id]))

(deftype User [entity])

(extend-protocol Queryable
  User
  (get-row [_ data]
    (first (select user
                   (with company
                         (where {:company_user.is-default 1}))
                   (where {:username (:username data)
                           :active 1}))))

  (insert [_ data]
    (let [data (:values data)]
      (:generated-key (km/insert user
                                 (values {:username (:username data)
                                          :name (:name data)
                                          :password (crypt/encrypt (:password data))}))))))

(extend-protocol IUser
  User
  (get-id [model username]
    (:id (get-row model {:username username})))

  (set-default-company [_ user-id company-id]
    (km/update company_user
               (set-fields {:is-default 0})
               (where {:user-id user-id}))
    (km/update company_user
               (set-fields {:is-default 1})
               (where {:user-id user-id
                       :company-id company-id}))))

(def users (User. user))
