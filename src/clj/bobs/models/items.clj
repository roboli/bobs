(ns bobs.models.items
  (:require [korma.core :as km :refer :all :exclude [insert update delete]]
            [korma.db :refer [transaction]]
            [bobs.models.entities :refer [item item-type item_log inventory currency brand uom lot lot_warehouse]]
            [bobs.models.query :refer :all]
            [bobs.models.inventory :refer [last-detail group-warehouse-details]]
            [bobs.config :refer [currencies inventory-types]]))

(defprotocol IItem
  (get-images [model id])
  (get-row-sku [model company-id sku] [model company-id sku exclude-non-active])
  (last-cost [model id])
  (use-lots? [model id])
  (update-quantity [model id f n])
  (sku-available? [model sku] [model sku company-id] [model sku company-id item-id])
  (generate-sku [model])
  (stock [model id]))

(deftype Item [entity])

(extend Item
  Queryable
  (assoc basic-query
    :get-all (fn [_ data]
               (map #(assoc % :currency-symbol (get-in currencies [(:currency-code %) :symbol]))
                    (select item
                            (with currency)
                            (with brand)
                            (with uom)
                            (with inventory)
                            (fields :id :description :sku :brand-id [:brand.name :brand-name] :uom-id [:uom.abbreviation :uom-abbreviation] [:currency.code :currency-code]
                                    :price :image [:inventory.quantity :quantity])
                            (where (and {:company-id (:company-id data)}
                                        (if (:exclude-non-active data) {:active 1} true)
                                        (or {(:search-field data) [like (str "%" (:search-value data) "%")]}
                                            {:sku (if-not (:exclude-sku data) (:search-value data))})))
                            (order (:order data) (if (= (:sort data) 1) :DESC :ASC))
                            (limit (:limit data))
                            (offset (:offset data)))))
    
    :get-row (fn [_ data]
               (let [record (first (select item
                                           (with currency)
                                           (with brand)
                                           (with item-type)
                                           (with uom)
                                           (with inventory)
                                           (fields :id :description :sku :brand-id [:brand.name :brand-name] :uom-id [:uom.abbreviation :uom-abbreviation] [:uom.name :uom-name] :currency-id
                                                   [:currency.code :currency-code] :price :image [:inventory.quantity :quantity] [:inventory.allocated :allocated] :inventory.inventory-type
                                                   :inventory.lifo :is-service [:item-type.name :item-type] :item-type-id :active)
                                           (where {:id (:id data)})
                                           (limit 1)))]
                 (if record (assoc record
                              :currency-symbol (get-in currencies [(:currency-code record) :symbol])
                              :can-change-inventory-type (= (int (:quantity record)) 0)))))

    :get-count (fn [model data]
                 (:count (first (select item
                                        (aggregate (count :*) :count)
                                        (where (and {:company-id (:company-id data)}
                                                    (if (:exclude-non-active data) {:active 1} true)
                                                    (or {(:search-field data) [like (str "%" (:search-value data) "%")]}
                                                        {:sku (:search-value data)})))))))
    
    :insert (fn [model data]
              (transaction
               (let [id ((:insert basic-query) model (update-in data [:values] dissoc :inventory-type :lifo))]
                 (if-not (get-in data [:values :is-service])
                   (km/insert inventory
                              (values {:item-id id
                                       :inventory-type (get-in data [:values :inventory-type])
                                       :lifo (get-in data [:values :lifo])})))
                 (if (:audit data)
                   (let [vals (:values data)]
                     (km/insert item_log
                                (values {:item-id id
                                         :sku (:sku vals)
                                         :description (:description vals)
                                         :price (:price vals)
                                         :active (:active vals)
                                         :user-id (get-in data [:audit :user-id])}))))
                 id)))

    :update (fn [model data]
              (transaction
               ((:update basic-query) model (update-in data [:values] dissoc :inventory-type :lifo))
               (km/update inventory
                          (set-fields {:inventory-type (get-in data [:values :inventory-type])
                                       :lifo (get-in data [:values :lifo])})
                          (where {:item-id (:id data)}))
               (if (:audit data)
                 (let [vals (:values data)]
                   (km/insert item_log
                              (values {:item-id (:id data)
                                       :sku (:sku vals)
                                       :description (:description vals)
                                       :price (:price vals)
                                       :active (:active vals)
                                       :user-id (get-in data [:audit :user-id])}))))))

    :delete (fn [model data]
              (transaction
               (if (:audit data)
                 (let [vals ((:get-row basic-query) model data)]
                   (km/insert item_log
                              (values {:deleted-item-id (:id data)
                                       :sku (:sku vals)
                                       :description (:description vals)
                                       :price (:price vals)
                                       :active (:active vals)
                                       :user-id (get-in data [:audit :user-id])}))))
               ((:delete basic-query) model data)))))

(extend-protocol IItem
  Item
  (get-images [_ id]
    (first (select item
                   (fields :image :thumbnail)
                   (where {:id id}))))
  
  (get-row-sku
    ([model company-id sku] (get-row-sku model company-id sku nil))
    ([_ company-id sku exclude-non-active]
       (first (select item
                      (with brand)
                      (with uom)
                      (with inventory)
                      (fields :id :description :sku :brand-id [:brand.name :brand-name] :uom-id [:uom.abbreviation :uom-abbreviation] :price :image
                              [:inventory.quantity :quantity] [:inventory.allocated :allocated])
                      (where (and {:company-id company-id}
                                  {:sku sku}
                                  (if exclude-non-active {:active 1} true)))
                      (limit 1)))))

  (last-cost [_ id]
    (:amount (last-detail id +)))
  
  (use-lots? [model id]
    (and (not= (:is-service (get-row model {:id id})) 0)
         (= (:inventory-type (first (select inventory
                                            (where {:item-id id})))) (:perpetual inventory-types))))
  
  (update-quantity [model id f n]
    (let [qry "UPDATE inventory SET quantity = quantity %s ? WHERE item_id = ?"]
      (condp = f
        inc (exec-raw [(format qry "+") [n id]])
        dec (exec-raw [(format qry "-") [n id]]))))
  
  (sku-available?
    ([model sku] (sku-available? model sku nil nil))
    ([model sku company-id] (sku-available? model sku company-id nil))
    ([_ sku company-id item-id]
       (or (empty? sku)
           (= 0 (:count (first (select item
                                       (aggregate (count :*) :count)
                                       (where (and {:sku sku}
                                                   (if company-id {:company-id [= company-id]} true)
                                                   (if item-id {:id [not= item-id]} true))))))))))

  (generate-sku [model]
    (loop [sku nil]
      (if (nil? sku)
        (recur (let [sku (str (+ (rand-int 9999) 99))]
                 (if (sku-available? model sku) sku)))
        sku)))

  (stock [model id]
    (if-not (use-lots? model id)
      (let [entries (group-warehouse-details id +)
            withdraws (group-warehouse-details id -)]
        (vec (filter (complement nil?) (for [e entries
                                             w withdraws]
                                         (if (= (:warehouse-id e) (:warehouse-id w))
                                           (assoc e :quantity (- (:quantity e)
                                                                 (:quantity w))))))))
      (select lot
              (with lot_warehouse)
              (fields [:lot_warehouse.id :warehouse-id] [:lot_warehouse.name :warehouse-name])
              (aggregate (sum :quantity) :quantity)
              (where {:item-id id
                      :quantity [> 0]})
              (group :warehouse-id)
              (order :warehouse-id :ASC)))))

(def items (Item. item))
