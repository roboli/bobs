(ns bobs.models.entries-drafts
  (:require [bobs.models.drafts :as drafts]))
  
(def entries-drafts (drafts/->Draft (atom nil) :entry))
