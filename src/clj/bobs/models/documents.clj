(ns bobs.models.documents
  (:require [korma.core :refer :all]
            [korma.db :refer [transaction rollback]]
            [bobs.models.entities :refer :all]
            [bobs.models.query :refer [get-row]]
            [bobs.models.items :refer [items update-quantity use-lots?]]
            [bobs.models.inventory :refer [generate-history balance-at]]
            [bobs.models.lots :as lots]
            [bobs.config :refer [currencies]]
            [bobs.utils :refer [document-type-fn]]
            [bobs.i18n :refer [time-zone-offset]]))

(defprotocol IDocument
  (get-document-id [model id])
  (get-details-count [model id])
  (get-details [model data])
  (insert-from-draft [model draft-id data])
  (void-try [model id])
  (void [model id]))

(def basic-query-document
  {:build-row (fn [model data]
                (let [record (if (:include-details data)
                               (assoc (:record data) :details (get-details model {:id (:id data)}))
                               (:record data))]
                  (assoc record :currency-symbol (get-in currencies [(:currency-code record) :symbol]))))
   
   :get-count (fn [model data]
                (:count (first (select (.entity model)
                                       (with document
                                             (with document_configuration))
                                       (with warehouse)
                                       (aggregate (count :*) :count)
                                       (where (and {:document-configuration.company-id (:company-id data)}
                                                   (if (:number data) {:document.number (:number data)} true)
                                                   (if (:warehouse-id data) {:warehouse.id (:warehouse-id data)} true)
                                                   (if (:from-date data) {(raw (str "DATE(CONVERT_TZ(document.created_on, @@session.time_zone,'" time-zone-offset "'))")) [>= (:from-date data)]} true)
                                                   (if (:to-date data) {(raw (str "DATE(CONVERT_TZ(document.created_on, @@session.time_zone,'" time-zone-offset "'))")) [<= (:to-date data)]} true)))))))

   :get-withdraw-count (fn [model data]
                         ;; First group subquery by document_id,
                         ;; then COUNT on that subquery.
                         ;; This code needs improvement as I found
                         ;; no way to make a table alias properly.
                         (let [ent (assoc
                                       (create-entity (table
                                                       (subselect (.entity model)
                                                                  (with document
                                                                        (with document_configuration))
                                                                  (join document_item (= :document-item.document-id :document.id))
                                                                  (join document_item_lot (= :document-item-lot.document-item-id :document-item.id))
                                                                  (join lot (= :lot.id :document-item-lot.lot-id))
                                                                  (fields :document.id)
                                                                  (where (and {:document-configuration.company-id (:company-id data)}
                                                                              (if (:number data) {:document.number (:number data)} true)
                                                                              (if (:warehouse-id data) {:lot.warehouse-id (:warehouse-id data)} true)
                                                                              (if (:from-date data) {(raw (str "DATE(CONVERT_TZ(document.created_on, @@session.time_zone,'" time-zone-offset "'))")) [>= (:from-date data)]} true)
                                                                              (if (:to-date data) {(raw (str "DATE(CONVERT_TZ(document.created_on, @@session.time_zone,'" time-zone-offset "'))")) [<= (:to-date data)]} true)))
                                                                  (group :document.id))
                                                       :false-alias))
                                     :alias :t)]
                           (:count (first (select ent (aggregate (count :*) :count))))))

   :get-record-row-number (fn [model data]
                            (transaction
                             (exec-raw ["SET @row := 0"])
                             (select [(subselect (.entity model)
                                                 (with document
                                                       (with document_configuration))
                                                 (fields [(raw "@row := @row + 1") :row] :id)
                                                 (where {:document-configuration.company-id (:company-id data)})
                                                 (order (:order data) (if (= (:sort data) 1) :DESC :ASC))) :temp]
                                     (where {:id (:id data)}))))})

(def basic-document
  {:get-document-id (fn [model id]
                      (:document-id (first (select (.entity model)
                                                   (fields :document-id)
                                                   (where {:id id})))))

   :get-details-count (fn [model id]
                        (:count (first (select document_item
                                               (aggregate (count :*) :count)
                                               (where {:document-id (get-document-id model id)})))))

   :get-details (fn [model data]
                  (select document_item
                          (with item)
                          (with uom)
                          (with document_item_inventory
                                (with warehouse))
                          (with document_item_lot
                                (with lot
                                      (with lot_warehouse)))
                          (fields :id :row-number :item-id [:item.description :item-description] [:uom.abbreviation :uom-abbreviation] [:amount (.amount-key model)]
                                  :quantity [:item.image :image]
                                  [(raw "IFNULL(warehouse.id, lot_warehouse.id) AS 'warehouse-id'")]
                                  [(raw "IFNULL(warehouse.name, lot_warehouse.name) AS 'warehouse-name'")]
                                  [(raw "document_item.amount * document_item.quantity") :total])
                          (modifier "DISTINCT")
                          (where {:document-id (get-document-id model (:id data))})
                          (order :row-number (if (= (:sort data) 1) :DESC :ASC))
                          (limit (:limit data))
                          (offset (:offset data))))

   :void-try (fn [model id]
               (let [id (get-document-id model id)
                     doc (first (select document
                                        (with document_configuration)
                                        (fields :void [:document_configuration.document-type :document-type])
                                        (where {:id id})))]
                 (if (:void doc)
                   :already-void
                   (if (= (document-type-fn (:document-type doc)) -)
                     nil
                     (let [items-qtys (select document_item
                                              (with document_item_lot)
                                              (fields :item-id :quantity [:document_item_lot.lot-id :lot-id])
                                              (where {:document-id id}))]
                       (loop [xs items-qtys
                              voidable? true]
                         (if (empty? xs)
                           nil
                           (if-not voidable?
                             :negative-balance
                             (let [x (first xs)
                                   item-id (:item-id x)]
                               (if (use-lots? items item-id)
                                 (if (lots/used? (:lot-id x))
                                   (recur xs false)
                                   (recur (rest xs) true))
                                 (if (neg? (:quantity (get-row items {:id item-id})))
                                   (recur xs false)
                                   (if (some neg? (map :balance (generate-history item-id
                                                                                  :begin-document-id (inc id)
                                                                                  :initial-balance (balance-at item-id (dec id)))))
                                     (recur xs false)
                                     (recur (rest xs) true)))))))))))))

   :void-document (fn [model id f]
                    (transaction
                     (try
                       (if-let [document-id (get-document-id model id)]
                         (let [details (select document_item
                                               (with document_item_lot)
                                               (fields :item-id :quantity [:document_item_lot.lot-id :lot-id])
                                               (where {:document-id document-id}))]
                           (dorun (map (fn [detail]
                                         (update-quantity items (:item-id detail) f (:quantity detail))
                                         (if (use-lots? items (:item-id detail))
                                           (lots/update-quantity (:lot-id detail) f (:quantity detail))))
                                       details))
                           (update document
                                   (set-fields {:void true})
                                   (where {:id document-id}))))
                       (catch Exception ex
                         (rollback)
                         (throw ex)))))})
