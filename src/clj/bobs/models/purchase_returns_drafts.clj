(ns bobs.models.purchase-returns-drafts
  (:require [bobs.models.drafts :as drafts]))

(def purchase-returns-drafts (drafts/->Draft (atom nil) :withdraw))
