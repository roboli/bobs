(ns bobs.models.purchase-receipts-drafts
  (:require [bobs.models.drafts :as drafts]))

(def purchase-receipts-drafts (drafts/->Draft (atom nil) :entry))
