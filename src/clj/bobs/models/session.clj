(ns bobs.models.session
  (:refer-clojure :exclude [get get-in])
  (:require [noir.session :as nse]
            [noir.util.crypt :as crypt]
            [bobs.models.query :refer [get-row]]
            [bobs.models.users :refer [users]]))

;; Use only for testing/debugging purposes
(def one-user-app (atom nil))

(defn- get-id []
  (if-not (nil? @one-user-app)
    (if-not (nil? (:id @one-user-app))
      (:id @one-user-app)
      (let [id (clojure.core/get-in @one-user-app [:user :username])]
        (swap! one-user-app assoc :id id)
        id))
    (if-not (nil? (nse/get :id))
      (nse/get :id)
      (let [id (str (nse/get-in [:user :username]) (rand-int 9999))]
        (nse/put! :id id)
        id))))

(defn- build-user [user]
  {:id (:id user)
   :username (:username user)
   :name (:name user)})

(defn- build-company [user]
  (let [company (first (:company user))]
    {:id (:id company)
     :alias (:alias company)
     :name (:name company)}))

(defn active? []
  (not (and (nil? @one-user-app) (nil? (nse/get :user)))))

(defn get [k]
  (if (= k :id)
    (get-id)
    (or (k @one-user-app) (nse/get k))))

(defn get-in [ks]
  (or (clojure.core/get-in @one-user-app ks) (nse/get-in ks)))

(defn is-valid-user? [username password]
  (let [user (get-row users {:username username})]
    (and user (crypt/compare password (:password user)))))

(defn start! [user]
  (nse/put! :user (build-user user))
  (nse/put! :company (build-company user)))

(defn end! []
  (nse/remove! :user))

(defn one-user-start! [user]
  (reset! one-user-app {:user (build-user user)
                        :company (build-company user)}))

(defn one-user-end! []
  (reset! one-user-app nil))
