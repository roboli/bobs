(ns bobs.models.sales-returns-drafts
  (:require [bobs.models.drafts :as drafts]))

(def sales-returns-drafts (drafts/->Draft (atom nil) :entry))
