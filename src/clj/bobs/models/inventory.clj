(ns bobs.models.inventory
  (:require [korma.core :refer :all]
            [bobs.models.entities :refer :all]
            [bobs.utils :refer [document-type-fn find-doc-types]]
            [bobs.config :refer [document-types]]))

(def document-types-id (reduce #(assoc %1 (:id (val %2)) (key %2)) {} document-types))

(defn- build-query [item-id document-id warehouse-id]
  (fn [f]
    (-> (select* document_item)
        (with document
              (with document_configuration))
        (with document_item_inventory
              (with warehouse))
        (with document_item_lot
              (with lot
                    (with lot_warehouse)))
        (aggregate (sum :document_item.quantity) :quantity)
        (where {:item-id item-id
                :document-id [<= document-id]
                :document.void 0
                :document_configuration.document-type [in (find-doc-types f)]})
        (cond-> warehouse-id
                (where {(raw "IFNULL(warehouse.id,lot_warehouse.id)") warehouse-id})))))

(defn balance-at [item-id document-id & {:keys [warehouse-id]}]
  (let [init-balance (:balance-forward (first (select inventory
                                                      (where {:item-id item-id}))))
        qry (build-query item-id document-id warehouse-id)
        entries (or (:quantity (first (select (qry +)))) 0)
        withdraws (or (:quantity (first (select (qry -)))) 0)]
    (- (+ init-balance entries) withdraws)))

(defn count-history-lines [item-id & {:keys [begin-document-id
                                             end-document-id
                                             warehouse-id]
                                      :or {begin-document-id 0}}]
  (-> (select* document_item)
      (with document_item_inventory
            (with warehouse))
      (with document_item_lot
            (with lot
                  (with lot_warehouse)))
      (aggregate (count :*) :count)
      (where {:item-id item-id})
      (cond-> end-document-id
              (where {:document-id [between [begin-document-id end-document-id]]}))
      (cond-> (nil? end-document-id)
              (where {:document-id [>= begin-document-id]}))
      (cond-> warehouse-id
              (where {(raw "IFNULL(warehouse.id,lot_warehouse.id)") warehouse-id}))
      (exec)
      (first)
      :count))

(defn generate-history [item-id & {:keys [begin-document-id
                                          end-document-id
                                          initial-balance
                                          warehouse-id]
                                   :or {begin-document-id 0
                                        initial-balance 0}
                                   :as opts}]
  (let [details (-> (select* document_item)
                    (with document
                          (with document_configuration))
                    (with document_item_inventory
                          (with warehouse))
                    (with document_item_lot
                          (with lot
                                (with lot_warehouse)))
                    (fields :document-id [:document.number :number] [:document.created_on :date]
                            [:document_configuration.document-type :document-type] [:document.void :void] :quantity
                            [(raw "IFNULL(warehouse.id,lot_warehouse.id)") :warehouse-id]
                            [(raw "IFNULL(warehouse.name, lot_warehouse.name)") :warehouse-name])
                    (modifier "DISTINCT")
                    (where {:item-id item-id})
                    (cond-> warehouse-id
                            (where {(raw "IFNULL(warehouse.id,lot_warehouse.id)") warehouse-id}))
                    (cond-> end-document-id
                            (where {:document-id [between [begin-document-id end-document-id]]}))
                    (cond-> (nil? end-document-id)
                            (where {:document-id [>= begin-document-id]}))
                    (limit (:limit opts))
                    (offset (:offset opts))
                    (exec))]
    (reduce #(conj %1 (hash-map :document-id (:document-id %2)
                                :number (:number %2)
                                :document-type (get document-types-id (:document-type %2))
                                :date (:date %2)
                                :void (:void %2)
                                :warehouse-name (:warehouse-name %2)
                                :withdraw (if (= (document-type-fn (:document-type %2)) -)
                                            (if-not (:void %2) (:quantity %2) 0))
                                :entry (if (= (document-type-fn (:document-type %2)) +)
                                         (if-not (:void %2) (:quantity %2) 0))
                                :balance (if-not (:void %2)
                                           ((document-type-fn (:document-type %2)) (:balance (last %1)) (:quantity %2))
                                           (:balance (last %1)))))
            [{:balance initial-balance}]
            details)))

(defn last-detail [item-id document-type-fn]
  (first (select document_item
                 (with document
                       (with document_configuration))
                 (where {:item-id item-id
                         :document_configuration.document-type [in (find-doc-types document-type-fn)]})
                 (order :id :DESC)
                 (limit 1))))

(defn first-document-id [item-id date]
  (:id (first (select document_item
                      (with document)
                      (fields [:document.id :id])
                      (where (and {:item-id item-id}
                                  {(raw "DATE(document.created_on)") [>= date]}))
                      (order :id :ASC)
                      (limit 1)))))

(defn last-document-id [item-id date]
  (:id (first (select document_item
                      (with document)
                      (fields [:document.id :id])
                      (where (and {:item-id item-id}
                                  {(raw "DATE(document.created_on)") [<= date]}))
                      (order :id :DESC)
                      (limit 1)))))

(defn group-warehouse-details [item-id f & {:keys [begin-document-id
                                                   end-document-id
                                                   initial-balance]
                                            :or {begin-document-id 0
                                                 initial-balance 0}
                                            :as opts}]
  (-> (select* document_item)
      (with document
            (with document_configuration))
      (with document_item_inventory
            (with warehouse))
      (fields [:warehouse.id :warehouse-id] [:warehouse.name :warehouse-name])
      (aggregate (sum :quantity) :quantity)
      (where (and {:item-id item-id}
                  {:document_configuration.document-type [in (find-doc-types f)]}))
      (cond-> end-document-id
              (where {:document-id [between [begin-document-id end-document-id]]}))
      (cond-> (nil? end-document-id)
              (where {:document-id [>= begin-document-id]}))
      (group :warehouse.id)
      (order :warehouse.id :ASC)
      (exec)))
