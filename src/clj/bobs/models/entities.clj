(ns bobs.models.entities
  (:require [korma.core :refer [defentity table belongs-to has-one has-many many-to-many]]
            [bobs.models.db :only [db]]))

(declare partner document)

(defentity company)

(defentity company_user)

(defentity user
  (many-to-many company :company_user))

(defentity currency)

(defentity brand_log)

(defentity brand)

(defentity uom)

(defentity supplier
  (belongs-to partner))

(defentity customer
  (belongs-to partner))

(defentity partner
  (has-one supplier)
  (has-one customer))

(defentity warehouse)

(defentity inventory)

(defentity item-type)

(defentity item_log)

(defentity item
  (has-one inventory)
  (belongs-to currency)
  (belongs-to brand)
  (belongs-to item-type)
  (belongs-to uom))

(defentity lot_warehouse
  (table :warehouse :lot_warehouse))

(defentity lot
  (belongs-to lot_warehouse))

(defentity document_item_inventory
  (belongs-to warehouse))

(defentity document_item_lot
  (belongs-to lot))

(defentity document_item
  (has-one document_item_inventory)
  (has-one document_item_lot)
  (belongs-to document)
  (belongs-to item)
  (belongs-to uom))

(defentity document_configuration)

(defentity document
  (has-many document_item)
  (belongs-to document_configuration)
  (belongs-to currency))

(defentity purchase_receipt
  (belongs-to document)
  (belongs-to supplier)
  (belongs-to warehouse))

(defentity purchase_return
  (belongs-to document)
  (belongs-to supplier))

(defentity shipment
  (belongs-to document)
  (belongs-to customer))

(defentity sales_return
  (belongs-to document)
  (belongs-to customer)
  (belongs-to warehouse))

(defentity entry
  (belongs-to document)
  (belongs-to warehouse))

(defentity withdraw
  (belongs-to document))
