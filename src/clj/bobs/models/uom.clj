(ns bobs.models.uom
  (:require [bobs.models.entities :as ent])
  (:import bobs.models.query.Query))

(def uom (Query. ent/uom))
