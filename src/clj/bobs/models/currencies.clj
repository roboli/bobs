(ns bobs.models.currencies
  (:require [korma.core :as km :refer :all :exclude [insert update delete]]
            [bobs.models.entities :refer [currency]]
            [bobs.models.query :refer :all]
            [bobs.config :as config :exclude [currencies]]))

(defprotocol ICurrency
  (get-default [model company-id]))

(deftype Currency [entity])

(extend-protocol Queryable
  Currency
  (insert [model data]
    ((:insert basic-query) model data)))

(extend-protocol ICurrency
  Currency
  (get-default [_ company-id]
    (let [record (first (select currency
                                (where {:company-id company-id
                                        :is-default true})))]
      (assoc (get config/currencies (:code record)) :id (:id record)))))

(def currencies (Currency. currency))
