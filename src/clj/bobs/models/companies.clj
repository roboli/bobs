(ns bobs.models.companies
  (:require [korma.core :as km :refer :all :exclude [insert update delete]]
            [bobs.models.entities :refer [company company_user]]
            [bobs.models.query :refer :all]))

(defprotocol ICompany
  (add-user [model data]))

(deftype Company [entity])

(extend Company
  Queryable
  basic-query)

(extend-protocol ICompany
  Company
  (add-user [_ data]
    (:generated-key (km/insert company_user
                               (values (:values data))))))

(def companies (Company. company))
