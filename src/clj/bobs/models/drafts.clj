(ns bobs.models.drafts
  (:require [bobs.models.query :as query]
            [bobs.models.items :refer [items get-row-sku]]
            [bobs.models.warehouses :refer [warehouses]]
            [bobs.models.currencies :refer [currencies get-default]]
            [bobs.models.session :as session]
            [bobs.models.lots :as lots]
            [bobs.config :refer [amount-keys]]
            [bobs.utils :refer [str->int]]))

(defn find-detail [details amount-key item-id warehouse-id amount]
  (some #(if (and (= item-id (:item-id %))
                  (= warehouse-id (:warehouse-id %))
                  (= amount (amount-key %))) %)
        details))

(defn group-items [details]
  (let [groups (group-by #(vector (:item-id %)
                                  (:warehouse-id %)) details)]
    (for [gp (vals groups)]
      (reduce #(assoc %1
                 :item-id (:item-id %2)
                 :description (:item-description %2)
                 :warehouse-id (:warehouse-id %2)
                 :quantity (+ (:quantity %1)
                              (:quantity %2)))
              {:quantity 0}
              gp))))

(defprotocol IDraft
  (get-all [model])
  (get-row [model id])
  (insert [model company-id])
  (delete [model id])
  (get-currency-symbol [model id])
  (get-quantity [model id])
  (get-total [model id])
  (get-details-count [model id])
  (get-details-page [model id data])
  (get-detail-row [model id detail-id])
  (get-detail-record-row-number [model id detail-id])
  (insert-detail [model id data])
  (delete-detail [model id detail-id])
  (sum-item-quantity [model id item-id warehouse-id])
  (items-not-available [model id]))

(def basic-draft
  {:get-all (fn [model]
              (get (deref (.atom model)) (session/get :id)))

   :get-row (fn [model id]
              (let [draft (get-in (deref (.atom model)) [(session/get :id) id])]
                {:id id
                 :currency-id (:currency-id draft)
                 :quantity (:quantity draft)
                 :total (:total draft)
                 :details (vals (:details draft))}))

   :insert (fn [model company-id]
             (let [id (str (rand-int 9999))
                   currency (get-default currencies company-id)]
               (swap! (.atom model) update-in [(session/get :id)] assoc id {:currency-id (:id currency)
                                                                            :currency-symbol (:symbol currency)
                                                                            :quantity 0
                                                                            :total 0
                                                                            :details (sorted-map)})
               id))

   :delete (fn [model id]
             (swap! (.atom model) update-in [(session/get :id)] dissoc id))

   :get-currency-symbol (fn [model id]
                           (get-in (deref (.atom model)) [(session/get :id) id :currency-symbol]))

   :get-quantity (fn [model id]
                   (get-in (deref (.atom model)) [(session/get :id) id :quantity]))

   :get-total (fn [model id]
                (get-in (deref (.atom model)) [(session/get :id) id :total]))

   :get-details-count (fn [model id]
                        (count (get-in (deref (.atom model)) [(session/get :id) id :details])))

   :get-details-page (fn [model id data]
                       (let [details (get-in (deref (.atom model)) [(session/get :id) id :details])]
                         (vals (take (:limit data) (drop (:offset data) details)))))

   :get-detail-row (fn [model id detail-id]
                     (get-in (deref (.atom model)) [(session/get :id) id :details (str->int detail-id)]))

   :get-detail-record-row-number (fn [model id detail-id]
                                   (first (keep-indexed (fn [i v] (if (= (str->int detail-id) (:id v)) (inc i))) (:details (get-row model id)))))

   :insert-detail (fn [model id data]
                    (let [atom (.atom model)
                          amount-key ((.draft-type model) amount-keys)
                          item (get-row-sku items (:company-id data) (:sku data))
                          detail {:item-id (:id item)
                                  amount-key (Double/parseDouble (amount-key data))
                                  :quantity (Double/parseDouble (:quantity data))}
                          detail (if (= (.draft-type model) :withdraw)
                                   (let [warehouse (query/get-row warehouses {:id (:warehouse-id data)})]
                                     (-> detail
                                         (assoc :warehouse-id (:id warehouse))
                                         (assoc :warehouse-name (:name warehouse))))
                                   detail)
                          detail (if-let [old (find-detail (:details (get-row model id))
                                                           amount-key
                                                           (:item-id detail)
                                                           (:warehouse-id detail)
                                                           (amount-key detail))]
                                   (let [detail (assoc detail :quantity (+ (:quantity old) (:quantity detail)))]
                                     (delete-detail model id (:id old))
                                     detail)
                                   detail)
                          detail-id (inc (or (last (keys (get-in @atom [(session/get :id) id :details]))) 0))
                          detail (-> detail
                                     (assoc :item-description (:description item)
                                            :image (:image item)
                                            :uom-id (:uom-id item)
                                            :uom-abbreviation (:uom-abbreviation item))
                                     (assoc :id detail-id)
                                     (assoc :total (* (amount-key detail) (:quantity detail))))]
                      (swap! atom update-in [(session/get :id) id :details] assoc detail-id detail)
                      (swap! atom update-in [(session/get :id) id :quantity] (fn [quantity] (+ quantity (:quantity detail))))
                      (swap! atom update-in [(session/get :id) id :total] (fn [total] (+ total (:total detail))))
                      detail-id))

   :delete-detail (fn [model id detail-id]
                    (let [row (get-detail-row model id detail-id)]
                      (swap! (.atom model) update-in [(session/get :id) id :total] (fn [total] (- total (:total row))))
                      (swap! (.atom model) update-in [(session/get :id) id :quantity] (fn [quantity] (- quantity (:quantity row))))
                      (swap! (.atom model) update-in [(session/get :id) id :details] dissoc (str->int detail-id))))

   :sum-item-quantity (fn [model id item-id warehouse-id]
                        (or (:quantity (some #(if (and (= (:item-id %) item-id)
                                                       (= (:warehouse-id %) (str->int warehouse-id))) %)
                                             (group-items (:details (get-row model id))))) 0))

   :items-not-available (fn [model id]
                          (let [items (group-items (:details (get-row model id)))]
                            (some #(if-not (:available %) {:id (:id %)
                                                           :description (:description %)
                                                           :quantity (:quantity %)})
                                  (map #(hash-map
                                         :id (:item-id %)
                                         :description (:description %)
                                         :quantity (:quantity %)
                                         :available (lots/available? (:item-id %) (:warehouse-id %) (str (:quantity %))))
                                       items))))})

(deftype Draft [atom draft-type])

(extend Draft
  IDraft
  basic-draft)
