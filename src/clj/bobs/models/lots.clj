(ns bobs.models.lots
  (:require [korma.core :as km :refer :all :exclude [insert update delete]]
            [bobs.models.entities :refer [lot document_item document document_configuration document_item_lot]]
            [bobs.models.query :refer :all]
            [bobs.models.items :refer [items use-lots?]]
            [bobs.utils :refer [str->double find-doc-types]]))

(defn used? [id]
  (let [entry-quantity (:quantity (first (select document_item
                                                 (with document
                                                       (with document_configuration))
                                                 (with document_item_lot)
                                                 (fields :quantity)
                                                 (where {:document_configuration.document-type [in (find-doc-types +)]
                                                         :document_item_lot.lot-id id}))))]
    (not= entry-quantity (:quantity (first (select lot
                                                   (fields :quantity)
                                                   (where {:id id})))))))

(defn available? [item-id warehouse-id quantity]
  (if (use-lots? items item-id)
    (if-let [available (:quantity (first (select lot
                                                 (aggregate (sum :quantity) :quantity)
                                                 (where {:item-id item-id
                                                         :warehouse-id warehouse-id
                                                         :quantity [> 0]}))))]
      (>= available (str->double quantity))
      false)
    true))

(defn available [item-id warehouse-id quantity]
  (let [item (get-row items {:id item-id})
        lots (select lot
                     (fields :id :quantity)
                     (where {:item-id item-id
                             :warehouse-id warehouse-id
                             :quantity [> 0]})
                     (order :id (if (:lifo item) :DESC :ASC)))]
    (loop [lots lots
           quantity quantity
           available []]
      (if (= quantity 0)
        available
        (let [lot (first lots)]
          (if (>= (:quantity lot) quantity)
            (recur nil 0 (conj available {:id (:id lot)
                                          :quantity quantity}))
            (recur (rest lots) (- quantity (:quantity lot)) (conj available {:id (:id lot)
                                                                             :quantity (:quantity lot)}))))))))

(defn update-quantity [id f quantity]
  (let [qry "UPDATE lot SET quantity = quantity %s ? WHERE id = ?"]
    (condp = f
      inc (exec-raw [(format qry "+") [quantity id]])
      dec (exec-raw [(format qry "-") [quantity id]]))))
