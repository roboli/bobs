(ns bobs.models.withdraws-drafts
  (:require [bobs.models.drafts :as drafts]))

(def withdraws-drafts (drafts/->Draft (atom nil) :withdraw))
