(ns bobs.models.partners
  (:require [korma.core :as km :refer :all :exclude [insert update delete]]
            [korma.db :refer [transaction rollback]]
            [bobs.models.entities :refer [partner supplier customer]]
            [bobs.models.query :refer :all])
  (:import [com.mysql.jdbc.exceptions.jdbc4 MySQLIntegrityConstraintViolationException]))

(def not-empty? (complement empty?))

(defprotocol IPartner
  (supplier? [model id])
  (customer? [model id])
  (uid-available? [model uid] [model uid company-id] [model uid company-id partner-id])
  (get-all-suppliers [model id])
  (get-all-customers [model id])
  (upgrade-to-supplier [model id])
  (upgrade-to-customer [model id])
  (can-downgrade-from-supplier? [model id])
  (can-downgrade-from-customer? [model id])
  (downgrade-from-supplier [model id])
  (downgrade-from-customer [model id]))

(deftype Partner [entity])

(extend Partner
  Queryable
  (assoc basic-query
    :get-all (fn [_ data]
               (select partner
                       (with supplier)
                       (with customer)
                       (fields :id :name :address :telephone :email :uid [:supplier.partner-id :is-supplier] [:customer.partner-id :is-customer])
                       (where (and {:company-id (:company-id data)}
                                   {(:search-field data) [like (str "%" (:search-value data) "%")]}
                                   (if (:suppliers-only data) {:supplier.partner-id [not= nil]} true)
                                   (if (:customers-only data) {:customer.partner-id [not= nil]} true)))
                       (order (:order data) (if (= (:sort data) 1) :DESC :ASC))
                       (limit (:limit data))
                       (offset (:offset data))))

    :get-row (fn [model data]
               (let [id (:id data)
                     row (first (select partner
                                        (with supplier)
                                        (with customer)
                                        (fields :id :name :address :telephone :email :uid [:supplier.partner-id :is-supplier] [:customer.partner-id :is-customer])
                                        (where {:id id})
                                        (limit 1)))]
                 (if-not (empty? row)
                   (assoc row
                     :can-downgrade-from-supplier (can-downgrade-from-supplier? model id)
                     :can-downgrade-from-customer (can-downgrade-from-customer? model id))
                   row)))

    :insert (fn [model data]
              (let [values (:values data)
                    id ((:insert basic-query) model {:values (dissoc values :is-supplier :is-customer)})]
                (if (:is-supplier values)
                  (upgrade-to-supplier model id))
                (if (:is-customer values)
                  (upgrade-to-customer model id))
                id))

    :update (fn [model data]
              (let [values (:values data)
                    id (:id data)]
                ((:update basic-query) model {:id id
                                              :values (dissoc values :is-supplier :is-customer)})
                (if-not (:is-supplier values)
                  (downgrade-from-supplier model id)
                  (if-not (supplier? model id)
                    (upgrade-to-supplier model id)))
                (if-not (:is-customer values)
                  (downgrade-from-customer model id)
                  (if-not (customer? model id)
                    (upgrade-to-customer model id)))))))

(extend-protocol IPartner
  Partner
  (supplier? [_ id]
    (not-empty? (select supplier
                        (where {:partner-id id}))))

  (customer? [_ id]
    (not-empty? (select customer
                        (where {:partner-id id}))))

  (uid-available?
    ([model uid] (uid-available? model uid nil nil))
    ([model uid company-id] (uid-available? model uid company-id nil))
    ([_ uid company-id partner-id]
       (or (empty? uid)
           (= 0 (:count (first (select partner
                                       (aggregate (count :*) :count)
                                       (where (and {:uid uid}
                                                   (if company-id {:company-id [= company-id]} true)
                                                   (if partner-id {:id [not= partner-id]} true))))))))))

  (get-all-suppliers [_ company-id]
    (select supplier
            (with partner)
            (fields :id :partner.name)
            (where {:partner.company-id company-id})
            (order :partner.name)))
  
  (get-all-customers [_ company-id]
    (select customer
            (with partner)
            (fields :id :partner.name)
            (where {:partner.company-id company-id})
            (order :partner.name)))
  
  (upgrade-to-supplier [_ id]
    (km/insert supplier
               (values {:partner-id id})))
  
  (upgrade-to-customer [_ id]
    (km/insert customer
               (values {:partner-id id})))

  (can-downgrade-from-supplier? [_ id]
    (transaction
     (try
       (km/delete supplier (where {:partner-id id}))
       (rollback)
       true
       (catch MySQLIntegrityConstraintViolationException ex
         false))))
  
  (can-downgrade-from-customer? [_ id]
    (transaction
     (try
       (km/delete customer (where {:partner-id id}))
       (rollback)
       true
       (catch MySQLIntegrityConstraintViolationException ex
         false))))
  
  (downgrade-from-supplier [_ id]
    (km/delete supplier
               (where {:partner-id id})))
  
  (downgrade-from-customer [_ id]
    (km/delete customer
               (where {:partner-id id}))))

(def partners (Partner. partner))
