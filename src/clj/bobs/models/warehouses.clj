(ns bobs.models.warehouses
  (:require [bobs.models.entities :refer [warehouse]])
  (:import bobs.models.query.Query))

(def warehouses (Query. warehouse))
