(ns bobs.models.brands
  (:require [korma.core :as km :refer :all :exclude [insert update delete]]
            [korma.db :refer [transaction]]
            [bobs.models.entities :refer [brand brand_log]]
            [bobs.models.query :refer :all]))

(deftype Brand [entity])

(extend Brand
  Queryable
  (assoc basic-query
    :insert (fn [model data]
              (transaction
               (let [id ((:insert basic-query) model data)]
                 (if (:audit data)
                   (let [vals (:values data)]
                     (km/insert brand_log
                                (values {:brand-id id
                                         :name (:name vals)
                                         :active (:active vals)
                                         :user-id (get-in data [:audit :user-id])}))))
                 id)))

    :update (fn [model data]
              (transaction
               ((:update basic-query) model data)
               (if (:audit data)
                 (let [vals (:values data)]
                   (km/insert brand_log
                              (values {:brand-id (:id data)
                                       :name (:name vals)
                                       :active (:active vals)
                                       :user-id (get-in data [:audit :user-id])}))))))

    :delete (fn [model data]
              (transaction
               (if (:audit data)
                 (let [vals ((:get-row basic-query) model data)]
                   (km/insert brand_log
                              (values {:deleted-brand-id (:id data)
                                       :name (:name vals)
                                       :active (:active vals)
                                       :user-id (get-in data [:audit :user-id])}))))
               ((:delete basic-query) model data)))))

(def brands (Brand. brand))
