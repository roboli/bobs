(ns bobs.models.shipments-drafts
  (:require [bobs.models.drafts :as drafts]))
  
(def shipments-drafts (drafts/->Draft (atom nil) :withdraw))
