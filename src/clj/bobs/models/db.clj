(ns bobs.models.db
  (:use [lobos.core :only (defcommand migrate rollback)]
        [korma.db :only (defdb)])
  (:require [lobos.migration :as lm]
            [camel-snake-kebab.core :refer [->kebab-case ->snake_case]]
            [bobs.config :refer [db-subname db-user db-password]]))

(def db-spec
  {:classname "com.mysql.jdbc.Driver"
   :subprotocol "mysql"
   :delimiters "`"
   :unsafe true
   :subname db-subname
   :user db-user
   :password db-password
   :naming {:keys ->kebab-case
            :fields ->snake_case}})

(defcommand pending-migrations []
  (lm/pending-migrations db-spec sname))

(defn actualized?
  "checks if there are no pending migrations"
  []
  (empty? (pending-migrations)))

(def actualize migrate)
(def deactualize rollback)

(defdb db db-spec)
