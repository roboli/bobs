(ns bobs.models.document-configurations
  (:require [korma.core :as km :refer :all :exclude [insert update delete]]
            [bobs.models.entities :refer [document_configuration]]
            [bobs.models.query :refer :all]))

(defprotocol IDocumentConfiguration
  (get-default [model company-id document-type])
  (pop-next-number! [model id]))

(deftype DocumentConfiguration [entity])

(extend DocumentConfiguration
  Queryable
  basic-query)

(extend-protocol IDocumentConfiguration
  DocumentConfiguration
  (get-default [_ company-id document-type]
    (first (select document_configuration
                   (where {:company-id company-id
                           :document-type document-type
                           :is-default 1}))))

  (pop-next-number! [model id]
    (let [seq-num (get-row model {:id id})
          number (if (= (:current seq-num) 0) (:initial seq-num) (inc (:current seq-num)))]
      (update model {:id id
                     :values {:current number}})
      number)))

(def document-configurations (DocumentConfiguration. document_configuration))
