(ns bobs.main
  (:use bobs.core
        ring.server.standalone
        [ring.middleware file-info file])
  (:require [clojure.tools.cli :refer [cli]]
            [bobs.models.session :as session])
  (:gen-class))

(defn get-handler []
  ;; #'app expands to (var app) so that when we reload our code,
  ;; the server is forced to re-resolve the symbol in the var
  ;; rather than having its own copy. When the root binding
  ;; changes, the server picks it up without having to restart.
  (-> #'app
      ; Makes static assets in $PROJECT_DIR/resources/public/ available.
      (wrap-file "resources")
      ; Content-Type, Content-Length, and Last Modified headers for files in body
      (wrap-file-info)))

(defn -main [& args]
  (let [[opts args banner] (cli args
                                ["-h" "--help" "Print help"
                                 :default false :flag true]
                                ["-p" "--port" "Port to use"
                                 :default 3000 :parse-fn #(Integer/parseInt %)]
                                ["-u" "--one-user-app" "Run application with only one logged in user for all requests"])
        username (:one-user-app opts)
        port (:port opts)]
    (println "Run from lein trampoline or java for displaying --help correctly")
    (if (:help opts)
      (println banner)
      (do (when username
            (let [conso (. System console)]
              (if (nil? conso)
                (println "Please run from lein trampoline or java when using --one-user-app option")
                (let [pass (String/valueOf (.readPassword conso "Password: " nil))]
                  (if (session/is-valid-user? username pass)
                    (session/one-user-start! username)
                    (do (println "Wrong username or password")
                        (System/exit 0)))))))
          (serve (get-handler)
                 {:port port
                  :init init
                  :auto-reload? true
                  :destroy destroy
                  :join? false})
          (println (str "You can view the site at http://localhost:" port))))))
