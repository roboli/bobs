(ns bobs.routes.partners
  (:require [compojure.core :refer :all]
            [liberator.core :as lib :refer [defresource request-method-in]]
            [bobs.models.query :refer :all]
            [bobs.models.partners :refer [partners uid-available? can-downgrade-from-supplier? can-downgrade-from-customer?]]
            [bobs.validators.validator :as validator]
            [bobs.validators.partners :refer [rules]]
            [bobs.routes.resources :as rsc :refer [resource-defaults resource-validator resource-delete get-params build-entry-url]]
            [bobs.models.session :as session]
            [bobs.i18n :refer [t]]))

(def validate (validator/validate rules))

(defn do-validation [data]
  (let [data (if-not (:email data) (assoc data :email "") data)]
    (validate data)))

(defn format-data [data] data)

(defresource resource (merge resource-defaults (resource-validator do-validation))
  :allowed-methods [:get :post]
  :handle-ok (fn [ctxt]
               (let [params (get-params ctxt)]
                 (get-all partners {:company-id (session/get-in [:company :id])
                                 :search-field :name
                                 :order (keyword (:order params))
                                 :suppliers-only (:suppliers-only params)
                                 :customers-only (:customers-only params)})))
  :processable? (fn [ctxt]
                  (or ((request-method-in :get) ctxt)
                      (let [params (get-params ctxt)]
                        (if-not (or (empty? (:uid params)) (uid-available? partners (:uid params) (session/get-in [:company :id])))
                          [false {:errors {:validations {:uid [(t :validations/uid-available)]}}}]
                          (if-not (or (:is-customer params) (:is-supplier params))
                            [false {:errors (t :validations/partner-must-be)}]
                            true)))))
  :handle-unprocessable-entity :errors
  :post! (fn [ctxt] {::id (insert partners {:values (assoc (format-data (get-params ctxt)) :company-id (session/get-in [:company :id]))})})
  :post-redirect? (fn [ctxt] {:location (str (build-entry-url (get ctxt :request)) "/" (get ctxt ::id))}))

(defn resource-id [id]
  (lib/resource (merge resource-defaults (resource-validator do-validation) (resource-delete partners id))
                :allowed-methods [:get :put :delete]
                :handle-ok (fn [_] (get-row partners {:id id}))
                :new? false
                :processable? (fn [ctxt]
                                (or ((request-method-in :get :delete) ctxt)
                                    (let [params (get-params ctxt)]
                                      (if-not (or (empty? (:uid params)) (uid-available? partners (:uid params) (session/get-in [:company :id]) id))
                                        [false {:errors {:validations {:uid [(t :validations/uid-available)]}}}]
                                        (if-not (or (:is-customer params) (:is-supplier params))
                                          [false {:errors (t :validations/partner-must-be)}]
                                          (if (and (not (:is-customer params)) (not (can-downgrade-from-customer? partners (:id params))))
                                            [false {:errors (t :validations/partner-cannot-downgrade-from-customer)}]
                                            (if (and (not (:is-supplier params)) (not (can-downgrade-from-supplier? partners (:id params))))
                                              [false {:errors (t :validations/partner-cannot-downgrade-from-supplier)}]
                                              true)))))))
                :handle-unprocessable-entity :errors
                :put! #(update partners {:id id
                                         :values (format-data (get-params %))})))

(def resource-pagination (rsc/resource-pagination partners :name))

(def resource-pagination-id (partial rsc/resource-pagination-id partners :name))

(defroutes partners-routes
  (ANY "/partners" [] resource)
  (ANY "/partners/page" [] resource-pagination)
  (ANY "/partners/page/:id" [id] (resource-pagination-id id))
  (ANY "/partners/:id" [id] (resource-id id)))
