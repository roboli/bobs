(ns bobs.routes.users
  (:require [compojure.core :refer :all]
            [liberator.core :refer [defresource]]
            [bobs.models.users :as users]))

(defresource resource-user-company [user-id company-id]
  :available-media-types ["text/html"]
  :handle-ok (fn [_]
               (users/set-default-company users/users user-id company-id)
               (str "user-id:  " user-id ", company-id: " company-id " ... done!")))

(defroutes users-routes
  (ANY "/users/default-company/:user-id/:company-id" [user-id company-id] (resource-user-company user-id company-id)))
