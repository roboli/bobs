(ns bobs.routes.documents
  (:require [liberator.core :as lib :exclude [resource] :refer [request-method-in]]
            [liberator.representation :refer [as-response]]
            [pdfkit-clj.core :refer [gen-pdf as-stream]]
            [bobs.routes.resources :as rsc :refer [resource-defaults resource-validator get-params build-entry-url validate-data]]
            [bobs.models.session :as session]
            [bobs.models.query :refer :all]
            [bobs.models.documents :refer :all]
            [bobs.models.drafts :as drafts]
            [bobs.models.warehouses :refer [warehouses]]
            [bobs.views.printables.summary :as summary]
            [clojure.string :as str]
            [bobs.i18n :refer [t]]))

(defn resource [model]
  (lib/resource resource-defaults
                :allowed-methods [:get]
                :handle-ok (fn [ctxt]
                             (let [params (get-params ctxt)]
                               (get-all model {:company-id (session/get-in [:company :id])
                                               :id (:id params)
                                               :from-date (:from-date params)
                                               :to-date (:to-date params)
                                               :order (keyword (:order params))})))))

(defn resource-id [model draft validator id]
  (lib/resource (merge resource-defaults (resource-validator validator))
                :allowed-methods [:get :post :put]
                :handle-ok (fn [ctxt] (get-row model {:id id
                                                      :include-details (:include-details (get-params ctxt))}))
                :new? #(not ((request-method-in :put) %))
                :malformed? #(if ((request-method-in :post) %)
                               (validate-data % validator))
                :processable? (fn [ctxt]
                                (or ((request-method-in :get) ctxt)
                                    (if ((request-method-in :post) ctxt)
                                      (if-not (> (drafts/get-details-count draft id) 0)
                                        [false {:errors {:validations {:details-count [(t :validations/details-empty)]}}}]
                                        true)
                                      (if (and ((request-method-in :put) ctxt) (:void (get-params ctxt)))
                                        (if-let [error (void-try model id)]
                                          (condp = error
                                            :already-void [false {:errors (t :validations/already-void)}]
                                            :negative-balance [false {:errors (t :validations/void-leaves-negative-balance)}])
                                          true)
                                        true))))
                :handle-unprocessable-entity :errors
                :post! (fn [ctxt]
                         {::id (insert-from-draft model id (assoc (get-params ctxt)
                                                             :company-id (session/get-in [:company :id])
                                                             :user-id (session/get-in [:user :id])))})
                :post-redirect? (fn [ctxt]
                                  (let [params (get-params ctxt)
                                        url (str
                                             (str/join "/" (drop-last (str/split (build-entry-url (get ctxt :request)) #"/")))
                                             "/"
                                             (get ctxt ::id))]
                                    (if (:respond-with-details-pagination params)
                                      {:location (str url "/details/page?include-header=1"
                                                      "&page=" (:page params)
                                                      "&sort=" (:sort params)
                                                      "&page-size=" (:page-size params))}
                                      {:location url})))
                :put! (fn [ctxt]
                        (if (:void (get-params ctxt))
                          (void model id)))))

(defn resource-id-withdraw [model draft validator id]
  (lib/resource (merge resource-defaults (resource-validator validator))
                :allowed-methods [:get :post :put]
                :handle-ok (fn [ctxt] (get-row model {:id id
                                                      :include-details (:include-details (get-params ctxt))}))
                :new? #(not ((request-method-in :put) %))
                :malformed? #(if ((request-method-in :post) %)
                               (validate-data % validator))
                :processable? (fn [ctxt]
                                (or ((request-method-in :get) ctxt)
                                    (if ((request-method-in :post) ctxt)
                                      (if-not (> (drafts/get-details-count draft id) 0)
                                        [false {:errors {:validations {:details-count [(t :validations/details-empty)]}}}]
                                        (if-let [item (drafts/items-not-available draft id)]
                                          [false {:errors (format (t :validations/quantity-no-longer-available) (:quantity item) (:description item))}]
                                          true))
                                      (if (and ((request-method-in :put) ctxt) (:void (get-params ctxt)))
                                        (if-let [error (void-try model id)]
                                          (condp = error
                                            :already-void [false {:errors (t :validations/already-void)}]
                                            :negative-balance [false {:errors (t :validations/void-leaves-negative-balance)}])
                                          true)
                                        true))))
                :handle-unprocessable-entity :errors
                :post! (fn [ctxt]
                         {::id (insert-from-draft model id (assoc (get-params ctxt)
                                                             :company-id (session/get-in [:company :id])
                                                             :user-id (session/get-in [:user :id])))})
                :post-redirect? (fn [ctxt]
                                  (let [params (get-params ctxt)
                                        url (str
                                             (str/join "/" (drop-last (str/split (build-entry-url (get ctxt :request)) #"/")))
                                             "/"
                                             (get ctxt ::id))]
                                    (if (:respond-with-details-pagination params)
                                      {:location (str url "/details/page?include-header=1"
                                                      "&page=" (:page params)
                                                      "&sort=" (:sort params)
                                                      "&page-size=" (:page-size params))}
                                      {:location url})))
                :put! (fn [ctxt]
                        (if (:void (get-params ctxt))
                          (void model id)))))

(defn resource-pagination [model]
  (lib/resource resource-defaults
                :allowed-methods [:get]
                :handle-ok (fn [ctxt]
                             (let [params (get-params ctxt)
                                   page (Integer/parseInt (:page params))
                                   page-size (Integer/parseInt (:page-size params))
                                   order (:order params)
                                   sort (:sort params)
                                   number (:number params)
                                   warehouse-id (:warehouse-id params)
                                   from-date (:from-date params)
                                   to-date (:to-date params)
                                   count (get-count model {:company-id (session/get-in [:company :id])
                                                           :number number
                                                           :warehouse-id warehouse-id
                                                           :from-date from-date
                                                           :to-date to-date})
                                   total-pages (int (Math/ceil (/ count page-size)))
                                   page (if (and (> page total-pages) (:last-page-on-miss params))
                                          total-pages
                                          page)]
                               {:page page
                                :page-size page-size
                                :order order
                                :sort sort
                                :total-pages total-pages
                                :total-records count
                                :number number
                                :warehouse-id warehouse-id
                                :from-date from-date
                                :to-date to-date
                                :records (get-all model {:company-id (session/get-in [:company :id])
                                                         :number number
                                                         :warehouse-id warehouse-id
                                                         :from-date from-date
                                                         :to-date to-date
                                                         :order (keyword order)
                                                         :sort (Integer/parseInt sort)
                                                         :limit page-size
                                                         :offset (* page-size (Math/abs (dec page)))})}))))

(defn resource-pagination-id [model id]
  (rsc/resource-pagination-id model :id id))

(defn resource-id-details [model id]
  (lib/resource resource-defaults
                :allowed-methods [:get]
                :handle-ok (fn [_] (get-details model id))))

(defn resource-id-details-pagination [model id]
  (lib/resource resource-defaults
                :allowed-methods [:get]
                :handle-ok (fn [ctxt]
                             (let [params (get-params ctxt)
                                   page (Integer/parseInt (:page params))
                                   page-size (Integer/parseInt (:page-size params))
                                   sort (:sort params)
                                   count (get-details-count model id)
                                   total-pages (int (Math/ceil (/ count page-size)))
                                   page (if (and (> page total-pages) (:last-page-on-miss params))
                                          total-pages
                                          page)
                                   response {:page page
                                             :page-size page-size
                                             :sort sort
                                             :total-pages total-pages
                                             :total-records count
                                             :records (get-details model {:id id
                                                                          :sort (Integer/parseInt sort)
                                                                          :limit page-size
                                                                          :offset (* page-size (Math/abs (dec page)))})}]
                               (if (:include-header params)
                                 (assoc response :header (get-row model {:id id}))
                                 response)))))

(defn resource-print-id [render id]
  (lib/resource
   :authorized? (:authorized? resource-defaults)
   :available-media-types ["text/html"]
   :allowed-methods [:get]
   :handle-ok (fn [_] (render id))))

(defn resource-pdf-id [doc-name model render id]
  (lib/resource
   :authorized? (:authorized? resource-defaults)
   :available-media-types ["application/octet-stream"]
   :allowed-methods [:get]
   :as-response (fn [d ctx]
                  (-> (as-response d ctx)
                      (assoc-in
                       [:headers "Content-Disposition"]
                       (str "attachment; filename=" doc-name "-" (:number (get-row model {:id id})) ".pdf"))))
   :handle-ok (fn [_] (as-stream (gen-pdf (render id)
                                          :stylesheets ["css/styles.css"
                                                        "css/bootstrap.css"])))))

(defn resource-print-summary [doc-name model render-doc]
  (lib/resource
   :authorized? (:authorized? resource-defaults)
   :available-media-types ["text/html"]
   :allowed-methods [:get]
   :handle-ok (fn [ctxt] (summary/render (get-params ctxt)
                                         doc-name
                                         model
                                         render-doc))))

(defn resource-pdf-summary [doc-name model render-doc]
  (lib/resource
   :authorized? (:authorized? resource-defaults)
   :available-media-types ["application/octet-stream"]
   :allowed-methods [:get]
   :as-response (fn [d ctxt]
                  (-> (as-response d ctxt)
                      (assoc-in
                       [:headers "Content-Disposition"]
                       (let [params (get-params ctxt)
                             from-date (:from-date params)
                             to-date (:to-date params)]
                         (str "attachment; filename="
                              (t :default/summary)
                              doc-name
                              (if (= from-date to-date)
                                (if from-date (str "_" from-date) "")
                                (str "_"
                                     (or from-date "N")
                                     "_"
                                     (or to-date "N")))
                              "_"
                              (if (:warehouse-id params)
                                (:name (get-row warehouses {:id (:warehouse-id params)}))
                                (t :default/all))
                              ".pdf")))))
   :handle-ok (fn [ctxt]
                (as-stream (gen-pdf (summary/render (get-params ctxt)
                                                    doc-name
                                                    model
                                                    render-doc)
                                    :stylesheets ["css/styles.css"
                                                  "css/bootstrap.css"])))))
