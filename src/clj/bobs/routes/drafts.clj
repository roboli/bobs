(ns bobs.routes.drafts
  (:require [liberator.core :as lib :exclude [resource] :refer [request-method-in]]
            [bobs.routes.resources :as rsc :refer [resource-defaults resource-validator get-params build-entry-url]]
            [bobs.models.session :as session]
            [bobs.models.drafts :refer :all]
            [bobs.models.items :refer [items get-row-sku sku-available?]]
            [bobs.models.document-configurations :refer [document-configurations get-default]]
            [bobs.models.lots :as lots]
            [bobs.config :refer [document-types]]
            [bobs.i18n :refer [t]]))

(defn do-validation [f amount-key]
  (fn [data]
    (let [data (assoc data
                 amount-key (or (amount-key data) "")
                 :quantity (or (:quantity data) ""))]
      (f data))))

(defn format-data [amount-key]
  (fn [data]
    (assoc data
      amount-key (apply str (remove #((set ",") %) (amount-key data)))
      :quantity (apply str (remove #((set ",") %) (:quantity data))))))

(defn resource [model document-type]
  (lib/resource resource-defaults
                :allowed-methods [:get :post]
                :handle-ok (fn [_] (get-all model))
                :processable? (fn [_] (get-default document-configurations (session/get-in [:company :id]) (get-in document-types [document-type :id])))
                :handle-unprocessable-entity (t :validations/cannot-create-document-without-sequential-number)
                :post! (fn [_] {::id (insert model (session/get-in [:company :id]))})
                :post-redirect? (fn [ctxt]
                                  (let [params (get-params ctxt)
                                        url (str (build-entry-url (get ctxt :request)) "/" (get ctxt ::id))]
                                    (if (:respond-with-details-pagination params)
                                      {:location (str url "/page?&page=" (:page params)
                                                      "&sort=" (:sort params)
                                                      "&page-size=" (:page-size params))}
                                      {:location url})))))

(defn resource-id [model validator format id]
  (lib/resource (merge resource-defaults (resource-validator validator))
                :allowed-methods [:get :post]
                :handle-ok (fn [_] (get-row model id))
                :processable? (fn [ctxt]
                                (or ((request-method-in :get :delete) ctxt)
                                    (not (sku-available? items (:sku (get-params ctxt)) (session/get-in [:company :id])))))
                :handle-unprocessable-entity {:validations {:sku [(t :validations/sku-non-existent)]}}
                :post! (fn [ctxt] {::id (insert-detail model id (format (assoc (dissoc (get-params ctxt) :id) :company-id (session/get-in [:company :id]))))})
                :post-redirect? (fn [ctxt]
                                  (let [params (get-params ctxt)
                                        url (str (build-entry-url (get ctxt :request)) "/page/" (get ctxt ::id))]
                                    (if (:respond-with-details-pagination params)
                                      {:location (str url "?page-size=" (:page-size params))}
                                      {:location url})))))

(defn resource-id-withdraw [model validator format id]
  (lib/resource (merge resource-defaults (resource-validator validator))
                :allowed-methods [:get :post]
                :handle-ok (fn [_] (get-row model id))
                :processable? (fn [ctxt]
                                (or ((request-method-in :get :delete) ctxt)
                                    (let [params (get-params ctxt)
                                          sku (:sku params)
                                          company-id (session/get-in [:company :id])]
                                      (if (sku-available? items sku company-id)
                                        [false {:errors {:validations {:sku [(t :validations/sku-non-existent)]}}}]
                                        (let [item-id (:id (get-row-sku items company-id sku))
                                              warehouse-id (:warehouse-id params)]
                                          (if-not (lots/available? item-id warehouse-id (+ (Double/parseDouble (:quantity params)) (sum-item-quantity model id item-id warehouse-id)))
                                            [false {:errors {:validations {:quantity [(t :validations/quantity-not-available)]}}}]
                                            true))))))
                :handle-unprocessable-entity :errors
                :post! (fn [ctxt] {::id (insert-detail model id (format (assoc (dissoc (get-params ctxt) :id) :company-id (session/get-in [:company :id]))))})
                :post-redirect? (fn [ctxt]
                                  (let [params (get-params ctxt)
                                        url (str (build-entry-url (get ctxt :request)) "/page/" (get ctxt ::id))]
                                    (if (:respond-with-details-pagination params)
                                      {:location (str url "?page-size=" (:page-size params))}
                                      {:location url})))))

(defn resource-detail-id [model id detail-id]
  (lib/resource resource-defaults
                :allowed-methods [:get :delete]
                :handle-ok (fn [_] (get-detail-row model id detail-id))
                :delete! (fn [_] (delete-detail model id detail-id))))

(defn resource-id-detail-pagination [model id]
  (lib/resource resource-defaults
                :allowed-methods [:get]
                :handle-ok (fn [ctxt]
                             (let [params (get-params ctxt)
                                   page (Integer/parseInt (:page params))
                                   page-size (Integer/parseInt (:page-size params))
                                   count (get-details-count model id)
                                   total-pages (int (Math/ceil (/ count page-size)))
                                   page (if (and (> page total-pages) (:last-page-on-miss params))
                                          total-pages
                                          page)]
                               {:id id
                                :page page
                                :page-size page-size
                                :total-pages total-pages
                                :total-records count
                                :currency-symbol (get-currency-symbol model id)
                                :quantity (get-quantity model id)
                                :total (get-total model id)
                                :records (get-details-page model id {:limit (if (> page-size count) count page-size)
                                                                     :offset (* page-size (Math/abs (dec page)))})}))))

(defn resource-id-detail-pagination-id [model id detail-id]
  (lib/resource resource-defaults
                :allowed-methods [:get]
                :handle-ok (fn [ctxt]
                             (let [params (get-params ctxt)
                                   row (get-detail-record-row-number model id detail-id)
                                   page-size (Integer/parseInt (:page-size params))
                                   page (int (Math/ceil (/ row page-size)))
                                   count (get-details-count model id)]
                               {:page page
                                :page-size page-size
                                :total-pages (Math/ceil (/ count page-size))
                                :total-records count
                                :currency-symbol (get-currency-symbol model id)
                                :quantity (get-quantity model id)
                                :total (get-total model id)
                                :records (get-details-page model id {:limit (if (> page-size count) count page-size)
                                                                     :offset (* page-size (Math/abs (dec page)))})}))))
