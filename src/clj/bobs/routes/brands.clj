(ns bobs.routes.brands
  (:require [compojure.core :refer :all]
            [liberator.core :as lib :refer [defresource request-method-in]]
            [bobs.models.query :refer [get-all insert]]
            [bobs.models.session :as session]
            [bobs.models.brands :refer [brands]]
            [bobs.validators.validator :as validator]
            [bobs.validators.brands :refer [rules]]
            [bobs.routes.resources :as rsc]))

(def validate (validator/validate rules))

(defn do-validation [data]
  (validate data))

(defn format-data [data] data)

(defresource resource (merge rsc/resource-defaults (rsc/resource-validator do-validation))
  :allowed-methods [:get :post]
  :handle-ok (fn [ctxt]
               (get-all brands {:company-id (session/get-in [:company :id])
                                :search-field :name
                                :order (keyword (:order (rsc/get-params ctxt)))}))
  :post! (fn [ctxt] {::id (insert brands {:values (assoc
                                                      (format-data (rsc/get-params ctxt))
                                                    :company-id (session/get-in [:company :id])
                                                    :user-id (session/get-in [:user :id]))
                                          :audit {:user-id (session/get-in [:user :id])}})})
  :post-redirect? (fn [ctxt] {:location (str (rsc/build-entry-url (get ctxt :request)) "/" (get ctxt ::id))}))

(def resource-id (partial rsc/resource-id brands do-validation format-data))

(def resource-pagination (rsc/resource-pagination brands :name))

(def resource-pagination-id (partial rsc/resource-pagination-id brands :name))

(defroutes brands-routes
  (ANY "/brands" [] resource)
  (ANY "/brands/page" [] resource-pagination)
  (ANY "/brands/page/:id"[id] (resource-pagination-id id))
  (ANY "/brands/:id" [id] (resource-id id)))
