(ns bobs.routes.warehouses
  (:require [compojure.core :refer :all]
            [bobs.models.warehouses :refer [warehouses]]
            [bobs.validators.validator :as validator]
            [bobs.validators.warehouses :refer [rules]]
            [bobs.routes.resources :as rsc]))

(def validate (validator/validate rules))

(defn do-validation [data]
  (validate data))

(defn format-data [data] data)

(def resource (rsc/resource warehouses do-validation format-data :name))

(def resource-id (partial rsc/resource-id warehouses do-validation format-data))

(def resource-pagination (rsc/resource-pagination warehouses :name))

(def resource-pagination-id (partial rsc/resource-pagination-id warehouses :name))

(defroutes warehouses-routes
  (ANY "/warehouses" [] resource)
  (ANY "/warehouses/page"[] resource-pagination)
  (ANY "/warehouses/page/:id" [id] (resource-pagination-id id))
  (ANY "/warehouses/:id" [id] (resource-id id)))
