(ns bobs.routes.shipments-drafts
  (:require [compojure.core :refer :all]
            [bobs.routes.drafts :as rsc]
            [bobs.models.shipments-drafts :refer [shipments-drafts]]
            [bobs.validators.validator :as validator]
            [bobs.validators.shipment-details :refer [rules]]))

(def validate-detail (validator/validate rules))

(def resource (rsc/resource shipments-drafts :shipment))

(def resource-id (partial rsc/resource-id-withdraw shipments-drafts
                          (rsc/do-validation validate-detail :price)
                          (rsc/format-data :price)))

(def resource-detail-id (partial rsc/resource-detail-id shipments-drafts))

(def resource-id-detail-pagination (partial rsc/resource-id-detail-pagination shipments-drafts))

(def resource-id-detail-pagination-id (partial rsc/resource-id-detail-pagination-id shipments-drafts))

(defroutes shipments-drafts-routes
  (ANY "/shipments/drafts" [] resource)
  (ANY "/shipments/drafts/:id" [id] (resource-id id))
  (ANY "/shipments/drafts/:id/page" [id] (resource-id-detail-pagination id))
  (ANY "/shipments/drafts/:id/page/:detail-id" [id detail-id] (resource-id-detail-pagination-id id detail-id))
  (ANY "/shipments/drafts/:id/:detail-id" [id detail-id] (resource-detail-id id detail-id)))
