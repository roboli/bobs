(ns bobs.routes.currencies
  (:require [compojure.core :refer :all]
            [liberator.core :refer [defresource]]
            [bobs.routes.resources :refer [resource-defaults]]
            [bobs.models.session :as session]
            [bobs.models.currencies :refer [currencies get-default]]))

(defresource resource-default resource-defaults
  :allowed-methods [:get]
  :handle-ok (fn [_] (get-default currencies (session/get-in [:company :id]))))

(defroutes currencies-routes
  (ANY "/currencies/default" [] resource-default))
