(ns bobs.routes.withdraws
  (:require [compojure.core :refer :all]
            [bobs.routes.documents :as rsc]
            [bobs.models.withdraws :refer :all]
            [bobs.models.withdraws-drafts :refer [withdraws-drafts]]
            [bobs.validators.validator :as validator]
            [bobs.validators.withdraws :refer [rules]]
            [bobs.views.printables.withdraws :refer [render render-doc]]
            [bobs.i18n :refer [t]]))


(def validate (validator/validate rules))

(defn do-validation [data]
  (validate data))

(def resource (rsc/resource withdraws))

(def resource-id (partial rsc/resource-id-withdraw withdraws withdraws-drafts do-validation))

(def resource-pagination (rsc/resource-pagination withdraws))

(def resource-pagination-id (partial rsc/resource-pagination-id withdraws))

(def resource-id-details (partial rsc/resource-id-details withdraws))

(def resource-id-details-pagination (partial rsc/resource-id-details-pagination withdraws))

(def resource-print-id (partial rsc/resource-print-id render))

(def resource-pdf-id (partial rsc/resource-pdf-id (t :default/withdraw) withdraws render))

(def resource-print-summary (rsc/resource-print-summary (t :default/withdraw) withdraws render-doc))

(def resource-pdf-summary (rsc/resource-pdf-summary (t :default/withdraw) withdraws render-doc))

(defroutes withdraws-routes
  (ANY "/withdraws" [] resource)
  (ANY "/withdraws/print/summary" [] resource-print-summary)
  (ANY "/withdraws/pdf/summary" [] resource-pdf-summary)
  (ANY "/withdraws/print/:id" [id] (resource-print-id id))
  (ANY "/withdraws/pdf/:id" [id] (resource-pdf-id id))
  (ANY "/withdraws/page" [] resource-pagination)
  (ANY "/withdraws/page/:id" [id] (resource-pagination-id id))
  (ANY "/withdraws/:id" [id] (resource-id id))
  (ANY "/withdraws/:id/details" [id] (resource-id-details id))
  (ANY "/withdraws/:id/details/page" [id] (resource-id-details-pagination id)))
