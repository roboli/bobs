(ns bobs.routes.uom
  (:require [compojure.core :refer :all]
            [bobs.models.uom :refer [uom]]
            [bobs.validators.validator :as validator]
            [bobs.validators.uom :refer [rules]]
            [bobs.routes.resources :as rsc]))

(def validate (validator/validate rules))

(defn do-validation [data]
  (validate data))

(defn format-data [data] data)

(def resource (rsc/resource uom do-validation format-data :name))

(def resource-id (partial rsc/resource-id uom do-validation format-data))

(def resource-pagination (rsc/resource-pagination uom :name))

(def resource-pagination-id (partial rsc/resource-pagination-id uom :name))

(defroutes uom-routes
  (ANY "/uom" [] resource)
  (ANY "/uom/page"[] resource-pagination)
  (ANY "/uom/page/:id" [id] (resource-pagination-id id))
  (ANY "/uom/:id" [id] (resource-id id)))
