(ns bobs.routes.item-types
  (:require [compojure.core :refer :all]
            [bobs.models.item-types :refer [item-types]]
            [bobs.validators.validator :as validator]
            [bobs.validators.item-types :refer [rules]]
            [bobs.routes.resources :as rsc]))

(def validate (validator/validate rules))

(defn do-validation [data]
  (validate data))

(defn format-data [data] data)

(def resource (rsc/resource item-types do-validation format-data :name))

(def resource-id (partial rsc/resource-id item-types do-validation format-data))

(def resource-pagination (rsc/resource-pagination item-types :name))

(def resource-pagination-id (partial rsc/resource-pagination-id item-types :name))

(defroutes item-types-routes
  (ANY "/item-types" [] resource)
  (ANY "/item-types/page" [] resource-pagination)
  (ANY "/item-types/page/:id"[id] (resource-pagination-id id))
  (ANY "/item-types/:id" [id] (resource-id id)))
