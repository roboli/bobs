(ns bobs.routes.sales-returns-drafts
  (:require [compojure.core :refer :all]
            [bobs.routes.drafts :as rsc]
            [bobs.models.sales-returns-drafts :refer [sales-returns-drafts]]
            [bobs.validators.validator :as validator]
            [bobs.validators.sales-return-details :refer [rules]]))

(def validate-detail (validator/validate rules))

(def resource (rsc/resource sales-returns-drafts :sales-return))

(def resource-id (partial rsc/resource-id sales-returns-drafts
                          (rsc/do-validation validate-detail :cost)
                          (rsc/format-data :cost)))

(def resource-detail-id (partial rsc/resource-detail-id sales-returns-drafts))

(def resource-id-detail-pagination (partial rsc/resource-id-detail-pagination sales-returns-drafts))

(def resource-id-detail-pagination-id (partial rsc/resource-id-detail-pagination-id sales-returns-drafts))

(defroutes sales-returns-drafts-routes
  (ANY "/sales-returns/drafts" [] resource)
  (ANY "/sales-returns/drafts/:id" [id] (resource-id id))
  (ANY "/sales-returns/drafts/:id/page" [id] (resource-id-detail-pagination id))
  (ANY "/sales-returns/drafts/:id/page/:detail-id" [id detail-id] (resource-id-detail-pagination-id id detail-id))
  (ANY "/sales-returns/drafts/:id/:detail-id" [id detail-id] (resource-detail-id id detail-id)))
