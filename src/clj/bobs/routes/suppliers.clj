(ns bobs.routes.suppliers
  (:require [compojure.core :refer :all]
            [liberator.core :refer [defresource]]
            [bobs.routes.resources :refer [resource-defaults]]
            [bobs.models.partners :refer [partners get-all-suppliers]]
            [bobs.models.session :as session]))

(defresource resource resource-defaults
  :allowed-methods [:get]
  :handle-ok (fn [_] (get-all-suppliers partners (session/get-in [:company :id]))))

(defroutes suppliers-routes
  (ANY "/suppliers" [] resource))
