(ns bobs.routes.entries-drafts
  (:require [compojure.core :refer :all]
            [bobs.routes.drafts :as rsc]
            [bobs.models.entries-drafts :refer [entries-drafts]]
            [bobs.validators.validator :as validator]
            [bobs.validators.entry-details :refer [rules]]))

(def validate-detail (validator/validate rules))

(def resource (rsc/resource entries-drafts :entry))

(def resource-id (partial rsc/resource-id entries-drafts
                          (rsc/do-validation validate-detail :cost)
                          (rsc/format-data :cost)))

(def resource-detail-id (partial rsc/resource-detail-id entries-drafts))

(def resource-id-detail-pagination (partial rsc/resource-id-detail-pagination entries-drafts))

(def resource-id-detail-pagination-id (partial rsc/resource-id-detail-pagination-id entries-drafts))

(defroutes entries-drafts-routes
  (ANY "/entries/drafts" [] resource)
  (ANY "/entries/drafts/:id" [id] (resource-id id))
  (ANY "/entries/drafts/:id/page" [id] (resource-id-detail-pagination id))
  (ANY "/entries/drafts/:id/page/:detail-id" [id detail-id] (resource-id-detail-pagination-id id detail-id))
  (ANY "/entries/drafts/:id/:detail-id" [id detail-id] (resource-detail-id id detail-id)))
