(ns bobs.routes.items
  (:require [ring.util.codec :refer [url-decode]]
            [compojure.core :refer :all]
            [liberator.core :as lib :refer [defresource request-method-in]]
            [bobs.models.query :refer :all]
            [bobs.models.items :refer :all]
            [bobs.models.inventory :refer [generate-history count-history-lines balance-at first-document-id last-document-id]]
            [bobs.validators.validator :as validator]
            [bobs.validators.items :refer [rules]]
            [bobs.routes.resources :as rsc :refer [resource-defaults resource-validator resource-delete get-params build-entry-url]]
            [bobs.models.session :as session]
            [bobs.config :refer [items-images-path image-size thumbnail-size thumbnail-prefix]]
            [bobs.utils :as utils :refer [truthy?]]
            [bobs.i18n :refer [t]]
            [bobs.views.printables.history :as history]))

(defn do-validation [data]
  (let [rules (if (truthy? (:auto-sku data)) (dissoc rules :sku) rules)
        data (assoc data
               :currency-id (str (:currency-id data))
               :price (or (:price data) "")
               :image-filename (get-in data [:image :filename])
               :image-file (get-in data [:image :tempfile])
               :image-file-size (get-in data [:image :size]))]
    ((validator/validate rules) data)))

(defn format-data [data]
  (let [brand-id (:brand-id data)
        item-type-id (:item-type-id data)
        uom-id (:uom-id data)
        data (assoc data
               :price (apply str (remove #((set ",") %) (:price data)))
               :brand-id (if (or (empty? brand-id) (= brand-id "null")) nil brand-id)
               :item-type-id (if (or (empty? item-type-id) (= item-type-id "null")) nil item-type-id)
               :uom-id (if (or (empty? uom-id) (= uom-id "null")) nil uom-id)
               :is-service (or (boolean (truthy? (:is-service data))) false)
               :lifo (or (boolean (truthy? (:lifo data))) false)
               :active (or (boolean (truthy? (:active data))) false))
        data (dissoc data :auto-sku)]
    data))

(defn generate-images [params]
  (if-let [filename (get-in params [:image :filename])]
    (let [tempfile (:tempfile (:image params))
          filename (utils/generate-unique-filename filename)
          img (utils/scale-image (:tempfile (:image params)) image-size)
          thumb-filename (str thumbnail-prefix filename)
          thumb-img (utils/scale-image tempfile thumbnail-size)]
      (utils/save-image items-images-path img filename :create-path? true)
      (utils/save-image items-images-path thumb-img thumb-filename)
      {:filename filename
       :thumb-filename thumb-filename})))

(defn delete-images [{:keys [image thumbnail]}]
  (when (and image thumbnail)
    (utils/delete-file items-images-path image)
    (utils/delete-file items-images-path thumbnail)))

(defresource resource (merge resource-defaults (resource-validator do-validation))
  :allowed-methods [:get :post]
  :handle-ok (fn [ctxt]
               (let [params (get-params ctxt)]
                 (get-all items {:company-id (session/get-in [:company :id])
                                 :search-field :description
                                 :exclude-sku (:exclude-sku params)
                                 :order (keyword (:order params))
                                 :search-value (url-decode (or (:search params) ""))
                                 :exclude-non-active true})))
  :processable? (fn [ctxt]
                  (or ((request-method-in :get) ctxt)
                      (let [params (get-params ctxt)]
                        (or (truthy? (:auto-sku params))
                            (sku-available? items (:sku params) (session/get-in [:company :id]))))))
  :handle-unprocessable-entity (fn [_] {:validations {:sku [(t :validations/sku-available)]}})
  :post! (fn [ctxt]
           (let [params (get-params ctxt)
                 {:keys [filename thumb-filename] :as filenames} (generate-images params)
                 params (if filenames
                          (assoc params :image filename :thumbnail thumb-filename)
                          (dissoc params :image))
                 params (if (truthy? (:auto-sku params)) (assoc params :sku (generate-sku items)) params)]
             {::id (insert items {:values (assoc
                                              (format-data params)
                                            :company-id (session/get-in [:company :id])
                                            :user-id (session/get-in [:user :id]))
                                  :audit {:user-id (session/get-in [:user :id])}})}))
  :post-redirect? (fn [ctxt] {:location (str (build-entry-url (get ctxt :request)) "/" (get ctxt ::id))}))

(defn resource-id [id]
  (lib/resource (merge resource-defaults (resource-validator do-validation) (resource-delete items id))
                :allowed-methods [:get :put :delete]
                :handle-ok (fn [_] (get-row items {:id id}))
                :new? false
                :processable? (fn [ctxt]
                                (or ((request-method-in :get :delete) ctxt)
                                    (let [params (get-params ctxt)]
                                      (or (truthy? (:auto-sku params))
                                          (sku-available? items (:sku params) (session/get-in [:company :id]) (:id params))))))
                :handle-unprocessable-entity (fn [_] {:validations {:sku [(t :validations/sku-available)]}})
                :put! (fn [ctxt]
                        (let [params (get-params ctxt)
                              {:keys [filename thumb-filename] :as filenames} (generate-images params)
                              no-image (= (:image params) "0")
                              params (if (or filenames no-image)
                                       (assoc params :image filename :thumbnail thumb-filename)
                                       (dissoc params :image))
                              old-images (if (or filenames no-image)
                                           (get-images items (:id params)))
                              params (if (truthy? (:auto-sku params)) (assoc params :sku (generate-sku items)) params)]
                          (update items {:id id
                                         :values (format-data params)
                                         :audit {:user-id (session/get-in [:user :id])}})
                          (delete-images old-images)))
                :delete! (fn [_]
                           (let [images (get-images items id)]
                             (delete items {:id id
                                            :audit {:user-id (session/get-in [:user :id])}})
                             (delete-images images)))))

(defn resource-image [id]
  (lib/resource resource-defaults
                :allowed-methods [:get]
                :available-media-types ["image/jpeg" "image/png"]
                :last-modified (fn [_]
                                 (let [{:keys [thumbnail]} (get-images items id)]
                                   (if thumbnail
                                     (.lastModified (utils/get-file items-images-path thumbnail)))))
                :handle-ok (fn [ctxt]
                             (let [{:keys [image thumbnail]} (get-images items id)
                                   params (get-params ctxt)]
                               (if image
                                 (if (:thumbnail params)
                                   (utils/get-file items-images-path thumbnail)
                                   (utils/get-file items-images-path image)))))))

(defn resource-sku [sku]
  (lib/resource resource-defaults
                :allowed-methods [:get]
                :handle-ok (fn [_] (-> (get-row-sku items (session/get-in [:company :id]) sku true)
                                       (select-keys [:id :sku :description :uom-abbreviation :price])
                                       (#(if-not (empty? %) (assoc % :cost (last-cost items (:id %)))))))))

(defn resource-history-id [id]
  (lib/resource resource-defaults
                :allowed-methods [:get]
                :handle-ok (fn [ctxt]
                             (let [params (get-params ctxt)
                                   first-doc-id (if (:from-date params) (first-document-id id (:from-date params)) 0)
                                   last-doc-id (last-document-id id (:to-date params))
                                   warehouse-id (:warehouse-id params)
                                   page (Integer/parseInt (:page params))
                                   page-size (dec (Integer/parseInt (:page-size params))) ; Decrease since the balance will be included in every page
                                   count (count-history-lines id
                                                              :begin-document-id first-doc-id
                                                              :end-document-id last-doc-id
                                                              :warehouse-id warehouse-id)
                                   total-pages (int (Math/ceil (/ count page-size)))
                                   count (+ count total-pages) ; Count every balance record for each page
                                   total-pages (int (Math/ceil (/ count (inc page-size)))) ; Recalculate total pages
                                   initial-balance (if first-doc-id (balance-at id (dec first-doc-id) :warehouse-id warehouse-id) 0)
                                   initial-balance (if (= page 1)
                                                     initial-balance
                                                     (-> (generate-history id
                                                                           :begin-document-id first-doc-id
                                                                           :end-document-id last-doc-id
                                                                           :initial-balance initial-balance
                                                                           :warehouse-id warehouse-id
                                                                           :limit (* page-size (Math/abs (dec page)))
                                                                           :offset 0)
                                                         (last)
                                                         :balance))
                                   response {:page page
                                             :page-size (inc page-size) ; Send original page size
                                             :total-pages total-pages
                                             :total-records count
                                             :from-date (:from-date params)
                                             :to-date (:to-date params)
                                             :warehouse-id warehouse-id
                                             :records (generate-history id
                                                                        :begin-document-id first-doc-id
                                                                        :end-document-id last-doc-id
                                                                        :initial-balance initial-balance
                                                                        :warehouse-id warehouse-id
                                                                        :limit page-size
                                                                        :offset (* page-size (Math/abs (dec page))))}]
                               (if (:include-header params)
                                 (assoc response :header (get-row items {:id id}))
                                 response)))))

(defn resource-history-print-id [id]
  (lib/resource
   :authorized? (:authorized? resource-defaults)
   :available-media-types ["text/html"]
   :allowed-methods [:get]
   :handle-ok (fn [ctxt]
                (let [params (get-params ctxt)
                      first-doc-id (if (:from-date params) (first-document-id id (:from-date params)) 0)
                      last-doc-id (last-document-id id (:to-date params))
                      initial-balance (if first-doc-id (balance-at id (dec first-doc-id)) 0)]
                  (history/render id first-doc-id last-doc-id initial-balance)))))

(defn resource-stock-sku [sku]
  (lib/resource resource-defaults
                :allowed-methods [:get]
                :handle-ok (fn [ctxt]
                             (let [item (get-row-sku items (session/get-in [:company :id]) sku)
                                   id (:id item)
                                   response {:records (stock items id)
                                             :total (:quantity item)}]
                               (if (:include-header (get-params ctxt))
                                 (assoc response :header (get-row items {:id id}))
                                 response)))))

(def resource-pagination (rsc/resource-pagination items :description))

(def resource-pagination-id (partial rsc/resource-pagination-id items :description))

(defroutes items-routes
  (ANY "/items" [] resource)
  (ANY "/items/page" [] resource-pagination)
  (ANY "/items/page/:id"[id] (resource-pagination-id id))
  (ANY "/items/sku/:sku" [sku] (resource-sku sku))
  (ANY "/items/:id" [id] (resource-id id))
  (ANY "/items/:id/image" [id] (resource-image id))
  (ANY "/items/:id/history" [id] (resource-history-id id))
  (ANY "/items/:id/history/print" [id] (resource-history-print-id id))
  (ANY "/items/:sku/stock" [sku] (resource-stock-sku sku)))
