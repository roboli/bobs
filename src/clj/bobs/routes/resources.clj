(ns bobs.routes.resources
  (:require [ring.util.codec :refer [url-decode]]
            [liberator.core :as lib :exclude [resource] :refer [request-method-in]]
            [bobs.models.session :as session]
            [bobs.models.query :refer :all]
            [bobs.i18n :refer [t]]))

(defn build-entry-url [request]
  (format "%s://%s:%s%s"
          (name (:scheme request))
          (:server-name request)
          (:server-port request)
          (:uri request)))

(defn get-params [ctxt]
  (get-in ctxt [:request :params]))

(defn return-errors [ctxt]
  (str (get ctxt ::errors)))

(defn validate-data [ctxt validate]
  (if ((request-method-in :post :put) ctxt)
    (let [errors (validate (get-params ctxt))]
      (and errors {::errors errors}))))

(def resource-defaults
  {:authorized? (fn [_] (session/active?))
   :available-media-types ["application/edn"]})

(defn resource-validator [validator]
  {:malformed? #(validate-data % validator)
   :handle-malformed #(return-errors %)})

(defn resource-delete [model id]
  {:allowed? (fn [ctxt]
               (or (not ((request-method-in :delete) ctxt)) (can-delete? model {:id id})))
   :handle-forbidden (t :validations/cannot-delete-record-in-use)
   :delete! (fn [_] (delete model {:id id
                                   :audit {:user-id (session/get-in [:user :id])}}))})

(defn resource [model validator format search-field]
  (lib/resource (merge resource-defaults (resource-validator validator))
                :allowed-methods [:get :post]
                :handle-ok (fn [ctxt]
                             (get-all model {:company-id (session/get-in [:company :id])
                                             :search-field search-field
                                             :order (keyword (:order (get-params ctxt)))}))
                :post! (fn [ctxt] {::id (insert model {:values (assoc (format (get-params ctxt)) :company-id (session/get-in [:company :id]))
                                                       :audit {:user-id (session/get-in [:user :id])}})})
                :post-redirect? (fn [ctxt] {:location (str (build-entry-url (get ctxt :request)) "/" (get ctxt ::id))})))

(defn resource-id [model validator format id]
  (lib/resource (merge resource-defaults (resource-validator validator) (resource-delete model id))
                :allowed-methods [:get :put :delete]
                :handle-ok (fn [_] (get-row model {:id id}))
                :new? false
                :put! #(update model {:id id
                                      :values (format (get-params %))
                                      :audit {:user-id (session/get-in [:user :id])}})))

(defn resource-pagination [model search-field]
  (lib/resource resource-defaults
                :allowed-methods [:get]
                :handle-ok (fn [ctxt]
                             (let [params (get-params ctxt)
                                   page (Integer/parseInt (:page params))
                                   page-size (Integer/parseInt (:page-size params))
                                   order (:order params)
                                   sort (:sort params)
                                   search (url-decode (or (:search params) ""))
                                   count (get-count model {:company-id (session/get-in [:company :id])
                                                           :search-field search-field
                                                           :search-value search})
                                   total-pages (int (Math/ceil (/ count page-size)))
                                   page (if (and (> page total-pages) (:last-page-on-miss params))
                                          total-pages
                                          page)]
                               {:page page
                                :page-size page-size
                                :order order
                                :sort sort
                                :search search
                                :total-pages total-pages
                                :total-records count
                                :records (get-all model {:company-id (session/get-in [:company :id])
                                                         :search-field search-field
                                                         :search-value search
                                                         :order (keyword order)
                                                         :sort (Integer/parseInt sort)
                                                         :limit page-size
                                                         :offset (* page-size (Math/abs (- page 1)))})}))))

(defn resource-pagination-id [model search-field id]
  (lib/resource resource-defaults
                :allowed-methods [:get]
                :handle-ok (fn [ctxt]
                             (let [params (get-params ctxt)
                                   order (:order params)
                                   sort (Integer/parseInt (:sort params))
                                   row (:row (first (get-record-row-number model {:company-id (session/get-in [:company :id])
                                                                                  :id id
                                                                                  :order (keyword order)
                                                                                  :sort sort})))
                                   page-size (Integer/parseInt (:page-size params))
                                   page (int (Math/ceil (/ row page-size)))
                                   count (get-count model {:company-id (session/get-in [:company :id])
                                                           :search-field search-field})]
                               {:page page
                                :page-size page-size
                                :order order
                                :sort sort
                                :total-pages (Math/ceil (/ count page-size))
                                :total-records count
                                :records (get-all model {:company-id (session/get-in [:company :id])
                                                         :search-field search-field
                                                         :order (keyword order)
                                                         :sort sort
                                                         :limit page-size
                                                         :offset (* page-size (- page 1))})}))))
