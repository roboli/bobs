(ns bobs.routes.entries
  (:require [compojure.core :refer :all]
            [bobs.routes.documents :as rsc]
            [bobs.models.entries :refer :all]
            [bobs.models.entries-drafts :refer [entries-drafts]]
            [bobs.validators.validator :as validator]
            [bobs.validators.entries :refer [rules]]
            [bobs.views.printables.entries :refer [render render-doc]]
            [bobs.i18n :refer [t]]))

(def validate (validator/validate rules))

(defn do-validation [data]
  (validate data))

(def resource (rsc/resource entries))

(def resource-id (partial rsc/resource-id entries entries-drafts do-validation))

(def resource-pagination (rsc/resource-pagination entries))

(def resource-pagination-id (partial rsc/resource-pagination-id entries))

(def resource-id-details (partial rsc/resource-id-details entries))

(def resource-id-details-pagination (partial rsc/resource-id-details-pagination entries))

(def resource-print-id (partial rsc/resource-print-id render))

(def resource-pdf-id (partial rsc/resource-pdf-id (t :default/entry) entries render))

(def resource-print-summary (rsc/resource-print-summary (t :default/entry) entries render-doc))

(def resource-pdf-summary (rsc/resource-pdf-summary (t :default/entry) entries render-doc))

(defroutes entries-routes
  (ANY "/entries" [] resource)
  (ANY "/entries/print/summary" [] resource-print-summary)
  (ANY "/entries/pdf/summary" [] resource-pdf-summary)
  (ANY "/entries/print/:id" [id] (resource-print-id id))
  (ANY "/entries/pdf/:id" [id] (resource-pdf-id id))
  (ANY "/entries/page" [] resource-pagination)
  (ANY "/entries/page/:id" [id] (resource-pagination-id id))
  (ANY "/entries/:id" [id] (resource-id id))
  (ANY "/entries/:id/details" [id] (resource-id-details id))
  (ANY "/entries/:id/details/page" [id] (resource-id-details-pagination id)))
