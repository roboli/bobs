(ns bobs.routes.sales-returns
  (:require [compojure.core :refer :all]
            [bobs.routes.documents :as rsc]
            [bobs.models.sales-returns :refer :all]
            [bobs.models.sales-returns-drafts :refer [sales-returns-drafts]]
            [bobs.validators.validator :as validator]
            [bobs.validators.sales-returns :refer [rules]]
            [bobs.views.printables.sales-returns :refer [render render-doc]]
            [bobs.i18n :refer [t]]))

(def validate (validator/validate rules))

(defn do-validation [data]
  (let [data (assoc data :purchase-date (or (:purchase-date data) ""))]
    (validate data)))

(def resource (rsc/resource sales-returns))

(def resource-id (partial rsc/resource-id sales-returns sales-returns-drafts do-validation))

(def resource-pagination (rsc/resource-pagination sales-returns))

(def resource-pagination-id (partial rsc/resource-pagination-id sales-returns))

(def resource-id-details (partial rsc/resource-id-details sales-returns))

(def resource-id-details-pagination (partial rsc/resource-id-details-pagination sales-returns))

(def resource-print-id (partial rsc/resource-print-id render))

(def resource-pdf-id (partial rsc/resource-pdf-id (t :default/sales-return) sales-returns render))

(def resource-print-summary (rsc/resource-print-summary (t :default/sales-return) sales-returns render-doc))

(def resource-pdf-summary (rsc/resource-pdf-summary (t :default/sales-return) sales-returns render-doc))

(defroutes sales-returns-routes
  (ANY "/sales-returns" [] resource)
  (ANY "/sales-returns/print/summary" [] resource-print-summary)
  (ANY "/sales-returns/pdf/summary" [] resource-pdf-summary)
  (ANY "/sales-returns/print/:id" [id] (resource-print-id id))
  (ANY "/sales-returns/pdf/:id" [id] (resource-pdf-id id))
  (ANY "/sales-returns/page" [] resource-pagination)
  (ANY "/sales-returns/page/:id" [id] (resource-pagination-id id))
  (ANY "/sales-returns/:id" [id] (resource-id id))
  (ANY "/sales-returns/:id/details" [id] (resource-id-details id))
  (ANY "/sales-returns/:id/details/page" [id] (resource-id-details-pagination id)))
