(ns bobs.routes.customers
  (:require [compojure.core :refer :all]
            [liberator.core :refer [defresource]]
            [bobs.routes.resources :refer [resource-defaults]]
            [bobs.models.partners :refer [partners get-all-customers]]
            [bobs.models.session :as session]))

(defresource resource resource-defaults
  :allowed-methods [:get]
  :handle-ok (fn [_] (get-all-customers partners (session/get-in [:company :id]))))

(defroutes customers-routes
  (ANY "/customers" [] resource))
