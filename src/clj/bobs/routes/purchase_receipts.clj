(ns bobs.routes.purchase-receipts
  (:require [compojure.core :refer :all]
            [bobs.routes.documents :as rsc]
            [bobs.models.purchase-receipts :refer :all]
            [bobs.models.purchase-receipts-drafts :refer [purchase-receipts-drafts]]
            [bobs.validators.validator :as validator]
            [bobs.validators.purchase-receipts :refer [rules]]
            [bobs.views.printables.purchase-receipts :refer [render render-doc]]
            [bobs.i18n :refer [t]]))

(def validate (validator/validate rules))

(defn do-validation [data]
  (let [data (assoc data :purchase-date (or (:purchase-date data) ""))]
    (validate data)))

(def resource (rsc/resource purchase-receipts))

(def resource-id (partial rsc/resource-id purchase-receipts purchase-receipts-drafts do-validation))

(def resource-pagination (rsc/resource-pagination purchase-receipts))

(def resource-pagination-id (partial rsc/resource-pagination-id purchase-receipts))

(def resource-id-details (partial rsc/resource-id-details purchase-receipts))

(def resource-id-details-pagination (partial rsc/resource-id-details-pagination purchase-receipts))

(def resource-print-id (partial rsc/resource-print-id render))

(def resource-pdf-id (partial rsc/resource-pdf-id (t :default/purchase-receipt) purchase-receipts render))

(def resource-print-summary (rsc/resource-print-summary (t :default/purchase-receipt) purchase-receipts render-doc))

(def resource-pdf-summary (rsc/resource-pdf-summary (t :default/purchase-receipt) purchase-receipts render-doc))

(defroutes purchase-receipts-routes
  (ANY "/purchase-receipts" [] resource)
  (ANY "/purchase-receipts/print/summary" [] resource-print-summary)
  (ANY "/purchase-receipts/pdf/summary" [] resource-pdf-summary)
  (ANY "/purchase-receipts/print/:id" [id] (resource-print-id id))
  (ANY "/purchase-receipts/pdf/:id" [id] (resource-pdf-id id))
  (ANY "/purchase-receipts/page" [] resource-pagination)
  (ANY "/purchase-receipts/page/:id" [id] (resource-pagination-id id))
  (ANY "/purchase-receipts/:id" [id] (resource-id id))
  (ANY "/purchase-receipts/:id/details" [id] (resource-id-details id))
  (ANY "/purchase-receipts/:id/details/page" [id] (resource-id-details-pagination id)))
