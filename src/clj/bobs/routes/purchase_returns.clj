(ns bobs.routes.purchase-returns
  (:require [compojure.core :refer :all]
            [bobs.routes.documents :as rsc]
            [bobs.models.purchase-returns :refer :all]
            [bobs.models.purchase-returns-drafts :refer [purchase-returns-drafts]]
            [bobs.validators.validator :as validator]
            [bobs.validators.purchase-returns :refer [rules]]
            [bobs.views.printables.purchase-returns :refer [render render-doc]]
            [bobs.i18n :refer [t]]))

(def validate (validator/validate rules))

(defn do-validation [data]
  (validate data))

(def resource (rsc/resource purchase-returns))

(def resource-id (partial rsc/resource-id-withdraw purchase-returns purchase-returns-drafts do-validation))

(def resource-pagination (rsc/resource-pagination purchase-returns))

(def resource-pagination-id (partial rsc/resource-pagination-id purchase-returns))

(def resource-id-details (partial rsc/resource-id-details purchase-returns))

(def resource-id-details-pagination (partial rsc/resource-id-details-pagination purchase-returns))

(def resource-print-id (partial rsc/resource-print-id render))

(def resource-pdf-id (partial rsc/resource-pdf-id (t :default/purchase-return) purchase-returns render))

(def resource-print-summary (rsc/resource-print-summary (t :default/purchase-return) purchase-returns render-doc))

(def resource-pdf-summary (rsc/resource-pdf-summary (t :default/purchase-return) purchase-returns render-doc))

(defroutes purchase-returns-routes
  (ANY "/purchase-returns" [] resource)
  (ANY "/purchase-returns/print/summary" [] resource-print-summary)
  (ANY "/purchase-returns/pdf/summary" [] resource-pdf-summary)
  (ANY "/purchase-returns/print/:id" [id] (resource-print-id id))
  (ANY "/purchase-returns/pdf/:id" [id] (resource-pdf-id id))
  (ANY "/purchase-returns/page" [] resource-pagination)
  (ANY "/purchase-returns/page/:id" [id] (resource-pagination-id id))
  (ANY "/purchase-returns/:id" [id] (resource-id id))
  (ANY "/purchase-returns/:id/details" [id] (resource-id-details id))
  (ANY "/purchase-returns/:id/details/page" [id] (resource-id-details-pagination id)))
