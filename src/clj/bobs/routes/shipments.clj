(ns bobs.routes.shipments
  (:require [compojure.core :refer :all]
            [bobs.routes.documents :as rsc]
            [bobs.models.shipments :refer :all]
            [bobs.models.shipments-drafts :refer [shipments-drafts]]
            [bobs.validators.validator :as validator]
            [bobs.validators.shipments :refer [rules]]
            [bobs.views.printables.shipments :refer [render render-doc]]
            [bobs.i18n :refer [t]]))

(def validate (validator/validate rules))

(defn do-validation [data]
  (validate data))

(def resource (rsc/resource shipments))

(def resource-id (partial rsc/resource-id-withdraw shipments shipments-drafts do-validation))

(def resource-pagination (rsc/resource-pagination shipments))

(def resource-pagination-id (partial rsc/resource-pagination-id shipments))

(def resource-id-details (partial rsc/resource-id-details shipments))

(def resource-id-details-pagination (partial rsc/resource-id-details-pagination shipments))

(def resource-print-id (partial rsc/resource-print-id render))

(def resource-pdf-id (partial rsc/resource-pdf-id (t :default/shipment) shipments render))

(def resource-print-summary (rsc/resource-print-summary (t :default/shipment) shipments render-doc))

(def resource-pdf-summary (rsc/resource-pdf-summary (t :default/shipment) shipments render-doc))

(defroutes shipments-routes
  (ANY "/shipments" [] resource)
  (ANY "/shipments/print/summary" [] resource-print-summary)
  (ANY "/shipments/pdf/summary" [] resource-pdf-summary)
  (ANY "/shipments/print/:id" [id] (resource-print-id id))
  (ANY "/shipments/pdf/:id" [id] (resource-pdf-id id))
  (ANY "/shipments/page" [] resource-pagination)
  (ANY "/shipments/page/:id" [id] (resource-pagination-id id))
  (ANY "/shipments/:id" [id] (resource-id id))
  (ANY "/shipments/:id/details" [id] (resource-id-details id))
  (ANY "/shipments/:id/details/page" [id] (resource-id-details-pagination id)))
