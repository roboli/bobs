(ns bobs.routes.purchase-returns-drafts
  (:require [compojure.core :refer :all]
            [bobs.routes.drafts :as rsc]
            [bobs.models.purchase-returns-drafts :refer [purchase-returns-drafts]]
            [bobs.validators.validator :as validator]
            [bobs.validators.purchase-return-details :refer [rules]]))

(def validate-detail (validator/validate rules))

(def resource (rsc/resource purchase-returns-drafts :purchase-return))

(def resource-id (partial rsc/resource-id-withdraw purchase-returns-drafts
                          (rsc/do-validation validate-detail :price)
                          (rsc/format-data :price)))

(def resource-detail-id (partial rsc/resource-detail-id purchase-returns-drafts))

(def resource-id-detail-pagination (partial rsc/resource-id-detail-pagination purchase-returns-drafts))

(def resource-id-detail-pagination-id (partial rsc/resource-id-detail-pagination-id purchase-returns-drafts))

(defroutes purchase-returns-drafts-routes
  (ANY "/purchase-returns/drafts" [] resource)
  (ANY "/purchase-returns/drafts/:id" [id] (resource-id id))
  (ANY "/purchase-returns/drafts/:id/page" [id] (resource-id-detail-pagination id))
  (ANY "/purchase-returns/drafts/:id/page/:detail-id" [id detail-id] (resource-id-detail-pagination-id id detail-id))
  (ANY "/purchase-returns/drafts/:id/:detail-id" [id detail-id] (resource-detail-id id detail-id)))
