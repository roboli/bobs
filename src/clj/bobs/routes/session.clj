(ns bobs.routes.session
  (:require [compojure.core :refer :all]
            [liberator.core :refer [defresource request-method-in]]
            [bobs.routes.resources :refer [resource-defaults]]
            [bobs.models.query :refer [get-row]]
            [bobs.models.session :as session]
            [bobs.models.users :refer [users]]
            [bobs.i18n :refer [t]]
            [bobs.routes.resources :refer [get-params build-entry-url]]))

(defresource resource
  :allowed-methods [:get :post :delete]
  :available-media-types (:available-media-types resource-defaults)
  :authorized? (fn [ctxt]
                 (or (not ((request-method-in :get) ctxt)) (session/active?)))
  :handle-ok (fn [_] {:user {:username (session/get-in [:user :username])
                             :name (session/get-in [:user :name])}
                      :company {:alias (session/get-in [:company :alias])
                                :name (session/get-in [:company :name])}})
  :processable? (fn [ctxt]
                  (or ((request-method-in :get :delete) ctxt)
                      (let [params (get-params ctxt)]
                        (session/is-valid-user? (:username params) (:password params)))))
  :handle-unprocessable-entity (str {:validations {:all [(t :validations/invalid-username-password)]}})
  :post! #(session/start! (get-row users {:username (:username (get-params %))}))
  :post-redirect? (fn [ctxt] {:location (str (build-entry-url (get ctxt :request)))})
  :delete! (fn [_] (session/end!)))

(defroutes session-routes
  (ANY "/session" [] resource))
