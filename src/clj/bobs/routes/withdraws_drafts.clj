(ns bobs.routes.withdraws-drafts
  (:require [compojure.core :refer :all]
            [bobs.routes.drafts :as rsc]
            [bobs.models.withdraws-drafts :refer [withdraws-drafts]]
            [bobs.validators.validator :as validator]
            [bobs.validators.withdraw-details :refer [rules]]))

(def validate-detail (validator/validate rules))

(def resource (rsc/resource withdraws-drafts :withdraw))

(def resource-id (partial rsc/resource-id-withdraw withdraws-drafts
                          (rsc/do-validation validate-detail :price)
                          (rsc/format-data :price)))

(def resource-detail-id (partial rsc/resource-detail-id withdraws-drafts))

(def resource-id-detail-pagination (partial rsc/resource-id-detail-pagination withdraws-drafts))

(def resource-id-detail-pagination-id (partial rsc/resource-id-detail-pagination-id withdraws-drafts))

(defroutes withdraws-drafts-routes
  (ANY "/withdraws/drafts" [] resource)
  (ANY "/withdraws/drafts/:id" [id] (resource-id id))
  (ANY "/withdraws/drafts/:id/page" [id] (resource-id-detail-pagination id))
  (ANY "/withdraws/drafts/:id/page/:detail-id" [id detail-id] (resource-id-detail-pagination-id id detail-id))
  (ANY "/withdraws/drafts/:id/:detail-id" [id detail-id] (resource-detail-id id detail-id)))
