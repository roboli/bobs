(ns bobs.routes.purchase-receipts-drafts
  (:require [compojure.core :refer :all]
            [bobs.routes.drafts :as rsc]
            [bobs.models.purchase-receipts-drafts :refer [purchase-receipts-drafts]]
            [bobs.validators.validator :as validator]
            [bobs.validators.purchase-receipt-details :refer [rules]]))

(def validate-detail (validator/validate rules))

(def resource (rsc/resource purchase-receipts-drafts :purchase-receipt))

(def resource-id (partial rsc/resource-id purchase-receipts-drafts
                          (rsc/do-validation validate-detail :cost)
                          (rsc/format-data :cost)))

(def resource-detail-id (partial rsc/resource-detail-id purchase-receipts-drafts))

(def resource-id-detail-pagination (partial rsc/resource-id-detail-pagination purchase-receipts-drafts))

(def resource-id-detail-pagination-id (partial rsc/resource-id-detail-pagination-id purchase-receipts-drafts))

(defroutes purchase-receipts-drafts-routes
  (ANY "/purchase-receipts/drafts" [] resource)
  (ANY "/purchase-receipts/drafts/:id" [id] (resource-id id))
  (ANY "/purchase-receipts/drafts/:id/page" [id] (resource-id-detail-pagination id))
  (ANY "/purchase-receipts/drafts/:id/page/:detail-id" [id detail-id] (resource-id-detail-pagination-id id detail-id))
  (ANY "/purchase-receipts/drafts/:id/:detail-id" [id detail-id] (resource-detail-id id detail-id)))
