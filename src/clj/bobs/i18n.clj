(ns bobs.i18n
  (:require [taoensso.tower :as tower :refer [with-locale with-tscope *locale*] :exclude [t]]
            [bobs.dictionary.validations :as validations]
            [bobs.dictionary.default :as default]
            [bobs.formats :as fmts]
            [bobs.config :refer [app-lang]]))

(def tconfig
  {:fallback-locale :en
   :dictionary {:en {:validations (get-in validations/msgs [:en :validations])
                     :default (get-in default/msgs [:en :default])
                     :missing "<Missing translation: [%1$s %2$s %3$s]>"}
                :es {:validations (get-in validations/msgs [:es :validations])
                     :default (get-in default/msgs [:es :default])
                     :missing "<Traduccion no disponible: [%1$s %2$s %3$s]>"}}})

(def date-fmt (fmts/date-fmt app-lang))

(def datetime-fmt (fmts/datetime-fmt app-lang))

(def time-zone-id (condp = app-lang
                    :en "US/Central"
                    :es "America/Guatemala"))

(def time-zone-offset (condp = app-lang
                        :en "-5:00"
                        :es "-6:00"))

(defn t [dic-key]
  (tower/t app-lang tconfig dic-key))
