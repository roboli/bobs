(ns bobs.utils
  (:require [clojure.java.io :as io]
            [noir.io :as nio]
            [clj-time.core :as ti]
            [clj-time.format :as tf]
            [clj-time.coerce :as tc]
            [bobs.config :refer [document-types]])
  (:import [java.io File]
           [java.awt.image AffineTransformOp BufferedImage]
           java.awt.geom.AffineTransform
           javax.imageio.ImageIO
           java.text.DecimalFormat))

(def truthy? #{"true"})

(defn str->int [n]
  (if ((complement integer?) n) (Integer/parseInt n) n))

(defn str->double [n]
  (if ((complement number?) n) (Double/parseDouble n) n))

(defn format-currency [n]
  (let [fmtr (DecimalFormat. "#,##0.00")]
    (.format fmtr n)))

(defn format-quantity [n]
  (let [fmtr (DecimalFormat. "#,##0.##")]
    (.format fmtr n)))

(defn format-datetime [format time-zone-id value]
  (tf/unparse
   (tf/with-zone (tf/formatter format) (ti/time-zone-for-id time-zone-id))
   (tc/from-date value) ))

(defn format-datetime-str [format-from format-to time-zone-id value]
  (tf/unparse
   (tf/formatter format-to)
   (ti/from-time-zone (tf/parse (tf/formatter format-from) value) (ti/time-zone-for-id time-zone-id))))

(defn generate-unique-filename [filename]
  (let [parts (clojure.string/split filename #"\.")
        name (clojure.string/join "." (drop-last parts))
        extension (last parts)]
    (str name (rand-int 9999) "." extension)))

(defn scale [img ratio width height]
  (let [scale        (AffineTransform/getScaleInstance 
                      (double ratio) (double ratio))
        transform-op (AffineTransformOp.
                      scale AffineTransformOp/TYPE_BILINEAR)]
    (.filter transform-op img (BufferedImage. width height (.getType img)))))

(defn scale-image [file size]
  (let [img        (ImageIO/read file)
        img-width  (.getWidth img)
        img-height (.getHeight img)
        ratio      (/ size img-height)]
    (scale img ratio (int (* img-width ratio)) size)))

(defn save-image [path img filename & {:keys [create-path?]}]
  (nio/create-path path create-path?)
  (let [path (str path File/separator)]
    (ImageIO/write
     img
     "png"
     (File. (java.net.URLDecoder/decode (str path filename) "utf-8")))))

(defn get-file [path filename]
  (io/file (str path File/separator filename)))

(defn delete-file [path filename]
  (io/delete-file (str path File/separator filename)))

(defn document-type-fn [id]
  (some #(if (= (:id (val %)) id) (:affect (val %))) document-types))

(defn find-doc-types [f]
  (keep #(if (= (:affect (val %)) f) (:id (val %))) document-types))
