(ns bobs.views.index
  (:require [hiccup.page :refer [html5 include-css include-js]]
            [bobs.models.session :as session]
            [bobs.config :refer [app-name app-name-ext app-lang]]))

(defn render [debug]
  (html5
   [:head
    [:title (str app-name app-name-ext)]
    [:meta {:name "viewport"
            :content "width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"}]
    (include-css (if debug
                   "/css/bootstrap.css"
                   "//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css")
                 "/css/styles.css")
    (if debug
      (include-js "js/jquery.js" "js/bootstrap.js")
      (include-js "//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"
                  "//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"))
    [:style
     (str "#loader{position:fixed;top:0;width:100%;background-color:white;height:100%;text-align:center;margin:0;padding:0;overflow:hidden;}"
          "#loader img {position:relative;top:35%;}")]]
   [:body
    [:div#loader
     [:img {:src "images/ajax-loader-big.gif"}]]
    [:div#app]
    [:script (str "$(window).load(function() {"
                  "$('#loader').fadeOut('fast');"
                  "});")]
    [:script (str "var Session = {};"
                  "Session.user = {};"
                  "Session.company = {};"
                  (if (session/active?)
                    (str "Session.loggedIn = true;"
                         "Session.user.username = \"" (session/get-in [:user :username]) "\";"
                         "Session.user.name = \"" (session/get-in [:user :name]) "\";"
                         "Session.company.alias = \"" (session/get-in [:company :alias]) "\";"
                         "Session.company.name = \"" (session/get-in [:company :name]) "\";")
                    "Session.loggedIn = false;"))]
    [:script (str "var input = document.createElement(\"input\");"
                  "input.setAttribute(\"type\", \"date\");"
                  "var BrowserFeatures = {};"
                  "BrowserFeatures.inputDate = input.type != \"text\";"
                  "if (!BrowserFeatures.inputDate) {"
                  "  $(\"<link/>\", {"
                  "    rel: \"stylesheet\","
                  "    type: \"text/css\","
                  "    href: \"/css/jquery-ui.css\""
                  "  }).appendTo(\"head\");"
                  "  $(\"<link/>\", {"
                  "    rel: \"stylesheet\","
                  "    type: \"text/css\","
                  "    href: \"/css/jquery-ui.theme.css\""
                  "  }).appendTo(\"head\");"
                  "  $.getScript(\"js/jquery-ui.js\");"
                  (if (= (keyword app-lang) :es) "$.getScript(\"js/datepicker-es.js\");")
                  "}")]
    (if debug
      (include-js "js/out/goog/base.js"))
    (include-js (if debug
                  "js/react.js"
                  "//cdnjs.cloudflare.com/ajax/libs/react/0.9.0/react-with-addons.min.js")
                "js/bobs.js")
    (if debug
      [:script "goog.require(\"bobs.core\");goog.require(\"repl.browser\");"])]))
