(ns bobs.views.printables.history
  (:require [hiccup.page :refer [html5]]
            [bobs.models.query :refer [get-row]]
            [bobs.models.items :refer [items]]
            [bobs.models.inventory :refer [generate-history]]
            [bobs.i18n :refer [t date-fmt time-zone-id]]
            [bobs.utils :refer [format-datetime format-quantity]]))

(defn render [id first-doc-id last-doc-id initial-balance]
  (if-let [item (get-row items {:id id})]
    (html5
     [:body
      [:h4 (t :default/history)]
      [:table.info
       [:tbody
        [:tr
         [:td [:label (t :default/description)]]
         [:td (:description item)]]
        [:tr
         [:td [:label (t :default/sku)]]
         [:td (:sku item)]]
        [:tr
         [:td [:label (t :default/uom)]]
         [:td (:uom-name item)]]]]
      [:table.table
       [:thead
        [:tr
         [:th (t :default/number-abbr)]
         [:th (t :default/document)]
         [:th (t :default/date)]
         [:th]
         [:th (t :default/warehouse)]
         [:th (t :default/withdraw)]
         [:th (t :default/entry)]
         [:th (t :default/balance)]]]
       [:tbody
        (for [row (generate-history id
                                    :begin-document-id first-doc-id
                                    :end-document-id last-doc-id
                                    :initial-balance initial-balance)]
          [:tr
           [:td (:number row)]
           [:td (if (:document-type row) (t (keyword (str "default/" (name (:document-type row))))))]
           [:td (if (:date row) (format-datetime date-fmt time-zone-id (:date row)))]
           [:td (if (:void row) (t :default/voided))]
           [:td (:warehouse-name row)]
           [:td (if (:withdraw row) (format-quantity (:withdraw row)))]
           [:td (if (:entry row) (format-quantity (:entry row)))]
           [:td (format-quantity (:balance row))]])]]])))
