(ns bobs.views.printables.summary
  (:require [hiccup.page :refer [html5 include-css include-js]]
            [bobs.models.session :as session]
            [bobs.models.query :refer [get-count get-row get-all]]
            [bobs.models.currencies :refer [get-default currencies]]
            [bobs.models.warehouses :refer [warehouses]]
            [bobs.views.printables.documents :as docs]
            [bobs.i18n :refer [t date-fmt time-zone-id]]
            [bobs.utils :refer [format-datetime-str format-currency]]))

(def format-date
  (partial format-datetime-str "yyyy-MM-dd" date-fmt time-zone-id))

(defn render [params doc-name model render-doc]
  (let [order (:order params)
        sort (:sort params)
        number (:number params)
        warehouse-id (:warehouse-id params)
        from-date (:from-date params)
        to-date (:to-date params)
        records (get-all model {:company-id (session/get-in [:company :id])
                                :number number
                                :warehouse-id warehouse-id
                                :from-date from-date
                                :to-date to-date
                                :order (keyword order)
                                :sort (Integer/parseInt sort)})
        currency-symbol (:symbol (get-default currencies (session/get-in [:company :id])))]
    (html5
     [:head]
     [:body
      [:h4 (str (t :default/summary) " " doc-name)]
      [:table.info
       [:tbody
        [:tr
         [:td [:label (t :default/from)]]
         [:td (if from-date (format-date from-date) (t :default/undefined))]]
        [:tr
         [:td [:label (t :default/to)]]
         [:td (if to-date (format-date to-date) (t :default/undefined))]]
        [:tr
         [:td [:label (t :default/warehouse)]]
         [:td (if warehouse-id (:name (get-row warehouses {:id warehouse-id})) (t :default/all))]]]]
      [:table.table
       [:thead
        [:tr
         [:th (t :default/number-abbr)]
         [:th (t :default/date)]
         [:th.text-right (t :default/amount)]
         [:th.spacer]]]
       [:tbody
        (for [row records]
          [:tr
           [:td (:number row)]
           [:td (docs/format-datetime (:date row))]
           [:td.text-right (str currency-symbol " " (format-currency (:total row)))]
           [:td]])]
       [:tfoot
        [:tr
         [:td]
         [:td.text-right.sum [:strong (t :default/total)]]
         [:td.text-right.sum [:strong (str currency-symbol " " (format-currency (reduce + (map :total records))))]]
         [:td]]]]
      ;; Hack because of wkhtmltopdf in AWS
      [:div.page-break {:style "page-break-before:always;"}]
      [:h4 (t :default/details)]
      (for [row (interpose [:div.page-break {:style "page-break-before:always;"}] records)]
        (if (map? row)
          (render-doc (:id row))
          row))])))
