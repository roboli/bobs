(ns bobs.views.printables.documents
  (:require [bobs.config :refer [amount-keys]]
            [bobs.utils :as utils :refer [format-currency format-quantity]]
            [bobs.i18n :refer [t date-fmt datetime-fmt time-zone-id]]))

(def format-date (partial utils/format-datetime date-fmt time-zone-id))

(def format-datetime (partial utils/format-datetime datetime-fmt time-zone-id))

(defn render-table-head [doc-type]
  [:tr
   [:th (t :default/description)]
   (if (= doc-type :withdraw) [:th (t :default/warehouse)])
   [:th.text-right (t :default/quantity)]
   [:th.text-right (t :default/price)]
   [:th.text-right (t (keyword (str "default/" (name (doc-type amount-keys)))))]
   [:th.spacer]])

(defn render-details [document currency-symbol doc-type]
  (for [row (:details document)]
    [:tr
     [:td (:item-description row)]
     (if (= doc-type :withdraw) [:td (:warehouse-name row)])
     [:td.text-right (str (format-quantity (:quantity row)) " " (:uom-abbreviation row))]
     [:td.text-right (str currency-symbol " " (format-currency ((doc-type amount-keys) row)))]
     [:td.text-right (str currency-symbol " " (format-currency (:total row)))]
     [:td]]))

(defn render-foot [document currency-symbol doc-type]
  [:tr
   [:td.text-right.sum {:colspan (if (= doc-type :withdraw) "2" "1")} [:strong (t :default/quantity)]]
   [:td.text-right.sum [:strong (:quantity document)]]
   [:td.text-right.sum [:strong (t :default/total)]]
   [:td.text-right.sum [:strong (str  currency-symbol " " (format-currency (:total document)))]]
   [:td]])
