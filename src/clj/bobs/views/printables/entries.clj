(ns bobs.views.printables.entries
  (:require [hiccup.page :refer [html5 include-css include-js]]
            [hiccup.core :refer [html]]
            [bobs.models.session :as session]
            [bobs.models.query :refer [get-row]]
            [bobs.models.entries :refer [entries]]
            [bobs.i18n :refer [t]]
            [bobs.views.printables.documents :as docs]))

(defn render-doc [id]
  (if-let [doc (get-row entries {:id id
                                 :include-details true})]
    (html
     [:table.info
      [:tbody
       [:tr
        [:td [:label (t :default/number-abbr)]]
        [:td (str (:number doc) (if (:void doc) (str " ** " (t :default/voided) " **")))]]
       [:tr
        [:td [:label (t :default/date)]]
        [:td (docs/format-datetime (:date doc))]]
       [:tr
        [:td [:label (t :default/comment)]]
        [:td (:comment doc)]]]]
     [:table.table
      [:thead (docs/render-table-head :entry)]
      [:tbody (docs/render-details doc (:currency-symbol doc) :entry)]
      [:tfoot (docs/render-foot doc (:currency-symbol doc) :entry)]])))

(defn render [id]
  (html5
   [:head]
   [:body
    [:h4 (t :default/entry)]
    (render-doc id)]))
