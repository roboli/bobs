(ns bobs.views.printables.purchase-returns
  (:require [hiccup.page :refer [html5 include-css include-js]]
            [hiccup.core :refer [html]]
            [bobs.models.session :as session]
            [bobs.models.query :refer [get-row]]
            [bobs.models.purchase-returns :refer [purchase-returns]]
            [bobs.i18n :refer [t date-fmt time-zone-id]]
            [bobs.views.printables.documents :as docs]))

(defn render-doc [id]
  (if-let [doc (get-row purchase-returns {:id id
                                          :include-details true})]
    (html
     [:table.info
      [:tbody
       [:tr
        [:td [:label (t :default/number-abbr)]]
        [:td (str (:number doc) (if (:void doc) (str " ** " (t :default/voided) " **")))]]
       [:tr
        [:td [:label (t :default/date)]]
        [:td (docs/format-datetime (:date doc))]]
       [:tr
        [:td [:label (t :default/customer)]]
        [:td (:customer-name doc)]]
       [:tr
        [:td [:label (t :default/comment)]]
        [:td (:comment doc)]]]]
     [:table.table
      [:thead (docs/render-table-head :withdraw)]
      [:tbody (docs/render-details doc (:currency-symbol doc) :withdraw)]
      [:tfoot (docs/render-foot doc (:currency-symbol doc) :withdraw)]])))

(defn render [id]
  (html5
   [:head]
   [:body
    [:h4 (t :default/purchase-return)]
    (render-doc id)]))
