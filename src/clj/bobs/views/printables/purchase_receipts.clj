(ns bobs.views.printables.purchase-receipts
  (:require [hiccup.page :refer [html5 include-css include-js]]
            [hiccup.core :refer [html]]
            [bobs.models.session :as session]
            [bobs.models.query :refer [get-row]]
            [bobs.models.purchase-receipts :refer [purchase-receipts]]
            [bobs.i18n :refer [t date-fmt datetime-fmt time-zone-id]]
            [bobs.views.printables.documents :as docs]))

(defn render-doc [id]
  (if-let [doc (get-row purchase-receipts {:id id
                                           :include-details true})]
    (html
     [:table.info
      [:tbody
       [:tr
        [:td [:label (t :default/number-abbr)]]
        [:td (str (:number doc) (if (:void doc) (str " ** " (t :default/voided) " **")))]]
       [:tr
        [:td [:label (t :default/date)]]
        [:td (docs/format-datetime (:date doc))]]
       [:tr
        [:td [:label (t :default/supplier)]]
        [:td (:supplier-name doc)]]
       [:tr
        [:td [:label (t :default/warehouse)]]
        [:td (:warehouse-name doc)]]
       [:tr
        [:td [:label (t :default/purchase-date)]]
        [:td (docs/format-date (:purchase-date doc))]]
       [:tr
        [:td [:label (t :default/reference)]]
        [:td (:reference doc)]]
       [:tr
        [:td [:label (t :default/comment)]]
        [:td (:comment doc)]]]]
     [:table.table
      [:thead (docs/render-table-head :entry)]
      [:tbody (docs/render-details doc (:currency-symbol doc) :entry)]
      [:tfoot (docs/render-foot doc (:currency-symbol doc) :entry)]])))

(defn render [id]
  (html5
   [:head]
   [:body
    [:h4 (t :default/purchase-receipt)]
    (render-doc id)]))
