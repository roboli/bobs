(ns bobs.core
  (:use compojure.core)
  (:require [ring.middleware.params :refer [wrap-params]]
            [ring.middleware.multipart-params :refer [wrap-multipart-params]]
            [ring.middleware.keyword-params :refer [wrap-keyword-params]]
            [ring.middleware.edn :refer [wrap-edn-params]]
            [ring.middleware.session.memory :refer [memory-store]]
            [noir.session :as session]
            [compojure.route :as route]
            [bobs.routes.session :refer [session-routes]]
            [bobs.routes.currencies :refer [currencies-routes]]
            [bobs.routes.brands :refer [brands-routes]]
            [bobs.routes.uom :refer [uom-routes]]
            [bobs.routes.item-types :refer [item-types-routes]]
            [bobs.routes.items :refer [items-routes]]
            [bobs.routes.warehouses :refer [warehouses-routes]]
            [bobs.routes.partners :refer [partners-routes]]
            [bobs.routes.suppliers :refer [suppliers-routes]]
            [bobs.routes.customers :refer [customers-routes]]
            [bobs.routes.purchase-receipts :refer [purchase-receipts-routes]]
            [bobs.routes.purchase-receipts-drafts :refer [purchase-receipts-drafts-routes]]
            [bobs.routes.purchase-returns :refer [purchase-returns-routes]]
            [bobs.routes.purchase-returns-drafts :refer [purchase-returns-drafts-routes]]
            [bobs.routes.shipments :refer [shipments-routes]]
            [bobs.routes.shipments-drafts :refer [shipments-drafts-routes]]
            [bobs.routes.sales-returns :refer [sales-returns-routes]]
            [bobs.routes.sales-returns-drafts :refer [sales-returns-drafts-routes]]
            [bobs.routes.entries :refer [entries-routes]]
            [bobs.routes.entries-drafts :refer [entries-drafts-routes]]
            [bobs.routes.withdraws :refer [withdraws-routes]]
            [bobs.routes.withdraws-drafts :refer [withdraws-drafts-routes]]
            [bobs.routes.users :refer [users-routes]]
            [bobs.middleware :refer [wrap-log-request]]
            [bobs.models.db :as db]
            [taoensso.timbre :as timbre]
            [taoensso.timbre.appenders.rotor :as rotor]
            [bobs.config :refer [dev lobos-src-dir]]
            [bobs.views.index :as index]))


(defn init
  "init will be called once when
   app is deployed as a servlet on
   an app server such as Tomcat
   put any initialization code here"
  []
  (alter-var-root #'lobos.migration/*reload-migrations*
                  (constantly false))
  (alter-var-root #'lobos.migration/*src-directory*
                  (constantly lobos-src-dir))
  (timbre/set-config!
   [:appenders :rotor]
   {:min-level :info
    :enabled? true
    :async? false ; should be always false for rotor
    :max-message-per-msecs nil
    :fn rotor/appender-fn})

  (timbre/set-config!
   [:shared-appender-config :rotor]
   {:path "bobs.log" :max-size (* 512 1024) :backlog 10})

  (if-not (db/actualized?)
    (db/actualize))
  (timbre/info "bobs started successfully"))

(defn destroy
  "destroy will be called when your application
   shuts down, put any clean up code here"
  []
  (timbre/info "bobs is shutting down..."))


(defroutes app-routes
  (GET "/" [] (index/render dev))
  (GET "/index.html" [] (index/render dev))
  (route/resources "/")
  (route/not-found "Not Found"))

(def app
  (-> (routes currencies-routes
              brands-routes
              uom-routes
              item-types-routes
              items-routes
              partners-routes
              suppliers-routes
              customers-routes
              warehouses-routes
              purchase-receipts-drafts-routes
              purchase-receipts-routes
              purchase-returns-drafts-routes
              purchase-returns-routes
              shipments-drafts-routes
              shipments-routes
              sales-returns-drafts-routes
              sales-returns-routes
              entries-drafts-routes
              entries-routes
              withdraws-drafts-routes
              withdraws-routes
              users-routes
              session-routes
              app-routes)
      (wrap-log-request)
      (wrap-edn-params)
      (wrap-keyword-params)
      (wrap-multipart-params)
      (wrap-params)
      (session/wrap-noir-session {:store (memory-store)})))
