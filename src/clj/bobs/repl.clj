(ns bobs.repl
  (:use bobs.core
        ring.server.standalone
        [ring.middleware file-info file])
  (:require [bobs.models.query :refer [get-row]]
            [bobs.models.session :refer [one-user-start!]]
            [bobs.models.users :refer [users]]))

(defonce server (atom nil))

;; Use only for testing/debugging purposes
(defn one-user-session-start! [username]
  (one-user-start! (get-row users {:username username})))

(defn get-handler []
  ;; #'app expands to (var app) so that when we reload our code,
  ;; the server is forced to re-resolve the symbol in the var
  ;; rather than having its own copy. When the root binding
  ;; changes, the server picks it up without having to restart.
  (-> #'app
      ; Makes static assets in $PROJECT_DIR/resources/public/ available.
      (wrap-file "resources")
      ; Content-Type, Content-Length, and Last Modified headers for files in body
      (wrap-file-info)))

(defn start-server
  "used for starting the server in development mode from REPL"
  [& {:keys [port one-user]}]
  (let [port (if port (Integer/parseInt port) 3000)]
    (reset! server
            (serve (get-handler)
                   {:port port
                    :init init
                    :auto-reload? true
                    :destroy destroy
                    :join? false
                    :open-browser? false}))
    (if one-user (one-user-session-start! one-user))
    (println (str "You can view the site at http://localhost:" port))))

(defn stop-server []
  (.stop @server)
  (reset! server nil))
