(ns bobs.config
  (:require [environ.core :refer [env]]
            [bobs.cross-config :as cc]))

(def app-name cc/app-name)
(def app-name-ext cc/app-name-ext)
(def app-brand cc/app-brand)
(def app-lang cc/app-lang)
(def db-subname (env :db-subname))
(def db-password (env :db-password))
(def db-user (env :db-user))
(def dev (if (string? (env :dev)) (read-string (env :dev)) (env :dev)))
(def image-size (or (env :image-size) 450))
(def image-file-size cc/image-file-size)
(def items-images-path (or (env :items-images-path) "images/items"))
(def lobos-src-dir (env :lobos-src-dir))
(def thumbnail-prefix (or (env :thumbnail-prefix) "thumb_"))
(def thumbnail-size (or (env :thumbnail-size) 140))

(def currencies {"USD" {:name "US Dollar"
                        :symbol "$"}
                 "GTQ" {:name "Quetzal"
                        :symbol "Q"}})

(def amount-keys
  {:entry :cost
   :withdraw :price})

(def document-types {:purchase-receipt {:id 1
                                        :affect +}
                     :purchase-return {:id 2
                                       :affect -}
                     :shipment {:id 3
                                :affect -}
                     :sales-return {:id 4
                                    :affect +}
                     :entry {:id 5
                             :affect +}
                     :withdraw {:id 6
                                :affect -}})

(def inventory-types {:perpetual 1
                      :periodic 2})
