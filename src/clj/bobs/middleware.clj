(ns bobs.middleware
  (:require [taoensso.timbre :as timbre]
            [bobs.config :refer [dev]]))

(defn wrap-log-request [handler]
  (if dev
    (fn [req]
      (timbre/debug req)
      (handler req))
    handler))
