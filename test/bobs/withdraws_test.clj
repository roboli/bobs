(ns bobs.withdraws-test
  (:require [clojure.test :refer [use-fixtures deftest testing is]]
            [ring.mock.request :as mock]
            [korma.core :as km]
            [bobs.config :refer [lobos-src-dir]]
            [bobs.models.entities :as ent]
            [bobs.core :refer [app]]
            [bobs.models.db :as db]
            [bobs.models.query :as q]
            [bobs.models.users :as u]
            [bobs.models.companies :as c]
            [bobs.models.currencies :as cu]
            [bobs.models.document-configurations :as dc]))

(def rand-str (apply str (repeatedly 20 #(rand-nth "abcdefghijklmnopqrstuvwxyz0123456789"))))
(def username (str "test-user-" rand-str))
(def company-alias (str "test-company-" rand-str))

(use-fixtures :once
  (fn [test-fn]
    (alter-var-root #'lobos.migration/*src-directory*
                    (constantly lobos-src-dir))
    (if-not (db/actualized?)
      (db/actualize))
    (let [user-id    (q/insert u/users {:values
                                        {:username username
                                         :name "Test User"
                                         :password "pass"}})
          company-id (q/insert c/companies {:values
                                            {:name "Test Company"
                                             :alias company-alias}})]
      (c/add-user c/companies {:values
                               {:company-id company-id
                                :user-id user-id
                                :is-default 1}})
      (q/insert cu/currencies {:values
                               {:company-id company-id
                                :code "GTQ"
                                :is-default 1}})
      (q/insert dc/document-configurations {:values
                                            {:company-id company-id
                                             :description "Egreso"
                                             :document-type 6
                                             :initial 6001
                                             :is-default 1}}))
    (test-fn)
    (km/delete ent/document_configuration)
    (km/delete ent/currency)
    (km/delete ent/company_user)
    (km/delete ent/company)
    (km/delete ent/user)))

(deftest get-withdraws
  (testing "get all withdraws"
    (let [response (app (-> (mock/request :post "/session"
                                          {:username username
                                           :password "pass"})
                            (mock/header :accept "application/edn")))
          cookie   (first (get-in response [:headers "Set-Cookie"]))
          response (app (-> (mock/request :get "/withdraws")
                            (mock/header :accept "application/edn")
                            (mock/header :cookie cookie)))]
      (is (= (:status response) 200))
      (is (= (:body response) "[]")))))
